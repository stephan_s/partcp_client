// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

enum ResultStatus {
  success,
  error,
  abort,
}