import 'dart:collection';

import 'package:partcp_client/utils/log.dart';

import 'stanza.dart';

class StanzaListener {
  static const String _TAG = "StanzaListener - ";

  static final StanzaListener _instance = StanzaListener._internal();
  final Map<String, Function(Stanza)> _stanzaMap = HashMap();

  StanzaListener._internal() {}

  factory StanzaListener() {
    return _instance;
  }

  void addStanzaListener({String? stanzaId, required Function(Stanza) callback}) {
    if (stanzaId == null) return;
    _stanzaMap[stanzaId] = callback;
  }

  void clearAllListener() {
    _stanzaMap.clear();
  }

  void onStanza(Stanza stanza) {
    var callback = _stanzaMap[stanza.stanzaId];
    if (callback != null) {
      _stanzaMap.remove(stanza.stanzaId);
      Log.d(_TAG, "onStanza stanzaId: " + stanza.stanzaId);
      callback(stanza);
    }
  }
}