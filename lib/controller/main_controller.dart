// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:partcp_client/objects/lot_code.dart';
import 'package:partcp_client/objects/lot_code_from_deposit.dart';
import 'package:partcp_client/objects/participant.dart';
import 'package:partcp_client/objects/storage/app_box.dart';
import 'package:partcp_client/objects/url_params.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/wallet.dart';
import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';
import 'package:partcp_client/utils/log.dart';

import 'package:intl/intl.dart';
import 'package:partcp_client/objects/storage/user_box.dart';
import 'package:partcp_client/utils/path_provider.dart';
import '../constants.dart';
import 'lifecycle_controller.dart';
import 'ws_client/ws_controller.dart';


class MainController extends GetxController {
  final _TAG = "MainController";

  String? appDataPath;
  FullLifeCycleController lifeCycleController =  Get.put(LifeCycleController());

  Future<void> initController() async {
    if (!GetPlatform.isWeb) {
      appDataPath = await PathProvider().getAppDataPath();
    }
    Log.d(_TAG, "initController() -> appDataPath: $appDataPath");

    await _initWsController();
  }

  static MainController to = Get.find<MainController>();
  WsController? wsController;

  static const MIN_KEY_PWD_LEN = 0;
  static const USER_BOX_NAME = "userBox";
  static const APP_BOX_NAME = "appBox";
  static const WALLETS_BOX_NAME = "walletsBox";
  static const FILE_DATE_FORMAT = "yyMMdd-HHmm";


  String get walletFileName {
    String dateString = "${DateFormat(FILE_DATE_FORMAT).format(DateTime.now())}";
    String fileName = Constants.WALLET_FILE_NAME.replaceFirst("{d}", dateString);
    if (wallet != null && wallet!.walletAccount != null) {
      fileName = fileName.replaceFirst("{n}", Constants.fileNameRegExp(wallet!.walletAccount!.name));
    }
    return fileName;
  }


  bool? get pubKeysOnServerValid {
    if (wallet == null) {
      return null;
    }
    if (wallet!.walletAccount == null) {
      return null;
    }
    return wallet!.walletAccount!.pubKeysOnServerValid;
  }


  bool cryptoLibWorks = false;
  bool isFullScreen = false;
  bool isInForeground = false;

  /// query params passed trough the url: https://domain.com?a=1&b=2
  Map<String, dynamic> urlQueryParams = {};

  Wallets wallets = Wallets();

  /// wallet with main account and server accounts
  Wallet? wallet;
  Map<String, Wallet> walletMap = {};

  /// vars Storage
  UserBox? userBox;
  AppBox? appBox;
  Box? walletsBox;

  /// current [VotingServer]
  VotingServer? votingServer;


  /// wrong LotCode list
  List<LotCodeFromDeposit>? wrongLotCodesFromDeposit;

  /// for notifications
  bool walletWasChanged = false;
  bool walletNotification = false;

  String? appRev;

  UrlParams urlParams = UrlParams();

  bool get isDesktopOrWeb => GetPlatform.isWeb || GetPlatform.isDesktop;

  String get currentWalletTitle => wallet != null
      ? wallet!.walletAccount!.title
      : "";

  String get currentWalletAppBarTitle => wallet != null
      ? "(${wallet!.walletAccount!.title})"
      : "";

  Future<String> getAppRev() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  bool get moderatorTools => wallet != null && userBox != null && userBox!.getValue(UserBoxEnum.moderator, ifNotExists: false);

  set moderatorTools (bool? val) {
    if (val != null && userBox!= null) {
      userBox!.setValue(UserBoxEnum.moderator, val);
    }
  }

  Future<void> _initWsController() async {
    wsController = await Get.put(WsController(), permanent: true);
  }
  

  Future<void> _initUserBox() async {
    Log.d(_TAG, "_initMainBox()");
    if (wallet != null && wallet!.pwdBytes != null) {
      Log.d(_TAG, "_initMainBox(); wallet != null && wallet!.pwdBytes != null => initBox");
      userBox = UserBox();
      await userBox!.initBox(await wallet!.walletAccount!.publicKeyX, await wallet!.pwdForStrings);
    }
  }

  // Future<void> _initWrongLotCodes() async {
  //   String? wrongLotCodes = box!.getValue(MyStorageEnum.wrongLotCodes);
  //   wrongLotCodesFromDeposit = [];
  //   if (wrongLotCodes != null) {
  //     try{
  //       List<dynamic> list = jsonDecode(wrongLotCodes);
  //       for (var jsonData in list) {
  //         LotCodeFromDeposit lc = LotCodeFromDeposit.fromJson(jsonData);
  //         wrongLotCodesFromDeposit!.add(lc);
  //       }
  //     }
  //     catch(e) {
  //       wrongLotCodesFromDeposit = [];
  //       await box!.setValue(MyStorageEnum.wrongLotCodes, jsonEncode(wrongLotCodesFromDeposit));
  //     }
  //   }
  // }


  // Future<void> updateWrongLotCodes() async {
  //   await box!.setValue(MyStorageEnum.wrongLotCodes, jsonEncode(wrongLotCodesFromDeposit));
  // }
  //
  // List<LotCodeFromDeposit> wrongLotCodeForEvent(VotingServer server, VotingEvent event) {
  //   List<LotCodeFromDeposit>? test = [];
  //   for (LotCodeFromDeposit lc in wrongLotCodesFromDeposit!) {
  //     if ((lc.server != null && lc.server == server.name) && (lc.eventId != null && lc.eventId == event.id)) {
  //       test.add(lc);
  //     }
  //   }
  //   return test;
  // }



  // Future<Box>getEventBox(VotingEvent event) async {
  //   String spKey = event.lotCodesInternalKeyName;
  //   return await Hive.openBox(spKey, encryptionCipher: HiveAesCipher(await wallet!.pwdForStrings));
  // }
  //
  // /// LOT-CODES FROM STORAGE
  // Future<LotCodeStorage?> getLotCodesFromStorage(VotingEvent event) async {
  //   String spKey = event.lotCodesInternalKeyName;
  //
  //   if (await Hive.boxExists(spKey)) {
  //     var box = await getEventBox(event);
  //     String? content = box.get(spKey);
  //
  //     if (content != null) {
  //       return await LotCodeStorage.fromStorage(
  //           content: content,
  //           eventId: event.id,
  //       );
  //     }
  //   }
  //   return null;
  // }

  Future<Box<LotCode>>getEventBoxLotCodes(VotingEvent event) async {
    if (!Hive.isAdapterRegistered(1)) {
      Hive.registerAdapter(LotCodeAdapter());
    }
    String spKey = event.eventBoxLotCodes;
    List<int> boxPwd = await wallet!.pwdForStrings;
    String boxId = MyEcCrypt.hashFromPublicKeyX(await wallet!.walletAccount!.publicKeyX);
    String boxName = "$spKey-$boxId";
    Log.d(_TAG, "getEventBoxLotCodes; boxName: $boxName");
    return await Hive.openBox<LotCode>(boxName, encryptionCipher: HiveAesCipher(boxPwd), path: appDataPath);
  }

  Future<Box<Participant>>getEventBoxParticipants(VotingEvent event) async {
    if (!Hive.isAdapterRegistered(2)) {
      Hive.registerAdapter(ParticipantAdapter());
    }
    String spKey = event.eventBoxParticipants;
    List<int> boxPwd = await wallet!.pwdForStrings;
    String boxId = MyEcCrypt.hashFromPublicKeyX(await wallet!.walletAccount!.publicKeyX);
    String boxName = "$spKey-$boxId";
    Log.d(_TAG, "getEventBoxParticipants; boxName: $boxName");
    return await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(await boxPwd), path: appDataPath);
  }


  Future<VotingServer?> getLastServer() async {
    String? lastUrl = userBox!.getValue(UserBoxEnum.userLastServer);
    if (lastUrl != null) {
      for(VotingServer server in wallet!.serverList) {
        if (server.url == lastUrl) {
          return server;
        }
      }
    }
    return null;
  }

  /// to: export encrypted LotCodes
  Future<String?> getLotCodesFromStorageAsString(VotingEvent event) async {
    /// returns the encrypted String
    // String spKey = event.lotCodesInternalKeyName;
    //
    // if (await Hive.boxExists(spKey)) {
    //   // var box = await Hive.openBox(spKey);
    //   var box = await getEventBox(event);
    //   String content = box.get(spKey);
    //   return content;
    // }
    return null;
  }


  /////////// WALLET ///////////////

  Future<void> onWalletDecrypted(Wallet wallet) async {
    this.wallet = wallet;
    this.wallet!.walletAccount!.dateU = DateTime.now();

    await _saveWalletInBox();
    await appBox!.setValue(AppBoxEnum.lastWallet, await this.wallet!.walletAccount!.walledId);
    await _initUserBox();
    wsController?.initWs();
  }

  bool walletIsDecrypted () {
    bool _keysLoaded = wallet != null && wallet!.walletAccount != null && wallet!.walletAccount!.keyPairX != null && wallet!.walletAccount!.keyPairEd != null;
    // Log.d(_TAG, "keysLoaded: $_keysLoaded");
    return _keysLoaded;
  }


  bool encryptedWalletExists () {
    bool _exists = wallet != null && wallet!.walletAccount != null && wallet!.walletAccount!.keyPairX == null;
    return _exists;
  }


  List<Wallet> walletsInBox() {
    var yamlWallets = walletsBox!.values;
    List<Wallet> wallets = [];
    for (String yamlWallet in yamlWallets) {
      Wallet? wallet = Wallet.fromYaml(yamlWallet);
      if (wallet != null) {
        wallets.add(wallet);
      }
    }

    wallets.sort((a, b) => b.walletAccount!.dateU!.compareTo(a.walletAccount!.dateU!));
    return wallets;
  }




  void onWalletDownloaded() {
    walletWasChanged = false;
    walletNotification = false;
  }


  Future<void> onWalletChanged({bool updateNotification = true}) async {
    wallet!.walletAccount!.dateU = DateTime.now();
    if (updateNotification) {
      walletNotification = true;
    }
    walletWasChanged = true;
    await _saveWalletInBox();
  }


  Future<void> initBoxen() async {
    if (appBox == null) {
      appBox = AppBox();
      await appBox!.initBox(APP_BOX_NAME);
    }

    if (walletsBox == null) {
      walletsBox = await Hive.openBox(WALLETS_BOX_NAME, path: appDataPath);
      Log.d(_TAG, "initBoxen(); walletsBox.isOpen: ${walletsBox!.isOpen}; walletsBox.path: ${walletsBox!.path}");
    }
  }


  Future<bool> loadWallet({String? walletId}) async {
    /// load last used
    if (walletId == null) {
      walletId = appBox!.getValue(AppBoxEnum.lastWallet);
    }

    if (walletId != null) {
      String? yamlWallet = getYamlWallet(walletId: walletId);
      if (yamlWallet != null) {
        wallet = Wallet.fromYaml(yamlWallet);
        if (wallet != null) {
          return true;
        }
      }
    }
    return false;
  }


  String? getYamlWallet({String? walletId}) {
    var wallets = walletsBox!.toMap();
    // Log.d(_TAG, "getYamlWallet: $wallets");
    String? yamlWallet = walletsBox!.get(walletId);
    return yamlWallet;
  }


  Future<bool> _saveWalletInBox() async {
    Log.d(_TAG, "_saveWalletInBox()");
    String yamlWallet = await wallet!.toYaml();
    String walledId = await wallet!.walletAccount!.walledId;
    await walletsBox!.put(walledId, yamlWallet);
    await walletsBox!.flush();
    await walletsBox!.compact();
    Log.d(_TAG, "_saveWalletInBox; yamlWallet: $yamlWallet");
    return true;
  }


  Future<void> deleteWallet({String? walletId}) async {
    if (walletId == null && wallet != null) {
      walletId = await wallet!.walletAccount!.walledId;
      wallet = null;
    }
    if (walletId != null && walletsBox != null) {
      await walletsBox!.delete(walletId);
      await walletsBox!.flush();
      await walletsBox!.compact();
    }
  }


  void forgetKeys() {
    /// todo
    Log.d(_TAG, "forgetKeys()");
  }


  void forgetServer() {
    Log.d(_TAG, "forgetServer()");
    votingServer = null;
  }


  Future<void> forgetKey() async {
    forgetKeys();
    forgetServer();
    wallet = null;
    await wsController?.closeWs();
    // await loadWallet();
    // account = null;
  }

}

