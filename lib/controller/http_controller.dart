// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:get/get.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/yaml_converter.dart';

const _HTTP_STATUS = "HTTP_STATUS";
const _HTTP_BODY = "HTTP_BODY";
const _SIGNATURE = "Signature";
const _PUBLIC_KEY = "Public-Key";
const _FROM = "From";

const HTTP_BODY_MAP = "HTTP_BODY_MAP";

class GetXProvider extends GetConnect {
  @override
  void onInit() {
    httpClient.timeout = const Duration(seconds: 5);
    httpClient.followRedirects = false;
    httpClient.maxAuthRetries = 3;
  }
}

enum ServerRequest {
  ping,
  keySubmission,
  votingListRequest,
}

class HttpController extends GetxController {
  final _TAG = "HttpController";

  // static HttpController to = Get.find<HttpController>();

  GetXProvider getXProvider = GetXProvider()..timeout = (Duration(seconds: 15));
  YamlConverter _yamlConverter = YamlConverter();

  /// SEND TO SERVER
  Future<ServerResponse> sendToServer(String url, String body, {bool post = true}) async {
    Log.spacer();
    Log.d(_TAG, "---------------------------------------------------------------------");
    Log.d(_TAG, "_sendToServer: url: $url");
    Log.d(_TAG, "_sendToServer: body: \n$body");
    Log.spacer();

    ServerResponse serverResponse = ServerResponse()
      ..receivingData = true
      ..request = body;

    /// send request to server via getX
    final _result = post ? await getXProvider.post(url, body, contentType: "test/plain") : await getXProvider.get(url);

    serverResponse.receivingData = false;
    Map<String, dynamic> defMap = {};

    if (_result.hasError) {
      var respTxt = "${_result.statusText}";
      if (_result.bodyString != null) {
        respTxt += " : ${_result.bodyString}";
      }
      serverResponse
        ..statusCode = _result.statusCode
        ..statusText = _result.statusText
        ..body = _result.bodyString
        ..bodyMap = defMap
        ..errorMessage = respTxt;
    } else {
      serverResponse
        ..statusCode = _result.statusCode
        ..statusText = _result.statusText
        ..body = _result.bodyString;

      try {
        // serverResponse.bodyMap = _yamlConverter.toMapUnCatch(_result.bodyString);
        dynamic result = _yamlConverter.toDynamicUnCatch(_result.bodyString!);
        if (result is Map) {
          serverResponse.bodyMap = result as Map<String, dynamic>;
        }
        else if (result is List) {
          serverResponse.bodyMap = {"list": result};
        }
        else {
          throw Exception('Wrong Server Response, ist weder Liste noch Map');
        }
      } catch (e) {
        serverResponse.errorMessage = "Wrong Server Response!\n Details:\n $e.toString()";
        serverResponse.statusCode = 500;
      }
    }

    Log.d(_TAG, "_sendToServer -> statusCode: ${serverResponse.statusCode}");
    Log.d(_TAG, "_sendToServer -> statusText: ${serverResponse.statusText}");
    Log.d(_TAG, "_sendToServer -> response: \n${serverResponse.body}");

    if (serverResponse.errorMessage != null) {
      Log.w(_TAG, "_sendToServer -> errorMessage: ${serverResponse.errorMessage}");
    }
    Log.d(_TAG, "---------------------------------------------------------------------");

    Log.spacer();
    return serverResponse;
  }

  /// CREATE (REGULAR) SERVER REQUEST
  Future<String> createServerRequest(Account account, Map<String, dynamic> messageMap) async {
    messageMap = _addSenderToMessage(account, messageMap);
    messageMap[_PUBLIC_KEY] = await account.publicKeysConcat;

    String messageYaml = _mapToYaml(messageMap).trim();
    var sig = await MyEcCrypt.createSignatureString(messageYaml, account.keyPairEd!);

    return _addSignatureToMessage(messageYaml, sig);
  }

  Map<String, dynamic> _addSenderToMessage(Account account, Map<String, dynamic> messageMap) {
    messageMap[_FROM] = account.userId;
    return messageMap;
  }

  /// CREATE SIGNED SERVER REQUEST, SEND IT AND VERIFY RESPONSE
  Future<ServerResponse> serverRequestSigned(Account account, Map<String, dynamic> messageMap, VotingServer server) async {
    final String message = await createServerRequest(account, messageMap);

    final ServerResponse serverResponse = await sendToServer(server.url, message);

    if (serverResponse.statusCode == 200) {
      serverResponse.signatureIsCorrect = await _verifyServerResponse(server, account, serverResponse.bodyMap!);
    }
    return serverResponse;
  }

  /// CREATE UNSIGNED SERVER REQUEST, SEND IT AND VERIFY RESPONSE
  Future<ServerResponse> serverRequest(Map<String, dynamic> messageMap, Account? account, String serverUrl) async {
    if (account != null) {
      messageMap = _addSenderToMessage(account, messageMap);
    }
    final message = _yamlConverter.toYaml(messageMap);
    return await sendToServer(serverUrl, message);
  }

  String _addSignatureToMessage(String message, String sig) {
    return "$_SIGNATURE: '$sig'" + "\n" + message;
  }

  String _mapToYaml(Map<String, dynamic> map) {
    return _yamlConverter.toYaml(map);
  }

  Future<bool> _verifyServerResponse(VotingServer server, Account account, Map<dynamic, dynamic> responseMap) async {
    final yamlMessage = responseMap[_HTTP_BODY];
    if (responseMap[_HTTP_STATUS] == 200) {
      return await _verifyResponseSignature(server, account, yamlMessage);
    }
    return false;
  }

  Future<bool> _verifyResponseSignature(VotingServer server, Account account, String yamlMessage) async {
    final splitted = yamlMessage.split("\n");
    // Log.d(_TAG, "verifyResponseSignature; splitted: $splitted");

    if (splitted[0].startsWith(_SIGNATURE)) {
      Map<String, dynamic> map = _yamlConverter.toMap(yamlMessage);
      String message = _extractMessageFromResponseBody(yamlMessage, splitted[0]);
      String sigB64 = (map[_SIGNATURE]).trim();
      // List<int> sigSeed = base64Decode(sigB64);

      Log.d(_TAG, "_verifyResponseSignature; sigB64: $sigB64");
      // Log.d(_TAG, "_verifyResponseSignature; sigSeed: $sigSeed");
      // Log.d(_TAG, "_verifyResponseSignature; message: $message");

      bool isCorrect = await MyEcCrypt.verifySignature(message, sigB64, server.publicKeyEd!);
      Log.d(_TAG, "_verifyResponseSignature; isCorrect: $isCorrect");

      return isCorrect;
    }
    return false;
  }

  String _extractMessageFromResponseBody(String yamlMessage, String signatureString) {
    String message = yamlMessage.replaceAll(signatureString + "\n", "");
    // Log.d(_TAG, "_extractMessageFromResponseBody; message: $message");
    return message;
  }
}
