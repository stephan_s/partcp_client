// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:partcp_client/utils/mark_down_to_html.dart';
import 'package:partcp_client/widgets/scroll_body.dart';

class MarkdownView extends StatelessWidget {
  final _TAG = "MarkdownView";
  final String markdownText;
  final String title;

  const MarkdownView({required this.title, required this.markdownText});

  @override
  Widget build(BuildContext context) {
    final String htmlContent = markdownToHtml(markdownText);

    Widget _backButton = GetPlatform.isWeb
        ? Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextButton(
              onPressed: () => Get.back(),
              child: Text("Zurück"),
            ),
          )
        : Container(height: 8,);

    return Scaffold(
      appBar: MyAppBar(
        title: Text(title),
      ),
      body: Builder(
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ScrollBody(children: [
              _backButton,
              HtmlWidget(
                htmlContent,
                textStyle: TextStyle(color: Colors.black87, fontSize: 14),
                customStylesBuilder: (element) {
                  if (element.classes.contains('h1')) {
                    return {'color': 'red'};
                  }
                  return null;
                },
              ),
              _backButton,
            ]),
          );
        },
      ),
    );
  }
}
