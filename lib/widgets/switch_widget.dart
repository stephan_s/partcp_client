// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';

Widget switchWidget(bool value, Function(bool value) onChanged) {
  return Center(
      child: Container(
        width: 60,
        child: Switch(
          value: value,
          onChanged: (value) => onChanged(value),
          // materialTapTargetSize: MaterialTapTargetSize.padded,
        ),
      ));
}
