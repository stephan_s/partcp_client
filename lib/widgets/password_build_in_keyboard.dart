// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'package:flutter/cupertino.dart';
// import 'package:partcp_client/widgets/build_in_keyboard.dart';
//
// class PasswordBuildInKeyboard extends StatelessWidget {
//   final TextEditingController textEditingController;
//   const PasswordBuildInKeyboard({
//     @required this.textEditingController
//   });
//
//   @override
//   Widget build(BuildContext context) {
//     return BuiltInKeyboard(
//       layoutType: 'DE',
//       borderRadius: BorderRadius.circular(8),
//       controller: textEditingController,
//       enableLongPressUppercase: true,
//       enableSpaceBar: true,
//       enableBackSpace: true,
//       enableCapsLock: true,
//     );
//
//   }
//
// }