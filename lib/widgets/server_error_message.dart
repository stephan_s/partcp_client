// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/widgets/button_sub.dart';

import '../constants.dart';

const _TAG = "ServerErrorMessage";

class ServerErrorMessage extends StatelessWidget{
  final String? errorMessage;
  final ServerResponse? serverResponse;
  final bool useCardWidget;

  const ServerErrorMessage({
    this.errorMessage,
    this.serverResponse,
    this.useCardWidget = true,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      final String? _message = errorMessage != null
        ? errorMessage
        : serverResponse != null
            ? serverResponse!.errorMessage
            : null;

      if (serverResponse != null) {
        // Log.d(_TAG, "clear errorMessage");
        serverResponse!.errorMessage = null;
      }

      if (_message != null) {
        Log.d(_TAG, "show errorMessage");

        String copyDataRequest = "";
        if (serverResponse != null && serverResponse!.request != null) {
          copyDataRequest = "Request:\n${serverResponse!.request!}\n\n=>\n";
        }

        final snackBar = SnackBar(
          // backgroundColor: const Color.fromARGB(255, 240, 240, 240),
          content: ListView(
            shrinkWrap: true,
            children: [
              SelectableText(
                "\n$_message\n",
                textScaleFactor: 1.2,
                style: TextStyle(
                    color: Colors.red
                ),
              ),
              
              ButtonSub(
                child: Text(
                  "Fehlermeldung in die Zwischanablage kopieren",

                ),
                onPressed: () => Clipboard.setData(ClipboardData(text: "$copyDataRequest $_message")),
              )
            ],
          ),
          duration: Constants.SNACK_BAR_DURATION_SERVER_ERROR,
        );

        ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);
      }
    });
    return Container();
  }

}

/*


 */