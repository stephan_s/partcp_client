// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/cupertino.dart';
import 'package:getwidget/getwidget.dart';

class Widgets {
  static get loader => GFLoader(
      type: GFLoaderType.ios,
      size: 25,
  );

  static get loaderLarge => GFLoader(
        type: GFLoaderType.ios,
        size: GFSize.LARGE,
      );

  static Widget loaderBox({required Widget child, required bool loading}) {
    return loading
        ? Stack(children: [
            Container(
              foregroundDecoration: BoxDecoration(
                color: Color.fromARGB(170, 250, 250, 250),
              ),
              child: child,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 50,
                  height: 50,
                  child: FittedBox(fit: BoxFit.fill, child: Widgets.loaderLarge),
                ),
              ),
            )
          ])
        : child;
  }
}
