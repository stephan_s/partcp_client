// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';


class ScData {
  ScrollController scrollController;
  double offset = 0;

  ScData(this.scrollController, this.offset);
}

class ScrollBodyController extends GetxController {
  Map<Key, ScData> scMap = {};

  void initController(Key key) {
    if (scMap.containsKey(key)) {
      scMap[key] = ScData(
          ScrollController(initialScrollOffset: scMap[key]!.offset),
          scMap[key]!.offset
      );
    }
  }

  ScrollController scrollController(Key key) {
    if (scMap.containsKey(key)) {
      return scMap[key]!.scrollController;
    }
    else {
      scMap[key] = ScData(ScrollController(), 0);
      return scMap[key]!.scrollController;
    }
  }

  void initScrollablePage(Key key) {
    ScrollController scrollController = scMap[key]!.scrollController;

    // print("initScrollablePage() -- 1");
    if(scrollController.hasClients) {
      // print("initScrollablePage() -- 2");
      scrollController.position.addListener(() => _onScrollControllerOffsetChange(key));
    }
    else{
      // print("initScrollablePage() -- 3");
    }
  }

  void _onScrollControllerOffsetChange(Key key){
    ScrollController scrollController = scMap[key]!.scrollController;
    if(scrollController.hasClients) {
      scMap[key]!.offset = scrollController.offset;
      // print("_onScrollControllerOffsetChange() -- offset = ${scrollController.offset}");
    }
  }

  void updateMe() {
    update();
  }
}



class ScrollBody extends StatelessWidget{
  final Key? key;
  final List<Widget> children;
  final ScrollPhysics? physics;
  final bool shrinkWrap;
  final bool enabled;

  ScrollBody({
    this.key,
    required this.children,
    this.physics,
    this.shrinkWrap = false,
    this.enabled = true,
  });


  final ScrollBodyController controller = Get.put(ScrollBodyController());

  @override
  Widget build(BuildContext context) {

    final Key _key = key == null
        ? ValueKey(children.hashCode)
        : key!;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.initScrollablePage(_key);
    });

    return GetBuilder<ScrollBodyController>(
      initState: (_) => controller.initController(_key),
      builder: (_) => Scrollbar(
        controller: controller.scrollController(_key),
        isAlwaysShown: true,
        child: ListView(
            controller: controller.scrollController(_key),
            physics: physics,
            children: children,
            shrinkWrap: shrinkWrap
        ),
      ),
    );
  }


}