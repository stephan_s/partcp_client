// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// class PasswordFieldWidget extends StatefulWidget {
//
//   final TextEditingController textController;
//   final String hint;
//   final String errorText;
//   final VoidCallback onTap;
//   final double fontSize;
//   final Color borderColor;
//   final double maxBoxLength;
//
//   const PasswordFieldWidget ({
//     @required this.textController,
//     @required this.hint,
//     @required this.onTap,
//     this.errorText,
//     this.fontSize = 18.0,
//     this.borderColor = Colors.blueAccent,
//     this.maxBoxLength = 300,
//   });
//
//   @override
//   _PasswordFieldWidgetState createState() => _PasswordFieldWidgetState();
// }
//
// class _PasswordFieldWidgetState extends State<PasswordFieldWidget> {
//
//   bool obscureText = true;
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       constraints: BoxConstraints(minWidth: 100, maxWidth: widget.maxBoxLength),
//       child: Row(
//         children: [
//           Expanded(
//             child: TextFormField(
//               controller: widget.textController,
//               readOnly: true,
//               keyboardType: TextInputType.visiblePassword,
//               obscureText: obscureText,
//               showCursor: true,
//               onTap: () => widget.onTap(),
//               style: TextStyle(
//                 fontWeight: FontWeight.bold,
//                 color: Colors.black87,
//                 fontSize: widget.fontSize,
//               ),
//               decoration: InputDecoration(
//                 contentPadding: EdgeInsets.fromLTRB(21.0, 0.0, 0.0, 0.0),
//                 filled: true,
//                 enabledBorder: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(5.0),
//                   borderSide: BorderSide(width: 1.0, color: widget.borderColor),
//                 ),
//                 focusedBorder: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(5.0),
//                   borderSide: BorderSide(width: 2.0, color: widget.borderColor),
//                 ),
//                 hintText: widget.hint,
//                 hintStyle: TextStyle(fontSize: widget.fontSize, fontWeight: FontWeight.w700, color: Colors.black38),
//                 floatingLabelBehavior: FloatingLabelBehavior.always,
//                 errorText: widget.errorText,
//               ),
//             ),
//           ),
//
//           Padding(
//             padding: const EdgeInsets.only(left: 10),
//             child: InkWell(
//               child: Icon(
//                   obscureText
//                     ? Icons.visibility_off_outlined
//                     : Icons.visibility_outlined,
//                   color: Colors.black54,
//                   size: widget.fontSize,
//               ),
//               onTap: () {
//                 setState(() {
//                   obscureText = !obscureText;
//                 });
//               },
//             ),
//           )
//         ],
//       ),
//     );;
//   }
// }