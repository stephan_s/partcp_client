// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';

class Sspacer {

  static final s5 = Container(
    height: 5,
  );

  static final s10 = Container(
    height: 10,
  );

  static final s20 = Container(
    height: 20,
  );
}
