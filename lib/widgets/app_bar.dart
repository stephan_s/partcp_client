// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ui/home/index_page.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget? title;
  final List<Widget>? actions;
  final isStartPage;

  MyAppBar({
    Key? key,
    this.title,
    this.actions,
    this.isStartPage = false
  }) : preferredSize = Size.fromHeight(kToolbarHeight), super(key: key);

  @override
  final Size preferredSize; // default is 56.0


  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: title,
      actions: actions,
      leading: _leading(),
    );
  }

  Widget? _leading () {
    if (isStartPage) {
      return MyAppBar.logo;
    }
    return null;
    // return GetPlatform.isWeb && !MainController.to.isFullScreen
    //   ? _homeLink
    //   : null;
  }

  Widget _homeLink = InkWell(
    onTap: () => Get.offAll(() => IndexPage()),
    child: MyAppBar.logo,
  );

  static Widget get logo {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Container(
        // width: (100/4.68),
        // height: 40,
        child: Image.asset(
          'assets/images/logo_app.png',
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
