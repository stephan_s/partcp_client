// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:partcp_client/styles/styles.dart';

class ButtonIcon extends StatelessWidget {
  ButtonIcon({
    Key? key,
    this.onPressed,
    required this.icon,
    this.color
  });

  final Function? onPressed;
  final Icon icon;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    IconThemeData iconThemeData = IconTheme.of(context);

    return IconButton(
      onPressed: onPressed != null
        ? () => onPressed!()
        : null,
      iconSize: iconThemeData.size!,
      icon: icon,
      color: color == null
        ? Styles.buttonColor
        : color,
    );
  }
}