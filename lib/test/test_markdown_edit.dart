// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'dart:math';
//
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:partcp_client/widgets/markdown_view.dart';
// import 'package:partcp_client/utils/form_validators.dart';
// import 'package:partcp_client/utils/log.dart';
// import 'package:partcp_client/utils/measure_size.dart';
// import 'package:partcp_client/widgets/app_bar.dart';
// import 'package:adaptive_dialog/adaptive_dialog.dart';
//
// /// https://spec.commonmark.org/0.30/
//
// class TestMarkdownController extends GetxController {
//   final String _TAG = "AdminMarkdownController";
//
//   /// we save each time while user clicks the save button
//   List<String> saved = [];
//
//   BuildContext context;
//   String text;
//   TextEditingController textController;
//
//   final double appBarHeight = 56.0;
//   final double toolbarHeight = 46;
//   double lineHeight = 19;
//
//   double _lastFormHeight = 0;
//   int _lastFormLines = 0;
//
//   double bodyHeight = 500;
//   int formLines = 10;
//
//   void initController(BuildContext context, String text) {
//     this.text = text;
//     this.context = context;
//
//     saved.add(text);
//     textController = TextEditingController(text: text);
//     // textController.buildTextSpan(context: context);
//   }
//
//   void onSave() {
//     if (saved.last != textController.text) {
//       saved.add(textController.text);
//     }
//   }
//
//   void onFormSizeChanges(Size size) {
//       Log.d(_TAG, "onFormSizeChanges; "
//           "screen height: ${context.height} "
//           "form height: ${size.height}");
//       if (_lastFormHeight == 0) {
//         _lastFormHeight = size.height;
//         _lastFormLines = formLines;
//
//         formLines++;
//         updateMe();
//       }
//       else {
//         lineHeight = (size.height - _lastFormHeight) / (formLines - _lastFormLines);
//         Log.d(_TAG, "onFormSizeChanges; lineHeight: $lineHeight");
//       }
//   }
//
//   void onBodySizeChanges(Size size) {
//     Log.d(_TAG, "onBodySizeChanges; "
//         "screen height: ${context.height} "
//         "form height: ${size.height}");
//
//     bodyHeight = size.height;
//     formLines = ((bodyHeight - toolbarHeight - appBarHeight) / lineHeight).floor();
//     if (formLines <=0) {
//       formLines = 1;
//     }
//     Log.d(_TAG, "onBodySizeChanges; textFieldLines:$formLines");
//
//     updateMe();
//   }
//
//   Future<bool> onClickGoBack() async {
//     Log.d(_TAG, "onClickGoBack(): ${textController.text}");
//
//     if (textController.text != text || textController.text != saved.last) {
//
//       /// ask to save
//       final result = await showOkCancelAlertDialog(
//         context: context,
//         message: 'Änderungen speichern?',
//         okLabel: 'YES!',
//
//         barrierDismissible: false,
//       );
//
//       Get.back(result: result == OkCancelResult.ok
//           ? textController.text
//           : null
//       );
//       return true;
//     }
//     else {
//       Get.back(result: saved.last);
//       return true;
//     }
//   }
//
//   Future<void> onPostFrameCallback() async {
//   }
//
//   void updateMe() {
//     update();
//   }
// }
//
//
// class TestMarkdownEdit extends StatelessWidget {
//   final String title;
//   final String text;
//   final String hintText;
//
//   TestMarkdownEdit({
//     @required this.title,
//     @required this.text,
//     this.hintText = "Beschreibung"
//   });
//
//   final TestMarkdownController controller = Get.put(TestMarkdownController());
//
//   final ScrollController _formScrollController = ScrollController();
//   final ScrollController _toolBarScrollController = ScrollController();
//
//   final _random = Random();
//   final _formKey = GlobalKey<FormState>();
//
//   @override
//   Widget build(BuildContext context) {
//
//     WidgetsBinding.instance.addPostFrameCallback((_) {
//       controller.onPostFrameCallback();
//     });
//
//     return GetBuilder<TestMarkdownController>(
//         initState: (_)  => controller.initController(context, text),
//         builder: (_) => WillPopScope(
//           onWillPop: controller.onClickGoBack,
//           child: Scaffold(
//             appBar: MyAppBar(
//               title: Text(title),
//               actions: [
//
//                 /// save
//                 IconButton(
//                     icon: Icon(Icons.save_outlined),
//                     onPressed: () => controller.onSave()
//                 ),
//
//                 /// preview
//                 IconButton(
//                   icon: Icon(Icons.preview),
//                   onPressed: () => Get.to(() => MarkdownView (
//                       title: title,
//                       markdownText: controller.textController.text)
//                   ),
//                 ),
//
//
//               ],
//             ),
//
//
//             body: OrientationBuilder(
//               builder: (_, __) =>
//                   MeasureSize(
//                     onChange: (size) => controller.onBodySizeChanges(size),
//                     child: Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Column(
//                         mainAxisSize: MainAxisSize.max,
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         children: [
//                           _editButtons,
//                           Divider(
//                             color: Colors.black26,
//                           ),
//                           Form(
//                             key: _formKey,
//                             child: MeasureSize(
//                               onChange: (size) => controller.onFormSizeChanges(size),
//                               child: Scrollbar(
//                                 isAlwaysShown: true,
//                                 controller: _formScrollController,
//                                 child: TextFormField(
//                                   validator: FormValidators.requiredField,
//                                   autofocus: true,
//                                   decoration: InputDecoration(
//                                     hintText: hintText,
//                                   ),
//                                   minLines: controller.formLines,
//                                   maxLines: controller.formLines,
//                                   controller: controller.textController,
//                                   scrollController: _formScrollController,
//                                   // keyboardType: TextInputType.multiline,
//                                 ),
//                               ),
//                             ),
//                           ),
//
//                         ],
//                       ),
//                     ),
//                   ),
//             ),
//           ),
//         ));
//   }
//
//   void _surroundTextSelection(String left, String right) {
//     final currentTextValue = controller.textController.value.text;
//     final selection = controller.textController.selection;
//     final middle = selection.textInside(currentTextValue);
//     final newTextValue = selection.textBefore(currentTextValue) +
//         '$left$middle$right' +
//         selection.textAfter(currentTextValue);
//
//     controller.textController.value = controller.textController.value.copyWith(
//       text: newTextValue,
//       selection: TextSelection.collapsed(
//         offset: selection.baseOffset + left.length + middle.length,
//       ),
//     );
//   }
//
//   Widget get _editButtons => Scrollbar(
//     controller: _toolBarScrollController,
//     child: SingleChildScrollView(
//       child: Container(
//         height: controller.toolbarHeight,
//         child: ListView(
//           primary: true,
//           scrollDirection: Axis.horizontal,
//           children: [
//             IconButton(
//               tooltip: 'Bold',
//               icon: Icon(Icons.format_bold),
//               onPressed: () => _surroundTextSelection(
//                 '**',
//                 '**',
//               ),
//             ),
//
//             IconButton(
//               tooltip: 'Underline',
//               icon: Icon(Icons.format_italic),
//               onPressed: () => _surroundTextSelection(
//                 '_',
//                 '_',
//               ),
//             ),
//
//             // IconButton(
//             //   tooltip: 'Code',
//             //   icon: Icon(Icons.code),
//             //   onPressed: () => _surroundTextSelection(
//             //     '```',
//             //     '```',
//             //   ),
//             // ),
//
//             IconButton(
//               tooltip: 'Strikethrough',
//               icon: Icon(Icons.strikethrough_s_rounded),
//               onPressed: () => _surroundTextSelection(
//                 '~~',
//                 '~~',
//               ),
//             ),
//
//             IconButton(
//               tooltip: 'List',
//               icon: Icon(Icons.list),
//               onPressed: () => _surroundTextSelection(
//                 '* ',
//                 ' ',
//               ),
//             ),
//
//             IconButton(
//               tooltip: 'Line',
//               icon: Icon(Icons.remove),
//               onPressed: () => _surroundTextSelection(
//                 '***',
//                 ' ',
//               ),
//             ),
//
//             IconButton(
//               tooltip: 'New Line',
//               icon: Icon(Icons.subdirectory_arrow_left_outlined),
//               onPressed: () => _surroundTextSelection(
//                 '\\',
//                 '',
//               ),
//             ),
//
//             TextButton(
//               child: Text('H1', style: TextStyle(color: Colors.black),),
//               // icon: Icon(Icons.h),
//               onPressed: () => _surroundTextSelection(
//                 '# ',
//                 '',
//               ),
//             ),
//
//             TextButton(
//               child: Text('H2', style: TextStyle(color: Colors.black),),
//               // icon: Icon(Icons.h),
//               onPressed: () => _surroundTextSelection(
//                 '## ',
//                 '',
//               ),
//             ),
//
//             TextButton(
//               child: Text('H3', style: TextStyle(color: Colors.black),),
//               // icon: Icon(Icons.h),
//               onPressed: () => _surroundTextSelection(
//                 '### ',
//                 '',
//               ),
//             ),
//
//             // IconButton(
//             //   tooltip: 'Link',
//             //   icon: Icon(Icons.link_sharp),
//             //   onPressed: () => _surroundTextSelection(
//             //     '[title](https://',
//             //     ')',
//             //   ),
//             // ),
//             // IconButton(
//             //   tooltip: 'Image Link',
//             //   icon: Icon(Icons.image),
//             //   onPressed: () => _surroundTextSelection(
//             //     '![](https://',
//             //     ')',
//             //   ),
//             // ),
//
//             // FutureBuilder(
//             //   future: _uploadImageFromGalleryFuture,
//             //   builder: (context, snapshot) {
//             //     final isLoading = snapshot.connectionState ==
//             //         ConnectionState.waiting;
//             //
//             //     void _onPressed() {
//             //       // setState(
//             //       //       () {
//             //       //     _uploadImageFromGalleryFuture =
//             //       //         _uploadImage(
//             //       //           ImageSource.gallery,
//             //       //         );
//             //       //   },
//             //       );
//             //     }
//             //
//             //     return IconButton(
//             //       tooltip: 'Upload image from gallery',
//             //       icon: isLoading
//             //           ? _iconButtonLoading
//             //           : Icon(Icons.photo_library),
//             //       onPressed: isLoading ? null : _onPressed,
//             //     );
//             //   },
//             // ),
//             //
//             // FutureBuilder(
//             //   future: _uploadImageFromCameraFuture,
//             //   builder: (context, snapshot) {
//             //     final isLoading = snapshot.connectionState ==
//             //         ConnectionState.waiting;
//             //
//             //     void _onPressed() {
//             //       setState(
//             //             () {
//             //           _uploadImageFromCameraFuture =
//             //               _uploadImage(
//             //                 ImageSource.camera,
//             //               );
//             //         },
//             //       );
//             //     }
//             //
//             //     return IconButton(
//             //       tooltip: 'Upload image from camera',
//             //       icon: isLoading
//             //           ? _iconButtonLoading
//             //           : Icon(Icons.camera_alt),
//             //       onPressed: isLoading ? null : _onPressed,
//             //     );
//             //   },
//             // )
//           ],
//         ),
//       ),
//     ),
//   );
//
// }