// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'dart:convert';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
//
// import 'package:cryptography/cryptography.dart' as ecLib;
// import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';
// import 'package:partcp_client/utils/log.dart';
// import 'package:partcp_client/widgets/app_bar.dart';
//
// const _TAG = "KeyTest";
//
// class KeyTestController extends GetxController {
//   ecLib.SimpleKeyPair keyPairLocal;
//   ecLib.SimpleKeyPair keyPairRemote;
//
//   ecLib.SimplePublicKey publicKeyLocal;
//   ecLib.SimplePublicKey publicKeyRemote;
//
//   updateMe() {
//     update();
//   }
//
//   void testKeys() async {
//     String testMsg = "Hello world";
//     List<int> testKey = [47, 216, 138, 214, 58, 216, 185, 40, 187, 35, 94, 172, 229, 41, 223, 35, 127, 62, 207, 53, 77, 244, 82, 33, 194, 89, 78, 101, 195, 34, 68, 80];
//
//     String privateKeyRemoteB64 = "pilCvpaNa/1AbXfUIISDmm+URQxqzynuOY69FXqpNcw=";
//     String ivB64 = "HneoOfaFnH9WLehLnB6M5A==";
//     String msgEnc = "elzzuSntXJHMXSi2+x83/xygYGKcNztxuDoUmB6WvSBonSncNDzM1p0C4NT1upW6KTmxg5Lbv/sI2zuDqIC/1w==";
//
//     List<int> privateKeyLocalSeed = [0, 37, 10, 129, 38, 77, 69, 203, 161, 227, 222, 14, 175, 177, 156, 78, 191, 133, 191, 128, 222, 13, 195, 50, 211, 32, 103, 171, 12, 195, 113, 94];
//
//     // String remoteKey = "qlb+NoP0VkRYfB6qA8nMhZ6LUpJHCkIvKJM7bSOpJlY=";
//     // List<int> remoteKeyBytes = base64Decode(remoteKey);
//     // Log.d(_TAG, "remoteKey: $remoteKey");
//     // Log.d(_TAG, "remoteKeyBytes (${remoteKeyBytes.length}): $remoteKeyBytes");
//
//     List<int> privateKeyRemoteSeed = base64Decode(privateKeyRemoteB64);
//
//     keyPairLocal = await MyEcCrypt.createKeyPairXFromSeed(privateKeyLocalSeed);
//     keyPairRemote = await MyEcCrypt.createKeyPairXFromSeed(base64Decode(privateKeyRemoteB64));
//
//     // keyPairLocal = await MyEcCrypt.createKeyPairXFromSeed(base64Decode("fdLx3/jC6we297C9FNw5Kuc6c0e/b93bsoA3wEQ1ymI="));
//     // keyPairRemote = await MyEcCrypt.createKeyPairXFromSeed(base64Decode("5cYl+Dh0I9knnAPKC2gwscchBpI2KqW+roUiUwhguhY="));
//
//     publicKeyLocal = await keyPairLocal.extractPublicKey();
//     publicKeyRemote = await keyPairRemote.extractPublicKey();
//
//     Log.d("", "---------------------------------");
//     Log.d(_TAG, "privateKeyLocal Extract bytes ${await keyPairLocal.extractPrivateKeyBytes()}");
//     Log.d(_TAG, "privateKeyLocal from Seed: $privateKeyLocalSeed}");
//     Log.d(_TAG, "privateKeyLocalB64: ${base64Encode(privateKeyLocalSeed)}");
//     Log.d(_TAG, "publicKeyLocal ${base64Encode(publicKeyLocal.bytes)}");
//     Log.d("", "---------------------------------");
//
//     Log.d(_TAG, "privateKeyRemoteB64 $privateKeyRemoteB64");
//     Log.d(_TAG, "privateKeyRemote: (${privateKeyRemoteSeed.length}) $privateKeyRemoteSeed");
//     Log.d(_TAG, "publicKeyRemote ${publicKeyRemote.bytes}");
//     Log.d(_TAG, "publicKeyRemoteB64: ${base64Encode(publicKeyRemote.bytes)}");
//     Log.d("", "---------------------------------");
//
//     ecLib.SecretKey secret = await MyEcCrypt.sharedSecretWithPubKey(keyPairLocal, publicKeyRemote);
//     List<int> secretBytes = await secret.extractBytes();
//     Log.d(_TAG, "secretBytes ${secretBytes}");
//     Log.d(_TAG, "secretB64: ${base64Encode(secretBytes)}");
//
//     ecLib.SecretKey secret2 = await MyEcCrypt.sharedSecretWithPubKey(keyPairRemote, publicKeyLocal);
//     List<int> secretBytes2 = await secret2.extractBytes();
//     Log.d(_TAG, "secretBytes2 ${secretBytes2}");
//     Log.d("", "---------------------------------");
//
//     // String decTest = MyAesCrypt.aesDecryptSteelWithPwd(
//     //     base64Encode(secretBytes), ivB64, msgEnc);
//     // Log.d(_TAG, "decTest ${decTest}");
//
//     // String testKeyB64 = base64Encode(testKey);
//     // Log.d(_TAG, "testKeyB64 ${testKeyB64}");
//     //
//     //
//     // String encMsg = MyAesCrypt.aesEncryptSteelWithPwd(testKeyB64, ivB64, testMsg);
//     // Log.d(_TAG, "encMsg ${encMsg}");
//     //
//     // String decTest = MyAesCrypt.aesDecryptSteelWithPwd(testKeyB64, ivB64, "DEUjCQBRSonlMAsL+76T");
//     // Log.d(_TAG, "decTest ${decTest}");
//
//
//   }
//
// }
//
// class KeyTest extends StatelessWidget {
//   final KeyTestController controller = Get.put(KeyTestController());
//
//   @override
//   Widget build(BuildContext context) {
//     return GetBuilder<KeyTestController>(
//       builder: (_) => Scaffold(
//         appBar: MyAppBar(
//           title: Text("Key-Test"),
//         ),
//         body: Column(
//           children: [
//             ElevatedButton(
//                 onPressed: () => controller.testKeys(),
//                 child: Text("Update")),
//           ],
//         ),
//       ),
//     );
//   }
//
// }