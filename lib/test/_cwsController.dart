// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'package:get/get.dart';
// import 'package:partcp_client/channels/connService/persistent/conn_service_handler.dart';
//
// class CwsController extends GetxController {
//
//   ConnServiceHandler handler;
//
//   void initController() {
//     if (handler == null) {
//       handler = ConnServiceHandler();
//     }
//   }
//
//   void connect() {
//     handler.onPushMessageReceived();
//   }
//
// }