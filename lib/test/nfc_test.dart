// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'dart:convert';
// import 'dart:typed_data';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:get/get.dart';
// import 'package:partcp_client/utils/log.dart';
// import 'package:partcp_client/widgets/app_bar.dart';
// import 'package:partcp_client/widgets/scroll_body.dart';
// import 'package:partcp_client/widgets/widgets.dart';
//
// import 'package:flutter_nfc_kit/flutter_nfc_kit.dart';
// import 'package:flutter_easy_nfc/flutter_easy_nfc.dart';
//
//
//
// class NfcController extends GetxController {
//   static const _TAG = "NfcController";
//
//   bool reading = false;
//
//   Future<void> read() async {
//     await read_ease_nfc();
//     await read_flutter_nfc_kit();
//   }
//
//   Future<void> finish() async {
//
//   }
//
//
//
//
//   Future<void>read_ease_nfc() async {
//     FlutterEasyNfc.startup();
//     FlutterEasyNfc.onNfcEvent((NfcEvent event) async {
//       if (event.tag is IsoDep) {
//         IsoDep isoDep = event.tag;
//         await isoDep.connect();
//         var test1 = await isoDep.transceive("00B0950000");
//         print("test1: $test1");
//
//         Uint8List file05 = await isoDep.transceive("00b0850000");
//         print("file05: $file05");
//
//         var test2 = await isoDep.transceive("00a40000023f01");
//         print("test2: $test2");
//
//         Uint8List file15 = await isoDep.transceive("00b0950000");
//         print("file15: $file15");
//
//         await isoDep.close();
//       }
//     });
//   }
//
//
//   Future<void> finish_flutter_nfc_kit() async {
//     await FlutterNfcKit.finish();
//   }
//
//   Future<void>read_flutter_nfc_kit() async {
//
//     var availability = await FlutterNfcKit.nfcAvailability;
//     if (availability != NFCAvailability.available) {
//       // oh-no
//       Log.d(_TAG, "availability: $availability");
//     }
//     print("start");
//
//     // timeout only works on Android, while the latter two messages are only for iOS
//     var tag = await FlutterNfcKit.poll(
//         timeout: Duration(seconds: 60),
//         iosMultipleTagMessage: "Multiple tags found!", iosAlertMessage: "Scan your tag",
//         readIso18092: true,
//         readIso14443B: true,
//         readIso14443A: true,
//         readIso15693: true,
//     );
//
//     print("pooled");
//     print(jsonEncode(tag));
//
//     if (tag.type == NFCTagType.iso7816) {
//       print("tag Type: NFCTagType.iso7816");
//
//       var test1 = await FlutterNfcKit.transceive("00B0950000", timeout: Duration(seconds: 5)); // timeout is still Android-only, persist until next change
//       print("test1: " + test1);
//
//       var file5 = await FlutterNfcKit.transceive("00b0850000", timeout: Duration(seconds: 5)); // timeout is still Android-only, persist until next change
//       print("file5: " + file5);
//
//       var test2 = await FlutterNfcKit.transceive("00a40000023f01", timeout: Duration(seconds: 5)); // timeout is still Android-only, persist until next change
//       print("test2: " + test2);
//
//       var file15 = await FlutterNfcKit.transceive("00b0950000", timeout: Duration(seconds: 5)); // timeout is still Android-only, persist until next change
//       print("file15: " + file15);
//
//     }
//
//     // iOS only: set alert message on-the-fly
//     // this will persist until finish()
//     await FlutterNfcKit.setIosAlertMessage("hi there!");
//
//     // read NDEF records if available
//     if (tag.ndefAvailable){
//       /// decoded NDEF records (see [ndef.NDEFRecord] for details)
//       /// `UriRecord: id=(empty) typeNameFormat=TypeNameFormat.nfcWellKnown type=U uri=https://github.com/nfcim/ndef`
//       for (var record in await FlutterNfcKit.readNDEFRecords(cached: false)) {
//         print("readNDEFRecords: " + record.toString());
//       }
//       /// raw NDEF records (data in hex string)
//       /// `{identifier: "", payload: "00010203", type: "0001", typeNameFormat: "nfcWellKnown"}`
//       for (var record in await FlutterNfcKit.readNDEFRawRecords(cached: false)) {
//         print("readNDEFRawRecords: " + jsonEncode(record).toString());
//       }
//     }
//
//     // write NDEF records if applicable
//     if (tag.ndefWritable) {
//       // decoded NDEF records
//       // await FlutterNfcKit.writeNDEFRecords([new ndef.UriRecord.fromUriString("https://github.com/nfcim/flutter_nfc_kit")]);
//       // // raw NDEF records
//       // await FlutterNfcKit.writeNDEFRawRecords([new NDEFRawRecord("00", "0001", "0002", "0003", ndef.TypeNameFormat.unknown)]);
//     }
//
//     // Call finish() only once
//     await FlutterNfcKit.finish();
//     // iOS only: show alert/error message on finish
//     await FlutterNfcKit.finish(iosAlertMessage: "Success");
//     // or
//     await FlutterNfcKit.finish(iosErrorMessage: "Failed");
//   }
//
//   void initController() {
//   }
// }
//
//
// class NfcTest extends StatelessWidget{
//   final NfcController controller = Get.put(NfcController());
//
//   @override
//   Widget build(BuildContext context) {
//
//     return OrientationBuilder(builder: (_, __) =>
//         GetBuilder<NfcController>(builder: (_) =>
//             Scaffold(
//               appBar: MyAppBar(
//                 title: Text("NFC Test"),
//               ),
//               body: Widgets.loaderBox(
//                   child: ScrollBody(children: _body),
//                   loading: controller.reading
//               ),
//             )
//         )
//     );
//   }
//
//   List<Widget> get _body {
//     return [
//       ButtonBar(
//         children: [
//           TextButton(
//               onPressed:  () => controller.read(),
//               child: Text("read")),
//
//           TextButton(
//               onPressed:  () => controller.finish(),
//               child: Text("stop")),
//
//         ],
//       )
//     ];
//   }
//
// }