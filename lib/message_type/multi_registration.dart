// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/voting_event.dart';


Map<String, dynamic> multiRegistration({
  required int count,
  required VotingEvent event,
}) {

  Map<String, dynamic> map = {
    "Message-Type": "multi-registration",
    "Event-Id": event.id,
    "Count": count
  };
  return map;
}