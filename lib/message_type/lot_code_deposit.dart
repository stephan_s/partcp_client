// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

/*
  https://demo01.partcp.org/modules/events/doc/lot-code-deposit.txt
 */

Map<String, dynamic> lotCodeDeposit({
  required participantId,
  required String lotCodeEnc,
  required eventId,
}) {
  Map<String, dynamic> map = {
    "Message-Type": "lot-code-deposit",
    "Participant-Id": participantId,
    "Event-Id": eventId,
    "Lot-Code~": lotCodeEnc
  };
  return map;
}