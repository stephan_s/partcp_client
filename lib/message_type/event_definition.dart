// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/client_data.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/utils/s_utils.dart';

Map<String, dynamic> eventDefinition(Group group, VotingEvent event) {

  Map<String, dynamic> namingRules = {
    "prefix": event.namingValues.prefix,
    "counter_width": event.namingValues.counterWidth,
    "group_length": event.namingValues.groupLength,
    "group_separator": event.namingValues.groupSeparator,
    "crc_length": event.namingValues.crcLength,
  };

  Map<String, dynamic> lotCodeRules = {
    "char_list": event.lotCodeValues.charList,
    "final_length": event.lotCodeValues.finalLength,
    "crc_length": event.lotCodeValues.crcLength,
    "group_length": event.lotCodeValues.groupLength,
    "group_separator": event.lotCodeValues.groupSeparator,
  };

  Map<String, dynamic> credentialRules = {
    "char_list": event.credentialValues.charList,
    "final_length": event.credentialValues.finalLength,
    "crc_length": event.credentialValues.crcLength,
    "group_length": event.credentialValues.groupLength,
    "group_separator": event.credentialValues.groupSeparator,
  };


  Map<String, dynamic> eventData = {};
  eventData["name"] = event.name;
  eventData["date"] = SUtils.dateToYaml(event.date);
  eventData["estimated_turnout"] = event.estimatedTurnout;
  eventData["naming_rules"] = namingRules;

  // var eventClientDataMap = event.clientData!.toMap();
  /*
  if (eventClientDataMap.isNotEmpty) {
    eventData[ClientData.CLIENT_DATA]=  eventClientDataMap;
  }
  */
  String eventClientDataString =  event.clientData!.toYaml();
  eventData[ClientData.CLIENT_DATA]=  eventClientDataString;


  if (event.paperLots) {
    eventData["credential_rules"] = credentialRules;
  }
  if (event.lotCodes) {
    eventData["lot_code_rules"] = lotCodeRules;
  };

  Map<String, dynamic> map = {
    "Message-Type": "event-definition",
    "Group-Id": group.id,
    "Event-Data": eventData,
  };
  return map;
}