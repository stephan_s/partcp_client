// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


Map<String, dynamic> ping() {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "ping";
  return map;
}