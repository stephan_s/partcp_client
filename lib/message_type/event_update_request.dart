// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/message_type/event_definition.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/voting_event.dart';

Map<String, dynamic> eventUpdateRequest(Group group, VotingEvent event) {

  Map<String, dynamic> map = eventDefinition(group, event);
  map["Message-Type"] = "event-update-request";
  map["Event-Id"] = event.id;

  if (map.containsKey("Group-Id")) {
    map.remove("Group-Id");
  }
  return map;
}