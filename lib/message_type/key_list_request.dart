// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/participant.dart';

/*
  Requests PublicKey for participant-ID
  https://demo01.partcp.org/modules/core/doc/key-list-request.txt
 */
Map<String, dynamic> keyListRequest({
  required List<Participant> participants,
}){

  List<String> items = [];
  participants.forEach((Participant p) {
    items.add(p.participantId!);
  });

  Map<String, dynamic> map = {
    "Message-Type": "key-list-request",
    "Participants": items
  };
  return map;
}