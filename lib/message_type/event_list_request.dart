// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/group.dart';

Map<String, dynamic> eventListRequest(Group group,
    {required bool includeVotingCount, required bool includeAdminInfo, required bool includeOpenVotings}) {
  Map<String, dynamic> map = {
    "Message-Type": "event-list-request",
    "Group-Id": group.id,
    "Include-Voting-Count": true,
    "Include-Open-Votings": includeOpenVotings,
    "Include-Admin-Info": includeAdminInfo
  };
  return map;
}