// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/foundation.dart';

/*
  https://demo01.partcp.org/modules/events/doc/lot-code-request.txt
 */

Map<String, dynamic> lotCodeRequest({
  @required eventId,
}) {
  Map<String, dynamic> map = {
    "Message-Type": "lot-code-request",
    "Event-Id": eventId
  };
  return map;
}