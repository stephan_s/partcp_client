// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/client_data.dart';
import 'package:partcp_client/objects/voting_event.dart';

Map<String, dynamic> eventUpdateRequestVotingSortOrder(VotingEvent event) {

  Map<String, dynamic> eventData = {};
  Map<String, dynamic> eventClientDataMap = {
    ClientData.VOTING_SORT_ORDER: event.clientData!.votingSortOrder.toList(growable: false)
  };

  if (eventClientDataMap.isNotEmpty) {
    eventData[ClientData.CLIENT_DATA] =  eventClientDataMap;
  }

  Map<String, dynamic> map = {
    "Message-Type": "event-update-request",
    "Event-Id": event.id,
    "Event-Data": eventData,
  };
  return map;
}