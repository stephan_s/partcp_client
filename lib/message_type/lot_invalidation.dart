// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/lot_code.dart';
import 'package:partcp_client/objects/voting_event.dart';

Map<String, dynamic> lotInvalidation({required List<LotCode> lotCodes, required VotingEvent event}) {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "lot-invalidation";
  map["Lot-Codes"] = lotCodes.cast();
  map["Event-Id"] = event.id;
  return map;
}

