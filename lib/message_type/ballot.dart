// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';

/*
Signature: ...
Date: 2021-04-02T07:32:55+02:00
From: martin@reg.virthos.net
To: reg.virthos.net
Message-Type: ballot
Voting-Id: abstimmung-01
Votes:
    - { id: 1, vote: 1 }
    - { id: 2, vote: 0 }
    - { id: 3, vote: -1 }
    - { id: 4, vote: 1 }
    - { id: 5, vote: 0 }
 */


Map<String, dynamic> ballot({
  required VotingEvent event,
  required Voting voting})
{
  List<Map<String, dynamic>> _votes = [];
  for (Vote vote in voting.votes) {
    _votes.add(vote.toMap);
  }

  Map<String, dynamic> map = {};
  map["Message-Type"] = "ballot";
  map["Event-Id"] = event.id;
  map["Voting-Id"] = voting.id;
  map["Votes"] = _votes;
  return map;
}