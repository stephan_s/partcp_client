// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';

Map<String, dynamic> votingDetailsRequest({
  required VotingEvent event,
  required Voting voting,
  bool includeSubgroups = true
  }) {

  Map<String, dynamic> map = {
    "Message-Type": "voting-details-request",
    "Event-Id": event.id,
    "Voting-Id": voting.id,
    "Include-Subgroups": includeSubgroups
  };
  return map;
}

