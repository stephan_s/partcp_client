


Map<String, dynamic> participantDetailsRequest({
  required String participantId
}) {

  Map<String, dynamic> map = {
    "Message-Type": "participant-details-request",
    "Participant-Id": participantId
  };
  return map;
}