// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:flutter/foundation.dart';
import 'package:partcp_client/objects/voting.dart';

// enum VotingStatusEnum {
//   all, idle, open, closed
// }

Map<String, dynamic> votingListRequest({
  VotingStatusEnum votingStatus = VotingStatusEnum.open,
  required String votingEventId
  }) {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "voting-list-request";
  map["Event-Id"] = votingEventId;
  map["Voting-Status"] = "${describeEnum(votingStatus)}";
  return map;
}

