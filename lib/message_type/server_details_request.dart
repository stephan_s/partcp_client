// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


/*
  https://demo01.partcp.org/modules/server/doc/server-details-request.txt
 */

Map<String, dynamic> serverDetailsRequest(bool includeEvents, bool includeSubgroups) {
  Map<String, dynamic> map = {
    "Message-Type": "server-details-request",
    "Include-Events": includeEvents,
    "Include-Subgroups": includeSubgroups
  };
  return map;
}

