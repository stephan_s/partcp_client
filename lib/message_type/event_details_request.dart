// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:partcp_client/objects/voting_event.dart';

Map<String, dynamic> eventDetailsRequest(VotingEvent event) {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "event-details-request";
  map["Event-Id"] = event.id;
  return map;
}


