// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/client_data.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/utils/s_utils.dart';


Map<String, dynamic> votingDefinition({
  required VotingEvent event,
  required Voting voting}) {

  return _votingMap(event: event, voting: voting, messageType: "voting-definition");
}


Map<String, dynamic> votingUpdateRequest({
  required VotingEvent event,
  required Voting voting}) {

  return _votingMap(event: event, voting: voting, messageType: "voting-update-request");
}



Map<String, dynamic> _votingMap({
  required String messageType,
  required VotingEvent event,
  required Voting voting}) {

  List<Map<String, dynamic>> _options = [];
  for (VotingOption option in voting.votingOptions) {
    _options.add(option.toMap());
  }

  Map<String, dynamic> _votingData() {
    var map = {
      Voting.NAME: voting.name.trim(),
      Voting.TITLE: voting.title.trim(),
      Voting.TYPE: voting.type,
      Voting.PERIOD_START: SUtils.dateTimeToServer(voting.periodStart),
      Voting.PERIOD_END: SUtils.dateTimeToServer(voting.periodEnd),
      VotingOption.OPTIONS: _options,
    };

    /*
    Map<String, dynamic> _votingClientDataMap = voting.clientData.toMap();
    if (_votingClientDataMap.isNotEmpty) {
      map[ClientData.CLIENT_DATA] = _votingClientDataMap;
    }
    */
    map[ClientData.CLIENT_DATA] = voting.clientData.toYaml();
    return map;
  };


  Map<String, dynamic> map = {};
  map["Message-Type"] = messageType;
  map["Event-Id"] = event.id;
  if (voting.id != null) {
    map["Voting-Id"] = voting.id;
  }
  map["Voting-Data"] = _votingData();
  return map;
}