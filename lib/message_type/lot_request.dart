// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/foundation.dart';

Map<String, dynamic> lotRequest({
  @required eventId,
  @required lotCodeEnc,
  lotCode,
}){
  Map<String, dynamic> map = {};
  map["Message-Type"] = "lot-request";
  map["Event-Id"] = eventId;

  map["Lot-Code~"] = lotCodeEnc;

  if (lotCode != null) {
    map["Lot-Code"] = lotCode;
  }

  return map;
}

