// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

Map<String, dynamic> groupListRequest(bool includeAdminInfo) {
  Map<String, dynamic> map = {
    "Message-Type": "group-list-request",
    "Include-Admin-Info": includeAdminInfo,
    "Include-Subgroups": true
  };
  return map;
}
