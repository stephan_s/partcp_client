// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';

Map<String, dynamic> voteCountRequest({
  required VotingEvent event,
  required Voting voting
}) {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "vote-count-request";
  map["Event-Id"] = event.id;
  map["Voting-Id"] = voting.id;
  return map;
}