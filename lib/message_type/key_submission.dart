// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

/*
  https://demo01.partcp.org/modules/local_id/doc/key-submission.txt
 */


Map<String, dynamic> keySubmission({String? credentialEnc, String? eventId, String? credential}) {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "key-submission";

  if (credentialEnc != null) {
    map["Credential~"] = credentialEnc;
  }
  if (eventId != null) {
    map["Event-Id"] = eventId;
  }
  if (credential != null) {
    map["Credential"] = credential;
  }
  return map;
}




