// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'dart:isolate';

import 'package:partcp_client/michel/ui/michel_controller.dart';
import 'package:path_provider_android/path_provider_android.dart';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/url_params.dart';
import 'package:partcp_client/ui/home/index_page.dart';

import 'package:universal_html/html.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:vibration/vibration.dart';

import 'utils/log.dart';

import 'package:partcp_client/michel/ui/tab_navigation.dart';
import 'package:partcp_client/michel/ui/login.dart';

/*
to prevent loading canvas from external url
flutter build web  --web-renderer html
flutter run -d chrome --web-renderer html
 */
const _TAG = "main";
bool isInBackground = false;

void _getUrlParams() {
  var uri = Uri.dataFromString(window.location.href);
  Map<String, String> params = uri.queryParameters;
  Log.d(_TAG, "params: $params");
  MainController.to.urlParams = UrlParams.fromMap(params);
}

/// background test
/// Be sure to annotate your callback function to avoid issues in release mode on Flutter >= 3.3.0
@pragma('vm:entry-point')
void _backgroundTest() {
  // isInBackground = true;
  // final DateTime now = DateTime.now();
  // final int isolateId = Isolate.current.hashCode;
  // print("[$now] Hello, world! isolate=${isolateId} function='$_backgroundTest'");
  // _doVibrate();
}

Future<void> _doVibrate() async {
  Log.d(_TAG, "doVibrate");
  if (isInBackground && GetPlatform.isAndroid) {
    bool? hasVib = await Vibration.hasVibrator();

    if (hasVib != null && hasVib) {
      Vibration.vibrate(duration: 1000);
    }
    // Future.delayed(const Duration(seconds: 10)).then((value) => _backgroundTest());
  }
}

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  Log.d(_TAG, "main");

  Get.put(MainController(), permanent: true);
  await MainController.to.initController();
  await Hive.initFlutter();

  if (GetPlatform.isAndroid) {
    await AndroidAlarmManager.initialize();
    // PathProviderAndroid.registerWith();
  }

  _getUrlParams();

  Get.put(MichelController(), permanent: true);
  Get.put(TabNavigationController(), permanent: true);
  runApp(MichelApp());

  if (GetPlatform.isAndroid) {
    final int helloAlarmID = 0;
    await AndroidAlarmManager.periodic(
      const Duration(minutes: 6),
      helloAlarmID,
      _backgroundTest,
      wakeup: true,
      rescheduleOnReboot: true
    );
  }
}

class MichelApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stimm-App',
      theme: ThemeData(
        fontFamily: 'Roboto',
        brightness: Brightness.light,
        primarySwatch: Colors.amber,
        textTheme: TextTheme(
          // bodySmall:
        ),
      ),
      home: LoginPage(),
    );
  }
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return AdaptiveTheme(
      light: ThemeData(
        fontFamily: 'Roboto',
        brightness: Brightness.light,
        // primarySwatch: Colors.red,
        primarySwatch: MaterialColor(
          0xff0126bf,
          <int, Color>{
            50: Color(0xff0126bf),
            100: Color(0xff0126bf),
            200: Color(0xff0126bf),
            300: Color(0xff0126bf),
            400: Color(0xff0126bf),
            500: Color(0xff0126bf),
            600: Color(0xff0126bf),
            700: Color(0xff0126bf),
            800: Color(0xff0126bf),
            900: Color(0xff0126bf)
          },
        ),
        accentColor: Colors.deepPurple,

        appBarTheme: AppBarTheme(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black87,
          toolbarTextStyle: TextStyle(
            color: Colors.black87,
            fontSize: 20
          ),
          actionsIconTheme: IconThemeData(color: Colors.black54),
          iconTheme: IconThemeData(color: Colors.black54),
        ),

        textTheme: TextTheme(
          bodyText1: TextStyle(fontSize: 18.0),
          bodyText2: TextStyle(fontSize: 16.0, color: Colors.black54),
          button: TextStyle(fontSize: 15.0),
        ),

      ),
      dark: ThemeData(
        fontFamily: 'Roboto',
        brightness: Brightness.dark,
        primarySwatch: Colors.red,
        accentColor: Colors.amber,
      ),
      initial: AdaptiveThemeMode.light,

      builder: (theme, darkTheme) => GetMaterialApp(

        builder: (context, child) =>
          MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true), child: child!,),

        theme: theme,
        darkTheme: darkTheme,

        //defaultTransition: GetPlatform.isMobile ? Transition.rightToLeftWithFade : Transition.fadeIn ,
        defaultTransition: GetPlatform.isMobile ? Transition.rightToLeftWithFade : Transition.noTransition ,

        initialRoute: '/',
        onGenerateRoute: (settings) {
          return MaterialPageRoute(
            builder: (context) => IndexPage(),
          );
        },

        title: 'ParTCP Client',
        enableLog: true,

        localizationsDelegates: GlobalMaterialLocalizations.delegates,

        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('de', 'DE'),
        ],
        // locale: Locale('de', 'DE'),
        locale: Get.deviceLocale,

        home: IndexPage(),
      ),
    );

  }
}
