// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


class MessageIdGenerator {

	static MessageIdGenerator _instance = MessageIdGenerator._internal();
	late int _lastTimeMs;

	MessageIdGenerator._internal() {
		_lastTimeMs = DateTime.now().millisecondsSinceEpoch;
	}
	factory MessageIdGenerator() {
		return _instance;
	}

	int getUniqueDate() {
		int thisTimeMs = DateTime.now().millisecondsSinceEpoch;
		if (thisTimeMs > _lastTimeMs) {
			_lastTimeMs = thisTimeMs;
		}
		else {
			_lastTimeMs = _lastTimeMs++;
			thisTimeMs = _lastTimeMs;
		}
		return thisTimeMs;
	}

	String create() {

		List<String> code = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

		// List<String> digits = _lastTimeMs.toString().split("");
		List<String> digits = getUniqueDate().toString().split("");

		String newId = "";
		bool doNext = true;
		int pos = 0;

		while (doNext) {
			String s1 = digits[pos];
			String s2 = "";
			if (pos+1 < digits.length) {
				s2 = digits[pos + 1];
			}
			String s = s1 + s2;
			int sInt = int.parse(s);

			if (sInt >= 10 && sInt < code.length + 10) {
				newId += code[sInt-10];
				pos += 2;
			}
			else {
				newId += s1;
				pos += 1;
			}

			if (pos >= digits.length) doNext = false;
		}

		return newId;
	}
}

