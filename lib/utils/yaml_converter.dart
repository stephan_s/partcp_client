// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:yaml/yaml.dart';
import 'package:json2yaml/json2yaml.dart';

import 'log.dart';


class YamlConverter {
  static const _TAG = "YamlConverter";

  String toYaml(Map<String, dynamic> map) {
    /// use 'package:json2yaml/json2yaml.dart'
    return json2yaml(map);
  }

  Map<String, dynamic> toMap(String yaml) {
    if (yaml.trim().isEmpty) {
      return Map();
    }
    try {
      /// use 'package:yaml/yaml.dart'
      return _toMap(yaml);
    }
    catch (e) {
      Log.e(_TAG, "toMap($yaml) => ERROR: $e");
      return Map();
    }
  }

  Map<String, dynamic> toMapUnCatch(String yaml) {
    return _toMap(yaml);
  }
  dynamic toDynamicUnCatch(String yaml) {
    return _toMap(yaml);
  }

  // Map<String, dynamic> _toMap(String yaml) {
  dynamic _toMap(String yaml) {
    var doc = loadYaml(yaml);
    String jsonDoc = jsonEncode(doc);
    // Map<String, dynamic> map = jsonDecode(jsonDoc);
    // return map;
    dynamic result = jsonDecode(jsonDoc);
    return result;

  }
}
