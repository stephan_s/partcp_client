// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

double get fieldBoxMaxWidth {
  double maxWidth = 1700;
  double maxWidthInPercent = 0.8;

  if (maxWidth > Get.width * maxWidthInPercent ) {
    maxWidth = Get.width * maxWidthInPercent;
  }
  return maxWidth;
}

EdgeInsetsGeometry get paddingForExpansionPanelTitle {
  double _p = (Get.width - fieldBoxMaxWidth) / 2;
  return EdgeInsets.fromLTRB(_p, 0, 0, 0);
}

EdgeInsetsGeometry get paddingForExpansionPanel {
  double _p = (Get.width - fieldBoxMaxWidth) / 2;
  return EdgeInsets.fromLTRB(_p, 0, _p, 0);
}