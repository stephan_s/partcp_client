// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'js_for_mobile.dart'
if (dart.library.html) 'dart:js'
as js;

class JsForWeb{

  static void openBrowserWindow(String url, String windowName) {
    js.context.callMethod('openWindow', [url, windowName]);
  }
  static void closeBrowserWindow() {
    js.context.callMethod('closeWindow');
  }
}