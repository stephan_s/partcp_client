// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

class FormValidators {
  static String? requiredField(String fieldValue) {
    if (fieldValue.isEmpty) {
      return 'Required';
    }

    return null;
  }
}