// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:filesystem_picker/filesystem_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:permission_handler/permission_handler.dart';

import "package:universal_html/html.dart" as webFile;

import '../constants.dart';
import 'log.dart';

const _TAG = "file_action";

class FileActions {

  static Future<void> downloadFileAction(String fileName, String fileContent) async {
    Log.d(_TAG,"downloadFileAction; fileContent:$fileContent");
    Log.d(_TAG,"downloadFileAction; fileName:$fileName");

    final bytes = utf8.encode(fileContent);
    // final blob = webFile.Blob([bytes]);
    final blob = webFile.Blob([bytes], "text/plain", 'native');
    final url = webFile.Url.createObjectUrlFromBlob(blob);

    final anchor = webFile.document.createElement('a') as webFile.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..title = "$fileName"
      ..download = "$fileName";
    webFile.document.body!.children.add(anchor);
    anchor.click();

    Log.d(_TAG,"downloadFileAction: FINISH");
  }


  static Future<String?> textFileFromExternalStorage({
    required String pageTitle,
    Directory? initialDirectory,
    String? fileExtension}) async {
    return await _textFileFromStorage(
        pageTitle: pageTitle,
        rootDirectory: await Constants.externalStorageRootDir,
        initialDirectory: initialDirectory);
  }


  static Future<String?> textFileFromInternalStorage({
    required String pageTitle,
    Directory? initialDirectory,
    String? fileExtension}) async {
    return await _textFileFromStorage(
        pageTitle: pageTitle,
        rootDirectory: await Constants.internalStorageRootDir,
        initialDirectory: initialDirectory);
  }


  static Future<String?> _textFileFromStorage({
    required String pageTitle,
    Directory? rootDirectory,
    Directory? initialDirectory,
    String? fileExtension,
  }) async {

    fileExtension = fileExtension == null
        ? "txt"
        : fileExtension;

    Log.d(_TAG, "GetPlatform isWeb: ${GetPlatform.isWeb}");
    Log.d(_TAG, "GetPlatform isAndroid: ${GetPlatform.isAndroid}");
    Log.d(_TAG, "GetPlatform isDesktop: ${GetPlatform.isDesktop}");
    Log.d(_TAG, "GetPlatform isLinux: ${GetPlatform.isLinux}");
    Log.d(_TAG, "GetPlatform isWindows: ${GetPlatform.isWindows}");

    /// WEB + DESKTOP
    if (MainController.to.isDesktopOrWeb) {
      FilePickerResult? filePickerResult = await _openFileAction(fileExtension);
      if (filePickerResult == null) return null;
      Log.d(_TAG, "loadWalletFile -> use for Web");
      Log.d(_TAG, "loadWalletFile file: ${filePickerResult.files.first.name}");

      try {
        return utf8.decode(filePickerResult.files.first.bytes!);
      }
      catch(e) {
        Log.e(_TAG, "textFileFromExternalStorage (Web); error: $e");
        return null;
      }
    }
    /// MOBILE
    else if(GetPlatform.isMobile) {
      FileTileSelectMode filePickerSelectMode = FileTileSelectMode.wholeTile;

      if (!initialDirectory!.existsSync()) {
        initialDirectory.createSync(recursive: true);
      }

      String? path = await FilesystemPicker.open(
        title: pageTitle,
        context: Get.context!,
        rootDirectory: rootDirectory!,
        directory: initialDirectory,
        fsType: FilesystemType.file,
        folderIconColor: Colors.teal,
        allowedExtensions: ['.txt'],
        fileTileSelectMode: filePickerSelectMode,
        requestPermission: () async =>
        await Permission.storage.request().isGranted,
      );

      if (path != null) {
        File file = File('$path');
        try {
          return await file.readAsString();
        }
        catch(e) {
          Log.e(_TAG, "textFileFromExternalStorage (Mobile); error: $e");
          return null;
        }
      }
      else {
        return null;
      }
    }
    else {
      return null;
    }

  }

  static Future<FilePickerResult?> _openFileAction(String? extension) async {
    Log.d(_TAG, "_openFileAction");

    try {
      FilePickerResult? filePickerResult = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: extension != null
          ? [extension]
          : null,
        withData: true,
      );
      return filePickerResult;
    }
    catch(e){
      Log.e(_TAG, "_openFileAction; filePickerCross error: $e");
      return null;
    }
  }


}