// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:uuid/uuid.dart';
import 'package:uuid/uuid_util.dart';

class SUtil {
  static String createUuid() {
    var uuid = new Uuid(options: {
      'grng': UuidUtil.cryptoRNG
    });
    return uuid.v4(options: {
      'rng': UuidUtil.cryptoRNG
    });
  }

}
