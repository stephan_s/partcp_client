// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:steel_crypt/steel_crypt.dart' as steelLib;
import 'package:cryptography/cryptography.dart' as ecLib;

class MyAesCrypt {
  static final String _TAG = "MyAesCrypt - ";

  /// do not change it in production !!!
  static final steelLib.PaddingAES _padding = steelLib.PaddingAES.pkcs7;


  /// generate Base64 encoded IV
  static String aesCreateIV() {
    var cryptKey = steelLib.CryptKey();
    return cryptKey.genDart(len: 16);
  }


  static Future<List<int>> encryptKey(
      List<int> inputBytes, List<int> pwdBytes) async {
      return await _encryptKey(inputBytes, pwdBytes);
  }


  static Future<List<int>> decryptKey(
      List<int> secretBoxConcatenation, List<int> pwdBytes) async {
      return await _decryptKey(secretBoxConcatenation, pwdBytes);
  }


  static Future<String?> encryptMessage({
    required String message,
    required ecLib.SimpleKeyPair keyPairX,
    required ecLib.SimplePublicKey remotePublicKeyX
  }) async {

    String ivB64 = aesCreateIV();
    String? encMsg = await _aesEncryptSteelWithEc(
        keyPairX: keyPairX,
        remotePublicKeyX: remotePublicKeyX,
        ivB64: ivB64,
        message: message
    );
    return "$ivB64:$encMsg";
  }


  static Future<String?> decryptMessage({
    required String message,
    required ecLib.SimpleKeyPair keyPairX,
    required ecLib.SimplePublicKey remotePublicKeyX
  }) async {
    final _splitted = message.split(":");
    var decrypted =  await _aesDecryptSteelWithEc(
        keyPairX: keyPairX,
        remotePublicKeyX: remotePublicKeyX,
        ivB64: _splitted.first,
        message: _splitted.last
    );
    return decrypted;
  }


  //////////////////////////////////////////////////////////////////////////////
  /// AES STEEL CRYPT
  //////////////////////////////////////////////////////////////////////////////

  static Future<String?> _aesEncryptSteelWithEc({
    required ecLib.SimpleKeyPair keyPairX,
    required ecLib.SimplePublicKey remotePublicKeyX,
    required String ivB64,
    required String message
  }) async {

    ecLib.SecretKey secretKey =
    await MyEcCrypt.sharedSecretWithPubKey(keyPairX, remotePublicKeyX);

    String? messageEnc = _aesEncryptSteelWithPwd(
        base64Encode(await secretKey.extractBytes()), ivB64, message);

    return messageEnc;
  }


  /// decrypt with keyPairX, PublicKeyX and IV
  static Future<String?> _aesDecryptSteelWithEc({
      required ecLib.SimpleKeyPair keyPairX,
      required ecLib.SimplePublicKey remotePublicKeyX,
      required String ivB64,
      required String message}) async {

    ecLib.SecretKey secretKey =
        await MyEcCrypt.sharedSecretWithPubKey(keyPairX, remotePublicKeyX);

    return _aesDecryptSteelWithPwd(
        base64Encode(await secretKey.extractBytes()), ivB64, message);
  }

  /// encrypt string
  static String? _aesEncryptSteelWithPwd(
      String key32B64, String ivB64, String message) {
    steelLib.AesCrypt aesCrypt =
        steelLib.AesCrypt(key: key32B64, padding: _padding);
    final satellite = aesCrypt.ctr;
    return satellite.encrypt(inp: message, iv: ivB64);
  }

  static String? _aesDecryptSteelWithPwd(
      String key32B64, String ivB64, String encInput) {

    Log.s(_TAG, "aesDecryptSteelWithPwd; key32B64: $key32B64");
    steelLib.AesCrypt aesCrypt =
        steelLib.AesCrypt(key: key32B64, padding: _padding);
    final satellite = aesCrypt.ctr;

    String? decrypted;
    try {
      decrypted = satellite.decrypt(enc: encInput, iv: ivB64);
    }
    catch(e) {
      Log.w(_TAG, "_aesDecryptSteelWithPwd: error: $e");
    }

    return decrypted!;
  }

//////////////////////////////////////////////////////////////////////////////
  /// AES CRYPTOGRAPHY
//////////////////////////////////////////////////////////////////////////////

  static final algorithm = ecLib.AesCtr.with256bits(
    macAlgorithm: ecLib.Hmac.sha256(),
  );


  static Future<List<int>> _encryptKey(
      List<int> inputBytes, List<int> pwdBytes) async {

    // AES-CBC with 256 bit keys and HMAC-SHA256 authentication.
    final algorithm = ecLib.AesCbc.with256bits(
      macAlgorithm: ecLib.Hmac.sha256(),
    );

    Log.s(_TAG, "encryptKey; pwd: ${String.fromCharCodes(pwdBytes)}");

    /// hash pwd
    final pwdHashAlgorithm = ecLib.Sha256();
    final pwdHash = await pwdHashAlgorithm.hash(pwdBytes);
    final secretKey = await algorithm.newSecretKeyFromBytes(pwdHash.bytes);

    Log.s(_TAG,
        "encryptKey; secretKey: ${base64Encode(await secretKey.extractBytes())}");

    final nonce = algorithm.newNonce();

    // Encrypt
    final ecLib.SecretBox secretBox =
        await algorithm.encrypt(inputBytes, secretKey: secretKey, nonce: nonce);

    Log.s(_TAG, "encryptKey; nonce: ${secretBox.nonce}");
    Log.s(_TAG, "encryptKey; MAC: ${secretBox.mac.bytes}");
    Log.s(_TAG, "encryptKey; cipherText: ${secretBox.cipherText}");

    /// lib Concatenation does't work, so we create our one
    List<int> secretBoxConcatenation = [];
    secretBoxConcatenation.addAll(secretBox.nonce);
    secretBoxConcatenation.addAll(secretBox.mac.bytes);
    secretBoxConcatenation.addAll(secretBox.cipherText);

    Log.s(_TAG,
        "encryptKey; secretBoxConcatenation: ${base64Encode(secretBoxConcatenation)}");

    String box = base64Encode(secretBoxConcatenation);
    Log.s(_TAG, "encryptKey; box: $box");

    String pwd = base64Encode(pwdBytes);
    Log.s(_TAG, "encryptKey; pwd: $pwd");

    secretBoxConcatenation = base64Decode(box);
    Log.s(_TAG, "encryptKey; secretBoxConcatenation: $secretBoxConcatenation");

    pwdBytes = base64Decode(pwd);
    Log.s(_TAG, "encryptKey; pwdBytes: $pwdBytes");

    // await decryptKey(secretBoxConcatenation, pwdBytes);

    Log.s(_TAG, "encryptKey; finish -> return secretBoxConcatenation");
    return secretBoxConcatenation;
  }


  /// decrypt string
  static Future<List<int>> _decryptKey(
      List<int> secretBoxConcatenation, List<int> pwdBytes) async {

    // AES-CBC with 256 bit keys and HMAC-SHA256 authentication.
    final algorithm = ecLib.AesCbc.with256bits(
      macAlgorithm: ecLib.Hmac.sha256(),
    );

    /// hash pwd
    final pwdHashAlgorithm = ecLib.Sha256();
    final pwdHash = await pwdHashAlgorithm.hash(pwdBytes);
    final secretKey = await algorithm.newSecretKeyFromBytes(pwdHash.bytes);

    Log.s(_TAG, "decryptKey; pwd: ${String.fromCharCodes(pwdBytes)}");
    Log.s(_TAG,
        "decryptKey; secretBoxConcatenation: ${base64Encode(secretBoxConcatenation)}");
    Log.s(_TAG,
        "decryptKey; secretKey: ${base64Encode(await secretKey.extractBytes())}");

    /// lib Concatenation does't work, so we create our one
    /// nonce(16byte) + mac(32byte) + message
    List<int> nonce = []..addAll(secretBoxConcatenation.getRange(0, 16));
    Log.s(_TAG, "decryptKey; nonce: ${nonce}; (${nonce.length})");

    List<int> macList = []
      ..addAll(secretBoxConcatenation.getRange(16, 16 + 32));
    var mac = ecLib.Mac(macList);
    Log.s(_TAG, "decryptKey; mac: ${mac.bytes}; ${mac.bytes.length})");

    List<int> ciperText = []..addAll(secretBoxConcatenation.sublist(32 + 16));
    Log.s(_TAG,
        "decryptKey; ciperText: ${ciperText.length}; ${ciperText.length})");

    ecLib.SecretBox secretBox =
        ecLib.SecretBox(ciperText, nonce: nonce, mac: mac);

    final clearText = await algorithm.decrypt(
      secretBox,
      secretKey: secretKey,
    );

    Log.s(_TAG, "decryptKey; cleartext: $clearText");
    Log.s(_TAG, "decryptKey; cleartext B64: ${base64Encode(clearText)}");

    return clearText;
  }
}
