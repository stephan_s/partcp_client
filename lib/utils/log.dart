// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/foundation.dart';

class Log {
  static const bool enableLogD = true;
  static const bool enableLogI = enableLogD;
  static const bool enableLogW = true;
  static const bool enableLogE = true;

  static const bool enableLogS = false;

  static const _ansiEsc = '\x1B[';
  static const _ansiReset = '\x1b[0m';

  static void spacer () {
    if (enableLogD)  print("");
  }

  static void _print(String? color, String msg) {
    // print('$_ansiEsc$msg$_ansiReset');
    if (color != null) {
      debugPrint('$_ansiEsc$color$msg$_ansiReset');
    }
    else {
      debugPrint(msg);
    }

  }

  /// decrypted stuff, for tests only
  static void s (String tag, dynamic msg) {
    var color = '30m';
    if (enableLogS)  _print(color, '$tag:  $msg');
    // if (enableLogS) logConsoleController.log("[$tag] $msg", level: DebugLogLevel.info);
  }


  static void d (String tag, dynamic msg) {
    if (enableLogD)  _print(null, '$tag:  $msg');
    // if (enableLogD) logConsoleController.log("[$tag] $msg", level: DebugLogLevel.info);
  }

  static void i (String tag, dynamic msg) {
    var color = '34m';
    if (enableLogI) _print(color, "$tag:  $msg");
    // if (enableLogI) logConsoleController.log("[$tag] $msg", level: DebugLogLevel.info);
  }

  static void e (String tag, dynamic msg) {
    var color = '31m';
    if (enableLogE) _print(color, "$tag [ERROR]: $msg");
    // if (enableLogE) logConsoleController.log("[$tag] $msg", level: DebugLogLevel.error);
  }

  static void w (String tag, dynamic msg) {
    var color = '95m';
    if (enableLogW) _print(color, "$tag [WARNING]:  $msg");
    // if (enableLogW) logConsoleController.log("[$tag] $msg", level: DebugLogLevel.warning);
  }



}