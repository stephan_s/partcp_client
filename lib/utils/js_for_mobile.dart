// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

/*
  Fake class to allow load JS lib in mobile app
 */

external Object get context;

class Object {
  external dynamic callMethod(dynamic method, [List args]);
}