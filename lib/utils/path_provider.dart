import 'package:get/get.dart';
import 'package:partcp_client/constants.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;

import 'log.dart';

class PathProvider {
  final _TAG = "PathProvider";

  String? appDataPath = null;
  Future<String?> getAppDataPath() async {
    try {
      /// Linux
      if (GetPlatform.isDesktop && GetPlatform.isLinux) {
        String applicationSupportPath = (await pathProvider.getApplicationSupportDirectory()).path;
        appDataPath = "${applicationSupportPath.split(".").first}.${Constants.APP_DIR_NAME}";
      }

      /// Windows
      else if (GetPlatform.isDesktop && GetPlatform.isWindows) {
        String applicationSupportPath = (await pathProvider.getApplicationSupportDirectory()).path;
        // appDataPath = "";
      }

      /// Android
      else if (GetPlatform.isAndroid) {
        // appDataPath = "";
      }
    } catch (e) {
      Log.e(_TAG, "initController error: $e");
    }
    Log.d(_TAG, "initController() -> appDataPath: $appDataPath");

    return appDataPath;
  }
}
