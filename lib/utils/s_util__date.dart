// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'package:partcp_client/internationalization.dart';
// import 'package:partcp_client/lang_enum.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class SUtilDate {

	static final String FILE_DATE_FROMAT = "yyMMdd-HHmm-ssSSS";

	/// return 01:30 or 01:30:20
	static String durationTime(int durationInMs) {
		Duration duration = new Duration(milliseconds: durationInMs);
		String twoDigits(int n) => n.toString().padLeft(2, "0");
		String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
		String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
		if (duration.inHours > 0) {
			return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
		}
		else {
			return "$twoDigitMinutes:$twoDigitSeconds";
		}
	}

	/// return 1.3.20 (de_DE) or 3/1/20 (en_US)
	static String shortDate({required DateTime dateTime, required BuildContext context}) {
		String mDate =  DateFormat("yMd", Localizations.localeOf(context).toString()).format(dateTime);
		return _createShortYear(dateTime, mDate);
	}


	/// return 13:05 (de_DE) or 1:05 PM (en_US)
	static String shortTime({required DateTime dateTime, required BuildContext context}) {
		return  DateFormat("jm", Localizations.localeOf(context).toString()).format(dateTime);
	}

	///     1.3.20 13:05 (de_DE), 3/1/20 1:05 PM (en_US)
	/// or  1.3 13:05 (de_DE) 3/1 1:05 PM when year is current year
	static String shortDateTime({required DateTime dateTime, required BuildContext context, required bool removeYearIfCurrentYear}) {
		if (removeYearIfCurrentYear) {
			final dateNow = new DateTime.now();
			if (dateNow.year == dateTime.year) {
				return DateFormat("Md", Localizations.localeOf(context).toString()).add_jm().format(dateTime);
			}
		}
		String mDate =  DateFormat("yMd", Localizations.localeOf(context).toString()).add_jm().format(dateTime);
		return _createShortYear(dateTime, mDate);
	}

	// static String dateTimeForAppBar(DateTime dateTime, String locale) {
	// 	final DateTime dateNow = new DateTime.now();
	// 	if (dateNow.day == dateTime.day) {
	// 		return Strings().valueOf(LangEnum.today) + ", " + shortTime(dateTime, locale);
	// 	}
	// 	if (dateNow.difference(dateTime).inDays == 1) {
	// 		return Strings().valueOf(LangEnum.yesterday) + ", " + shortTime(dateTime, locale);
	// 	}
	// 	if (dateNow.difference(dateTime).inDays < 7) {
	// 		return DateFormat("EEE", locale).format(dateTime) + ", " + shortTime(dateTime, locale);
	// 	}
	// 	return shortDateTime(dateTime, locale, true);
	// }

	// static String dateForChatDateSeparator(DateTime dateTime, String locale) {
	// 	final DateTime dateNow = new DateTime.now();
	// 	dateTime = new DateTime(dateTime.year, dateTime.month, dateTime.day, 0, 0);
	// 	int _dateDiff = dateDiff(dateTime, dateNow);
	//
	// 	/// today
	// 	if (dateNow.day == dateTime.day && dateNow.month == dateTime.month && dateNow.year == dateTime.year) {
	// 	// if (dateNow.difference(dateTime).inDays == 0) {
	// 		return Strings().valueOf(LangEnum.today);
	// 	}
	//
	// 	/// yesterday
	// 	// if (dateNow.difference(dateTime).inDays == 1) {
	// 	if (_dateDiff == 1) {
	// 		return Strings().valueOf(LangEnum.yesterday);
	// 	}
	//
	// 	/// Sun., 3. Mai
	// 	if (_dateDiff < 7) {
	// 		return DateFormat("EEE", locale).format(dateTime) + ", " + DateFormat.MMMMd(locale).format(dateTime);
	// 	}
	//
	// 	/// 3. Mai
	// 	if (dateNow.year == dateTime.year) {
	// 		return DateFormat.MMMMd(locale).format(dateTime);
	// 	}
	//
	// 	/// 3. Mai 2020
	// 	return DateFormat.yMMMMd(locale).format(dateTime);
	// }


	static int dayOfYear (DateTime date) {
		final now = DateTime.now();
		final diff = now.difference(new DateTime(date.year, 1, 1, 0, 0));
		final diffInDays = diff.inDays;
		return diffInDays;
	}

	static int dateDiff (DateTime oldDate, DateTime newDate) {
		final DateTime oldDateD = new DateTime(oldDate.year, oldDate.month, oldDate.day, 0, 0);
		final DateTime newDateD = new DateTime(newDate.year, newDate.month, newDate.day, 0, 0);
		int dateDiff = newDateD.difference(oldDateD).inDays;
		return dateDiff;
	}

	/// return xx from xxxx year
	static String _createShortYear (DateTime dateTime, String dateTimeFormatted) {
		return dateTimeFormatted.replaceAll(DateFormat("y").format(dateTime), DateFormat("yy").format(dateTime));
	}

	static String dateBasedFileName (DateTime dateTime) {
		return DateFormat(FILE_DATE_FROMAT).format(dateTime);
	}

}