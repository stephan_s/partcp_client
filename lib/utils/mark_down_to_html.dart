// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:markdown/markdown.dart' as md;

/// fix for new line
String markdownToHtml(String markdownText) {
  final _TAG = "markdownToHtml";

  final converted = markdownText;

  final String htmlContent = md.markdownToHtml(converted,
      // inlineSyntaxes: [new md.InlineHtmlSyntax()]);
      // inlineSyntaxes: [new md.LineBreakSyntax()]);
      blockSyntaxes: [new md.BlockTagBlockHtmlSyntax()]
  );

  return htmlContent;
}