// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {

	static Future<String> getNewStanzaId() async {
		SharedPreferences prefs = await SharedPreferences.getInstance();
		int stanzaId = (prefs.getInt('stanzaId') ?? 0) + 1;
		await prefs.setInt('stanzaId', stanzaId);
		return stanzaId.toString();
	}
}