// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:get/get_utils/src/platform/platform.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/utils/permissions.dart';
import 'package:path_provider/path_provider.dart';

import '../constants.dart';
import 'file_action.dart';
import 'log.dart';

class SaveFile {
  static const _TAG = "SaveFile";

  final bool saveInExternalStorage;
  final String fileName;
  final String? subDir;
  final String content;

  String? filePathLazy; /// exists after file was saved

  SaveFile({
    required this.saveInExternalStorage,
    required this.content,
    required this.fileName,
    required this.subDir,
  });

  Future<void> save() async {
    filePathLazy = await _filePath;
    File file = File(filePathLazy!);

    Log.d(_TAG, "save; filePathLazy: $filePathLazy");

    /// WEB
    if (GetPlatform.isWeb) {
      Log.d(_TAG, "Device is WEB");
      Log.d(_TAG, "file.path: ${file.path}");
      await FileActions.downloadFileAction(
          file.path, content);
    }

    /// DESKTOP
    else if(GetPlatform.isDesktop) {
      // String? selectedDirectory = await FilePicker.platform.getDirectoryPath();

      final directory = await getApplicationDocumentsDirectory();
      final path = directory.path;

      String? outputFile = await FilePicker.platform.saveFile(
        initialDirectory: path,
        dialogTitle: 'Please select an output file:',
        fileName: fileName,
      );

      if (outputFile != null) {
        File file = File(outputFile);
        file.writeAsString(content);
      }
    }

    /// MOBILE
    else {
      Log.d(_TAG, "Device is MOBILE");
      Log.d(_TAG, "file.path: ${file.path}");
      Log.d(_TAG, "saveInExternalStorage: $saveInExternalStorage");

      if (saveInExternalStorage) {
        if (await Permissions.requestPermission(
            Permissions.group(MyPermissionGroups.ExternalStorage))) {
          Log.d(_TAG, "Write file ...");
          await file.writeAsString(content);
          Log.d(_TAG, "Write file: OK");
        }
      }
      else {
        await file.writeAsString(content);
      }
    }
  }

  Future<String> get _filePath async {

    String _fileName = Constants.fileNameRegExp(fileName);

    String? _subDir;
    if (subDir != null) {
      _subDir = Constants.fileDirRegExp(subDir!);
    }

    /// on unsupported platforms [storageDir] is null
    String? storageDir;

    if (GetPlatform.isMobile && !MainController.to.isDesktopOrWeb) {
      storageDir = saveInExternalStorage
        ? (await Constants.appDirExternal)!.path
        : (await Constants.appDirInternal)!.path;
    }

    if (storageDir != null) {
      final finalDir = _subDir != null && _subDir.isNotEmpty
          ? storageDir + "/" + _subDir
          : storageDir;

      Directory appDir = Directory(finalDir);
      appDir.createSync(recursive: true);
      String filePath = "${appDir.path}/$_fileName";

      Log.d(_TAG, "getFilePath: $filePath");
      return filePath;
    }
    else {
      Log.d(_TAG, "getFilePath: $_fileName");
      return _fileName;
    }
  }
}




