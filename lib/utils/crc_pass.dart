// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:crclib/catalog.dart';

import 'log.dart';

bool crcPass({
  required String text,
  required int finalLength,
  required int crcLength,
}) {
  final _TAG = "crcPass";
  //text = text.replaceAll(new RegExp(r'[^0-9]'),'');

  Log.d(_TAG, "text: $text (text length: ${text.length}, id length: ${finalLength})");
  if (text.length == 0 || finalLength == 0) {
    return false;
  }


  bool crcPass = false;
  if (text.length >= finalLength - 1) {
    final crc = Crc32();
    final data = utf8.encode(text.substring(0, finalLength - 1));
    BigInt crcLong = crc.convert(data).toBigInt();
    Log.d(_TAG, "crcLong: $crcLong");

    /// we take the second digi
    var crcList = crcLong.toString().split("");
    Log.d(_TAG, "crcMap: $crcList");

    var crcVal = int.parse(crcList[crcLong
        .toString()
        .length - crcLength]).toString();
    Log.d(_TAG, "crcVal: $crcVal");


    if (text.length == finalLength) {
      final String lastDigit = text.substring(finalLength - crcLength);
      Log.d(_TAG, "lastDigit: $lastDigit");
      if (lastDigit == crcVal) {
        Log.d(_TAG, "crcVal: true");
        crcPass = true;
      }
    }
  }
  return crcPass;
}