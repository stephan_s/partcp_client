// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:math';

import 'package:flutter/cupertino.dart';

class SUtilConvert {
	static const String _TAG = "SUtilConvert - ";

	static dynamic convertToInt(dynamic inData, dynamic defaultOnFail) {
		if (inData is int) {
			return inData;
		}
		if (inData is String) {
			try {
				final test = int.parse(inData);
				return test;
			}
			catch(e) {
				debugPrint(_TAG + "convertToInt; inData:$inData; error:$e");
			}
		}
		return defaultOnFail;
	}

	static bool isNumeric(String? s) {
		if (s == null) {
			return false;
		}
		return int.tryParse(s) != null;
	}

	static double decimalRound(double input, {int places = 3}) {
		double mod = pow(10.0, places).toDouble();
		return ((input * mod).round().toDouble() / mod);
	}


	static String byteToString (int byte) {
		double kByte = byte/1000;
		int places = 1;

		String byteString;
		if (kByte < 100) {
			//byteString = "${decimalRound(kByte, places: places).toString()} ${Strings().valueOf(LangEnum.kb)}";
			byteString = "${decimalRound(kByte, places: places).toString()} KB}";
		}
		else {
			//byteString = "${decimalRound(kByte/1000, places: places).toString()} ${Strings().valueOf(LangEnum.mb)}";
			byteString = "${decimalRound(kByte/1000, places: places).toString()} MB}";
		}
		return byteString;
	}


}