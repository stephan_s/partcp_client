// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

typedef TransferableOnFinishCallback = void Function();
typedef TransferableOnErrorCallback = void Function(int status, String errorMessage);
typedef TransferableProgressCallback = void Function(int progress);
typedef TransferableOnResponseCallback = void Function(dynamic response);
