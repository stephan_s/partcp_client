// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:hive/hive.dart';
import 'package:partcp_client/controller/main_controller.dart';

import '../../utils/log.dart';

enum AppBoxEnum {
  lastWallet,
}

class AppBox {
  static const _TAG = "AppBox";
  late Box _box;


  Future<void> setValue(AppBoxEnum key, dynamic value) async {
    await _box.put(key.toString(), value);
  }

  dynamic getValue(AppBoxEnum key) {
    return _box.get(key.toString());
  }

  Future<void> initBox(String boxName) async{
    Log.d(_TAG, "initBox; boxName: $boxName");
    _box = await Hive.openBox(boxName, path: MainController.to.appDataPath);
  }

  Future<void> _setDefault(AppBoxEnum key, dynamic value) async{
    if (getValue(key) == null) {
      await setValue(key, value);
    }
  }
}