// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:hive/hive.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';

import '../../utils/log.dart';

enum UserBoxEnum {
  moderatorLastGroupId,
  userLastServer,
  wrongLotCodes,
  moderator,
  eventParticipantBoxes,
  eventLotCodeBoxes,
}

class UserBox {
  static const _TAG = "MyBox";
  late Box _box;
  
  
  Future<void> setValue(UserBoxEnum key, dynamic value) async {
    await _box.put(key.toString(), value);
  }

  dynamic getValue(UserBoxEnum key, {dynamic ifNotExists}) {
    var result;
    var test = _box.get(key.toString());
    if (test == null && ifNotExists != null) {
      result = ifNotExists;
    }
    else {
      result = test;
    }
    Log.d(_TAG, "getValue: $key, {ifNotExists: $ifNotExists} => $result");
    return result;
  }

  Future<void> setLastGroupIdByServer(VotingServer server, Group group) async {
    await _box.put(server.url, group.id);
  }
  String getLastGroupIdByServer(VotingServer server) {
    var test = _box.get(server.url);
    return test == null
        ? ""
        : test;
  }


  Future<void> initBox(ecLib.SimplePublicKey publicKeyX, List<int> pwd) async{
    String boxId = MyEcCrypt.hashFromPublicKeyX(publicKeyX);
    String boxName = "${MainController.USER_BOX_NAME}-$boxId";
    Log.d(_TAG, "initBox; boxName: $boxName");

    _box = await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(pwd), path: MainController.to.appDataPath);
    await _setDefault(UserBoxEnum.moderatorLastGroupId, "");
  }

  Future<void> _setDefault(UserBoxEnum key, dynamic value) async{
    if (getValue(key) == null) {
      await setValue(key, value);
    }
  }
}