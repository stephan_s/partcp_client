// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/message_type/voting_definition.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/objects/voting_admin.dart';
import 'package:partcp_client/objects/client_data.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_result.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/voting_type.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/utils/yaml_converter.dart';
import 'package:steel_crypt/steel_crypt.dart';

import 'package:collection/collection.dart';

import 'defaults.dart';

class Voting {
  static const _TAG = "Voting";

  static Voting copy(Voting v, {String? id, String? name}) {
    var newV = new Voting()
      ..id = id == null ? v.id : id
      ..name = name == null ? v.name : name
      ..title = v.title
      ..status = v.status
      ..clientData = v.clientData
      ..division = v.division
      ..votingOptions = v.votingOptions
      ..admins = v.admins
      ..createdOn = v.createdOn
      ..modifiedOn = v.modifiedOn
      ..createdBy = v.createdBy
      ..modifiedBy = v.modifiedBy
      ..type = v.type
      ..votes = v.votes
      ..periodStart = v.periodStart
      ..periodEnd = v.periodEnd;
    return newV;
  }

  String get hash {
    var hasher = HashCrypt(algo: HashAlgo.Sha1);
    String options = "";
    votingOptions.forEach((element) => options += element.toMap().toString());
    // final data = "${clientData.toMap().toString()}"
    final data = "${clientData.toYaml()}"
        "$name"
        "$title"
        "$type"
        // "${clientData.toMap().toString()}"
        "${clientData.toYaml()}"
        "$options"
        "${periodStart.toString()}"
        "${periodEnd.toString()}"
    ;
    Log.d(_TAG, "hash: $data");
    return hasher.hash(inp: data);
  }

  String? id;
  String name = "";
  String title = "";
  String status = "";


  ClientData clientData = ClientData();
  String division = "";

  List<VotingOption> votingOptions = [];
  List<VotingAdmin> admins = [];
  late VotingResult votingResult;

  DateTime? createdOn;
  DateTime? modifiedOn;
  String createdBy = "";
  String modifiedBy = "";

  DateTime? periodStart;
  DateTime? periodEnd;

  String? type;

  List<Vote> votes = [];

  String get typeTitle => VotingType.title(type);

  String get statusText {
    if (status == STATUS_OPEN) {
      return "aktiv";
    }
    else if (status == STATUS_CLOSED) {
      return "beendet";
    }
    else if (status == STATUS_FINISHED) {
      return "ausgezählt";
    }
    else {
      return "vorbereitet";
    }
  }


  Icon get statusIcon {
    IconData data;
    Color color;

    if (status == STATUS_OPEN) {
      data = Icons.stop_circle_outlined;
      color = Colors.red;
    }

    /// closed and waiting for generating voting stat
    else if (status == STATUS_CLOSED) {
      data = Icons.check_circle_outline;
      color = Colors.green;
    }

    /// closed and waiting for generating voting stat
    else if (status == STATUS_FINISHED) {
      data = Icons.insert_chart_outlined;
      color = Colors.green;
    }

    /// IDLE
    else if (status == STATUS_IDLE) {
      data = Icons.play_circle_fill;
      color = Colors.black45;
    }

    /// unknown
    else {
      data = Icons.device_unknown;
      color = Colors.black45;
    }

    return Icon(
      data,
      color: color,
    );
  }

  static Voting fromMap(Map<String, dynamic> map) {
    Voting voting = Voting()
      ..id = SUtils.mapValue(map, ID, "").trim()
      ..name = SUtils.mapValue(map, NAME, "")
      ..title = SUtils.mapValue(map, TITLE, "n.a")
      ..status = SUtils.mapValue(map, STATUS, "n.a")
      ..type = SUtils.mapValue(map, TYPE, "n.a")
      // ..clientData = ClientData.fromMap(map[ClientData.CLIENT_DATA])
      ..clientData = ClientData.fromYaml(map[ClientData.CLIENT_DATA])
      ..createdBy = SUtils.mapValue(map, "created_by", "n.a")
      ..modifiedBy = SUtils.mapValue(map, "modified_by", "n.a")
      ..votingOptions = []
      ..periodStart = SUtils.mapParseDateTime(map, PERIOD_START, null)
      ..periodEnd = SUtils.mapParseDateTime(map, PERIOD_END, null)
      ..createdOn = SUtils.mapParseDateTime(map, "created_on", null)
      ..modifiedOn = SUtils.mapParseDateTime(map, "modified_on", null)
      ..votingResult = VotingResult.fromMap(SUtils.mapValue(map, "voting_result", defaultMap))
    ;

    /// OPTIONS
    /// server can response not all options for a voting
    if (map.containsKey(VotingOption.OPTIONS)) {
      if (map[VotingOption.OPTIONS] is List<dynamic>) {
        List<dynamic> optionsList = map[VotingOption.OPTIONS];
        for (var i = 0; i < optionsList.length; i++) {
          try {
            voting.votingOptions.add(VotingOption.fromMap(optionsList[i]));
          } catch (e, s) {
            Log.e(_TAG, "Voting fromMap -> create votingOptions -> error: $e: Stack: $s");
          }
        }
      }
    }

    /// ADMINS
    if (map.containsKey(VotingAdmin.ADMINS)) {
      if (map[VotingAdmin.ADMINS] is List<dynamic>) {
        List<dynamic> adminList = map[VotingAdmin.ADMINS];
        for (var i = 0; i < adminList.length; i++) {
          try {
            voting.admins.add(VotingAdmin(adminList[i]));
          } catch (e) {
            Log.e(_TAG, "Voting fromMap -> create votingOptions -> error: $e");
          }
        }
      }
    }

    return voting;
  }

  /// DETAILS REQUEST
  Future<ServerResponse> sVotingDetailRequest(VotingServer server, Account account, VotingEvent event) async {
    final Map<String, dynamic> messageMap = votingDetailsRequest(event: event, voting: this);

    ServerResponse serverResponse = await HttpController().serverRequest(messageMap, account, server.url);

    if (serverResponse.statusCode == 200 && serverResponse.bodyMap!.containsKey(VOTING_DATA)) {
      serverResponse.object = Voting.fromMap(serverResponse.bodyMap![VOTING_DATA]);
    }
    return serverResponse;
  }

  /// LOAD FROM STRING
  static Voting? fromYamlString(String votingYaml) {
    try {
      Map<String, dynamic> votingMap = YamlConverter().toMap(votingYaml);

      if (votingMap.containsKey(VOTING_DATA)) {
        Voting _voting = Voting.fromMap(votingMap[VOTING_DATA]);
        _voting.id = SUtils.mapValue(votingMap, "Voting-Id", null);
        if (_voting.id != null) {
          _voting.id!.trim();
        }
        return _voting;
      }
    } catch (e, s) {
      Log.e(_TAG, "createFromYamlString; error: $e; stack:$s");
    }
    return null;
  }

  /// NEW OR UPDATE
  Future<ServerResponse> sendDefinitionOrUpdateRequest(VotingServer server, Account account, VotingEvent event) async {
    final Map<String, dynamic> messageMap =
        id == null ? votingDefinition(event: event, voting: this) : votingUpdateRequest(event: event, voting: this);

    ServerResponse serverResponse = await HttpController().serverRequestSigned(account, messageMap, server);
    return serverResponse;
  }

  /// SEND STATUS CHANGE REQUEST
  Future<ServerResponse> sendStatusChangeRequest(VotingServer server, VotingEvent event, Account account) async {
    Map<String, dynamic>? messageMap;

    if (status == STATUS_IDLE) {
      messageMap = votingStartDeclaration(event: event, voting: this);
    } else if (status == STATUS_OPEN) {
      messageMap = votingEndDeclaration(event: event, voting: this);
    } else if (status == STATUS_CLOSED) {
      messageMap = voteCountRequest(event: event, voting: this);
    }

    assert(messageMap != null);
    ServerResponse serverResponse = await HttpController().serverRequestSigned(account, messageMap!, server);

    // if (status == STATUS_CLOSED) {
    //   votingResult = VotingResult.fromMap(SUtils.mapValue(serverResponse.bodyMap, "Voting-Result", defaultMap));
    // }
    return serverResponse;
  }

  Vote? vote(String id) {
    return votes.firstWhereOrNull((Vote vote) => vote.id == id);
  }

  // Future<ServerResponse>getVoteCount(VotingServer server, VotingEvent event, Account account) async {
  //   final Map<String, dynamic> messageMap = voteCountRequest(event: event, voting: this);
  //   final ServerResponse serverResponse = await HttpController().serverRequestSigned(account, messageMap, server);
  //   votingResult = VotingResult.fromMap(SUtils.mapValue(serverResponse.bodyMap, "Voting-Result", defaultMap));
  //   return serverResponse;
  // }

  /// voting for consensus, yes-no
  void setVoteValued(String id, int value) {
    Vote? vote = votes.firstWhereOrNull((element) => element.id == id);
    if (vote == null) {
      vote = Vote(id, value);
      votes.add(vote);
    } else {
      vote.value = value;
    }
  }

  /// voting for multiple-choice
  bool setVoteMulti(String id, bool selected) {
    bool success = false;
    if (selected) {
      Iterable<Vote> _votes = votes.where((element) => element.value == 1);
      Log.d(_TAG, "setVoteMulti; selected: $selected; votes: ${_votes.length};");

      if (_votes.length < VotingType.getMultipleChoiceCountFromType(type!)) {
        Vote vote = Vote(id, 1);
        votes.add(vote);
        success = true;
      } else {
        success = false;
      }
    } else {
      Vote? vote = votes.firstWhereOrNull((element) => element.id == id);
      if (vote != null) {
        votes.remove(vote);
      }
      success = true;
    }
    return success;
  }

  /// voting for single choice
  void setVoteSingle(id) {
    votes.clear();
    votes.add(Vote(id, 1));
  }

  /// List Key from [votingListRequest]
  static const VOTINGS = "Votings";

  /// List Key from [votingDetailsRequest]
  static const VOTING_DATA = "Voting-Data";

  static const ID = "id";
  static const NAME = "name";
  static const TITLE = "title";
  static const STATUS = "status";
  static const DIVISION = "division";
  static const TYPE = "type";
  static const PERIOD_START = "period_start";
  static const PERIOD_END = "period_end";

  static const STATUS_IDLE = "idle";
  static const STATUS_OPEN = "open";
  static const STATUS_CLOSED = "closed";
  static const STATUS_FINISHED = "finished";
}

enum VotingStatusEnum { open, closed, idle, all }


