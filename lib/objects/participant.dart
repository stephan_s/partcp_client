// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:hive/hive.dart';
import 'package:partcp_client/utils/crypt/my_aes_crypt.dart';
import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';
import 'package:partcp_client/utils/s_utils.dart';

import '../utils/log.dart';
import 'account.dart';

part 'participant.g.dart';


/*
  Stores participant data in Event-Box
  for generating and submitting lot-codes
 */

const _TAG = "Participant";


/////////////////////////////////////////////////
/// on changes run
///   flutter packages pub run build_runner build
/////////////////////////////////////////////////

@HiveType(typeId: 2)
class Participant {
  String get userKey => "$BOX_ITEM_ID_PREFIX$userId";
  static String userKeyById (String userId) => "$BOX_ITEM_ID_PREFIX$userId";

  @HiveField(0)
  String userId;

  @HiveField(1)
  String? participantId;

  @HiveField(2)
  String? publicKeyConcatB64;

  @HiveField(3)
  dynamic lotCodeKey;   /// Hive Box ID from corresponding lotCodes for better find the key

  @HiveField(4)
  bool lotCodeDelivered = false;

  String? lotCode;
  String? lotCodeEnc;

  ecLib.SimplePublicKey? publicKeyX;
  ecLib.SimplePublicKey? publicKeyEd;


  Participant({
    required this.userId,
    this.participantId,
    this.publicKeyConcatB64,
    // this.lotCode,
    this.lotCodeKey,
    this.lotCodeDelivered = false,
  });

  factory Participant.fromJson(String jsonData) {
    Log.d(_TAG, "fromJson: ${jsonData}");

    Map<String, dynamic> map = json.decode(jsonData);
    Participant m = Participant(
      userId: SUtils.mapValue(map, PARTICIPANT_ID, null),
      participantId: SUtils.mapValue(map, PARTICIPANT_ID, null),
      // lotCode: SUtils.mapValue(map, LOT_CODE, null),
      lotCodeKey: SUtils.mapValue(map, LOT_CODE_KEY, null),
      lotCodeDelivered: SUtils.mapValue(map, LOT_CODE_DELIVERED, false),

    );
    m.createPublicKeysFromConcat();
    return m;
  }

  String toJson() {
    Map<String, dynamic> map =  {
      USER_ID: userId,
      PARTICIPANT_ID: participantId,
      PUBLIC_KEY_CONCAT: publicKeyConcatB64,
      LOT_CODE_KEY: lotCodeKey,
      LOT_CODE_DELIVERED: lotCodeDelivered
    };
    return json.encode(map);
  }

  /// Create Keys from concat Ed and X key
  bool? createPublicKeysFromConcat() {
    if (publicKeyConcatB64 == null) return null;

    publicKeyConcatB64 = publicKeyConcatB64!.trim();
    List<int> pubKeyConcatSeed = base64.decode(publicKeyConcatB64!);
    Map<String, dynamic> keyMap = MyEcCrypt.createPublicKeysFromConcat(pubKeyConcatSeed);

    if (keyMap.containsKey("X") && keyMap.containsKey("Ed")) {
      publicKeyX =  keyMap["X"];
      publicKeyEd = keyMap["Ed"];
    }
    else {
      publicKeyX =  null;
      publicKeyEd = null;
    }
    return publicKeyX != null && publicKeyEd != null;
  }

  Future<void>encryptLotCode(Account account) async {
    if (publicKeyX == null) {
      createPublicKeysFromConcat();
    }
    lotCodeEnc = await MyAesCrypt.encryptMessage(
      message: lotCode!,
      keyPairX: account.keyPairX!,
      remotePublicKeyX: publicKeyX!
    );
  }

  Future<void>decryptLotCode(Account account) async {
    lotCodeEnc = await MyAesCrypt.decryptMessage(
        message: lotCode!,
        keyPairX: account.keyPairX!,
        remotePublicKeyX: publicKeyX!
    );
  }

  static const BOX_ITEM_ID_PREFIX = "ID-";

  static const USER_ID = "id";
  static const PARTICIPANT_ID = "p";
  static const PUBLIC_KEY_CONCAT = "k";
  static const LOT_CODE = "l";
  static const LOT_CODE_ENC = "l~";
  static const LOT_CODE_DELIVERED = "d";
  static const LOT_CODE_KEY = "lk";   /// Hive Box ID from corresponding lotCodes for better find the key




}