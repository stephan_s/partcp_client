// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/objects/credential.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/ws_server.dart';
import 'package:partcp_client/utils/crypt/my_aes_crypt.dart';
import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/utils/yaml_converter.dart';

import '../../constants.dart';
import 'participant_data.dart';


const _TAG = "Account";

/// we create for each votingEvent one account
enum Mode {
  voting,
  wallet
}

class Account {

  Account (Mode mode) {
    this.mode = mode;
  }

  String? userId;
  String? userIdEnc;

  String name = "";
  Mode mode = Mode.voting;

  String get title {
    if (name.isNotEmpty) {
      return name;
    }
    else {
      return userId != null
          ? userId!
          : "";
    }
  }

  /// for registration on voting server
  String? credential;

  String? privateKeyXB64Enc;
  String? privateKeyEdB64Enc;

  ecLib.SimpleKeyPair? keyPairX;
  ecLib.SimpleKeyPair? keyPairEd;

  bool? pubKeysOnServerValid;

  String? votingEventId;
  String? lotCodeEnc;

  /// list of [Voted] to find past votings
  List<dynamic> votedList = [];

  DateTime? dateC;
  DateTime? dateU;


  String lotCodesSubDir(VotingEvent event) {
    return "${Constants.LOT_CODES_DIR_NAME}/${event.id}";
  }


  Future<String> get publicKeysConcat async => MyEcCrypt.concatPublicKeys(
    pubKeyEd: await publicKeyEd,
    pubKeyX: await publicKeyX,
  );

  Future<ecLib.SimplePublicKey> get publicKeyX async => await keyPairX!.extractPublicKey();
  Future<ecLib.SimplePublicKey> get publicKeyEd async => await keyPairEd!.extractPublicKey();

  Future<String> get walledId async => MyEcCrypt.hashFromPublicKeyX(await publicKeyX);


  /// CREATE NEW KEYS
  Future<bool> createNewKeys() async{
    try {
      Log.d(_TAG, "createNewKeys ...");
      keyPairX = await MyEcCrypt.createKeyPairX();
      keyPairEd = await MyEcCrypt.createKeyPairEd();

      Log.d(_TAG, "createNewKeys OK");
      Log.s(_TAG, "createNewKeys PrivateKeyXBytes: ${await keyPairX!.extractPrivateKeyBytes()}");

      dateC = DateTime.now();
      dateU = DateTime.now();
      if (keyPairX == null || keyPairEd == null) {
        return false;
      }
    }
    catch(e,s) {
      Log.e(_TAG, "createNewKeys error: $e, stack: $s");
      return false;
    }
    return true;
  }

  /// ENCRYPT KEYS
  /// don't use this directly
  /// this method will be called in [Wallet.addAccount]
  Future<bool> encryptKeys(List<int> pwdBytes) async {
    try {
      final List<int> privKeyXEnc = await MyAesCrypt.encryptKey(
          await keyPairX!.extractPrivateKeyBytes(),
          pwdBytes
      );
      privateKeyXB64Enc = base64Encode(privKeyXEnc);
      Log.d(_TAG, "encryptKeys: X: $privateKeyXB64Enc");

      final List<int> privKeyEdEnc = await MyAesCrypt.encryptKey(
          await keyPairEd!.extractPrivateKeyBytes(),
          pwdBytes
      );
      privateKeyEdB64Enc = base64Encode(privKeyEdEnc);
      Log.d(_TAG, "encryptKeys: Ed: $privateKeyEdB64Enc");
      Log.d(_TAG, "encryptKeys: OK -> return true");
      return true;
    }
    catch (e, s) {
      Log.e(_TAG, "encryptKeys; error:$e; stack: $s");
      return false;
    }
  }


  /// DECRYPT KEYS
  Future<bool> decryptKeys(List<int> pwdBytes) async {
    if (privateKeyXB64Enc != null) {
      try {
        final List<int> privKeyXSeed = await MyAesCrypt.decryptKey(
            base64Decode(privateKeyXB64Enc!),
            pwdBytes
        );
        keyPairX = await MyEcCrypt.createKeyPairXFromSeed(privKeyXSeed);

        final List<int> privKeyEdSeed = await MyAesCrypt.decryptKey(
            base64Decode(privateKeyEdB64Enc!),
            pwdBytes
        );
        keyPairEd = await MyEcCrypt.createKeyPairEdFromSeed(privKeyEdSeed);

        Log.s(_TAG, "decryptKeys : privateKeyX: ${await keyPairX!.extractPrivateKeyBytes()}");
        Log.s(_TAG, "decryptKeys: privateKeyEd: ${await keyPairEd!.extractPrivateKeyBytes()}");

        Log.s(_TAG, "decryptKeys: privateKeyX B64: ${base64Encode(await keyPairX!.extractPrivateKeyBytes())}");
        Log.s(_TAG, "decryptKeys: privateKeyEd B64: ${base64Encode(await keyPairEd!.extractPrivateKeyBytes())}");
        Log.d(_TAG, "decryptKeys for $userId: OK");

        return true;
      }
      catch (e, s) {
        Log.e(_TAG, "encryptKeys; error:$e; stack: $s");
        return false;
      }
    }
    return false;
  }


  /// ENCRYPT userId
  Future<void> encryptUserId(List<int> pwdBytes) async {
    userIdEnc = base64Encode(await MyAesCrypt.encryptKey(
        utf8.encode(userId!),
        pwdBytes
    ));
  }

  /// DECRYPT USER_ID
  Future<bool> decryptUserId(List<int> pwdBytes) async {
    if (userIdEnc != null) {
      userId = utf8.decode(await MyAesCrypt.decryptKey(
          base64Decode(userIdEnc!),
          pwdBytes)
      );
    }
    return true;
  }



  /// ENCRYPT SERVER MESSAGE
  Future<String?> encryptServerMessage({
    required VotingServer server, required String message}) async {
    return await MyAesCrypt.encryptMessage(
      message: message,
      keyPairX: keyPairX!,
      remotePublicKeyX: server.publicKeyX!,
    );
  }


  /// DECRYPT SERVER MESSAGE
  Future<String?> decryptServerMessage({
    required VotingServer server, required String messageEnc}) async {

    messageEnc = messageEnc.replaceAll("\n", "");
    return await MyAesCrypt.decryptMessage(
        message: messageEnc,
        keyPairX: keyPairX!,
        remotePublicKeyX: server.publicKeyX!
    );
  }


  /// ENCRYPT WS SERVER MESSAGE
  Future<String?> encryptWsServerMessage({
    required WsServer server, required String message}) async {
    return await MyAesCrypt.encryptMessage(
      message: message,
      keyPairX: keyPairX!,
      remotePublicKeyX: server.publicKeyX!,
    );
  }


  /// DECRYPT WS SERVER MESSAGE
  Future<String?> decryptWsServerMessage({
    required WsServer server, required String messageEnc}) async {

    messageEnc = messageEnc.replaceAll("\n", "");
    return await MyAesCrypt.decryptMessage(
        message: messageEnc,
        keyPairX: keyPairX!,
        remotePublicKeyX: server.publicKeyX!
    );
  }




  Future<ServerResponse> submitIdToServer(VotingServer server) async {
    ServerResponse serverResponse = ServerResponse();

    if (credential == null) {
      serverResponse.errorMessage = "Credential nay not be null";
    }
    else {
      String? credentialEnc = await encryptServerMessage(
        message: credential!,
        server: server,
      );
      final Map<String, dynamic> messageMap = keySubmission(
        // credential: credential,
          credentialEnc: credentialEnc,
          eventId: votingEventId != null ? votingEventId : null
      );
      serverResponse = await HttpController().serverRequestSigned(this, messageMap, server);
      // Log.d(_TAG, "submitId; resultMap: $resultMap");
    }
    return serverResponse;
  }


  /// checks users publicKey on server
  Future<dynamic> checkUsersPubKeyOnServer(VotingServer server) async {
    if (userId != null) {
      final _messageMap = participantDetailsRequest(participantId: userId!);
      final serverResponse = await HttpController().serverRequestSigned(this, _messageMap, server);

      if (serverResponse.statusCode == 200 && serverResponse.bodyMap!.containsKey("Participant-Data")) {
        ParticipantData participantData = ParticipantData.fromMap(serverResponse.bodyMap!["Participant-Data"]);
        serverResponse.object = participantData;
        Log.d(_TAG, "checkUsersPubKeyOnServer; participantData.publicKey; ${participantData.publicKey}; publicKeysConcat:${await publicKeysConcat}");
        return participantData.publicKey != null && (await publicKeysConcat) == participantData.publicKey;
      }
      else {
        return serverResponse;
      }
    }
    return null;
  }




  static Account? fromMap(Map<String, dynamic> map) {
    var serverMap = SUtils.mapValue(map, SERVER, null);
    if (serverMap == "") serverMap = null;

    String _modeString = SUtils.mapValue(map, MODE, "");
    Mode _mode = Mode.values.firstWhere((mode) => mode.toString() == _modeString,
        orElse: () => Mode.voting);

    Account account = Account(_mode)
      ..userIdEnc = SUtils.mapValue(map, USER_ID_ENC, null)
      ..userId = SUtils.mapValue(map, USER_ID, null)
      ..name = SUtils.mapValue(map, NAME, "")
      ..privateKeyXB64Enc = SUtils.mapValue(map, PRIVATE_KEY_X_ENC, null)
      ..privateKeyEdB64Enc = SUtils.mapValue(map, PRIVATE_KEY_ED_ENC, null)
      ..votingEventId = SUtils.mapValue(map, EVENT_ID, null)
      ..lotCodeEnc = SUtils.mapValue(map, LOT_CODE_ENC, null);

    if (_mode == Mode.wallet) {
      account.userId = SUtils.mapValue(map, USER_ID, null);
    }
    else {
      /// [userId] is stored encrypted
      /// temp, bis alle accounts auf Enc umgestellt wurden
      String? userIdTest = SUtils.mapValue(map, USER_ID, null);
      String? userIdEncTest = SUtils.mapValue(map, USER_ID_ENC, null);
      if (userIdEncTest != null) {
        account.userIdEnc = userIdEncTest;
      }
      if (userIdEncTest == null ) {
        account.userId = userIdTest;
      }
    }

    String? _dateCIso = SUtils.mapValue(map, DATE_C, null);
    String? _dateUIso = SUtils.mapValue(map, DATE_U, null);

    if (_dateCIso != null) {
      account.dateC = _createDateTimeFromIsoString(_dateCIso);
    }
    if (_dateUIso != null) {
      account.dateU = _createDateTimeFromIsoString(_dateUIso);
    }

    if (map.containsKey(VOTINGS) && map[VOTINGS] != null) {
      account.votedList.addAll(map[VOTINGS]);
    }

    return account;
  }


  static _createDateTimeFromIsoString (String isoDate) {
    return DateTime.parse(isoDate);
  }


  Future<Map<String, dynamic>> toMap(bool isWalletAccount, List<int>? pwdBytes) async {
    if (isWalletAccount) {
      return {
        PRIVATE_KEY_X_ENC: privateKeyXB64Enc,
        PRIVATE_KEY_ED_ENC: privateKeyEdB64Enc,
        USER_ID: userId,
        NAME: name,
        DATE_C: _convertDateToIsoString(dateC),
        DATE_U: _convertDateToIsoString(dateU),
        MODE: mode,
      };
    }
    else {
      if (userIdEnc == null && userId != null) {
        await encryptUserId(pwdBytes!);
      }

      var _map = {
        USER_ID_ENC: userIdEnc,
        PRIVATE_KEY_X_ENC: privateKeyXB64Enc,
        PRIVATE_KEY_ED_ENC: privateKeyEdB64Enc,
        DATE_C: _convertDateToIsoString(dateC),
        DATE_U: _convertDateToIsoString(dateU),
        MODE: mode,
        LOT_CODE_ENC: lotCodeEnc
      };
      if (votingEventId != null) {
        _map[EVENT_ID] = votingEventId;
      }
      if (votedList.isNotEmpty) {
        _map[VOTINGS] = votedList;
      }

      return _map;
    }
  }


  String? _convertDateToIsoString (DateTime? dateTime) {
    return dateTime != null
        ? dateTime.toIso8601String()
        : null;
  }


  bool isValidAccount() {
    bool _isValid =
        (keyPairX != null && keyPairEd != null) ||
            (privateKeyXB64Enc != null && privateKeyEdB64Enc != null);

    Log.d(_TAG, "isValidAccount: $_isValid");
    return _isValid;
  }


  /// ADD VOTED item
  void saveVoted(Voting voting) {
    if (!votedList.contains(voting.id)) {
      votedList.add(voting.id);
    }
    dateU = DateTime.now();
  }


  /// CHECK if already voted
  bool isVoted(Voting voting) {
    return votedList.contains(voting.id);
  }


  /// server respond:
  /// Lot-Content~: >
  ///   wsoo3iKDqKABvWCL0WP+pw==:Y1BYUTJzS1BscFIrZjdrQW96OHNwdzNYZGZJSGN6aWFpZWk5eTJSVEZ0T0UwMVpwVUYvaUdnRWo3UzI1Y1JzPQ==
  ///        IV Base64          :           encrypted Yaml with "participant_id" and "credential" keys
  Future<Credential> decryptCredentialFromLotContent(VotingServer server, String lotContent) async {
    try {
      String? yamlString = (await MyAesCrypt.decryptMessage(
          message:  lotContent,
          keyPairX: keyPairX!,
          remotePublicKeyX: server.publicKeyX!
      ));

      if (yamlString != null) {
        yamlString = yamlString.trim();

        Log.s(_TAG,
            "decryptCredentialFromLotContent: yamlString (decrypted): $yamlString");

        /// YamlConverter converts a large number, eg. 4393685097626700267668050536035 to 4.3936850976267e+30
        /// We replace the String "credential: " with "credential: A"
        yamlString = yamlString.replaceFirst("credential: ", "credential: A");
        Map<String, dynamic> yamlMap = YamlConverter().toMap(yamlString);
        String userId = '${yamlMap["participant_id"]}'.trim();
        String credential = '${yamlMap["credential"]}'.trim();

        if (credential.substring(0, 1) == "A") {
          credential = credential.substring(1);
        }
        return Credential(userId, credential);
      }
      else {
        return Credential(null, null);
      }
    }
    catch(e) {
      return Credential.error("$e");
    }
  }




  static const USER_ID = "USER_ID";
  static const USER_ID_ENC = "USER_ID~";

  static const NAME = "NAME";
  static const PRIVATE_KEY_X_ENC = "PRIVATE_KEY_X_ENC";
  static const PRIVATE_KEY_ED_ENC = "PRIVATE_KEY_ED_ENC";
  static const KEY_PAIR_X = "KEY_PAIR_X";
  static const KEY_PAIR_ED = "KEY_PAIR_ED";
  static const PUBLIC_KEY_X = "PUBLIC_KEY_X";
  static const PUBLIC_KEY_ED = "PUBLIC_KEY_ED";
  static const DATE_C = "DATE_C";
  static const DATE_U = "DATE_U";
  static const MODE = "MODE";
  static const LAST_GROUP_ID = "Last-Group-Id";
  static const LOT_CODE_ENC = "LOT_CODE~";

  static const SERVER = "SERVER";

  static const VOTINGS = "VOTINGS";
  // static const VOTE = "VOTE";

  static const EVENT_ID = "EVENT_ID";
  static const EVENT_NAME = "EVENT_NAME";
}




