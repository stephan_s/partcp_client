// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/constants.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/message_type/event_update_request_voting_sort_order.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/objects/client_data.dart';
import 'package:partcp_client/objects/credential_rules.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_utils.dart';

import 'package:collection/collection.dart';
import 'account.dart';
import 'defaults.dart';
import 'server_response.dart';
import 'voting_count.dart';

/// will be build from server info, is not stored in wallet

class VotingEvent {
  static const _TAG = "VotingEvent";

  final String id;
  String name = "";
  DateTime? date = DateTime.now();

  DateTime? createdOn;
  String createdBy = "";
  DateTime? modifiedOn;
  String modifiedBy = "";
  bool administrable = false;

  bool lotCodes = false;
  bool paperLots = false;
  
  int estimatedTurnout = -1;

  late CredentialValues credentialValues;
  late LotCodeValues lotCodeValues;
  late NamingValues namingValues;

  ClientData? clientData;

  List<String> admins = [];

  /// from event-details-request [Votings]
  List<Voting> votings = [];

  /// from event-details-request [Participants]
  int lotsCreated = 0;
  int lotsRedeemed = 0;
  int keysSubmitted = 0;
  bool lotCodeDeposited = false;

  VotingServer server;

  late VotingCount votingCount;

  VotingEvent(this.id, this.server) {
    clientData = ClientData();
  }

  bool? isAdmin(String admin) {
    return (admins.firstWhereOrNull((element) => element == admin) != null);

  }

  String get lotCodesFileNameDecryptedCsv {
    final String _fileName = Constants.LOT_CODES_FILE_NAME_CSV.replaceAll("{}", "${group}_${idWoGroup}");
    return Constants.fileNameRegExp(_fileName);
  }

  String get lotCodesFileNameEncrypted {
    final String _fileName = Constants.LOT_CODES_FILE_NAME_ENCRYPTED.replaceAll("{}", "${group}_${idWoGroup}");
    return Constants.fileNameRegExp(_fileName);
  }

  String get eventBoxLotCodes {
    String serverNameRegExp = Constants.fileNameRegExp((server.url).replaceAll("https://", ""));
    String keyName = "Event-$serverNameRegExp-$id-LotCodes".replaceAll("https://", "");
    return Constants.fileNameRegExp(keyName);
  }
  String get eventBoxParticipants {
    String serverNameRegExp = Constants.fileNameRegExp((server.url).replaceAll("https://", ""));
    String keyName = "Event-$serverNameRegExp-$id-Participants";
    return Constants.fileNameRegExp(keyName);
  }


  String get group => id.split("/").first;
  String get idWoGroup => id.split("/").last;

  static VotingEvent fromMap(Map<String, dynamic> map, VotingEvent? event, VotingServer server) {
    if (event == null) {
      event = VotingEvent(map["id"].trim(), server);
    }
    event
      ..name = SUtils.mapValue(map, "name", "")
      ..paperLots = SUtils.mapValueBool(map, "paper_lots", false)
      ..lotCodes = SUtils.mapValueBool(map, "lot_codes", false)
      ..estimatedTurnout = SUtils.mapValue(map, "estimated_turnout", 0)
      ..createdBy = SUtils.mapValue(map, "created_by", "")
      ..modifiedBy = SUtils.mapValue(map, "modified_by", "")
      ..administrable = SUtils.mapValueBool(map, "administrable", false)
      ..lotCodeDeposited = SUtils.mapValue(map, "lot_code_deposited", false)
      ..date = SUtils.mapParseDateTime(map, "date", null)
      ..createdOn = SUtils.mapParseDateTime(map, "created_on", null)
      ..modifiedOn = SUtils.mapParseDateTime(map, "modified_on", null)
      ..credentialValues = CredentialValues.fromMap(SUtils.mapValue(map, "naming_rules", defaultMap))
      ..lotCodeValues = LotCodeValues.fromMap(SUtils.mapValue(map, "lot_code_rules", defaultMap))
      ..namingValues = NamingValues.fromMap(SUtils.mapValue(map, "credential_rules", defaultMap))
      ..votingCount = VotingCount.fromMap(SUtils.mapValue(map, "voting_count", defaultMap))
    ;

    /// we get "open_votings" key, when we call [eventListRequest] with includeOpenVotings = true
    List<dynamic>? openVotings = SUtils.mapValue(map, "open_votings", null);
    if (openVotings != null) {
      Log.d(_TAG, "openVotings: $openVotings");
      for (Map<String, dynamic> map in openVotings ) {
        Voting v = Voting.fromMap(map);
        event.votings.add(v);
      }
    }

    try {
      event.clientData = ClientData.fromYaml(SUtils.mapValue(map, ClientData.CLIENT_DATA, null));
    } catch (e) {
      Log.w(_TAG, "fromMap; error: $e => Setting clientData = ClientData()");
      event.clientData = ClientData();
    }

    /// ADMINS
    var tmpMap = SUtils.mapValue(map, "admins", defaultStringList);
    if (tmpMap is List<String>) {
      event.admins.addAll(tmpMap);
    }

    return event;
  }

  void _sortVotingList() {
    if (votings.isNotEmpty) {
      for (var newIndex = 0; newIndex < clientData!.votingSortOrder.length; newIndex++) {
        Voting? item = votings.firstWhereOrNull((element) => element.id == clientData!.votingSortOrder[newIndex]);
        if (item != null) {
          int oldIndex = votings.indexOf(item);

          if (newIndex > oldIndex) {
            newIndex -= 1;
          }
          votings.removeAt(oldIndex);
          votings.insert(newIndex, item);
        }
      }
      updateClientDataVotingOrder();
    }
  }
  
  void updateClientDataVotingOrder() {
    List<String> votingList = [];
    votings.forEach((element) {
      votingList.add(element.id!);
    });
    clientData!.votingSortOrder = votingList;
  }

  /// UPDATE CLIENT-DATA (voting sort order)
  Future<ServerResponse> sendUpdateRequestClientData(
      VotingServer server, Account account, VotingEvent event) async {

    final Map<String, dynamic> messageMap = eventUpdateRequestVotingSortOrder(this);

    ServerResponse serverResponse =
    await HttpController().serverRequestSigned(account, messageMap, server);
    return serverResponse;
  }


  /// EVENT DETAILS REQUEST
  Future<ServerResponse> sEventDetailsRequest(VotingServer server, Account account) async {
    final _messageMap = eventDetailsRequest(this);
    final ServerResponse serverResponse = await HttpController().serverRequest(_messageMap, account, server.url);

    if (serverResponse.statusCode != 200) {
      return serverResponse;
    }

    final Map<String, dynamic>? bodyMap = serverResponse.bodyMap;

    VotingEvent.fromMap(bodyMap!["Event-Data"], this, server);

    final Map<String, dynamic> participants = SUtils.mapValue(bodyMap, "Participants", defaultMap);
    lotsCreated = SUtils.mapValue((participants), "lots_created", 0);
    lotsRedeemed = SUtils.mapValue((participants), "lots_redeemed", 0);
    keysSubmitted = SUtils.mapValue((participants), "keys_submitted", 0);

    /// Votings
    votings.clear();
    List<dynamic>? _votings = SUtils.mapValue(bodyMap, "Votings", []);
    if (_votings != null) {
      for (var i = 0; i < _votings.length; i++) {
        Log.d(_TAG, "sEventDetailsRequest: votings: ${_votings.length}");

        Voting v = Voting.fromMap(_votings[i]);
        if (v.status == Voting.STATUS_OPEN) {
          votingCount.open ++;
        }
        else if (v.status == Voting.STATUS_IDLE) {
          votingCount.idle ++;
        }
        else if (v.status == Voting.STATUS_CLOSED) {
          votingCount.closed ++;
        }
        else if (v.status == Voting.STATUS_FINISHED) {
          votingCount.finished ++;
        }
        votings.add(v);
      }
      _sortVotingList();
    } else {
      Log.e(_TAG, "sEventDetailsRequest: bodyMap[Voting.VOTINGS_KEY] == null");
    }
    return serverResponse;
  }

  /// VOTING DETAIL REQUEST
  Future<ServerResponse> sVotingListRequest(VotingServer server, Account? account, VotingStatusEnum status) async {
    votings.clear();

    final Map<String, dynamic> messageMap = votingListRequest(
      votingEventId: id,
      votingStatus: status,
    );
    ServerResponse serverResponse = await HttpController().serverRequest(messageMap, account, server.url);

    if (serverResponse.statusCode != 200) {
      return serverResponse;
    }

    final Map<String, dynamic>? bodyMap = serverResponse.bodyMap;
    final List<dynamic> _votingsList = SUtils.mapValue(bodyMap, Voting.VOTINGS, defaultMap);

    Log.d(_TAG, "sVotingListRequest; _votingsList: ${_votingsList.length}");

    for (var i = 0; i < _votingsList.length; i++) {
      Voting voting = Voting.fromMap(_votingsList[i]);
      votings.add(voting);
    }
    return serverResponse;
  }


}

// extension FirstWhereOrNullExtension<E> on Iterable<E> {
//   E? firstWhereOrNull(bool Function(E) test) {
//     for (E element in this) {
//       if (test(element)) return element;
//     }
//     return null;
//   }
// }
