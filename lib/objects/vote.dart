// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

class Vote {

  static const int NO_VOTE_VALUE = -100;
  String id;
  int value;

  Vote(this.id, this.value);

  Map<String, dynamic> get toMap => {
    "id": id,
    "vote": value
  };
}


