// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:collection/src/iterable_extensions.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/utils/s_utils.dart';

class VoteResult {
  final int voteValue;
  final int count;

  const VoteResult({
    required this.voteValue,
    required this.count
  });
}

class VotingResult {
  int participants = 0;
  int invalid = 0;

  /// <option.id, VoteResult>
  Map<String, List<VoteResult>> results = {};
  
  static VotingResult fromMap(Map<String, dynamic> map) {
    VotingResult votingResult = VotingResult()
      ..participants = SUtils.mapValue(map, "participants", 0)
      ..invalid = SUtils.mapValue(map, "invalid", 0)
    ;
    
    /// optionsList
    /*
      Voting-Result:
        participants: 4
        invalid: 0
        options:
          "option:0":
            "vote:0": 3
            "vote:2": 1
          "option:1":
            "vote:5": 1
            "vote:1": 2
            "vote:0": 1
          "option:2":
            "vote:-100": 1
            "vote:0": 1
            "vote:3": 1
            "vote:1": 1
     */

    dynamic _options = SUtils.mapValue(map, "options", []);

    if (_options is Map<String, dynamic>) {
      _options = SUtils.mapValue(map, "options", []);
      _options.keys.forEach((option) {
        String optionId = _parseString(option);
        votingResult.results[optionId] = [];

        Map<String, dynamic> _votesMap = SUtils.mapValue(_options, option, null);

        _votesMap.forEach((key, value) {
          VoteResult voteResult = VoteResult(
              voteValue: _parseInt(key),
              count: value
          );
          votingResult.results[optionId]!.add(voteResult);
        });
      });
    }
    return votingResult;
  }


  double resistance (String id) {
    List<VoteResult>? items = results[id];
    if (items == null) {
      return 0;
    }
    int sum = 0;
    int count = 0;

    items.forEach((VoteResult voteResult) {
      if (voteResult.voteValue != Vote.NO_VOTE_VALUE) {
        sum += voteResult.voteValue * voteResult.count;
        count += voteResult.count;
      }
    });
    return sum/count;
  }
  

  int valueByVote(String id, int vote) {
    List<VoteResult>? items = results[id];
    if (items == null) {
      return 0;
    }
    //--// VoteResult voteResult = items.firstWhere((voteResult) => voteResult.voteValue == vote, orElse: () => null);
    VoteResult? voteResult = items.firstWhereOrNull((voteResult) => voteResult.voteValue == vote);
    return voteResult != null ? voteResult.count : 0;
  }
  
  int highestConsensusCount(String id) {
    List<VoteResult>? items = results[id];
    if (items == null) {
      return 0;
    }
    int max = 0;
    items.forEach((VoteResult voteResult) {
      if (voteResult.count > max) {
        max = voteResult.count;
      }
    });
    return max;
  }

  int highestOptionCount() {
    int count = 0;
    results.forEach((key, List<VoteResult> voteResults) {
      VoteResult voteResult = voteResults[0];
      if (voteResult.count > count) {
        count = voteResult.count;
      }
    });
    return count;
  }

  int votingsCount(String id) {
    List<VoteResult>? items = results[id];
    if (items == null) {
      return 0;
    }
    int count = 0;
    items.forEach((VoteResult voteResult) {
      count += voteResult.count;
    });
    return count;
  }

  static String _parseString(String option) {
    return option.split(":").last.toString();
  }

  static int _parseInt(String option) {
    return int.tryParse(option.split(":").last)!;
  }

}