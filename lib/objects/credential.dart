// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

class Credential {
  String? userId;
  String? credential;
  String? errorMsg;

  Credential(this.userId, this.credential);
  Credential.error(this.errorMsg);
}