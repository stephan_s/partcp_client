// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'package:partcp_client/utils/s_utils.dart';
//
// class CredentialTransferable {
//
//   final String participantId;
//   final String eventId;
//   final String lotCodeEnc;
//
//   const CredentialTransferable({this.participantId, this.lotCodeEnc, this.eventId});
//
//   static CredentialTransferable fromMap (Map<String, dynamic> map) {
//     CredentialTransferable c = CredentialTransferable(
//         participantId: SUtils.mapValue(map, PARTICIPANT_ID, null),
//         eventId: SUtils.mapValue(map, EVENT_ID, null),
//         lotCodeEnc: SUtils.mapValue(map, LOT_CODE_ENC, null)
//     );
//     return c;
//   }
//
//   Map<String, dynamic> toMap() {
//     return {
//       PARTICIPANT_ID: participantId,
//       EVENT_ID: eventId,
//       LOT_CODE_ENC: lotCodeEnc
//     };
//   }
//
//   static const PARTICIPANT_ID = "Participant-Id";
//   static const EVENT_ID = "Event-Id";
//   static const LOT_CODE_ENC = "Lot-Code~";
//
// }