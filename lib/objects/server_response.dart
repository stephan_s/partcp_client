// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

class ServerResponse {
  String? request;
  int? statusCode;
  String? statusText;
  String? body;
  Map<String, dynamic>? bodyMap;

  String? _errorMessage;

  bool signatureIsCorrect = false;
  bool receivingData = false;

  set errorMessage(String? message) {
    _errorMessage = message;

    if (statusCode == null) {
      statusCode = message == null
        ? null
        : 400;
    }
  }

  String? get errorMessage => _errorMessage != null
      ? "[ERROR]: $_errorMessage"
      : null;


  /// we can append some data to it
  dynamic object;

}