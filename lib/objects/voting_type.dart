// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

class VotingType {

  static const values = [
    YES_NO_RATING,
    CONSENSUS_3,
    CONSENSUS_5,
    CONSENSUS_10,
    SINGLE_CHOICE,
    MULTIPLE_CHOICE_2,
    MULTIPLE_CHOICE_3,
    MULTIPLE_CHOICE_4,
    MULTIPLE_CHOICE_ALL,
  ];




  static String title(String? type) {
    if (type == null) return "null";

    return _titleMap.containsKey(type)
      ? _titleMap[type]!
      : "n/a";
  }


  static bool isConsensus(String type) {
    return type.startsWith("consensus");
  }

  static bool isMultipleChoice(String type) {
    return type.startsWith("multiple-choice");
  }

  static int getMultipleChoiceCountFromType(String type) {
    if (type.startsWith("multiple-choice-")) {
      var splitted = type.split("multiple-choice-");
      if (splitted[1] == "all") {
        return 9999;
      }
      else {
        var test = int.tryParse(splitted[1]);
        return test != null
            ? test
            : -1;
      }
    }
    else {
      return -1;
    }
  }

  static const Map<String, String> _titleMap = {
    YES_NO_RATING: "Ja-Nein Abstimmung",
    CONSENSUS_3: "Konsensierung (3 Punkte)",
    CONSENSUS_5: "Konsensierung (5 Punkte)",
    CONSENSUS_10: "Konsensierung (10 Punkte)",
    SINGLE_CHOICE: "Single Choice",
    MULTIPLE_CHOICE_2: "Multiple Choice (2)",
    MULTIPLE_CHOICE_3: "Multiple Choice (3)",
    MULTIPLE_CHOICE_4: "Multiple Choice (4)",
    MULTIPLE_CHOICE_ALL: "Multiple Choice (alle)",
  };


  static const YES_NO_RATING = "yes-no-rating";
  static const SIMPLE_10_0_10 = "simple-10-0-10";
  static const CONSENSUS_3 = "consensus-3";
  static const CONSENSUS_5 = "consensus-5";
  static const CONSENSUS_10 = "consensus-10";
  static const SINGLE_CHOICE = "single-choice";
  static const MULTIPLE_CHOICE_2 = "multiple-choice-2";
  static const MULTIPLE_CHOICE_3 = "multiple-choice-3";
  static const MULTIPLE_CHOICE_4 = "multiple-choice-4";
  static const MULTIPLE_CHOICE_ALL = "multiple-choice-all";

}