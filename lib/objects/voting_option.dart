// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:partcp_client/objects/client_data.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:uuid/uuid.dart';

class VotingOption {

  String id;
  late String name;
  ClientData clientData = ClientData();

  /// for moderator mode
  String uuid;
  bool edit;


  VotingOption({
    this.id = "",
    this.name = "",
    this.uuid = "",
    this.edit = false,
    required this.clientData,
  });


  static VotingOption fromMap(Map<String, dynamic> map) {
    Map<String, dynamic> defMap = {};
    return VotingOption(
      id: map[ID].toString(),
      name: map[NAME].trim(),
      uuid: VotingOption.createUuid(),
      // clientData: ClientData.fromMap(
      //       SUtils.mapValue(map, ClientData.CLIENT_DATA, defMap))
      clientData: ClientData.fromYaml(
            SUtils.mapValue(map, ClientData.CLIENT_DATA, defMap))
    );
  }

  static String createUuid() {
    return Uuid().v1();
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      ID: id,
      NAME: name.trim(),
    };

    // var clientDataMap = clientData.toMap();
    // if (clientDataMap.isNotEmpty) {
    //   map[ClientData.CLIENT_DATA] = clientDataMap;
    // }
    map[ClientData.CLIENT_DATA] = clientData.toYaml();
    return map;
  }

  /// Server Array keys
  static const OPTIONS = "options";
  static const ID = "id";
  static const NAME = "name";

}