// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/utils/yaml_converter.dart';


class ClientData {

  /// for future format changes
  static const int clientDataRev = 1;

  int rev = 0;
  String createdBy;
  String description;
  String shortDescription;
  String linkName;
  String linkUrl;
  String? imageB64;
  String? thumbB64;
  List<dynamic> votingSortOrder;

  ClientData({
    this.createdBy = "",
    this.shortDescription = "",
    this.description = "",
    this.linkName = "",
    this.linkUrl = "",
    this.imageB64,
    this.thumbB64,
  }) : votingSortOrder = [] ;


  /*
  static ClientData fromMap (Map<String, dynamic>? map) {
    if (map == null) {
      map = {};
    };

    return ClientData()
      ..rev = SUtils.mapValue(map, REV, 0)
      ..createdBy = SUtils.mapValue(map, CREATED_BY, "")
      ..shortDescription = SUtils.mapValue(map, SHORT_DESCRIPTION, "")
      ..description = SUtils.mapValue(map, DESCRIPTION, "")
      ..imageB64 = SUtils.mapValue(map, IMAGE, null)
      ..thumbB64 = SUtils.mapValue(map, THUMB, null)
      ..linkName = SUtils.mapValue(map, LINK_NAME, "")
      ..linkUrl = SUtils.mapValue(map, LINK_URL, "")
      ..votingSortOrder = SUtils.mapValue(map, VOTING_SORT_ORDER, [])
    ;
  }
  */


  static ClientData fromYaml (dynamic yaml) {

    /// old ClientData was stored as Map, now, we store it as Yaml-String
    if (yaml == null) {
      yaml = "";
    };

    if (yaml is Map<String, dynamic>) {
      yaml = "";
    }

    Map<String, dynamic> map = YamlConverter().toMap(yaml);

    return ClientData()
      ..rev = SUtils.mapValue(map, REV, 0)
      ..createdBy = SUtils.mapValue(map, CREATED_BY, "")
      ..shortDescription = SUtils.mapValue(map, SHORT_DESCRIPTION, "")
      ..description = SUtils.mapValue(map, DESCRIPTION, "")
      ..imageB64 = SUtils.mapValue(map, IMAGE, null)
      ..thumbB64 = SUtils.mapValue(map, THUMB, null)
      ..linkName = SUtils.mapValue(map, LINK_NAME, "")
      ..linkUrl = SUtils.mapValue(map, LINK_URL, "")
      ..votingSortOrder = SUtils.mapValue(map, VOTING_SORT_ORDER, [])
    ;
  }


  String toYaml() {
    Map<String, dynamic> map = _toMap();
    String yaml = YamlConverter().toYaml(map);
    return yaml;
  }

  Map<String, dynamic> _toMap()  {
    shortDescription.trim();
    description.trim();
    linkName.trim();
    linkUrl.trim();

    Map<String, dynamic> map = {};

    map[SHORT_DESCRIPTION] = shortDescription;
    map[DESCRIPTION] = description;

    if (imageB64 != null && imageB64!.isNotEmpty) {
      map[IMAGE] = imageB64;
    }

    if (thumbB64 != null && thumbB64!.isNotEmpty) {
      map[THUMB] = thumbB64;
    }

    if (linkName.isNotEmpty) {
      map[LINK_NAME] = linkName;
    }

    map[LINK_URL] = linkUrl;

    map[VOTING_SORT_ORDER] = votingSortOrder.toList(growable: false);


    if (map.isNotEmpty) {
      map[REV] = clientDataRev;
      // map[CREATED_BY] = clientDataCreatedBy;
    }

    return map;
  }
  
  

  static const CLIENT_DATA = "client_data";

  /// YAML description
  static const REV = "data-rev";
  static const CREATED_BY = "created_by";
  static const SHORT_DESCRIPTION = "short-description";
  static const DESCRIPTION = "description";
  static const IMAGE = "image";
  static const THUMB = "thumb";
  static const LINK_URL = "link-url";
  static const LINK_NAME = "link-name";
  static const VOTING_SORT_ORDER = "voting-sort-order";

}