import 'dart:convert';

import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:partcp_client/controller/ws_client/stanza.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:uuid/uuid.dart';

const _TAG = "WsServer";

class WsServer {
  String? publicKeyConcatB64;

  ecLib.SimplePublicKey? publicKeyX;
  ecLib.SimplePublicKey? publicKeyEd;

  WsServer ({this.publicKeyConcatB64}) {

    /// Create Keys from concat Ed and X key
    Map<String, dynamic> keyMap = createPublicKeysFromConcat();
    if (keyMap.containsKey("X") && keyMap.containsKey("Ed")) {
      publicKeyX = keyMap["X"];
      publicKeyEd = keyMap["Ed"];
    }
  }


  /// Create Keys from concat Ed and X key
  dynamic createPublicKeysFromConcat() {
    if (publicKeyConcatB64 == null) return null;

    List<int> pubKeyConcatSeed = base64.decode(publicKeyConcatB64!.trim());
    Map<String, dynamic> keyMap = MyEcCrypt.createPublicKeysFromConcat(pubKeyConcatSeed);

    if (keyMap.containsKey("X") && keyMap.containsKey("Ed")) {
      publicKeyX = keyMap["X"];
      publicKeyEd = keyMap["Ed"];
    }
    Log.d(_TAG, "createPublicKeysFromConcat; publicKeyX: ${publicKeyX!.bytes}");
    Log.d(_TAG, "createPublicKeysFromConcat; publicKeyEd: ${publicKeyEd!.bytes}");
    return keyMap;
  }


  Future<Stanza> createWelcomeResponse ({required Account account, required Stanza inStanza}) async {
    String? enc = await account.encryptWsServerMessage(server: this, message: inStanza.data!["test"]);
    Map<String, dynamic> data = {
      "test~": enc == null ? "" : enc
    };
    Uuid uuid = Uuid();

    Stanza stanza = Stanza(
        type: StanzaType.auth,
        // to: publicKeyConcatB64!,
        from: await account.publicKeysConcat,
        stanzaId: uuid.v4(), /// random
        data: data
    );
    return stanza;
  }

}