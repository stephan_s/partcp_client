// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/utils/s_utils.dart';

class UrlParams {
  String? url;
  String? eventId;
  String? votingId;
  String? lotCode;
  String? serverPubKeyConcat;

  UrlParams();

  /// ?eventId=0001&votingId=abstimmung-01

  static UrlParams fromMap(Map<String, dynamic> paramsMap) {
    return UrlParams()
      ..url = SUtils.mapValueUrlDecoded(paramsMap, "url", null)
      ..serverPubKeyConcat = SUtils.mapValueUrlDecoded(paramsMap, "pubKey", null)
      ..eventId = SUtils.mapValueUrlDecoded(paramsMap, "eventId", null)
      ..votingId = SUtils.mapValueUrlDecoded(paramsMap, "votingId", null)
      ..lotCode = SUtils.mapValueUrlDecoded(paramsMap, "lotCode", null);
  }
}