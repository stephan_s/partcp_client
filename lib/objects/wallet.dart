// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/message_type/lot_request.dart';
import 'package:partcp_client/objects/credential.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/utils/crypt/my_aes_crypt.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/yaml_converter.dart';
import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:uuid/uuid.dart';

import 'account.dart';

const _TAG = "Wallet";

YamlConverter _yamlConverter = YamlConverter();

class Wallets {
  List<Wallet> walletList = [];
  
  void fromYaml (String? yamlString) {
    
    if (yamlString != null) {
      final Map<String, dynamic> yamlMap = _yamlConverter.toMap(yamlString);
      
      for(String walletYaml in yamlMap[WALLETS]) {
        Wallet? wallet = Wallet.fromYaml(walletYaml);
        if (wallet != null) {
          walletList.add(wallet);
        }
      }
    }
  }
  

  Future<String> toYaml() async {
    List<String> wallets = [];

    for (Wallet wallet in walletList) {
      String walletYaml = await wallet.toYaml();
      wallets.add(walletYaml);
    }
    
    String walletsYaml = _yamlConverter.toYaml({WALLETS: wallets});
    return walletsYaml;
  }
  
  static const WALLETS = "wallets";
  
}

class Wallet {

  List<VotingServer> serverList = [];

  Account? walletAccount;
  List<int>? pwdBytes;

  /// for [encryptString] and [decryptString}
  /// we use as pwd a hash of wallet's PrivateKey from keyPairX
  Future<List<int>> get pwdForStrings async {
    final key = await walletAccount!.keyPairX!.extractPrivateKeyBytes();
    final algorithm = ecLib.Sha256();
    final ecLib.Hash hash = await algorithm.hash(key);
    return hash.bytes;
  }

  Future<bool> addAccount(VotingServer votingServer, Account account) async {
    await account.encryptKeys(pwdBytes!);
    serverList.firstWhere((element) => element.publicKeyConcatB64 == votingServer.publicKeyConcatB64).accounts.add(account);
    return true;
  }

  bool addServer(VotingServer server) {
    for (var i=0; i<serverList.length; i++) {
      if (serverList[i].hashCode == server.hashCode) {
        return false;
      }
    }
    serverList.add(server);
    updateWalletDateU();
    return true;
  }

  VotingServer? getServerByIdHash(String idHashCode) {
    for (var i=0; i<serverList.length; i++) {
      if (serverList[i].idHashCode == idHashCode) {
        // Log.d(_TAG, "getServerByHash: server exists");
        return serverList[i];
      }
    }
    // Log.d(_TAG, "getServerByHash: server not exists");
    return null;
  }


  static Wallet? fromYaml (String? yamlString) {
    Wallet wallet = Wallet();

    try {
      if (yamlString != null) {
        final Map<String, dynamic> yamlMap = _yamlConverter.toMap(yamlString);

        /// main wallet account
        final Map<String, dynamic>? walletAccountMap = yamlMap[WALLET];
        if (walletAccountMap != null) {
          wallet.walletAccount = Account.fromMap(walletAccountMap);
        }

        /// VotingServer
        final List<dynamic>? yamlServer = yamlMap[SERVER];
        if (wallet.walletAccount != null && yamlServer != null) {
          for (var i = 0; i < yamlServer.length; i++) {
            final VotingServer? server = VotingServer.fromMap(yamlServer[i]);
            if (server != null) {
              wallet.serverList.add(server);
            }
          }
        }
      }
    }
    catch(e) {
      Log.e(_TAG, "fromYaml error: $e");
    }

    return wallet.walletAccount == null
      ? null
      : wallet;
  }


  Future<bool> decryptAccounts (List<int>? pwdBytes) async {
    List<int> _pwdBytes = pwdBytes != null
      ? pwdBytes
      : this.pwdBytes!;

    bool isSuccess = false;

    isSuccess = await walletAccount!.decryptKeys(_pwdBytes);
    if (!isSuccess) {
      return false;
    }

    this.pwdBytes = _pwdBytes;

    /// loop over server
    for (var i = 0; i<serverList.length; i++) {
      /// loop over accounts
      for(var a = 0; a<serverList[i].accounts.length; a++) {
        Log.d(_TAG, "decryptAccount for ${serverList[i].accounts[a].userIdEnc}");
        serverList[i].accounts[a].decryptKeys(_pwdBytes);
      }
    }
    return true;
  }


  Account? accountForEvent(VotingServer server, String eventId, Mode mode, {bool keyPairMustExists = false}) {
    for (var i=0; i<server.accounts.length; i++) {
      Account _account = server.accounts[i];
      if (_account.votingEventId == eventId && _account.userId != null && _account.mode == mode) {
        Log.d(_TAG, "accountForEvent ($eventId); userId~: ${server.accounts[i].userIdEnc}");
        if (keyPairMustExists && _account.privateKeyEdB64Enc == null) {
          Log.i(_TAG, "accountForEvent ($eventId): ${server.accounts[i].userId} -> keyPairMustExists but is null");
        }
        else {
          return server.accounts[i];
        }
      }
    }
    Log.d(_TAG, "accountForEvent ($eventId): null");
    return null;
  }


  ///////////////////////////////////////////////////////////////////
  /// request anonymous userId and tan with lotCode for Voting
  Future<VotingCredentialResponse> requestVotingCredentialsFromLot({
    required String lotCode,
    required Mode mode,
    required VotingEvent event,
    required VotingServer server
  }) async {

    VotingCredentialResponse votingCredentialResponse = VotingCredentialResponse();

    /// we create a temp account with temp keys to decrypt the credentials
    Account _account = Account(mode);
    await _account.createNewKeys();

    /// Generate a v4 (random) id as userId
    var _uuid = Uuid();
    _account.userId = _uuid.v4();

    String? lotCodeEnc = await _account.encryptServerMessage(
      message: lotCode,
      server: server,
    );

    final Map<String, dynamic> messageMap = lotRequest(
      // lotCode: lotCode,
        lotCodeEnc: lotCodeEnc,
        eventId: event.id
    );
    ServerResponse serverResponse = await HttpController().serverRequestSigned(_account, messageMap, server);
    votingCredentialResponse.lotRequestServerResponse = serverResponse;

    if (serverResponse.statusCode != 200) {
      votingCredentialResponse.lotRequestSuccess = false;
      return votingCredentialResponse;
    }
    else {
      /// server should response IV  and encrypted yaml credentials
      /// Lot-Content~: >
      ///   wsoo3iKDqKABvWCL0WP+pw==:Y1BYUTJzS1BscFIrZjdrQW96OHNwdzNYZGZJSGN6aWFpZWk5eTJSVEZ0T0UwMVpwVUYvaUdnRWo3UzI1Y1JzPQ==

      String lotContent = (serverResponse.bodyMap!["Lot-Content~"]).trim();
      Log.d(_TAG, "_requestUserCredentialsFromLot: Lot-Content~: $lotContent");

      Credential credential = await _account.decryptCredentialFromLotContent(server, lotContent);
      /// we got credential
      if (credential.userId != null && credential.credential != null) {
        votingCredentialResponse.lotRequestSuccess = true;

        return await keySubmission(
            votingCredentialResponse: votingCredentialResponse,
            userId: credential.userId!,
            credential: credential.credential!,
            event: event,
            server: server,
            mode: mode
        );
      }
      /// something goes wrong
      else {
        serverResponse.errorMessage = credential.errorMsg != null
            ? credential.errorMsg
            : "Unexpected error";

        votingCredentialResponse
            ..lotRequestSuccess = false
            ..lotRequestServerResponse = serverResponse;

        return votingCredentialResponse;            
      }
    }
  }


  /// on paperLots or Admin accounts we have userId and credential
  /// and can send it directly
  Future<VotingCredentialResponse> keySubmission ({
    required VotingCredentialResponse votingCredentialResponse,
    required String userId,
    required String credential,
    required Mode mode,
    required VotingEvent? event,
    required VotingServer server
  }) async {

    Account? account;

    /// first check, if account for this event already exists (without keyPair)
    if (mode == Mode.voting) {
      account = accountForEvent(server, event!.id, mode);
    }

    if (account == null) {
      account = Account(mode);
    }

    account.userId = userId;
    account.credential = credential;
    await account.createNewKeys();

    if (account.mode == Mode.voting) {
      account.votingEventId = event!.id;
    }

    ServerResponse serverResponse = await account.submitIdToServer(server);
    votingCredentialResponse.keySubmissionServerResponse = serverResponse;

    if (serverResponse.statusCode != 200) {
      votingCredentialResponse.keySubmissionSuccess = false;
    }
    else {
      /// add account to Wallet
      await addAccount(server, account);

      /// save wallet in shared prefs
      await MainController.to.onWalletChanged(updateNotification: true);
      votingCredentialResponse.keySubmissionSuccess = true;
    }
    return votingCredentialResponse;
  }
  ////////////////////////////////////////////////////////////////////


  /// ENCRYPT STRING
  Future<String?> encryptString(String content) async {
    return base64Encode(await MyAesCrypt.encryptKey(content.codeUnits, await pwdForStrings));
  }


  Future<String?> computeEncryptString(String content) async {
    Map data = {
      "content": content,
      "pwdBytes": await pwdForStrings,
    };
    return await compute(_computeEncryptString, data);
  }


  static Future<String> _computeEncryptString(Map data) async {
    String content = data["content"];
    var pwdBytes = data["pwdBytes"];
    return base64Encode(await MyAesCrypt.encryptKey(content.codeUnits, pwdBytes));
  }


  /// DECRYPT STRING
  Future<String?> decryptString(String contentEnc) async {
    try {
      var test = base64.normalize(contentEnc);
      Log.d(_TAG, "base64 normalize: $test");
    }
    on FormatException catch(e) {
      return null;
    }
    catch(e) {
      Log.e(_TAG, "decryptString error: $e");
      return null;
    }
    return Utf8Codec().decode(
        await MyAesCrypt.decryptKey(base64Decode(contentEnc), await pwdForStrings)
    );
  }


  Future<String?> computeDecryptString(String contentEnc) async {
    try {
      base64.normalize(contentEnc);
    }
    on FormatException catch(e) {
      return null;
    }
    catch(e) {
      return null;
    }

    Map data = {
      "content": contentEnc,
      "pwdBytes": await pwdForStrings,
    };
    return await compute(_computeDecryptString, data);
  }


  static Future<String?> _computeDecryptString(Map data) async {
    String content = data["content"];
    var pwdBytes = data["pwdBytes"];

    try {
      return Utf8Codec().decode(
          await MyAesCrypt.decryptKey(base64Decode(content), pwdBytes)
      );
    }
    catch(e) {
      return null;
    }
  }


  void updateWalletDateU() {
    walletAccount!.dateU = DateTime.now();
  }


  Future<String> toYaml() async {
    Map<String, dynamic> map = {};

    /// wallet account
    map[WALLET] = await walletAccount!.toMap(true, null);

    /// accounts
    List<dynamic> sList = [];
    for (var i=0; i<serverList.length; i++) {
      sList.add(await serverList[i].toMap(pwdBytes!));
    }
    map[SERVER] = sList;

    String yamlString = _yamlConverter.toYaml(map);
    return yamlString;
  }

  static const SERVER = "SERVER";
  static const WALLET = "WALLET";
}


class VotingCredentialResponse{
  bool? lotRequestSuccess;
  ServerResponse? lotRequestServerResponse;

  bool? keySubmissionSuccess;
  ServerResponse? keySubmissionServerResponse;
}