// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:partcp_client/utils/s_utils.dart';

class VotingCount {
  int idle;
  int open;
  int closed;
  int finished;

  VotingCount({
    required this.idle,
    required this.open,
    required this.closed,
    required this.finished,
  });

  static VotingCount fromMap(Map<String, dynamic> map) {
    return VotingCount(
        idle: SUtils.mapValue((map), "idle", 0),
        open: SUtils.mapValue((map), "open", 0),
        closed: SUtils.mapValue((map), "closed", 0),
        finished: SUtils.mapValue((map), "finished", 0),
    );
  }
}