// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:collection/src/iterable_extensions.dart';
import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:flutter/services.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/message_type/event_list_request.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/utils/crypt/my_ec_crypt.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_utils.dart';

import '../constants.dart';
import 'group.dart';

const _TAG = "VotingServer";

class VotingServer {
  String url;
  String name = "";
  String description = "";
  String? publicKeyConcatB64;

  String? keyManagerUrl = "";
  List<Account> accounts = [];

  bool isAssetServer = false;
  bool lotCodesReceived = false;

  /// once we get events from server, we cache it in the map
  Map<String, List<VotingEvent>> groupEventsMap = {};

  List<Group> serverGroups = [];
  List<Group> subscribedGroups = [];

  ecLib.SimplePublicKey? publicKeyX;
  ecLib.SimplePublicKey? publicKeyEd;

  VotingServer(this.url);

  static VotingServer? fromMap(Map<String, dynamic>? map) {
    if (map == null) {
      return null;
    }

    VotingServer vs = VotingServer(SUtils.mapValue(map, URL, null));
    vs.name = SUtils.mapValue(map, NAME, "");
    vs.description = SUtils.mapValue(map, COMMUNITY, "");
    vs.publicKeyConcatB64 = SUtils.mapValue(map, PUBLIC_KEY_CONCAT_B64, null);

    /// Create Keys from concat Ed and X key
    Map<String, dynamic> keyMap = vs.createPublicKeysFromConcat();
    if (keyMap.containsKey("X") && keyMap.containsKey("Ed")) {
      vs.publicKeyX = keyMap["X"];
      vs.publicKeyEd = keyMap["Ed"];
    }

    /// accounts
    final List<dynamic>? yamlAccounts = map[ACCOUNTS];
    if (yamlAccounts != null) {
      for (var i = 0; i < yamlAccounts.length; i++) {
        final Account? account = Account.fromMap(yamlAccounts[i]);
        // if (account != null && account.isValidAccount()) {
        if (account != null) {
          vs.accounts.add(account);
        }
      }
    }
    return vs;
  }

  void addAccount(Account account) {
    for (var i = 0; i < accounts.length; i++) {
      if (accounts[i].votingEventId == account.votingEventId) {
        accounts.removeAt(i);
        break;
      }
    }
    accounts.add(account);
  }

  dynamic createPublicKeyXFromB64Seed(String publicKeyXB64) {
    publicKeyX = MyEcCrypt.createPublicKeyXFromSeed(base64Decode(publicKeyXB64));
    Log.d(_TAG, "createPublicKeyXFromB64Seed; publicKeyX: ${publicKeyX!.bytes}");
    return publicKeyX;
  }

  dynamic createPublicKeyEdFromB64Seed(String publicKeyEdB64) {
    publicKeyEd = MyEcCrypt.createPublicKeyXFromSeed(base64Decode(publicKeyEdB64));
    Log.d(_TAG, "createPublicKeyEdFromB64Seed; publicKeyEd: ${publicKeyEd!.bytes}");
    return publicKeyEd;
  }

  /// Create Keys from concat Ed and X key
  dynamic createPublicKeysFromConcat() {
    if (publicKeyConcatB64 == null) return null;

    List<int> pubKeyConcatSeed = base64.decode(publicKeyConcatB64!.trim());
    Map<String, dynamic> keyMap = MyEcCrypt.createPublicKeysFromConcat(pubKeyConcatSeed);

    if (keyMap.containsKey("X") && keyMap.containsKey("Ed")) {
      publicKeyX = keyMap["X"];
      publicKeyEd = keyMap["Ed"];
    }
    Log.d(_TAG, "createPublicKeysFromConcat; publicKeyX: ${publicKeyX!.bytes}");
    Log.d(_TAG, "createPublicKeysFromConcat; publicKeyEd: ${publicKeyEd!.bytes}");
    return keyMap;
  }

  Account? getVotingAccountForEvent(VotingEvent event) {
    return accounts.firstWhereOrNull((account) => account.votingEventId == event.id && account.mode == Mode.voting);
  }

  Account? getAccountByMode(Mode mode) {
    //-// return accounts.firstWhere((account) => account.mode == mode, orElse: () => null);
    return accounts.firstWhereOrNull((account) => account.mode == mode);
  }

  /// Request events from server or get it from cache
  /// on force == true, we always request a list from server
  Future<ServerResponse> eventsByGroup(Account? account, Group group, {
    bool? force = false, bool includeAdminInfo = false, bool includeVotingCount = false, bool includeOpenVotings = false
  }) async {
    ServerResponse serverResponse = ServerResponse();
    if (groupEventsMap.containsKey(group.id) && force != null && !force && groupEventsMap[group.id]!.isNotEmpty) {
      serverResponse
          ..object = groupEventsMap[group.id]
          ..receivingData = false;
      return serverResponse;
    } else {
      ServerResponse serverResponse = await _sEventListRequest(
          account, group,
          includeVotingCount: includeVotingCount,
          includeAdminInfo: includeAdminInfo,
          includeOpenVotings: includeOpenVotings
      );
      groupEventsMap[group.id] = serverResponse.object;
      return serverResponse;
    }
  }

  Group? getGroupById(String? id) {
    if (id == null || id.isEmpty) {
      return null;
    }
    return serverGroups.firstWhereOrNull((element) => element.id == id);
  }

  Future<void> decryptAccountsUserId(List<int> pwdBytes) async {
    for (Account account in accounts) {
      await account.decryptUserId(pwdBytes);
    }
  }

  VotingEvent? getEventById(Group group, String eventId) {
    var list = groupEventsMap[group.id];
    Log.d(_TAG, "getEventById ($eventId) => ${list!.firstWhereOrNull((element) => element.id == eventId)}");
    return list.firstWhereOrNull((element) => element.id == eventId);
  }

  bool isValid() {
    return publicKeyX != null && publicKeyEd != null;
  }

  Future<Map<String, dynamic>> toMap(List<int> pwdBytes) async {
    /// accounts list
    List<dynamic> accountsList = [];
    for (var i = 0; i < accounts.length; i++) {
      accountsList.add(await accounts[i].toMap(false, pwdBytes));
    }
    return {URL: url, NAME: name, COMMUNITY: description, PUBLIC_KEY_CONCAT_B64: publicKeyConcatB64, ACCOUNTS: accountsList};
  }

  String get idHashCode {
    var _url = Uri.parse(url);
    // Log.d(_TAG, "idHashCode url: $url, host:${_url.host}, authority:${_url.authority}, scheme:${_url.scheme}");
    String hash = "${_url.scheme}-${_url.host}-$name".hashCode.toString();
    return hash;
  }

  Future<ServerResponse> getServerInfo({Account? account}) async {
    final messageMap = serverDetailsRequest(true, true);

    ServerResponse response = account == null
        ? await HttpController().serverRequest(messageMap, null, url)
        : await HttpController().serverRequestSigned(account, messageMap, this);

    // Log.d(_TAG, "response: ${response.body}");

    Map<String, dynamic>? bodyMap = response.bodyMap;

    /// STATUS CODE != 200
    if (response.statusCode != 200) {
      return response;
    }

    /// NO PUBLIC KEY
    if (!bodyMap!.containsKey("Public-Key")) {
      response.errorMessage = "Missing PublicKey";
      return response;
    }

    /// NO Server-Data
    if (!bodyMap.containsKey("Server-Data")) {
      response.errorMessage = "Wrong Server, can not talk with him";
      return response;
    }

    Map<String, dynamic> serverDataMap = bodyMap["Server-Data"];
    String pubKeyConcat = bodyMap["Public-Key"];

    Log.d(_TAG, "pubKeyConcat: $pubKeyConcat");
    publicKeyConcatB64 = pubKeyConcat.trim();
    createPublicKeysFromConcat();

    /// keys error
    if (publicKeyX == null || publicKeyEd == null) {
      response.errorMessage = "Can not create Public Keys";
      return response;
    }

    name = SUtils.mapValue(serverDataMap, "name", "NoNameDefined");
    description = SUtils.mapValue(serverDataMap, "community", "");
    keyManagerUrl = SUtils.mapValue(serverDataMap, "key_manager", null);

    /// SERVER GROUPS
    serverGroups.clear();
    _fillGroupList(bodyMap);

    return response;
  }


  Future<ServerResponse> _sEventListRequest(Account? account, Group group,
      {required bool includeAdminInfo, required bool includeVotingCount, required bool includeOpenVotings}) async {
    final _messageMap = eventListRequest(group, includeAdminInfo: includeAdminInfo, includeVotingCount: includeVotingCount, includeOpenVotings: includeOpenVotings);
    final response = await HttpController().serverRequest(_messageMap, account, url);
    List<VotingEvent> _events = [];

    final Map<String, dynamic>? bodyMap = response.bodyMap;

    if (response.statusCode == 200) {
      List<dynamic> eventsList = bodyMap!["Events"];
      for (var i = 0; i < eventsList.length; i++) {
        VotingEvent? event = VotingEvent.fromMap(eventsList[i], null, this);
        _events.add(event);
      }
    }
    response.object = _events;
    return response;
  }

  Future<ServerResponse> sGroupListRequest(Account? account, {bool includeAdminInfo = false}) async {
    final _messageMap = groupListRequest(includeAdminInfo);
    final ServerResponse response = await HttpController().serverRequest(_messageMap, account, url);

    final Map<String, dynamic>? bodyMap = response.bodyMap;
    _fillGroupList(bodyMap!);
    return response;
  }

  void _fillGroupList(Map<String, dynamic> bodyMap) {
    serverGroups.clear();

    if (bodyMap["Groups"] is List<dynamic>) {
      List<dynamic> groupList = bodyMap["Groups"];

      for (var i = 0; i < groupList.length; i++) {
        serverGroups.add(Group.fromMap(groupList[i]));
      }
    }
  }

  static Future<List<VotingServer>> createServerListFromAssetUrl() async {
    List<VotingServer> serverList = [];

    final String serverListData = await rootBundle.loadString(Constants.ASSET_SERVER_LIST_FILE_NAME);
      Log.d(_TAG, "serverData from asset: $serverListData");
      var data = json.decode(serverListData);

      final ServerResponse serverResponse = await HttpController().sendToServer(SUtils.mapValue(data, VotingServer.URL, "https://"), "");
      if(serverResponse.statusCode == 200 && serverResponse.bodyMap != null) {
        for(Map<String, dynamic> map in serverResponse.bodyMap!["Server-List"]) {
            VotingServer votingServer = VotingServer("https://${map["name"]}")
              ..name = SUtils.mapValue(map, "name", "NoNameDefined")
              ..description = SUtils.mapValue(map, "description", "")
              ..keyManagerUrl = SUtils.mapValue(map, "key_manager", null)
              ..isAssetServer = true;
            serverList.add(votingServer);
        }
      }
      return serverList;
  }



  // void _clearEventData() {
  //   accounts.forEach((account) {
  //
  //   });
  // }

  static const URL = "URL";
  static const NAME = "NAME";
  static const COMMUNITY = "COMMUNITY";
  static const IS_LOCAL = "IS_LOCAL";
  static const PUBLIC_KEY_X = "PUBLIC_KEY_X";
  static const PUBLIC_KEY_ED = "PUBLIC_KEY_ED";
  static const PUBLIC_KEY_CONCAT_B64 = "PUBLIC_KEY_CONCAT_B64";

  static const ACCOUNTS = "ACCOUNTS";
}


