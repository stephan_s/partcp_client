// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'package:collection/src/iterable_extensions.dart';
// import 'package:flutter/foundation.dart';
// import 'package:get/get.dart';
// import 'package:partcp_client/objects/voting_event.dart';
// import 'package:partcp_client/objects/wallet.dart';
// import 'package:partcp_client/utils/log.dart';
// import 'package:partcp_client/utils/yaml_converter.dart';
//
// import 'lot_code.dart';
//
// enum LotCodeFileError {
//   wrongEventId,
//   wrongFile,
//   corruptFile,
//   decryptionError,
//   encryptionError
// }
//
//
//
//
// class ComputeResultData {
//   int count = 0;
//   int imported = 0;
//   List<LotCode> lotList = [];
//   LotCodeFileError? error;
// }
// class ComputeContentData {
//   final String? content;
//   final List<LotCode> lotList;
//   final String eventId;
//   final bool checkDuplicates;
//   const ComputeContentData(this.content, this.eventId, this.lotList, this.checkDuplicates);
// }
//
//
//
// class LotCodeStorage {
//   static const _TAG = "LotCodeStorage";
//
//   final String eventId;
//   List<LotCode> lotList = [];
//   LotCodeFileError? error;
//
//   LotCodeStorage(this.eventId);
//
//   String errorToString (LotCodeFileError error) {
//     switch (error) {
//
//       case LotCodeFileError.wrongEventId:
//         return "Falsche Event-ID";
//       case LotCodeFileError.wrongFile:
//         return "Falsche Datei";
//       case LotCodeFileError.corruptFile:
//         return "Datei beschädigt";
//       case LotCodeFileError.decryptionError:
//         return "Kann die Datei nicht entschlüsseln";
//         break;
//       case LotCodeFileError.encryptionError:
//         return "Kann die Datei nicht verschlüsseln";
//         break;
//     }
//     return "n.a";
//   }
//
//
//   Future<String> toCsvEncrypted({
//     required Wallet wallet,
//     required VotingEvent event
//   }) async {
//     String content = await toCsvDecrypted(event.id);
//     return (await wallet.computeEncryptString(content))!;
//   }
//
//   Future<String> toCsvDecrypted(String eventId) async {
//     var contentData = ComputeContentData(null, eventId, lotList, false);
//     String lots = await compute(_computeExportAsCsvString, contentData);
//     return lots;
//   }
//
//   void addLotCodesFromServerResponse(String yamlString) {
//     Map<String, dynamic> lotMap = YamlConverter().toMap(yamlString);
//     List<dynamic> lotCodes = lotMap["codes"];
//
//     for (var lot in lotCodes) {
//       LotCode lotCode = LotCode(lotCode: lot);
//       lotList.add(lotCode);
//     }
//
//     /// performance test
//     var tmpOrigCode = lotCodes[0];
//     for (var i=0; i<10000; i++) {
//       String tmpCode = "$tmpOrigCode-$i";
//       LotCode lotCode = LotCode(lotCode: tmpCode);
//       lotList.add(lotCode);
//     }
//   }
//
//
//   static String _computeExportAsCsvString (ComputeContentData data) {
//     String lots = "";
//
//     String col =
//         CSV_ROW_0 + CSV_ROW_SPLITTER +
//             CSV_ROW_1 + CSV_ROW_SPLITTER +
//             data.eventId + "\n";
//     lots += col;
//
//     for (var i = 0; i<data.lotList.length; i++) {
//       col =
//           data.lotList[i].lotCode! + CSV_ROW_SPLITTER +
//               data.lotList[i].status.toString() + CSV_ROW_SPLITTER +
//               "''\n";
//       lots += col;
//     }
//     return lots;
//   }
//
//   static Future<LotCodeStorage> fromStorage({
//     required String content,
//     required String eventId,
//     // @required Wallet wallet,
//   }) async {
//     LotCodeStorage lotCodeStorage = LotCodeStorage(eventId);
//     List<LotCode> list = [];
//     Log.d(_TAG, "fromStorage");
//
//     ComputeContentData data = ComputeContentData(content, eventId, list, false);
//     ComputeResultData result = await compute(_computeFromCsvExportDecrypted, data);
//     lotCodeStorage.lotList.addAll(result.lotList);
//     return lotCodeStorage;
//   }
//
//
//   /// todo: check, if first line contains csv header, then string is decrypted
//   Future<void> importFromString({
//     required String content,
//     required String eventId,
//     required Wallet wallet,
//     required Function (int count, int imported, LotCodeFileError? error) onFinish,
//   }) async {
//
//     final String? decrypted = await wallet.computeDecryptString(content);
//     final String _content = decrypted != null
//         ? decrypted
//         : content;
//
//     ComputeContentData data = ComputeContentData(_content, eventId, lotList, true);
//
//     /// todo
//     /// Do we need compute on < 100 lotCodes?
//     // ComputeResultData result = GetPlatform.isWeb
//     //   ? _computeFromCsvExportDecrypted(data)
//     //   : await compute(_computeFromCsvExportDecrypted, data);
//
//     ComputeResultData result = await compute(_computeFromCsvExportDecrypted, data);
//
//     Log.d(_TAG, "importFromString(1); from CSV; result.lotList.length: ${result.lotList.length}; lotList.length: ${lotList.length}");
//     lotList.clear();
//     Log.d(_TAG, "importFromString(2); from CSV; result.lotList.length: ${result.lotList.length}; lotList.length: ${lotList.length}");
//     lotList.addAll(result.lotList);
//     Log.d(_TAG, "importFromString(3); from CSV; result.lotList.length: ${result.lotList.length}; lotList.length: ${lotList.length}");
//     onFinish(result.count, result.imported, result.error);
//   }
//
//   static ComputeResultData _computeFromCsvExportDecrypted(ComputeContentData data) {
//     // List<LotCode> tempLotList = [];
//     // tempLotList.addAll(data.lotList);
//
//     ComputeResultData computeResult = ComputeResultData()
//       ..lotList.addAll(data.lotList);
//
//     int imported = 0;
//
//     var contentSplitted = data.content!.trim().split("\n");
//
//
//     for (var i=0; i < contentSplitted.length; i++) {
//
//       String row = contentSplitted[i];
//       var rows = row.trim().split(CSV_ROW_SPLITTER);
//
//       String row0;
//       String? row1;
//       String? row2;
//
//       row0 = rows[0];
//       if (rows.length >= 2) {
//         row1 = rows[1];
//       }
//       if (rows.length >= 3) {
//         row2 = rows[2].trim();
//       }
//
//       /// eventId
//       if (i == 0) {
//         if (row1 == null || row2 == null) {
//           computeResult.error = LotCodeFileError.wrongFile;
//           return computeResult;
//         }
//         else{
//           Log.d(_TAG, "_computeFromCsvExportDecrypted; eventId ${data.eventId}; row2:eventId $row2");
//           if (row2 != data.eventId) {
//             computeResult.error = LotCodeFileError.wrongEventId;
//             return computeResult;
//           }
//         }
//       }
//
//       if (row0.isNotEmpty && row0 != CSV_ROW_0) {
//         LotCode? _lotCode;
//
//         if (data.checkDuplicates) {
//           _lotCode = computeResult.lotList.firstWhereOrNull((lotCode) => lotCode.lotCode == row0);
//         }
//
//         if (_lotCode == null) {
//           _lotCode = LotCode(lotCode: row0);
//           /// status
//           if (row1 != null && int.tryParse(row1) != null) {
//             _lotCode.status = int.tryParse(row1);
//           }
//           computeResult.lotList.add(_lotCode);
//           imported++;
//         }
//         // else {
//         //   tempLotList.add(LotCode.fromMap(_lotCode.toMap));
//         // }
//       }
//     }
//
//     return computeResult
//       ..count = contentSplitted.length-1
//       ..imported = imported;
//   }
//
//
//
//   static const CSV_ROW_SPLITTER = ";";
//   static const CSV_ROW_0 = "lot-code";
//   static const CSV_ROW_1 = "status";
//   static const CSV_ROW_2 = "event-id";
//
//   // static const EVENT_ID = "Event-Id";
//   static const LOTS = "LOTS";
//
// }