// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';

Future<bool> changeVotingStatusBox(
  BuildContext context,
  VotingServer server,
  Account account,
  VotingEvent event,
  Voting voting,
) async {
  String title;
  String text;

  if (voting.status == Voting.STATUS_IDLE) {
    title = "Abstimmung starten";
    text = "Die Abstimmung kann nach dem Start nicht mehr verändert werden."
        "\n\n"
        "Möchtest Du fortfahren?";
    return await _showAlertBox(context, title, text) == OkCancelResult.ok;
  }
  else if (voting.status == Voting.STATUS_OPEN) {
    title = "Abstimmung beenden";
    text = "Die Abstimmung wird beendet"
        "\n\n"
        "Möchtest Du fortfahren?";
    return await _showAlertBox(context, title, text) == OkCancelResult.ok;
  }
  else if (voting.status == Voting.STATUS_CLOSED) {
    // return await _showAlertBox(context, title, text) == OkCancelResult.ok;
    return true;
  }

  return false;
}

Future<OkCancelResult> _showAlertBox(BuildContext context, String title, String text) async {
  return await showOkCancelAlertDialog(
    context: context,
    title: title,
    message: text,
    okLabel: 'Ja',
    cancelLabel: "Nein",
    barrierDismissible: true,
  );
}
