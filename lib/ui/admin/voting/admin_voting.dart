// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:partcp_client/constants.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/message_type/voting_definition.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/client_data.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/voting_type.dart';
import 'package:partcp_client/ui/admin/admin_markdown_edit.dart';
import 'package:partcp_client/ui/admin/controller/admin_controller.dart';
import 'package:partcp_client/utils/file_action.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/utils/save_file.dart';
import 'package:partcp_client/utils/yaml_converter.dart';
import 'package:partcp_client/widgets/alert_dialog.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_icon.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/utils/mark_down_to_html.dart';
import 'package:partcp_client/widgets/submit_button.dart';
import 'package:partcp_client/widgets/text_label.dart';
import 'package:partcp_client/widgets/widgets.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/text_field_widget.dart';

import 'voting_change_status.dart';
import '../../../widgets/markdown_view.dart';

const int TITLE_LENGTH = 200;
const int SHORT_DESCRIPTION_LENGTH = 500;

const int OPTION_TITLE_LENGTH = 200;
const int OPTION_SHORT_DESCRIPTION_LENGTH = 2000;
const int OPTION_LINK_NAME_LENGTH = 50;
const int OPTION_LINK_URL_LENGTH = 200;

enum _MenuItems {
  importVoting,
  exportVoting,
}

const _TAG = "AdminVoting";

class AdminVotingController extends GetxController {
  final String _TAG = "AdminVotingController";

  ServerResponse serverResponse = ServerResponse();
  static const votingNameError = "Diese Überschrift existiert bereits";

  late VotingServer server;
  late VotingEvent event;
  late Voting voting;
  String origVotingHash = "";
  late Account account;
  bool editMode = false;

  late Key scrollBodyKey;
  String? votingNameErrorText = null;

  TextEditingController titleController = TextEditingController();
  TextEditingController shortDescriptionController = TextEditingController();

  FocusNode typeFocus = FocusNode();

  bool get votingNameExists {
    for (Voting voting in event.votings) {
      if (voting.title == this.voting.title && voting != this.voting) {
        return true;
      }
    }
    return false;
  }

  bool get allowSave =>
      titleController.text.isNotEmpty && (voting.type != null && voting.type!.isNotEmpty) && voting.votingOptions.isNotEmpty && !votingNameExists;

  bool get allowChangeStatus =>
      AdminController.allow(EditType.votingChangeStatus, event) && AdminController.allow(EditType.votingChangeStatus, voting);

  bool get allowEdit => AdminController.allow(EditType.votingEdit, event) && AdminController.allow(EditType.votingEdit, voting);

  /// list of required fields
  List<Widget> get requiredFieldsWidget {
    List<Widget> _list = [];
    _list.add(Text("(*) Felder mit einem Stern dürfen nicht leer bleiben"));
    /*
    /// title
    if (titleController.text.isEmpty) {
      _list.add(Text("(*) Überschrift darf nicht leer sein"));
    }

    /// type
    if (voting.type == null || (voting.type != null && voting.type!.isEmpty)) {
      _list.add(Text("(*) Abstimmungs-Typ darf nicht leer sein"));
    }

    /// optionsList
    if (voting.votingOptions.isEmpty) {
      _list.add(Text("(*) Mindestens eine Abstimmbare Positionen muss definiert werden"));
    }
    */
    return _list;
  }

  void initController(VotingServer server, Account account, VotingEvent event, Voting? voting) {
    this.server = server;
    this.account = account;
    this.event = event;
    this.voting = voting == null ? Voting() : voting;
    origVotingHash = this.voting.hash;

    titleController.text = this.voting.title;
    shortDescriptionController.text = this.voting.clientData.shortDescription;

    editMode = AdminController.allow(EditType.votingEdit, event) && this.voting.id == null ? true : false;
    scrollBodyKey = ValueKey("admin_voting;" + DateTime.now().millisecondsSinceEpoch.toString());
  }

  // Future<void> onPostFrameCallback() async {
  //   // if (voting != null) {
  //   //   await _requestVotingDetail();
  //   //   updateMe();
  //   // }
  // }

  Future<void> onImportVoting() async {
    String? votingYaml = (await FileActions.textFileFromExternalStorage(
        pageTitle: "Voting Import", initialDirectory: (await Constants.appExportDir)));

    if (votingYaml != null) {
      Voting? _voting = Voting.fromYamlString(votingYaml);
      if (_voting != null) {
        voting = _voting;
        voting.id = null;
        titleController.text = voting.title.trim();
        shortDescriptionController.text = voting.clientData.shortDescription.trim();
        onVotingTitleChanged();
        updateMe();
      }
    }
  }

  Future<void> onExportVoting() async {
    Log.d(_TAG, "onExportVoting()");

    Map<String, dynamic> exportMap = votingDefinition(event: event, voting: voting);
    String exportYaml = YamlConverter().toYaml(exportMap);

    Log.d(_TAG, "onExportVoting; yaml: \n$exportYaml");
    _exportVoting(exportYaml);
  }

  Future<void> _exportVoting(String yamlString) async {
    String fileName = Constants.EXPORT_VOTING_FILE_NAME.replaceAll("{e}", "${event.id}");
    fileName = fileName.replaceAll("{d}", "${DateFormat(MainController.FILE_DATE_FORMAT).format(DateTime.now())}");

    SaveFile saveFile =
        SaveFile(saveInExternalStorage: true, content: yamlString, fileName: fileName, subDir: Constants.EXPORT_DIR_NAME);
    await saveFile.save();

    final snackBar = SnackBar(
      content: Text('Die Datei wurde gespeichert in: ${saveFile.filePathLazy}'),
      duration: Constants.SNACK_BAR_DURATION_LONG,
    );
    ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);
  }

  void onVotingTitleChanged() {
    voting.title = titleController.text.trim();
    votingNameErrorText = votingNameExists
        ? votingNameError
        : null;
    updateMe();
  }

  void onVotingShortDescriptionChanged() {
    voting.clientData.shortDescription = shortDescriptionController.text;
    updateMe();
  }

  void onVotingTypeChanged(String votingType) {
    voting.type = votingType;
    updateMe();
  }

  Future<void> onVotingDescriptionEdit() async {
    String? _result = await Get.to(() => AdminMarkdownEdit(
        // title: "${event.name} - ${voting.title}",
        title: "${voting.title}",
        text: voting.clientData.description));

    if (_result != null) {
      Log.d(_TAG, "onVotingDescriptionEdit => result: $_result");
      voting.clientData.description = _result;
    }
    updateMe();
  }

  void onAddVotingOption() {
    voting.votingOptions.add(VotingOption(
      uuid: VotingOption.createUuid(),
      edit: true,
      clientData: ClientData(),
    ));
    updateMe();
  }

  void onVotingOptionChanged(VotingOption option, bool isSubmit,
      {required String title, required String shortDescription, String linkName = "", String linkUrl = ""}) {
    Log.d(
        _TAG,
        "onVotingOptionSubmit; "
        "title: $title "
        "shortDescription: $shortDescription "
        "linkName: $linkName "
        "linkUrl: $linkUrl");

    linkUrl.trim();
    if (linkUrl == "http://" || linkUrl == "https://") {
      linkUrl = "";
    }

    option
      ..name = title.trim()
      ..clientData.shortDescription = shortDescription.trim()
      ..clientData.linkName = linkName.trim()
      ..clientData.linkUrl = linkUrl
      ..edit = !isSubmit;

    /// delete it, if empty
    if (isSubmit) {
      if (option.name.isEmpty && option.clientData.shortDescription.isEmpty) {
        voting.votingOptions.remove(option);
      }
      updateMe();
    }
  }

  void onVotingOptionReorder(int oldIndex, int newIndex) {
    if (!editMode) return;

    if (newIndex > oldIndex) {
      newIndex -= 1;
    }
    final VotingOption item = voting.votingOptions.removeAt(oldIndex);
    voting.votingOptions.insert(newIndex, item);
    updateMe();
  }

  void onVotingOptionStartEdit(VotingOption option) {
    option.edit = true;
    updateMe();
  }

  Future<void> onVotingOptionDelete(VotingOption option) async {
    /// ask to delete
    final result = await showOkCancelAlertDialog(
      context: Get.context!,
      message: 'Abstimmungs-Option löschen?',
      okLabel: 'Ja',
      cancelLabel: 'Abbrechen',
      barrierDismissible: true,
    );

    if (result == OkCancelResult.ok) {
      voting.votingOptions.remove(option);
      Log.d(_TAG, "onVotingOptionDelete; options len: ${voting.votingOptions.length}");
      updateMe();
    }
  }

  /// DATE
  Future<DateTime?> _pickDate(bool isStartDate) async {
    Log.d(_TAG, "onDateOpen; isStartDate:$isStartDate");

    //final DateTime _now = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
    final DateTime _now = DateTime.now();
    final DateTime _lastPossible = DateTime((_now.year) + 10);

    final DateTime? votingDate = isStartDate ? voting.periodStart : voting.periodEnd;

    // DateTime initialDate = votingDate != null ? votingDate : DateTime(_now.year, _now.month, _now.day + 1);
    DateTime initialDate;

    DateTime firstDate;
    DateTime lastDate;

    /// start date
    if (isStartDate) {
      initialDate = votingDate != null ? votingDate : DateTime(_now.year, _now.month, _now.day + 1);
      firstDate = _now;
      lastDate = voting.periodEnd != null ? voting.periodEnd! : _lastPossible;
    }

    /// end date
    else {
      if (votingDate != null) {
        initialDate = votingDate;
      } else {
        if (voting.periodStart != null) {
          initialDate = DateTime(voting.periodStart!.year, voting.periodStart!.month, voting.periodStart!.day + 1);
        } else {
          initialDate = DateTime(_now.year, _now.month, _now.day + 1);
        }
      }

      firstDate = voting.periodStart != null ? voting.periodStart! : _now;
      lastDate = _lastPossible;
    }

    var pickerDate =
        await showDatePicker(context: Get.context!, initialDate: initialDate, firstDate: firstDate, lastDate: lastDate);

    if (pickerDate == null) return votingDate!;

    TimeOfDay initialTime =
        votingDate != null ? TimeOfDay(hour: votingDate.hour, minute: votingDate.minute) : TimeOfDay(hour: 0, minute: 0);

    Log.d(_TAG, "initialTime: ${initialTime}");

    var time = await showTimePicker(context: Get.context!, initialTime: initialTime, initialEntryMode: TimePickerEntryMode.dial);

    if (time == null) {
      return null;
    }

    DateTime result = DateTime(pickerDate.year, pickerDate.month, pickerDate.day, time.hour, time.minute);

    if (result.difference(DateTime.now()) < Duration(minutes: 1)) {
      Log.d(_TAG, "_pickDate; show alert ");
      return null;
    }

    return DateTime(pickerDate.year, pickerDate.month, pickerDate.day, time.hour, time.minute);
  }

  Future<void> onDateChanged(DateTime? date, bool isStartDate) async {
    if (date != null) {
      /// set null
      if (date.year == 2000) {
        date = null;
      }

      /// pick new
      else if (date.year == 3000) {
        date = await _pickDate(isStartDate);
      }

      if (isStartDate) {
        voting.periodStart = date;
      } else {
        voting.periodEnd = date;
      }
      updateMe();
    }
  }

  List<DateTime> getVotingDates(bool isStartDate) {
    List<DateTime> list = [];
    DateTime now = DateTime.now();
    DateTime? _testDate;

    event.votings.forEach((thisVoting) {
      _testDate = _addVotingDateToList(isStartDate, list, thisVoting, now);
      if (_testDate != null) {
        list.add(_testDate!);
      }
    });

    /// new voting isn't in votings
    if (voting.id == null) {
      _testDate = _addVotingDateToList(isStartDate, list, voting, now);
      if (_testDate != null) {
        list.add(_testDate!);
      }
    }

    list.sort((DateTime a, DateTime b) => a.compareTo(b));
    return list;
  }

  DateTime? _addVotingDateToList(bool isStartDate, List<DateTime> list, Voting thisVoting, DateTime now) {
    if (isStartDate &&
        thisVoting.periodStart != null &&
        !list.contains(thisVoting.periodStart) &&
        thisVoting.periodStart!.compareTo(now) > 0) {
      return thisVoting.periodStart!;
    } else if (!isStartDate &&
        thisVoting.periodEnd != null &&
        !list.contains(thisVoting.periodEnd) &&
        thisVoting.periodEnd!.compareTo(now) > 0) {
      return thisVoting.periodEnd!;
    }
    return null;
  }

  void onDateClear(bool isStartDate) {
    if (isStartDate) {
      voting.periodStart = null;
    } else {
      voting.periodEnd = null;
    }
    updateMe();
  }

  void setEditMode(bool edit) {
    editMode = edit;
    updateMe();
  }

  Future<void> requestVotingDetails() async {
    serverResponse.receivingData = true;
    updateMe();

    serverResponse = await voting.sVotingDetailRequest(server, account, event);
    if (serverResponse.errorMessage == null && serverResponse.object != null) {
      voting = serverResponse.object;
      origVotingHash = voting.hash;
    }
    updateMe();
  }

  Future<dynamic> onVotingSave(bool goBack) async {
    serverResponse.errorMessage = null;

    Log.d(_TAG, "onVotingSave; options len: ${voting.votingOptions.length}");

    AdminController.to.forceUpdateEvents = true;
    AdminController.to.forceUpdateVotings = true;

    /// add id and remove empty option
    List<VotingOption> newList = [];
    for (VotingOption option in voting.votingOptions) {
      option.name.trim();
      if (option.name.isNotEmpty) {
        option.id = newList.length.toString();
        newList.add(option);
      }
    }
    voting.votingOptions = newList;

    voting
      ..title = titleController.text.trim()
      ..name = titleController.text.replaceAll(" ", "_")
      ..clientData.shortDescription = shortDescriptionController.text.trim();

    if (voting.votingOptions.isEmpty || voting.title.isEmpty) {
      return;
    }

    serverResponse.receivingData = true;
    updateMe();
    serverResponse = await voting.sendDefinitionOrUpdateRequest(server, account, event);

    if (!goBack) {
      origVotingHash = this.voting.hash;
    }

    if (serverResponse.statusCode != 200 || !goBack) {
      updateMe();
    } else {
      Get.back(result: true);
    }
  }

  /// SEND SERVER REQUEST TO CHANGE STATUS
  void onVotingStatusChang() async {
    bool result = await changeVotingStatusBox(Get.context!, server, account, event, voting);
    if (result) {
      serverResponse.receivingData = true;
      updateMe();

      serverResponse = await voting.sendStatusChangeRequest(server, event, account);
      updateMe();

      if (serverResponse.errorMessage != null) {
        await alertDialogErrorBox(Get.context!, serverResponse.errorMessage!);
      } else {
        await requestVotingDetails();
      }
    }
    AdminController.to.forceUpdateEvents = true;
    AdminController.to.forceUpdateVotings = true;
  }

  Future<bool> onWillPop() async {
    /// check, if and votingOptions are editing
    bool _allowPop = true;
    List<VotingOption> _options = [];

    try {
      for (VotingOption option in voting.votingOptions) {
        if (option.edit) {
          option.edit = false;
          _allowPop = false;
        }

        /// remove empty options
        if (option.name.isNotEmpty) {
          _options.add(option);
        }
      }

      if (!_allowPop) {
        /// save none empty options
        voting.votingOptions = _options;
        updateMe();
        return false;
      } else {
        return await _discard();
      }
    } catch (e) {
      Log.e(_TAG, "onWillPop; error: $e");
      return true;
    }
  }

  Future<bool> _discard() async {
    if (voting.hash == origVotingHash) {
      return true;
    }

    /// discard
    final result = await showOkCancelAlertDialog(
      context: Get.context!,
      message: 'Änderungen verwerfen?',
      okLabel: 'Ja',
      cancelLabel: "Nein",
      barrierDismissible: false,
    );
    return result == OkCancelResult.ok;
  }

  void updateMe() {
    update();
  }
}

class AdminVoting extends StatelessWidget {
  final VotingServer server;
  final Account account;
  final VotingEvent event;
  final Voting? voting;

  AdminVoting({required this.server, required this.account, required this.event, this.voting});

  final AdminVotingController controller = Get.put(AdminVotingController());
  final ScrollController scrollController = ScrollController();

  void _handleMenuClick(_MenuItems value) {
    switch (value) {
      case _MenuItems.importVoting:
        controller.onImportVoting();
        break;
      case _MenuItems.exportVoting:
        controller.onExportVoting();
        break;
    }
  }

  List<PopupMenuItem<_MenuItems>> _popupMenuItems() {
    List<PopupMenuItem<_MenuItems>> list = [];

    if (controller.allowEdit) {
      list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.importVoting, child: Text("Abstimmung importieren")));
    }
    list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.exportVoting, child: Text("Abstimmung exportieren")));
    return list;
  }

  @override
  Widget build(BuildContext context) {
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   controller.onPostFrameCallback();
    // });

    return WillPopScope(
      onWillPop: () => controller.onWillPop(),
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<AdminVotingController>(
            initState: (_) => controller.initController(server, account, event, voting),
            builder: (_) => Scaffold(
                appBar: MyAppBar(
                  title: Text("Abstimmung"),
                  actions: [
                    /// save
                    /// controller.onVotingSave()
                    controller.allowEdit && controller.editMode
                        ? IconButton(
                            icon: Icon(Icons.save_outlined),
                            onPressed: controller.allowSave ? () => controller.onVotingSave(false) : null)
                        : Container(),

                    /// refresh
                    !controller.editMode
                        ? IconButton(icon: Icon(Icons.refresh), onPressed: () => controller.requestVotingDetails()
                            // .then((value) => controller.updateMe()),
                            )
                        : Container(),

                    /// edit
                    controller.allowEdit && !controller.editMode
                        ? IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () => controller.setEditMode(true),
                          )
                        : Container(),

                    /// SETTINGS
                    PopupMenuButton<_MenuItems>(
                        icon: Icon(Icons.menu),
                        onSelected: _handleMenuClick,
                        itemBuilder: (BuildContext context) => _popupMenuItems()),
                  ],
                ),
                body: Widgets.loaderBox(
                    loading: controller.serverResponse.receivingData,
                    child: ScrollBody(
                      key: controller.scrollBodyKey,
                      children: _body,
                      enabled: true,
                    )))),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(ServerErrorMessage(key: ValueKey(DateTime.now()), serverResponse: controller.serverResponse));

    list.add(CardWidget(
        titleListTile: CardWidget.listTileTitle(
      leading: Icon(Icons.event),
      title: controller.event.name,
      subtitle: Text(SUtils.formatDate(controller.event.date)),
    )));

    if (!controller.editMode) {
      list.add(_status);
    }

    list.add(_mainData);

    // list.add(_type);
    list.add(RepaintBoundary(child: _description));
    list.add(RepaintBoundary(child: _votingOptions));

    list.add(_requiredFields);

    list.add(_saveBtn);

    return list;
  }

  /// main data
  Widget get _mainData => CardWidget(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _name,
            _shortDescription,
            _type,
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: _dateWidget,
            ),
          ],
        ),
      );

  Widget get _type => TextLabel(
      label: "Art der Abstimmung $_required",
      text: controller.editMode ? null : controller.voting.typeTitle,
      textIsBold: true,
      child: controller.editMode
          ? DropdownButton<String>(
              value: controller.voting.type,
              items: _votingTypeList,
              onChanged: (type) => controller.onVotingTypeChanged(type!),
              // itemHeight: controller.dropDownMenuSize.height,
              isExpanded: true,
            )
          : null);

  /// Name
  Widget get _name => TextLabel(
      label: "Überschrift $_required",
      labelIsBold: true,
      text: controller.titleController.text,
      textIsBold: true,
      child: controller.editMode
          ? TextFieldWidget(
              textController: controller.titleController,
              // labelText: "Name  $_required",
              // hint: "Name $_required",
              hint: "Überschrift der Abstimmung",
              onSubmitted: (_) => controller.onVotingTitleChanged(),
              onChanged: (_) => controller.onVotingTitleChanged(),
              onTap: () => {},
              autoCorrect: true,
              isPasswordField: false,
              keyboardType: TextInputType.text,
              textAlign: TextAlign.start,
              maxBoxLength: null,
              errorText: controller.votingNameErrorText,
              maxLength: TITLE_LENGTH,
              maxLengthEnforcement: MaxLengthEnforcement.enforced,
              enabled: controller.editMode,
              readOnly: !controller.editMode,
              bold: true,
              showBorder: controller.editMode,
            )
          : null);

  Widget get _dateWidget {
    return ButtonBar(
      alignment: MainAxisAlignment.spaceBetween,
      buttonPadding: EdgeInsets.only(left: 0),
      children: [
        _dateRow(true),
        _dateRow(false),
      ],
    );
  }

  Widget _dateRow(bool isStartDate) {
    DateTime? date = isStartDate ? controller.voting.periodStart : controller.voting.periodEnd;

    String dateString = date != null ? _dateString(date) : "-/-";

    return TextLabel(
      label: isStartDate ? "Automatisch starten" : "Automatisch beenden",
      text: controller.editMode ? null : dateString,
      textIsBold: false,
      child: controller.editMode ? _dateDropdownButton(isStartDate) : null,
    );
  }

  String _dateString(DateTime date) {
    return SUtils.formatDateTimeShort(date);
    // return date.hour > 0 || date.minute > 0 ? SUtils.formatDateTimeShort(date) : SUtils.formatDateShort(date);
  }

  Widget _dateDropdownButton(bool isStartDate) {
    final DateTime? thisDate = isStartDate ? controller.voting.periodStart : controller.voting.periodEnd;

    List<DropdownMenuItem<DateTime>> dateList = [];

    controller.getVotingDates(isStartDate).forEach((date) {
      dateList.add(_dateDropdownButtonItem(date));
    });

    /// delete date
    if (thisDate != null) {
      dateList.add(_dateDropdownButtonItem(DateTime(2000)));
    }

    /// add date
    dateList.add(_dateDropdownButtonItem(DateTime(3000)));

    /// on EventImport we must we must add this date
    bool dateInList = false;
    for (DropdownMenuItem<DateTime> element in dateList) {
      if (element.value == thisDate) {
        dateInList = true;
        break;
      }
    }
    if (!dateInList) {
      dateList.insert(0, _dateDropdownButtonItem(thisDate));
    }

    return DropdownButton<DateTime>(
      value: isStartDate ? controller.voting.periodStart : controller.voting.periodEnd,
      onChanged: (DateTime? newValue) => controller.onDateChanged(newValue, isStartDate),
      items: dateList,
    );
  }

  DropdownMenuItem<DateTime> _dateDropdownButtonItem(DateTime? date) {
    String dateString;
    if (date == null) {
      dateString = "";
    } else if (date.year == 2000) {
      dateString = "Entfernen";
    } else if (date.year == 3000) {
      dateString = "Neues Datum";
    } else {
      dateString = _dateString(date);
    }

    return DropdownMenuItem<DateTime>(
      value: date,
      child: Text(dateString),
    );
  }

  /// short description
  Widget get _shortDescription => TextLabel(
      label: "Zusatzinfo",
      text: controller.shortDescriptionController.text,
      child: controller.editMode
          ? TextFieldWidget(
              textController: controller.shortDescriptionController,
              hint: "Kurze Zusatzinformationen",
              minLines: 1,
              maxLines: 10,
              onSubmitted: (_) => controller.onVotingShortDescriptionChanged(),
              onChanged: (_) => controller.onVotingShortDescriptionChanged(),
              onTap: () => {},
              autoCorrect: true,
              keyboardType: TextInputType.text,
              maxBoxLength: null,
              errorText: null,
              textCapitalization: TextCapitalization.sentences,
              maxLength: SHORT_DESCRIPTION_LENGTH,
              maxLengthEnforcement: MaxLengthEnforcement.enforced,
              textInputAction: TextInputAction.done,
              enabled: controller.editMode,
              readOnly: !controller.editMode,
              showBorder: controller.editMode,
            )
          : null);

  /// description
  Widget get _description {
    double scrollbarHeight;
    if (controller.voting.clientData.description.isEmpty) {
      scrollbarHeight = 0;
    } else {
      scrollbarHeight = controller.voting.clientData.description.split("\n").length > 15 ? 300 : 150;
    }

    return CardWidget(
      titleListTile: CardWidget.listTileTitle(
          title: TextLabel.formatLabel(label: "Hintergrundinformationen"),
          trailing: controller.editMode

              /// edit
              ? ButtonIcon(
                  icon: Icon(Icons.edit),
                  onPressed: () => controller.onVotingDescriptionEdit(),
                )

              /// view
              : ButtonIcon(
                  icon: Icon(Icons.preview),
                  onPressed: () => Get.to(
                      () => MarkdownView(
                          title: "${controller.voting.title}",
                          markdownText: controller.voting.clientData.description)),
                )),
      child: Container(
        constraints: BoxConstraints(minHeight: 0, maxHeight: scrollbarHeight),
        decoration: BoxDecoration(border: Border.all(width: scrollbarHeight == 0 ? 0.5 : 1, color: Colors.black26)),
        child: Scrollbar(
          isAlwaysShown: true,
          controller: scrollController,
          child: ListView(
            controller: scrollController,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: HtmlWidget(
                  markdownToHtml(controller.voting.clientData.description),
                  textStyle: TextStyle(color: Colors.black87),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> get _votingTypeList {
    List<DropdownMenuItem<String>> list = [];

    for (String _type in VotingType.values) {
      list.add(DropdownMenuItem<String>(
          value: _type,
          child: Container(
            constraints: BoxConstraints(minWidth: 100, maxWidth: Get.width * 80 / 100),
            child: ListTile(
              title: Text(VotingType.title(_type)),
              selected: controller.voting.type == _type,
            ),
          )));
    }
    return list;
  }

  /// VOTING OPTIONS
  Widget get _votingOptions => CardWidget(
      titleListTile: CardWidget.listTileTitle(
          // leading: Icon(Icons.opt),
          title: "Abstimmbare Positionen $_required",
          trailing: controller.editMode

              /// edit
              ? ButtonIcon(
                  icon: Icon(Icons.add_circle_outline),
                  onPressed: () => controller.onAddVotingOption(),
                )

              /// view
              : Container(
                  width: 0,
                  height: 0,
                )),
      child: _votingOptionListView);

  /// VOTING OPTIONS LIST-VIEW
  Widget get _votingOptionListView {
    return controller.editMode
        ? Column(
            children: [
              ReorderableListView.builder(
                shrinkWrap: true,
                // anchor: 1,
                itemCount: controller.voting.votingOptions.length,
                itemBuilder: (_, int index) => _votingOptionItem(index),
                onReorder: (int oldIndex, int newIndex) => controller.onVotingOptionReorder(oldIndex, newIndex),
                physics: NeverScrollableScrollPhysics(),
              ),

              /// add + button on the bottom of options list
              controller.voting.votingOptions.length > 0
                  ? Align(
                      alignment: Alignment.centerRight,
                      child: ButtonIcon(
                        icon: Icon(Icons.add_circle_outline),
                        onPressed: () => controller.onAddVotingOption(),
                      ),
                    )
                  : Container()
            ],
          )
        : ListView.builder(
            shrinkWrap: true,
            // anchor: 1,
            itemCount: controller.voting.votingOptions.length,
            itemBuilder: (_, int index) => _votingOptionItem(index),
            physics: NeverScrollableScrollPhysics(),
          );
  }

  /// VOTING-OPTION ITEM
  Widget _votingOptionItem(int index) {
    VotingOption option = controller.voting.votingOptions[index];
    final double labelFontSize = 22;
    Widget w;

    /// on web we have a drug-icon from ReorderableListView, so we need extra space
    final double horizontalPadding = MainController.to.isDesktopOrWeb ? 50 : 0;

    TextEditingController _titleController = TextEditingController(text: option.name);
    TextEditingController _shortDescController = TextEditingController(text: option.clientData.shortDescription);
    TextEditingController _linkNameController = TextEditingController(text: option.clientData.linkName);
    TextEditingController _linkUrlController =
        TextEditingController(text: option.clientData.linkUrl.isEmpty ? "https://" : option.clientData.linkUrl);

    /// TITLE
    Widget _optionTitleField = Padding(
        padding: EdgeInsets.only(right: horizontalPadding, top: 10),
        child: TextFieldWidget(
          key: ValueKey("${option.hashCode}_t"),
          textController: _titleController,
          labelText: "Titel",
          hint: "Titel",
          maxLines: 10,
          maxBoxLength: null,
          fontSize: 16,
          onChanged: (text) => controller.onVotingOptionChanged(option, false,
              title: _titleController.text, shortDescription: _shortDescController.text, linkUrl: _linkUrlController.text),
          autoCorrect: true,
          keyboardType: TextInputType.text,
          textCapitalization: TextCapitalization.sentences,
          maxLength: option.edit ? OPTION_TITLE_LENGTH : null,
          maxLengthEnforcement: option.edit ? MaxLengthEnforcement.enforced : null,
          textInputAction: TextInputAction.done,
          bold: true,
          readOnly: !option.edit,
          enabled: option.edit,
          showBorder: option.edit,
          labelFontSize: labelFontSize,
        ));

    /// SHORT DESCRIPTION
    Widget _optionShortDescrField = Padding(
      padding: EdgeInsets.only(right: horizontalPadding, top: 10),
      child: TextFieldWidget(
        key: ValueKey("${option.hashCode}_d"),
        textController: _shortDescController,
        labelText: "Beschreibung",
        hint: "Beschreibung",
        maxLines: 30,
        maxBoxLength: null,
        onChanged: (text) => controller.onVotingOptionChanged(option, false,
            title: _titleController.text, shortDescription: _shortDescController.text, linkUrl: _linkUrlController.text),
        autoCorrect: true,
        keyboardType: TextInputType.multiline,
        textCapitalization: TextCapitalization.sentences,
        maxLength: option.edit ? OPTION_SHORT_DESCRIPTION_LENGTH : null,
        maxLengthEnforcement: option.edit ? MaxLengthEnforcement.enforced : null,
        textInputAction: TextInputAction.newline,
        readOnly: !option.edit,
        enabled: option.edit,
        showBorder: option.edit,
        labelFontSize: labelFontSize,
      ),
    );

    /// LINK URL
    Widget _optionLinkUrl = Padding(
      padding: EdgeInsets.only(right: horizontalPadding, top: 10),
      child: TextFieldWidget(
        textController: _linkUrlController,
        labelText: "Webseite mit zusätzlichen Informationen",
        hint: "Webseite mit zusätzlichen Informationen",
        minLines: 1,
        maxLines: 3,
        maxBoxLength: null,
        onChanged: (text) => controller.onVotingOptionChanged(option, false,
            title: _titleController.text, shortDescription: _shortDescController.text, linkUrl: _linkUrlController.text),
        autoCorrect: false,
        keyboardType: TextInputType.url,
        textCapitalization: TextCapitalization.none,
        maxLength: option.edit ? OPTION_LINK_URL_LENGTH : null,
        maxLengthEnforcement: option.edit ? MaxLengthEnforcement.enforced : null,
        textInputAction: TextInputAction.done,
        readOnly: !option.edit,
        enabled: option.edit,
        showBorder: option.edit,
        labelFontSize: labelFontSize,
      ),
    );

    Widget _urlBox = Wrap(
      direction: Axis.horizontal,
      spacing: 16,
      children: [_optionLinkUrl],
    );

    Widget _buttonsBoxEdit = Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        ButtonIcon(
          icon: Icon(
            Icons.check_circle_outline,
            color: Colors.green,
          ),
          onPressed: () => controller.onVotingOptionChanged(option, true,
              title: _titleController.text,
              shortDescription: _shortDescController.text,
              linkName: _linkNameController.text,
              linkUrl: _linkUrlController.text),
        )
      ],
    );

    Widget _buttonsBoxView = Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        ButtonIcon(
          icon: Icon(
            Icons.delete_forever_outlined,
            color: Colors.red,
          ),
          onPressed: () => controller.onVotingOptionDelete(option),
        ),
        ButtonIcon(
          icon: Icon(Icons.edit),
          onPressed: () => controller.onVotingOptionStartEdit(option),
        ),
      ],
    );

    List<Widget> _children;

    /// OPTION EDIT
    if (option.edit) {
      _children = [_optionTitleField, _optionShortDescrField, _urlBox];
    }

    /// OPTION VIEW
    else {
      _children = [_optionTitleField];
      if (option.clientData.shortDescription.isNotEmpty) {
        _children.add(_optionShortDescrField);
      }
      if (option.clientData.linkUrl.isNotEmpty) {
        _children.add(_urlBox);
      }
    }

    /// PAGE EDIT MODE
    if (controller.editMode) {
      _children.add(
          Padding(padding: EdgeInsets.only(right: horizontalPadding), child: option.edit ? _buttonsBoxEdit : _buttonsBoxView));
    }

    w = Padding(
      padding: const EdgeInsets.symmetric(vertical: 6),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(
              color: option.edit ? Colors.blueAccent : Colors.black26,
            ),
            borderRadius: BorderRadius.circular(6)),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6),
          child: Column(
            children: _children,
          ),
        ),
      ),
    );

    return Stack(
      key: Key(option.uuid),
      children: [
        w,
        Align(
          alignment: Alignment.topRight,
          child: _optionNr(index + 1),
        )
      ],
    );
  }

  /// REQUIRED
  Widget get _requiredFields {
    return controller.editMode

        /// edit
        ? CardWidget(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: controller.requiredFieldsWidget,
            ),
          )

        /// view
        : Container();
  }

  Widget get _status => CardWidget(
        child: TextLabel(
          label: "Status",
          child: ListTile(
            title: Text(controller.voting.statusText),
            leading: IconButton(
              icon: controller.voting.statusIcon,
              onPressed: controller.allowChangeStatus ? () => controller.onVotingStatusChang() : null,
            ),
          ),
        ),
      );

  /// SAVE BUTTON
  Widget get _saveBtn {
    /// edit
    if (controller.editMode) {
      return CardWidget(
        child: SubmitButton(
            submitting: controller.serverResponse.receivingData,
            onPressed: controller.allowSave ? () => controller.onVotingSave(true) : null,
            text: controller.voting.id == null ? "Abstimmung einrichten" : "Abstimmung ändern"),
      );
    }

    /// view
    else {
      return Container();
    }
  }

  Widget _optionNr(int index) {
    return CircleAvatar(
      radius: 10,
      // foregroundColor: Colors.black54,
      child: Center(
        child: Text(
          "$index",
          style: TextStyle(color: Colors.white, fontSize: 12),
        ),
      ),
    );
  }

  String get _required => controller.editMode ? " (*)" : "";
}
