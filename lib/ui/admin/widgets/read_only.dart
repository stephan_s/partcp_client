// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';

Widget readOnly({required Widget title, required bool allowModify}) {
  return allowModify
      ? title
      : _readOnly(title);
}

Widget _readOnly(Widget title) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      title,
      Text(
        "(Schreibgeschützt)",
        textScaleFactor: 0.9,
        style: TextStyle(color: Colors.red),
      )
    ],
  );
}


