// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';

Color get dividerColor => Colors.black26;
Color get waitingColor => Colors.green.withOpacity(1);

Widget get listDivider => Divider(
  thickness: 1,
  color: dividerColor,
);

