// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

final tableColumnWidth = {0: FractionColumnWidth(.7), 1: FractionColumnWidth(.3)};
final defaultVerticalAlignment = TableCellVerticalAlignment.middle;

Widget tableTextField({
  required Function onChanged,
  required TextEditingController controller,
  required int maxLen,
  required bool isInt,
  required bool administrable,
  Function? onSubmitted,
}) {
  return Padding(
    padding: const EdgeInsets.only(top: 16),
    child: TextField(
      controller: controller,
      onChanged: (_) => onChanged(_),
      onSubmitted: (_) => onSubmitted != null ? onSubmitted() : onChanged(_),
      onTap: () => {},
      maxLines: 1,
      textAlign: isInt ? TextAlign.right : TextAlign.start,
      keyboardType: isInt ? TextInputType.number : TextInputType.text,
      maxLength: maxLen,
      maxLengthEnforcement: MaxLengthEnforcement.enforced,
      decoration: const InputDecoration(
        contentPadding: const EdgeInsets.only(bottom: 4),
        isDense: false,
        isCollapsed: true,
      ),

      style: TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black87,
        fontSize: 18.0,
      ),

      readOnly: !administrable,
      enabled: administrable,

    ),
  );
}

