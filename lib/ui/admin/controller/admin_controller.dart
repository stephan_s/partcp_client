// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:get/get.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';

enum EditType {
  eventEdit,
  eventLotSettings,
  eventCreateLots,
  eventCreate,
  votingEdit,
  votingCreate,
  votingChangeStatus
}

class AdminController extends GetxController{
// class AdminController {

  static AdminController to = Get.find<AdminController>();

  late VotingServer server;

  bool forceUpdateEvents = false;
  bool forceUpdateVotings = false;



  static bool allow(EditType type, dynamic data) {
    switch (type) {
      case EditType.eventCreate:
        if (data is Group) return _group(type, data);
        if (data is VotingEvent) return _event(type, data);
        return false;

      case EditType.eventEdit:
      case EditType.eventLotSettings:
      case EditType.eventCreateLots:
      case EditType.votingCreate:
        return _event(type, data);

      case EditType.votingChangeStatus:
      case EditType.votingEdit:
        if (data is VotingEvent) return _event(type, data);
        if (data is Voting) return _voting(type, data);
        return false;
    }
  }

  static bool _group(EditType type, Group group) {
    switch (type) {
      case EditType.eventCreate:
        return group.administrable;
      default:
        return false;
    }
  }

  static bool _event(EditType type, VotingEvent event) {
    switch (type) {

      case EditType.eventCreate:
        if (event.administrable) return true;
        return false;

      case EditType.eventEdit:
        if (event.administrable) return true;
        return false;

      case EditType.eventCreateLots:
        return event.administrable;

      case EditType.eventLotSettings:
        return event.administrable && event.lotsCreated == 0;

      case EditType.votingCreate:
      case EditType.votingEdit:
      case EditType.votingChangeStatus:
        return event.administrable;

      default:
        return false;
    }
  }

  static bool _voting(EditType type, Voting voting) {
    switch (type) {

      case EditType.votingEdit:
        return voting.status == Voting.STATUS_IDLE || voting.status == "";

      case EditType.votingChangeStatus:
        return true;

      default:
        return false;
    }
  }

}