// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:partcp_client/constants.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/message_type/key_list_request.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/participant.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_utils.dart';

class ParticipantController extends GetxController {
  static const _TAG = "ParticipantController";

  // static ParticipantController to = Get.find<ParticipantController>();

  Account? account;
  VotingEvent? event;
  Box<Participant>? participantsBox;


  int get codeDelivered => (participantsBox!.values).toList().where((Participant p) => p.lotCodeDelivered).length;

  List<Participant> get participantsWithPubKey => (participantsBox!.values).toList().where((Participant p) => p.publicKeyConcatB64 != null).toList();
  List<Participant> get participantsWithOutPubKey => (participantsBox!.values).toList().where((Participant p) => p.publicKeyConcatB64 == null).toList();
  List<Participant> get participantsAll => (participantsBox!.values).toList();


  Future<void> initController(Account account, VotingEvent event) async {
    this.account = account;
    this.event = event;
    participantsBox = await MainController.to.getEventBoxParticipants(event);
  }

  /// request participantId from userId 
  Future<void> checkSavedParticipants() async {
    /// todo ?
    List<Participant> pIdRequestList = (participantsBox!.values).toList().where((Participant p) => p.publicKeyConcatB64 != null).toList();

    if (pIdRequestList.isNotEmpty) {
      await requestParticipantIds(pIdRequestList);
      await requestParticipantsPublicKey(pIdRequestList);
    }
    if (participantsWithOutPubKey.isNotEmpty) {
      await requestParticipantsPublicKey(participantsWithOutPubKey);
    }
  }


  // Future<void> updateParticipantInBox(Participant participant) async {
  //   await participantSave(participant);
  // }


  Future<void> saveParticipant(Participant participant) async {
    await participantsBox!.put(participant.userKey, participant);
  }


  // Future<void> loadParticipantListFromBox({Function()? onFinish}) async {
  //   eventBox = await MainController.to.getEventBox(event!);
  //   eventBox!.keys.forEach((key) {
  //     if (key.toString().startsWith(Participant.BOX_ITEM_ID_PREFIX)) {
  //       try {
  //         String jsonItem = eventBox!.get(key);
  //         Participant participant = Participant.fromJson(key.toString(), jsonItem);
  //         participants[participant.userId] = participant;
  //       }
  //       catch(e) {}
  //     }
  //   });
  //   await checkSavedParticipants();
  //   if (onFinish != null) {
  //     onFinish();
  //   }
  // }

  Participant? getParticipantByParticipantId(String participantId) {
    return  (participantsBox!.values).toList().firstWhereOrNull((Participant p) => p.participantId == participantId);
  }

  Participant getParticipantByIndex(int index) {
    return participantsBox!.getAt(index)!;
  }


  //////////////////////////////////////////////////////////////////////////////
  /// Request PARTICIPANT-ID from Key-Server
  Future<void> requestParticipantIds(List<Participant> requestList) async {
    Log.d(_TAG, "_requestParticipantIds");

    List<Participant> _requestList = _trimList(requestList);

    List<Map<String, dynamic>> list = [];
    for (Participant p in _requestList) {
      list.add({"id": p.userId});
    };
    String jsonList = json.encode(list);

    GetXProvider getXProvider = GetXProvider();
    dynamic response = await getXProvider.post(Constants.KEY_MANAGER_PROFILE_2_ID_URL, jsonList);

    Log.d(_TAG, "_requestParticipantIds; jsonList: $jsonList");

    if (response.hasError) {
      Log.w(_TAG, "_requestParticipantIds; error: ${response.statusText}");
    } else {
      Log.d(_TAG, "_requestParticipantIds; response: ${response.bodyString}");

      List<dynamic> pIds = json.decode(response.bodyString);
      for (var i = 0; i < pIds.length; i++) {
        _requestList[i].participantId = pIds[i].toString();
        await saveParticipant(_requestList[i]);
      }
    }
  }




  //////////////////////////////////////////////////////////////////////////////
  /// Request PARTICIPANT's PUBLIC-KEY from Key-Server
  Future<ServerResponse> requestParticipantsPublicKey(List<Participant> requestList) async {
    Log.d(_TAG, "requestParticipantsPublicKeys");

    List<Participant> _requestList = _trimList(requestList);
    Map<String, dynamic> map = keyListRequest(participants: _requestList);
    ServerResponse serverResponse = await HttpController().serverRequestSigned(account!, map, MainController.to.votingServer!);

    List<dynamic> keys = serverResponse.bodyMap!["Keys"];
    for(var i = 0; i<keys.length; i++) {
      String? id = SUtils.mapValue(keys[i], "id", null);
      String? publicKey = SUtils.mapValue(keys[i], "public_key", null);

      if (id != null && publicKey != null) {
        Participant? participant = getParticipantByParticipantId(id);
        if (participant != null) {
          participant.publicKeyConcatB64 = publicKey;
          participant.createPublicKeysFromConcat();
          await saveParticipant(participant);
        }
      }
    };

    return serverResponse;
  }


  List<Participant> _trimList(List<Participant> list) {
    return list.getRange(0, (list.length > 999 ? 999 : list.length)).toList();
  }

}