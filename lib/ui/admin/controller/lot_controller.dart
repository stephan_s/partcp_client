// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved



import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:partcp_client/constants.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/lot_code.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/ui/admin/event/admin_event_lots_reg.dart';
import 'package:partcp_client/ui/wallet/export_wallet.dart';
import 'package:partcp_client/utils/save_file.dart';
import 'package:partcp_client/utils/yaml_converter.dart';
import 'package:partcp_client/widgets/widgets.dart';

import 'admin_controller.dart';

class LotController extends GetxController {
  static const _TAG = "LotController";

  static LotController to = Get.find<LotController>();

  ServerResponse serverResponse = ServerResponse();
  late VotingServer server;
  late VotingEvent event;
  late Account account;
  bool importingCodes = false;
  Box<LotCode>? lotCodeBox;

  Future<void> initController(VotingServer server, VotingEvent event, Account account) async {
    this.server = server;
    this.event = event;
    this.account = account;
    lotCodeBox = await MainController.to.getEventBoxLotCodes(event);
    // lotCodeBox!.clear();
  }

  bool get allowRegisterLots => AdminController.allow(EditType.eventCreateLots, event);

  Future<void> onRefresh() async {
    await event.sEventDetailsRequest(server, account);
    updateMe();
  }

  // LotCodeStorage? get lotCodeStorage => account.lotCodeStorageMap[event.id];
  // set lotCodeStorage(LotCodeStorage? newLotCodeStorage) => account.lotCodeStorageMap[event.id] = newLotCodeStorage!;


  // Future<void> _importLotCodeStorageFromInternalStorage() async {
  //   Log.d(_TAG, "_importLotCodeStorageFromInternalStorage()");
  //
  //   if(lotCodeStorage == null) {
  //     importingCodes = true;
  //     updateMe();
  //
  //     LotCodeStorage? tmpCodes = await MainController.to.getLotCodesFromStorage(event);
  //     // await MainController.to.getLotCodesFromStorage(event);
  //     if (tmpCodes != null) {
  //       lotCodeStorage = tmpCodes;
  //     }
  //
  //     importingCodes = false;
  //     updateMe();
  //   }
  // }


  Future<void> onImportLotCodes() async {
/*
    Directory? lotCodeDir;
    if (GetPlatform.isMobile && !GetPlatform.isWeb) {
      String _lotCodeDirPath = (await Constants.appDirExternal)!.path + "/" + account.lotCodesSubDir(event);
      lotCodeDir = Directory(_lotCodeDirPath);
    }

    Log.d(_TAG, "onImportLotCodes; lotCodeDir:$lotCodeDir");

    String? fileContent =
    await FileActions.textFileFromExternalStorage(
        pageTitle: "Lot-Codes",
        initialDirectory: lotCodeDir);

    if (fileContent != null) {
      _workingDialog('Importiere Lot-Codes');

      if (lotCodeStorage == null) {
        lotCodeStorage = LotCodeStorage(event.id);
      }

      lotCodeStorage!.importFromString(
          content: fileContent,
          eventId: event.id,
          wallet: MainController.to.wallet!,
          onFinish: (count, imported, error) async {
            await _saveLotList(lotCodeStorage!);

            Log.d(_TAG, "onImportLotCodes; onFinish; count:$count; imported: $imported");

            updateMe();
            Get.back();

            String errorString = error != null
                ? "\n\nFehler:${lotCodeStorage!.errorToString(error)}"
                : "";
            await showOkAlertDialog(
              context: Get.context!,
              title: "Lot-Code Import",
              message: 'Lot-Codes gesamt: $count\n'
                  'Lot-Codes importiert: $imported'
                  '$errorString',
              okLabel: 'ok',
              barrierDismissible: true,
            );
          }
      );
    }

 */
  }

  /// todo: was passiert, wenn lotCodes registriert wurden?
  Future<void>onDeleteAllLotCodes() async {
    var result = await showOkCancelAlertDialog(
        context: Get.context!,
        title: "Lot-Codes löschen",
        message: "Bist Du sicher, dass Du alle Lot-Codes löschen möchtest?",
        okLabel: "Ja",
        cancelLabel: "Abbrechen"
    );

    if (result == OkCancelResult.ok) {
      await lotCodeBox!.clear();
      await lotCodeBox!.flush();
      updateMe();
    }
  }

  Future<void> _workingDialog(String title) async {
    showDialog(
        barrierDismissible: false,
        context: Get.context!,
        builder: (_) => AlertDialog(
          title: Text(title),
          content: Container(
            width: 50,
            height: 50,
            child: Widgets.loader,
          ),
        )
    );
  }


  Future<void> onExportWallet() async {
    await ExportWallet().download();
    updateMe();
  }

  ///------------------------------------------------------------------------------------- todo

  Future<void> onPostFrameCallback() async {
    // Future.delayed(Duration(milliseconds: 300)).then((_) {
    //   updateMe();
    // });
  }


  Future<String> get _lotListAsString async {
    String lots = "";

    String col =
        CSV_ROW_0 + CSV_ROW_SPLITTER +
            CSV_ROW_1 + CSV_ROW_SPLITTER +
            event.id + "\n";
    lots += col;

    for (var i = 0; i<lotCodeBox!.length; i++) {
      LotCode l = getLotCodeByIndex(i);
      col =  l.lotCode! + CSV_ROW_SPLITTER + l.status.toString() + CSV_ROW_SPLITTER + "''\n";
      lots += col;
    }
    return lots;
  }




  Future<void> onCopyLotListClipboard() async {
    _workingDialog("Kopiere Lot-Codes in das Clipboard");
    await Clipboard.setData(ClipboardData(text: await _lotListAsString));
    Get.back();
    final snackBar = SnackBar(
      content: Text('Codes wurden in das Clipboard kopiert'),
      duration: Constants.SNACK_BAR_DURATION_SHORT,
    );
    ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);
  }

  Future<void> onExportLotList() async {

    String? result = await _exportCodesDialog();
    if (result == null) return;

    String fileName;
    String? fileContent;

    /// encrypted
    if (result == "enc") {
      fileName = event.lotCodesFileNameEncrypted;
      String fileContentDecr = (await MainController.to.getLotCodesFromStorageAsString(event))!;
      fileContent = await MainController.to.wallet!.computeEncryptString(fileContentDecr);

      if (fileContent == null) {
        await showAlertDialog(
            context: Get.context!,
            message: "Ein unerwarteter Fehler ist eingetreten"
        );
        return;
      }
    }
    /// decrypted
    else {
      fileName = event.lotCodesFileNameDecryptedCsv;

      _workingDialog('Exportiere Lot-Codes');
      fileContent = await _lotListAsString;
      Get.back();
    }

    SaveFile saveFile = SaveFile(
      saveInExternalStorage: true,
      content: fileContent,
      fileName: fileName,
      subDir: account.lotCodesSubDir(event),
    );

    await saveFile.save();

    final snackBar = SnackBar(
      content: Text('Die Datei wurde gespeichert in: ${saveFile.filePathLazy}'),
      duration: Constants.SNACK_BAR_DURATION_LONG,
    );
    ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);
  }


  Future<String?> _exportCodesDialog() async {
    final result = await showConfirmationDialog(
      context: Get.context!,
      title: 'Lot-Codes Export',
      message: "Die Export-Funktion erlaubt Ihnen die erzeugten Lot-Codes"
          "zu sichern (verschlüsselt) oder in einer anderen Andendung zu verarbeiten (unverschlüsselt)"
          "\n\n"
          "Verschlüsselte Lot-Codes können nur mit diesem Schlüsselbund entschlüsselt werden",

      okLabel: 'Ok',
      cancelLabel: "Abbrechen",
      actions: [
        AlertDialogAction(key: "enc", label: "Verschlüsselt"),
        AlertDialogAction(key: "dec", label: "CSV unverschlüsselt"),
      ],
      barrierDismissible: false,
    );
    return result;
  }


  Future<void> onGoToAdminEventLotsReg() async {
    bool success = await Get.to(() => AdminEventLotsReg(server: server, event: event, account: account, lotController: this,));
    if (success) {
      serverResponse.receivingData = true;
      updateMe();
      serverResponse.receivingData = false;
      updateMe();
      onRefresh();
    }
  }

  void addLotCodesFromServerResponse(String yamlString) {
    Map<String, dynamic> lotMap = YamlConverter().toMap(yamlString);
    List<dynamic> lotCodes = lotMap["codes"];

    for (var lot in lotCodes) {
      LotCode lotCode = LotCode(lotCode: lot);
      lotCodeBox!.add(lotCode);
    }
    lotCodeBox!.flush();
  }


  Future<void> saveLotCode(LotCode lotCode) async {
    await lotCode.save();
  }

  LotCode getLotCodeByIndex(int index) {
    return lotCodeBox!.getAt(index)!;
  }

  List<LotCode> getLotCodesByStatus (int status) {
    return (lotCodeBox!.values).toList().where((LotCode l) => l.status == status).toList();
  }

  List<LotCode> getLotCodesAvailable() {
    return (lotCodeBox!.values).toList().where((LotCode l) => l.status == 1).toList();
  }

  List<LotCode> getLotCodesByString(String value) {
    return (lotCodeBox!.values).toList().where((LotCode l) => l.lotCode != null && l.lotCode!.contains(value)).toList();
  }

  void updateMe() {
    update();
  }

  static const CSV_ROW_SPLITTER = ";";
  static const CSV_ROW_0 = "lot-code";
  static const CSV_ROW_1 = "status";
  static const CSV_ROW_2 = "event-id";

  static const LOTS = "LOTS";
}

