// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/voting_type.dart';
import 'package:partcp_client/styles/styles.dart';
import 'package:partcp_client/ui/admin/controller/lot_controller.dart';
import 'package:partcp_client/ui/admin/controller/particicpant_conroller.dart';
import 'package:partcp_client/ui/admin/event/admin_event_edit_settings.dart';
import 'package:partcp_client/ui/admin/event/admin_event_edit_text.dart';
import 'package:partcp_client/ui/admin/event/admin_event_participants.dart';
import 'package:partcp_client/ui/admin/voting/admin_voting.dart';
import 'package:partcp_client/ui/admin/voting/voting_change_status.dart';
import 'package:partcp_client/ui/admin/controller/admin_controller.dart';
import 'package:partcp_client/ui/voting/voting_list_page.dart';
import 'package:partcp_client/ui/voting/voting_result_page.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/widgets/alert_dialog.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_icon.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/widgets.dart';

import 'admin_event_lots.dart';
import '../widgets/styles.dart';

class AdminEventController extends GetxController {
  static const _TAG = "AdminEventController";

  late ParticipantController pController;
  late LotController lotController;

  ServerResponse serverResponse = ServerResponse();
  late VotingServer server;
  late Group group;
  late VotingEvent event;
  late Account account;

  String? lastSelectedVotingId;

  late Key scrollBodyKey;

  void initController(VotingServer server, Account account, Group group, VotingEvent event) {
    pController = Get.put(ParticipantController())..initController(account, event);

    lotController = Get.put(LotController())..initController(server, event, account);

    this.server = server;
    this.group = group;
    this.account = account;
    this.event = event;
    scrollBodyKey = ValueKey("admin_event;" + DateTime.now().millisecondsSinceEpoch.toString());
  }

  Future<void> onPostFrameCallback() async {
    await requestEventDetails();
  }

  bool allowChangeStatus(Voting voting) =>
      AdminController.allow(EditType.votingChangeStatus, event) && AdminController.allow(EditType.votingChangeStatus, voting);

  Future<void> requestEventDetails() async {
    serverResponse.receivingData = true;
    updateMe();
    serverResponse = await event.sEventDetailsRequest(server, account);
    updateMe();
  }

  Future<void> onEventEditText() async {
    await Get.to(() => AdminEventEditText(server: server, account: account, group: group, event: event));
    updateMe();
  }

  Future<void> onEventEditSettings() async {
    await Get.to(() => AdminEventEditSettings(server: server, account: account, group: group, event: event));
    updateMe();
  }

  Future<void> onVotingReorder(int oldIndex, int newIndex) async {
    if (newIndex > oldIndex) {
      newIndex -= 1;
    }
    final Voting item = event.votings.removeAt(oldIndex);
    event.votings.insert(newIndex, item);
    event.updateClientDataVotingOrder();
    serverResponse.receivingData = true;
    updateMe();

    serverResponse = await event.sendUpdateRequestClientData(server, account, event);
    updateMe();
  }

  /// SEND SERVER REQUEST TO CHANGE STATUS
  void onVotingStatusChanged(Voting voting) async {
    AdminController.to.forceUpdateEvents = true;
    AdminController.to.forceUpdateVotings = true;

    if (voting.status == Voting.STATUS_FINISHED) {
      Get.to(() => VotingResultPage(server: server, account: null, voting: voting, votingEvent: event));
    } else if (allowChangeStatus(voting)) {
      bool result = await changeVotingStatusBox(Get.context!, server, account, event, voting);
      if (result) {
        serverResponse.receivingData = true;
        updateMe();

        serverResponse = await voting.sendStatusChangeRequest(server, event, account);

        if (serverResponse.errorMessage != null) {
          await alertDialogErrorBox(Get.context!, serverResponse.errorMessage!);
        }
        onPostFrameCallback();
      }
    }
  }

  Future<void> onGoToVoting(Voting? voting) async {
    if (voting != null) {
      lastSelectedVotingId = voting.id;
    }

    await Get.to(() => AdminVoting(server: server, event: event, account: account, voting: voting));

    if (AdminController.to.forceUpdateVotings) {
      await requestEventDetails();
      AdminController.to.forceUpdateVotings = false;
    }
    updateMe();
  }

  void onEventPreview() {
    Get.to(() => VotingListPage(
          server: server,
          account: account,
          votingEvent: event,
          previewOnly: true,
        ));
  }

  void updateMe() {
    update();
  }
}

enum _MenuItems {
  eventPreview,
}

class AdminEvent extends StatelessWidget {
  final VotingServer server;
  final Account account;
  final VotingEvent event;
  final Group group;

  AdminEvent({
    required this.server,
    required this.account,
    required this.event,
    required this.group,
  });

  final AdminEventController controller = Get.put(AdminEventController());

  void _handleMenuClick(_MenuItems value) {
    switch (value) {
      case _MenuItems.eventPreview:
        controller.onEventPreview();
        break;
    }
  }

  List<PopupMenuItem<_MenuItems>> _popupMenuItems() {
    List<PopupMenuItem<_MenuItems>> list = [];

    list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.eventPreview, child: Text("Veranstaltungsvorschau")));

    return list;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return WillPopScope(
      onWillPop: () async {
        await controller.lotController.lotCodeBox!.flush();
        await controller.lotController.lotCodeBox!.close();

        await controller.pController.participantsBox!.flush();
        await controller.pController.participantsBox!.close();

        return true;
      },
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<AdminEventController>(
            initState: (_) => controller.initController(server, account, group, event),
            builder: (_) => Scaffold(
                appBar: MyAppBar(
                  title: Text(controller.event.name),
                  actions: [
                    /// refresh
                    IconButton(
                      icon: Icon(Icons.refresh),
                      onPressed: () => controller.requestEventDetails(),
                    ),

                    /// SETTINGS
                    PopupMenuButton<_MenuItems>(
                        icon: Icon(Icons.menu), onSelected: _handleMenuClick, itemBuilder: (BuildContext context) => _popupMenuItems()),
                  ],
                ),
                body: Widgets.loaderBox(
                  loading: controller.serverResponse.receivingData,
                  child: ScrollBody(
                    key: controller.scrollBodyKey,
                    children: _body(),
                  ),
                ))),
      ),
    );
  }

  List<Widget> _body() {
    List<Widget> list = [];

    list.add(CardWidget(
        titleListTile: CardWidget.listTileTitle(
            leading: Icon(Icons.event),
            subtitle: Text(SUtils.formatDate(controller.event.date)),
            isThreeLine: true,
            title: controller.event.name,
            trailing: ButtonIcon(
                icon: Icon(AdminController.allow(EditType.eventEdit, controller.event) ? Icons.edit : Icons.info_outline_rounded),
                onPressed: () => controller.onEventEditText()))));

    list.add(_codesInfo());
    list.add(_participantsInfo());
    list.add(_votings());
    return list;
  }

  static const double leftPadding = 10;

  Widget _codesInfo() {
    return CardWidget(
      titleListTile: CardWidget.listTileTitle(
          title: "Teilnahmecodes",
          trailing: ButtonIcon(
              icon: Icon(AdminController.allow(EditType.eventLotSettings, controller.event) ? Icons.settings : Icons.info_outline_rounded),
              onPressed: () => controller.onEventEditSettings().then((_) => controller.updateMe()))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: leftPadding),
            child: Text("am Server erzeugt: ${controller.event.lotsCreated}"),
          ),
          Padding(
            padding: const EdgeInsets.only(left: leftPadding),
            child: Text("am Server eingelöst: ${controller.event.lotsRedeemed}"),
          ),
          Padding(
            padding: const EdgeInsets.only(left: leftPadding),
            child: controller.lotController.lotCodeBox != null
                ? Text("lokal gespeichert: ${controller.lotController.lotCodeBox!.length}")
                : Text("lokal gespeichert: -"),
          ),
          Padding(
            padding: const EdgeInsets.only(left: leftPadding),
            child: Text("Schlüssel hinterlegt: ${controller.event.keysSubmitted}"),
          ),
          Padding(
            padding: const EdgeInsets.only(left: leftPadding),
            child: AdminController.allow(EditType.eventCreateLots, controller.event)
                ? Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: TextButton(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: Icon(Icons.ballot_outlined),
                            ),
                            Text("Codes verwalten"),
                          ],
                        ),
                        onPressed: () => Get.to(() => AdminEventLots(
                                  server: server,
                                  event: event,
                                  account: account,
                                  lotController: controller.lotController,
                                  pController: controller.pController,
                                ))!
                            .then((value) => controller.updateMe())),
                  )
                : Container(),
          ),
        ],
      ),
    );
  }

  Widget _participantsInfo() {
    String participantsCount = "-";
    String codesDelivered = "-";
    if (controller.pController.participantsBox != null) {
      participantsCount = controller.pController.participantsBox!.length.toString();
      codesDelivered = controller.pController.codeDelivered.toString();
    }

    return CardWidget(
      titleListTile: CardWidget.listTileTitle(
          title: "Interne Einladungen",
          trailing: ButtonIcon(
              icon: Icon(AdminController.allow(EditType.eventCreateLots, controller.event) ? Icons.people : Icons.info_outline_rounded),
              onPressed: () => Get.to(() => AdminEventParticipants(
                        event: event,
                        account: account,
                        pController: controller.pController,
                        lotController: controller.lotController,
                      ))!
                  .then((value) => controller.updateMe()))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: leftPadding),
            child: Text("vorgemerkt: $participantsCount"),
          ),
          Padding(
            padding: const EdgeInsets.only(left: leftPadding),
            child: Text("versendet: $codesDelivered"),
          ),
        ],
      ),
    );
  }

  Widget _votings() {
    return CardWidget(
        titleListTile: CardWidget.listTileTitle(
            title: "Abstimmungen",
            trailing: AdminController.allow(EditType.votingCreate, event)
                ? ButtonIcon(icon: Icon(Icons.add_circle_outline), onPressed: () => controller.onGoToVoting(null))
                : null),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          /// Voting list
          _votingListView(),

          /// bottom (+) button
          controller.event.votings.length > 4 && AdminController.allow(EditType.votingCreate, event)
              ? Align(
                  alignment: Alignment.centerRight,
                  child: ButtonIcon(icon: Icon(Icons.add_circle_outline), onPressed: () => controller.onGoToVoting(null)),
                )
              : Container(),
        ]));
  }

  Widget _votingListView() {
    return AdminController.allow(EditType.votingEdit, event)
        ? ReorderableListView.builder(
            shrinkWrap: true,
            itemCount: controller.event.votings.length,
            itemBuilder: (_, int index) => _votingItem(index),
            physics: NeverScrollableScrollPhysics(),
            onReorder: (int oldIndex, int newIndex) => controller.onVotingReorder(oldIndex, newIndex),
          )
        : ListView.builder(
            shrinkWrap: true,
            itemCount: controller.event.votings.length,
            itemBuilder: (_, int index) => _votingItem(index),
            physics: NeverScrollableScrollPhysics(),
          );
  }

  Widget _votingItem(int index) {
    Voting voting = controller.event.votings[index];
    return Column(
      key: Key(voting.id!),
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: ListTile(
            tileColor: controller.lastSelectedVotingId != null && controller.lastSelectedVotingId! == voting.id ? Styles.selectedTileColor : null,
            contentPadding: EdgeInsets.only(right: GetPlatform.isWeb ? 30 : 0),
            leading: IconButton(
                icon: voting.statusIcon,

                /// VOTING STATUS BUTTON
                onPressed: () => controller.onVotingStatusChanged(voting)),

            /// TITLE
            title: Text(
              voting.title,
              style: AdminController.allow(EditType.votingEdit, event) ? TextStyle(color: Styles.buttonColor) : null,
            ),
            isThreeLine: true,
            subtitle: Text(
              "${VotingType.title(voting.type)}\n"
              "Status: ${voting.statusText}\n"
              ""
              "${voting.periodStart != null ? SUtils.formatDateTimeShort(voting.periodStart) : "-/-"}"
              " - "
              "${voting.periodEnd != null ? SUtils.formatDateTimeShort(voting.periodEnd) : "-/-"}",
              textScaleFactor: 0.9,
            ),

            /// GO TO VOTING
            onTap: () => controller.onGoToVoting(voting),
          ),
        ),
        Divider(
          thickness: 1,
          height: 1,
          color: dividerColor,
        )
      ],
    );
  }
}
// AdminController.allow(EditType.votingEdit, event)
