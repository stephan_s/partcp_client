// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/ui/admin/controller/particicpant_conroller.dart';
import 'package:partcp_client/ui/admin/widgets/styles.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_sub.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/widgets.dart';
import 'package:partcp_client/widgets/save_wallet_menu_icon.dart';

import '../controller/lot_controller.dart';
import 'admin_event_lots_list.dart';

class AdminEventLots extends StatelessWidget {
  final VotingServer server;
  final VotingEvent event;
  final Account account;
  final LotController lotController;
  final ParticipantController pController;

  AdminEventLots(
      {required this.server, required this.event, required this.account, required this.lotController, required this.pController});

  final ScrollController scrollController = ScrollController();

  Future<void> onEditLotCodes() async {
    Get.to(() => AdminEventLotsList(
              account: account,
              event: event,
              server: server,
              lotController: lotController,
              pController: pController,
            ))!
        .then((value) => lotController.updateMe());
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      lotController.onPostFrameCallback();
    });

    return GetBuilder<LotController>(
        // initState: (_) => controller.initController(server, event, account),
        builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: Text("Teilnahmecodes"),
                actions: [
                  /// refresh

                  saveWalletMenuIcon(onPressed: () => lotController.onExportWallet()),

                  IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () => lotController.onRefresh(),
                  ),
                ],
              ),
              body: OrientationBuilder(
                builder: (_, __) => Widgets.loaderBox(
                  loading: lotController.serverResponse.receivingData,
                  child: ListView(
                    children: _body(context),
                  ),
                ),
              ),
            ));
  }

  /// BODY
  List<Widget> _body(BuildContext context) {
    List<Widget> list = [];
    list.add(CardWidget(
        titleListTile: CardWidget.listTileTitle(
            leading: Icon(Icons.event),
            title: lotController.event.name,
            isThreeLine: true,
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(SUtils.formatDate(lotController.event.date)),
                Text("Erzeugte Codes: ${lotController.event.lotsCreated}")
              ],
            ))));

    list.add(_buttons);

    return list;
  }

  Widget get _lotsInfo {
    Widget _lotCount;
    if (lotController.importingCodes) {
      _lotCount = Widgets.loader;
    } else {
      _lotCount = Text(
        lotController.lotCodeBox != null ? "${lotController.lotCodeBox!.length}" : "0",
        style: TextStyle(fontWeight: FontWeight.bold),
      );
    }

    return Column(
      children: [
        Row(
          children: [
            Text("Gespeicherte Codes:"),
            Padding(
              padding: EdgeInsets.only(left: 10),
              child: _lotCount,
            )
          ],
        ),
        Row(
          children: [
            Text("Verfügbare Codes:"),
            Padding(
              padding: EdgeInsets.only(left: 10),
              child: Text(
                "${lotController.getLotCodesAvailable().length}",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget get _buttons {
    List<Widget> _items = [];

    if (lotController.lotCodeBox != null && lotController.lotCodeBox!.isNotEmpty) {
      _items.add(ButtonSub(
        child: Text("Codes erzeugen"),
        onPressed: lotController.allowRegisterLots ? () => lotController.onGoToAdminEventLotsReg() : null,
      ));
      _items.add(ButtonSub(child: Text("Codes kopieren"), onPressed: () => lotController.onCopyLotListClipboard()));
      _items.add(ButtonSub(child: Text("Codes bearbeiten"), onPressed: () => onEditLotCodes()));
      // _items.add(ButtonSub(child: Text("Codes exportieren"), onPressed: () => lotController.onExportLotList()));
      // _items.add(ButtonSub(child: Text("Codes importieren"), onPressed: () => lotController.onImportLotCodes()));
      // _items.add(ButtonSub(child: Text("Alle Codes löschen"), onPressed: () => lotController.onDeleteAllLotCodes()));
    } else {
      _items.add(ButtonSub(
        child: Text("Codes erzeugen"),
        onPressed: lotController.allowRegisterLots ? () => lotController.onGoToAdminEventLotsReg() : null,
      ));
      _items.add(ButtonSub(child: Text("Codes importieren"), onPressed: () => lotController.onImportLotCodes()));
    }

    return CardWidget(
        titleListTile: CardWidget.listTileTitle(
          leading: Icon(Icons.list),
          title: "Teilnahmecodes",
        ),
        child: Column(
          children: [
            _lotsInfo,
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 0),
              child: Divider(color: dividerColor),
            ),
            ButtonBar(
              children: _items,
            ),
          ],
        ));
  }
}
