// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:partcp_client/constants.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/lot_code.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/ui/admin/controller/particicpant_conroller.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/measure_size.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_sub.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/save_wallet_menu_icon.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/widgets.dart';

import '../controller/lot_controller.dart';

class AdminEventLotsListController extends GetxController {
  static const _TAG = "AdminEventLotsList";

  ServerResponse serverResponse = ServerResponse();
  late LotController lotController;
  late ParticipantController pController;

  late Account account;
  late VotingEvent event;
  late VotingServer server;

  double? _bodySize;
  double? _titleSize;
  double? _buttonsSize;

  List<LotCode>? lotSearchList;

  int get activeLotCodeCount => lotController.lotCodeBox == null
      ? 0
      : (lotController.lotCodeBox!.values).toList().where((LotCode l) => l.status == 1).length;

  double lotListHeight = 300;

  void initController(
      Account account, VotingEvent event, VotingServer server, LotController lotController, ParticipantController pController) {
    this.account = account;
    this.event = event;
    this.server = server;
    this.lotController = lotController;
    this.pController = pController;
  }

  Future<void> onDeactivateActive() async {

    final OkCancelResult result = await showOkCancelAlertDialog(
      context: Get.context!,
      title: "Aktive Teilnahmecodes deaktivieren",
      message: 'Bist Du sicher, dass Du alle aktiven Teilnahmecodes deaktivieren möchtest?\n\n'
          'Dieser Vorgang kann nicht rückgängig gemacht werden.',
      okLabel: 'Deaktivieren',
      cancelLabel: "Abbrechen",
      barrierDismissible: true,
    );

    Log.d(_TAG, "onDeactivateActive; result:$result");
    if (result == OkCancelResult.ok) {
      List<LotCode> lots = lotController.getLotCodesAvailable();

      if (lots.isNotEmpty) {
        serverResponse.receivingData = true;
        updateMe();
        onLotCodeStatusChanged(lots, 0);
      }
    }
  }

  Future<void> onLotCodeStatusChanged(List<LotCode> lotCodes, int status) async {
    List<LotCode> changedLotCodes = [];

    for (LotCode l in lotCodes) {
      if (l.status != status) {
        if (status == 0) {
          changedLotCodes.add(l);
        } else {
          await lotController.saveLotCode(l);
        }
      }
    }

    if (status == 0) {
      List<String> lotCodes = changedLotCodes.cast();
      final Map<String, dynamic> messageMap = lotInvalidation(event: event, lotCodes: changedLotCodes);

      Log.d(_TAG, "onLotCodeStatusChanged: messageMap: $messageMap");

      serverResponse.receivingData = true;
      updateMe();
      serverResponse = await HttpController().serverRequestSigned(account, messageMap, server);

      if (serverResponse.statusCode == 200) {
        for (LotCode l in changedLotCodes) {
          l.status = status;
          await lotController.saveLotCode(l);
        }
      }
    }

    updateMe();
  }

  void onLotCodeSearch(String value) {
    if (value.isEmpty) {
      lotSearchList = null;
    } else {
      lotSearchList = lotController.getLotCodesByString(value);
    }
    updateMe();
  }

  void updateLotListWidget() {
    Log.d(
        _TAG,
        "updateLotListWidget: "
        "Get.height: ${Get.height} "
        "_bodySize: $_bodySize; "
        "_titleSize: $_titleSize "
        "_buttonsSize: $_buttonsSize "
        "lotListHeight: $lotListHeight");

    if (_bodySize == null || _titleSize == null || _titleSize == null || _buttonsSize == null) return;
    // if (_bodySize == null || _titleSize == null || lotListHeight > 600) return;

    // lotListHeight = _bodySize! - MyAppBar().preferredSize.height - _titleSize! - _buttonsSize! - 20;
    lotListHeight = _bodySize! - MyAppBar().preferredSize.height - _titleSize! - _buttonsSize! - 120;

    // if (lotListHeight < 100) {
    //   lotListHeight = 100;
    // }
    Log.d(_TAG, "updateLotListWidget; lotListHeight: $lotListHeight");

    updateMe();
  }

  Future<void> onCopyLotToClipboard(String lot) async {
    await Clipboard.setData(ClipboardData(text: lot));
    final snackBar = SnackBar(
      content: Text('Teilnahmecode wurde in das Clipboard kopiert'),
      duration: Constants.SNACK_BAR_DURATION_SHORT,
    );
    ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);
  }

  void onBodySize(Size size) {
    _bodySize = size.height;
    updateLotListWidget();
  }

  void onTitleSize(Size size) {
    _titleSize = size.height;
    updateLotListWidget();
  }

  void onButtonsSize(Size size) {
    _buttonsSize = size.height;
    updateLotListWidget();
  }

  void updateMe() {
    update();
  }
}

class AdminEventLotsList extends StatelessWidget {
  final Account account;
  final VotingEvent event;
  final VotingServer server;
  final LotController lotController;
  final ParticipantController pController;

  final AdminEventLotsListController controller = Get.put(AdminEventLotsListController());

  AdminEventLotsList(
      {required this.account, required this.event, required this.server, required this.lotController, required this.pController});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AdminEventLotsListController>(
      initState: (_) => controller.initController(account, event, server, lotController, pController),
      builder: (_) => Scaffold(
        appBar: MyAppBar(
          title: Text("Teilnahmecodes"),
          actions: [
            saveWalletMenuIcon(onPressed: () => controller.lotController.onExportWallet()),
          ],
        ),
        body: OrientationBuilder(
            builder: (_, __) => MeasureSize(
                onChange: (size) => controller.onBodySize(size),
                // child: ListView(
                //   children: _body(context),
                // ),
                child: Widgets.loaderBox(
                  loading: controller.serverResponse.receivingData,
                  child: ScrollBody(
                    children: _body,
                  ),
                ))),
      ),
    );
  }

  /// BODY
  List<Widget> get _body {
    List<Widget> list = [ServerErrorMessage(serverResponse: controller.serverResponse)];

    list.add(MeasureSize(
      onChange: (size) => controller.onTitleSize(size),
      child: CardWidget(
          titleListTile: CardWidget.listTileTitle(
              leading: Icon(Icons.event),
              title: controller.lotController.event.name,
              isThreeLine: true,
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(SUtils.formatDate(controller.lotController.event.date)),
                  Text("Erzeugte Codes: ${controller.lotController.event.lotsCreated}")
                ],
              ))),
    ));

    list.add(_lotListBox);

    return list;
  }

  /// LOT SEARCH FIELD
  Widget get _lotSearchField => ListTile(
        title: TextField(
            onChanged: (value) => controller.onLotCodeSearch(value),
            decoration: InputDecoration(
                // suffixIcon: Icon(Icons.search),
                contentPadding: const EdgeInsets.only(bottom: 10),
                isDense: false,
                isCollapsed: true,
                hintText: "Suche nach Teilnahmecode")),
        contentPadding: EdgeInsets.all(0),
        trailing: Icon(Icons.search),
      );

  /// LOT-LIST
  Widget get _lotListBox => CardWidget(
        titleListTile: CardWidget.listTileTitle(
          leading: Icon(Icons.list),
          title: "Teilnahmecodes",
        ),
        child: Column(
          children: [
            _lotSearchField,
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                decoration: BoxDecoration(border: Border.all(color: Colors.black45, width: 1)),
                height: controller.lotListHeight,
                child: CupertinoScrollbar(
                    child: ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: controller.lotSearchList != null
                      ? controller.lotSearchList!.length
                      : controller.lotController.lotCodeBox!.length,
                  itemBuilder: (_, index) => _lotListItem(index),
                )),
              ),
            ),
            MeasureSize(onChange: (size) => controller.onButtonsSize(size), child: _buttons)
          ],
        ),
      );

  Widget _lotListItem(int index) {
    LotCode lotCode =
        controller.lotSearchList != null ? controller.lotSearchList![index] : controller.lotController.getLotCodeByIndex(index);

    TextStyle? textStyle;
    switch (lotCode.status) {
      case 0:

        /// deactivated
        textStyle = TextStyle(decoration: TextDecoration.lineThrough);
        break;
      case 2:

        /// Delivered (used)
        textStyle = TextStyle(fontStyle: FontStyle.italic);
        break;
      default:
        textStyle = null;
    }

    List<DropdownMenuItem<int>> actionList;

    /// used
    if (lotCode.status == 2) {
      actionList = [_dropDownItem(2)];
    }

    /// deleted
    else if (lotCode.status == 0) {
      actionList = [_dropDownItem(0)];
    } else {
      actionList = [_dropDownItem(1), _dropDownItem(0)];
    }

    return ListTile(
      title: Text(lotCode.lotCode!, style: textStyle),
      onLongPress: () => controller.onCopyLotToClipboard(lotCode.lotCode!),
      trailing: DropdownButton<int>(
          value: lotCode.status,
          onChanged: (int? newValue) => controller.onLotCodeStatusChanged([lotCode], newValue!),
          underline: Container(
            height: 2,
            color: Theme.of(Get.context!).primaryColor,
          ),
          items: actionList),
    );
  }

  DropdownMenuItem<int> _dropDownItem(int status) {
    String statusText;
    switch (status) {
      case 0:
        statusText = "gelöscht";
        break;
      case 1:
        statusText = "aktiv";
        break;
      default:
        statusText = "verwendet";
    }

    return DropdownMenuItem<int>(
      value: status,
      child: Text(statusText),
    );
  }

  // Widget get _buttons {
  //   List<Widget> _items = [];
  //
  //   _items.add(ButtonSub(
  //     child: Text("Aktive Codes deaktivieren"),
  //     onPressed: controller.activeLotCodeCount > 0 ? () => controller.onDeactivateActive() : null,
  //   ));
  //
  //   return CardWidget(
  //       titleListTile: CardWidget.listTileTitle(
  //         leading: Icon(Icons.list),
  //         title: "Teilnahmecodes",
  //       ),
  //       child: Column(
  //         children: [
  //           Padding(
  //             padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 0),
  //             child: Divider(color: dividerColor),
  //           ),
  //           ButtonBar(
  //             children: _items,
  //           ),
  //         ],
  //       ));
  // }

  Widget get _buttons {
    List<Widget> _items = [];

    _items.add(ButtonSub(
      child: Text("Aktive Codes deaktivieren"),
      onPressed: controller.activeLotCodeCount > 0 ? () => controller.onDeactivateActive() : null,
    ));

    return ButtonBar(
      children: _items,
    );
  }
}
