// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/message_type/envelope.dart';
import 'package:partcp_client/message_type/lot_code_deposit.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/lot_code.dart';
import 'package:partcp_client/objects/participant.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/ui/admin/controller/admin_controller.dart';
import 'package:partcp_client/ui/admin/controller/lot_controller.dart';
import 'package:partcp_client/ui/admin/controller/particicpant_conroller.dart';
import 'package:partcp_client/ui/admin/event/admin_event_lots.dart';
import 'package:partcp_client/ui/admin/widgets/styles.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_sub.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/text_field_widget.dart';
import 'package:partcp_client/widgets/widgets.dart';

const _TAG = "AdminMemberController";

/*
  20000130
  20001581
  20003351
  20004959
   */


/*

 */

class AdminParticipantsController extends GetxController {
  ServerResponse serverResponse = ServerResponse();

  late Account account;
  late VotingEvent event;
  late ParticipantController pController;
  late LotController lotController;

  List<Participant>? participantsSearchList;

  double memberListHeight = 200;
  bool importingCodes = false;

  List<LotCode> get lotCodesAvailable => lotController.getLotCodesAvailable();

  void initController(Account account, VotingEvent event, ParticipantController pController, LotController lotController) {
    this.account = account;
    this.event = event;
    this.pController = pController;
    this.lotController = lotController;
  }

  void onParticipantSearch(String value) {
    if (value.isEmpty) {
      participantsSearchList = null;
    } else {
      participantsSearchList = (pController.participantsBox!.values).toList().where((p) => p.userId.contains(value)).toList();
    }
    updateMe();
  }

  Future<void> onAddParticipants() async {
    List<String>? items = await showTextInputDialog(
      context: Get.context!,
      title: "Mitglieder hinzufügen",
      textFields: [DialogTextField(minLines: GetPlatform.isMobile ? 5 : 15, maxLines: 10000000)],
      message: 'Mitgliedsnummern (eine pro Zeile)',
      okLabel: 'ok',
      cancelLabel: "abbrechen",
      barrierDismissible: false,
    );
    Log.d(_TAG, "onAddParticipants: $items");

    if (items != null) {
      serverResponse.receivingData = true;
      updateMe();

      await _addParticipants(items[0]);
      serverResponse.receivingData = false;
      updateMe();
    }
  }

  Future<void> onReCheckParticipants() async {
    serverResponse.receivingData = true;
    updateMe();
    await pController.requestParticipantIds(pController.participantsAll.reversed.toList());
    await pController.requestParticipantsPublicKey(pController.participantsAll.reversed.toList());
    serverResponse.receivingData = false;
    updateMe();
  }

  Future<ServerResponse> _sendLotCodes(List<Participant> pList) async {
    List<String> yamlMsgList = [];

    for (var i = 0; i < pList.length; i++) {
      Participant p = pList[i];

      late LotCode lotCode;
      if (p.lotCodeKey != null) {
        lotCode = lotController.getLotCodeByIndex(p.lotCodeKey);
      } else {
        lotCode = lotCodesAvailable.first;
      }

      lotCode.status = 2;
      p.lotCode = lotCode.lotCode;
      p.lotCodeKey = lotCode.key;

      Log.d(_TAG, "_sendLotCodes; id: ${p.userId}, key: ${p.lotCodeKey}; lotCode: ${p.lotCode}");

      await lotController.saveLotCode(lotCode);
      await pController.saveParticipant(p);

      await p.encryptLotCode(account);

      Map<String, dynamic> map = lotCodeDeposit(participantId: p.participantId, lotCodeEnc: p.lotCodeEnc!, eventId: event.id);

      String message = await HttpController().createServerRequest(account, map);
      yamlMsgList.add(message);
    }

    Map<String, dynamic> map = envelope(messageList: yamlMsgList);
    return serverResponse = await HttpController().serverRequestSigned(account, map, MainController.to.votingServer!);
  }

  Future<void> onSendLotCodes({required bool sendForce}) async {
    serverResponse.receivingData = true;
    updateMe();

    List<Participant> pList = sendForce
        ? pController.participantsWithPubKey
        : pController.participantsWithPubKey.where((Participant p) => !p.lotCodeDelivered).toList().reversed.toList();

    if (pList.isNotEmpty) {
      serverResponse = await _sendLotCodes(pList);

      if (serverResponse.statusCode != null && serverResponse.statusCode == 200) {
        for (var i = 0; i < pList.length; i++) {
          Participant p = pList[i];
          p.lotCodeDelivered = true;
          await pController.saveParticipant(p);
        }
      } else {
        /// todo: show error message
      }
      updateMe();
    } else {
      serverResponse.receivingData = false;
      updateMe();
    }

    /// message: x codes send
    await showOkAlertDialog(
      context: Get.context!,
      title: "Teilnahmecodes Versand",
      message: 'Es wurden ${pList.length} Teilnahmecodes versendet',
      okLabel: 'ok',
      barrierDismissible: true,
    );
  }

  Future<void> _addParticipants(String itemsString) async {
    List<Participant> newItems = [];

    int imported = 0;
    int items = 0;

    List<String> itemsList = itemsString.split("\n");
    for (var i = 0; i < itemsList.length; i++) {
      var item = itemsList[i];
      String _id = item.trim();
      String id = _id.split(",")[0];
      id = id.split(" ")[0];
      id = id.split(";")[0];
      id = id.split(":")[0];
      id = id.split("|")[0];

      if (id.isNotEmpty) {
        items++;
        String userKey = Participant.userKeyById(id);
        Participant? participant = pController.participantsBox!.get(userKey);
        if (participant == null) {
          participant = Participant(userId: id);
          imported++;
          newItems.add(participant);
          await pController.saveParticipant(participant);
        }
      }
    }
    ;

    if (newItems.isNotEmpty) {
      await pController.requestParticipantIds(newItems);
      updateMe();
      await pController.requestParticipantsPublicKey(newItems);
      updateMe();
    }

    await showOkAlertDialog(
      context: Get.context!,
      title: "Mitglieder Import",
      message: 'Mitglieder gesamt: $items\n'
          'Mitglieder importiert: $imported',
      okLabel: 'ok',
      barrierDismissible: true,
    );
  }

  void onParticipantSelected(Participant participant) {
    if (participant.lotCodeKey != null) {
      participant.lotCode = lotController.getLotCodeByIndex(participant.lotCodeKey!).lotCode;
    }

    TextEditingController cParticipantId = TextEditingController(text: participant.participantId);
    TextEditingController cPubKey = TextEditingController(text: participant.publicKeyConcatB64);
    TextEditingController cLotCode = TextEditingController(text: participant.lotCode);
    TextEditingController cLotCodeEnc = TextEditingController(text: participant.lotCodeEnc);

    showDialog(
        context: Get.context!,
        builder: (BuildContext context) {
          return Container(
            width: 500,
            child: Dialog(
              child: Container(
                height: Get.height * 0.6,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Text(
                      "Mitgliedsnummer: ${participant.userId}",
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6),
                      child: Column(
                        children: [
                          SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 6),
                                  child: TextFieldWidget(
                                    textController: cParticipantId,
                                    hint: "Mitglied-Id",
                                    labelText: "Mitglied-Id",
                                    readOnly: true,
                                    enabled: false,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 12),
                                  child: TextFieldWidget(
                                    textController: cPubKey,
                                    hint: "PublicKey",
                                    labelText: "PublicKey",
                                    maxBoxLength: Get.width * 0.8,
                                    readOnly: true,
                                    enabled: false,
                                    maxLines: 2,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 12),
                                  child: TextFieldWidget(
                                    textController: cLotCode,
                                    hint: "Teilnahmecode",
                                    labelText: "Teilnahmecode",
                                    maxBoxLength: Get.width * 0.8,
                                    readOnly: true,
                                    enabled: false,
                                    maxLines: 2,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 12),
                                  child: CheckboxListTile(
                                    title: Text("Teilnahmecode versendet:"),
                                    value: participant.lotCodeDelivered,
                                    onChanged: (value) => {},
                                  ),
                                )
                              ],
                            ),
                          ),
                          ButtonBar(
                            children: [
                              /// request public key
                              TextButton(
                                  onPressed: () async {
                                    serverResponse.receivingData = true;
                                    serverResponse = await pController.requestParticipantsPublicKey([participant]);
                                    Get.back();
                                    updateMe();
                                    onParticipantSelected(participant);
                                  },
                                  child: Text("PubKey abrufen")),

                              /// send
                              TextButton(
                                  onPressed: participant.publicKeyConcatB64 == null

                                      /// deactivate send button
                                      ? null

                                      /// send button
                                      : () async {
                                          serverResponse.receivingData = true;
                                          updateMe();
                                          serverResponse = await _sendLotCodes([participant]);

                                          if (serverResponse.statusCode != null && serverResponse.statusCode == 200) {
                                            participant.lotCodeDelivered = true;
                                            pController.saveParticipant(participant);
                                          } else {
                                            // participant.lotCodeDelivered = false;
                                            /// todo: show error message
                                          }
                                          Get.back();
                                          updateMe();
                                          onParticipantSelected(participant);
                                        },
                                  child: participant.lotCodeDelivered ? Text("Erneut senden") : Text("Senden")),
                            ],
                          ),
                          ButtonBar(
                            children: [
                              /// delete
                              TextButton(
                                  onPressed: participant.lotCodeDelivered
                                      ? null
                                      : () async {
                                          final OkCancelResult result = await showOkCancelAlertDialog(
                                            context: Get.context!,
                                            title: "Mitglied löschen",
                                            message: 'Bist Du sicher, dass Du das Mitglied löschen möchtest?',
                                            okLabel: 'löschen',
                                            cancelLabel: "Abbrechen",
                                            barrierDismissible: true,
                                          );

                                          Log.d(_TAG, "del_user; result:$result");
                                          if (result == OkCancelResult.ok) {
                                            await pController.participantsBox!.delete(participant.userKey);
                                          }
                                          updateMe();
                                          Get.back();

                                        },
                                  child: Text("Löschen")),

                              /// cancel
                              TextButton(onPressed: () => Get.back(), child: Text("Abbrechen")),
                            ],
                          )
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
            ),
          );
        });
  }

  Future<void> onRefresh() async {
    serverResponse.receivingData = true;
    updateMe();
    await pController.checkSavedParticipants();
    serverResponse.receivingData = false;
    updateMe();
  }

  Future<void> onPostFrameCallback() async {
    // Future.delayed(Duration(milliseconds: 300)).then((_) {
    //   _importLotCodeStorageFromInternalStorage();
    // });
  }

  void updateMe() {
    update();
  }
}

class AdminEventParticipants extends StatelessWidget {
  final Account account;
  final VotingEvent event;
  final ParticipantController pController;
  final LotController lotController;

  AdminEventParticipants({required this.account, required this.event, required this.pController, required this.lotController});

  final AdminParticipantsController controller = Get.put(AdminParticipantsController());

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return OrientationBuilder(
      builder: (_, __) => GetBuilder<AdminParticipantsController>(
          initState: (_) => controller.initController(account, event, pController, lotController),
          builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: Text("Interne Einladungen"),
                actions: [
                  /// refresh
                  IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () => controller.onRefresh(),
                  ),
                ],
              ),
              body: Widgets.loaderBox(
                loading: controller.serverResponse.receivingData,
                child: ScrollBody(
                  children: _body,
                ),
              ))),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(CardWidget(
        titleListTile: CardWidget.listTileTitle(
            leading: Icon(Icons.event),
            title: controller.event.name,
            isThreeLine: true,
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Vorgemerkte Mitglieder: ${controller.pController.participantsBox!.length}"),
                Text("Gültige Mitglieder: ${controller.pController.participantsWithPubKey.length}"),
                _lotsInfo
              ],
            ))));

    list.add((_participantsBox));
    return list;
  }

  /// LOT SEARCH FIELD
  Widget get _participantSearchField => ListTile(
        title: TextField(
            onChanged: (value) => controller.onParticipantSearch(value),
            decoration: InputDecoration(
                // suffixIcon: Icon(Icons.search),
                contentPadding: const EdgeInsets.only(bottom: 10),
                isDense: false,
                isCollapsed: true,
                hintText: "Nach Mitglied suchen")),
        contentPadding: EdgeInsets.all(0),
        trailing: Icon(Icons.search),
      );

  /// PARTICIPANTS-LIST
  Widget get _participantsBox => CardWidget(
        titleListTile: CardWidget.listTileTitle(
          leading: Icon(Icons.list),
          title: "Mitglieder",
        ),
        child: Column(
          children: [
            _participantSearchField,
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
                decoration: BoxDecoration(border: Border.all(color: Colors.black45, width: 1)),
                height: controller.memberListHeight,
                child: CupertinoScrollbar(
                    child: ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: controller.pController.participantsBox!.length,
                  itemBuilder: (_, index) => _participantItem(index),
                )),
              ),
            ),
            _buttons
          ],
        ),
      );

  /// PARTICIPANT-ITEM
  Widget _participantItem(index) {
    final Participant participant = controller.pController.getParticipantByIndex(index);

    return InkWell(
      onTap: () => controller.onParticipantSelected(participant),
      child: Padding(
        padding: const EdgeInsets.only(top: 3),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text("${participant.userId}"),

                  /// PUB-KEY
                  Padding(
                    padding: const EdgeInsets.only(left: 18),
                    child: Row(
                      children: [Text("Public-Key:"), _pubKeyIcon(participant)],
                    ),
                  ),

                  /// Lot-Code Send
                  Padding(
                    padding: const EdgeInsets.only(left: 18),
                    child: Row(
                      children: [Text("Teilnahmecode:"), _lotCodeIcon(participant)],
                    ),
                  ),
                  // Text("Key:${controller.participants[index].userId}"),
                ],
              ),
              Divider(
                thickness: 1,
                color: dividerColor,
              )
            ],
          )),
        ),
      ),
    );
  }

  Widget get _lotsInfo {
    Widget _lotCount;
    if (controller.importingCodes) {
      _lotCount = Widgets.loader;
    } else {
      _lotCount = Text(
        controller.lotController.lotCodeBox != null ? "${controller.lotCodesAvailable.length}" : "0",
        style: TextStyle(fontWeight: FontWeight.bold),
      );
    }

    return Row(
      children: [
        Text("Verfügbare Teilnahmecodes:"),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: _lotCount,
        ),

        /// btn Codes verwalten
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: ButtonSub(
              child: Text("Codes verwalten"),
              onPressed: () => Get.to(() => AdminEventLots(
                  server: AdminController.to.server,
                  event: event,
                  account: account,
                  lotController: lotController,
                  pController: pController
              ))!.then((value) => controller.updateMe())),
        )

      ],
    );
  }

  Widget get _buttons {
    List<Widget> _items = [];
    _items.add(ButtonSub(child: Text("Mitglieder hinzufügen"), onPressed: () => controller.onAddParticipants()));
    if (controller.pController.participantsWithPubKey.length > 0) {
      _items.add(ButtonSub(
          child: Text("Einladungen versenden"),
          onPressed: controller.lotCodesAvailable.length >= controller.pController.participantsWithPubKey.length
              ? () => controller.onSendLotCodes(sendForce: false)
              : null));
    }
    if (controller.pController.participantsAll.isNotEmpty) {
      _items.add(ButtonSub(child: Text("Liste aktualisieren"), onPressed: () => controller.onReCheckParticipants()));
    }
    return ButtonBar(
      children: _items,
    );
  }

  Widget _pubKeyIcon(Participant participant) {
    return Icon(
      participant.publicKeyConcatB64 != null ? Icons.check : Icons.warning_amber_sharp,
      color: participant.publicKeyConcatB64 != null ? Colors.green : Colors.red,
      size: 18,
    );
  }

  Widget _lotCodeIcon(Participant participant) {
    return Icon(
      Icons.check,
      color: participant.lotCodeDelivered ? Colors.green : Colors.black26,
      size: 18,
    );
  }
}
