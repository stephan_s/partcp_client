// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/ui/admin/controller/lot_controller.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/submit_button.dart';

import '../widgets/table_textfield.dart';

class AdminEventLotsRegController extends GetxController {
  static const _TAG = "AdminEventLotsRegController";

  late VotingServer server;
  late VotingEvent event;
  late Account account;
  late LotController lotController;


  // /// [VotingEvent] can accept one or two registration rules:
  // /// [VotingEvent.paperLots] and [VotingEvent.lotCodes]
  // /// If booth are accepted, we must define, witch codes we want to generate
  // CredentialTypeEnum currentCredentialType;
  // List<CredentialTypeEnum> credentialTypeList = [];
  //
  // /// Values class
  // LotCodeValues lotCodeValues = LotCodeValues();
  // CredentialNamingValues namingValues = CredentialNamingValues();

  ServerResponse serverResponse = ServerResponse();

  bool get allowRequest => event.administrable &&
      count > 0 &&
      !serverResponse.receivingData;

  int count = 0;

  List<bool> expandedPanel = [false, false];

  /// lots count controller
  TextEditingController countController = TextEditingController();

  void initController(VotingServer server, VotingEvent event, Account account, LotController lotController) {
    this.server = server;
    this.event = event;
    this.account = account;
    this.lotController = lotController;
  }


  void onCount() {
    count = countController.text.isEmpty
      ? 0
      : _setIntValue(countController, count);
    updateMe();
  }


  int _setIntValue(TextEditingController controller, var lastValue) {
    var value = controller.text;
    var trimmed = controller.text.trim();

    if (SUtils.isInt(value) && value == trimmed) {
      return int.tryParse(controller.text)!;
    }
    else {
      controller.text = lastValue.toString();
      controller.selection = TextSelection.fromPosition(TextPosition(offset: controller.text.length));
      updateMe();
      return lastValue;
    }
  }

  Future<void> onCodesRequest() async {
    serverResponse.errorMessage = null;
    serverResponse.receivingData = true;
    updateMe();

    final Map<String, dynamic> messageMap = multiRegistration(
      event: event,
      count: count,
    );
    serverResponse = await HttpController().serverRequestSigned(account, messageMap, server);

    if (serverResponse.statusCode == 200) {
      String yamlStrgEnc = (serverResponse.bodyMap!["Lot-Codes~"]).trim();

      String? yamlString = await account.decryptServerMessage(
          messageEnc: yamlStrgEnc,
          server: server);

      if (yamlString != null) {
        lotController.addLotCodesFromServerResponse(yamlString);
      }

      else {
        /// todo: show error message
      }
      onGetBack(true);
    }

    updateMe();
  }

  Future<void> onPostFrameCallback() async {
    updateMe();
  }

  void onGetBack(bool isSuccess) {
    /// we send [true] to the caller page when codes was generated
    Get.back(result: isSuccess);
  }

  void updateMe() {
    update();
  }
}



class AdminEventLotsReg extends StatelessWidget {
  final VotingServer server;
  final VotingEvent event;
  final Account account;
  final LotController lotController;

  AdminEventLotsReg({
    required this.server,
    required this.event,
    required this.account,
    required this.lotController
  });

  final AdminEventLotsRegController controller =
      Get.put(AdminEventLotsRegController());
  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return WillPopScope(
      onWillPop: () async => !controller.serverResponse.receivingData,
      child: GetBuilder<AdminEventLotsRegController>(
          initState: (_) => controller.initController(server, event, account, lotController),
          builder: (_) => Scaffold(
                appBar: MyAppBar(
                  title: Text("Teilnahmecodes erzeugen"),
                ),
                body: OrientationBuilder(
                  builder: (_, __) => ListView(
                    children: _body(context),
                  ),
                ),
              )),
    );
  }

  /// BODY
  List<Widget> _body(BuildContext context) {
    List<Widget> list = [];

    // int lotsCreated = account.credentialLotCodes[event.id] != null
    //   ? account.credentialLotCodes[event.id].length
    //   : 0;
    ///------------------------------------------------------------------------------------- todo
    int lotsCreated = 0;

    list.add(CardWidget(
        titleListTile: CardWidget.listTileTitle(
            leading: Icon(Icons.event),
            title: controller.event.name,
            isThreeLine: true,
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(SUtils.formatDate(controller.event.date)),
                Text("Erstellte Codes: ${controller.event.lotsCreated}")
              ],
            ))));

    list.add(_codesCount);

    if (controller.serverResponse.errorMessage != null) {
      list.add(CardWidget(
          child: ServerErrorMessage(serverResponse: controller.serverResponse))
      );
    }

    list.add(_requestButton);
    return list;
  }


  Widget get _codesCount => CardWidget(
        titleListTile: CardWidget.listTileTitle(
          leading: Icon(Icons.list),
          title: "Teilnahmecodes erzeugen",
        ),
        child: Table(
          // defaultColumnWidth: FlexColumnWidth(1.5),
          columnWidths: {0: FractionColumnWidth(.7), 1: FractionColumnWidth(.3)},
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: [
            /// COUNT
            TableRow(children: [
              Text("Anzahl", style: TextStyle(fontWeight: FontWeight.bold)),
              tableTextField(
                  controller: controller.countController,
                  maxLen: 7,
                  isInt: true,
                  onChanged: (_) => controller.onCount(),
                  onSubmitted: controller.allowRequest
                      ? () => controller.onCodesRequest()
                      : null,
                  administrable: controller.event.administrable
              ),
            ]),

          ],
        ),
      );



  /// REQUEST BUTTON
  Widget get _requestButton => CardWidget(
        child: SubmitButton(
          enabled: controller.allowRequest,
          submitting: controller.serverResponse.receivingData,
          onPressed: () => controller.onCodesRequest(),
          text: "Codes erzeugen",
        ),
      );

}
