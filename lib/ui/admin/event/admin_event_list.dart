// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/styles/styles.dart';
import 'package:partcp_client/ui/admin/event/admin_event.dart';
import 'package:partcp_client/ui/admin/event/admin_event_edit_text.dart';
import 'package:partcp_client/ui/admin/controller/admin_controller.dart';
import 'package:partcp_client/utils/field_width.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/objects/storage/user_box.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_icon.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/widgets.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';

import '../widgets/styles.dart';

class AdminEventListController extends GetxController {
  static const _TAG = "AdminEventListController";

  static AdminEventListController to = Get.find<AdminEventListController>();

  late VotingServer server;
  late Account account;
  Group? group;
  ServerResponse serverResponse = ServerResponse();
  String? lastSelectedEventId;

  List<VotingEvent>? currentGroupEvents;

  void initController(VotingServer server, Account account) {
    Get.put(AdminController());
    AdminController.to.server = server;

    /// todo: remove VotingServer from method Calls and use ist from [AdminController]
    this.server = server;
    this.account = account;
    Log.d(_TAG, "initController; account: ${account.userId}");
  }

  Future<void> onGroupsChanges(Group group, {bool force = false}) async {
    this.group = group;
    await MainController.to.userBox!.setValue(UserBoxEnum.moderatorLastGroupId, group.id);
    serverResponse.receivingData = true;
    updateMe();

    serverResponse = await server.eventsByGroup(
        account, group, includeVotingCount: true, includeAdminInfo: true, force: force);

    currentGroupEvents = serverResponse.object;
    updateMe();
  }


  Future<void> onPostFrameCallback() async {
    Log.d(_TAG, "onPostFrameCallback");

    if (server.getGroupById(MainController.to.userBox!.getValue(UserBoxEnum.moderatorLastGroupId)) != null) {
      group = server.getGroupById(MainController.to.userBox!.getValue(UserBoxEnum.moderatorLastGroupId))!;
    } else {
      group = server.serverGroups[0];
    }
    await onRefresh();
  }


  Future<void> onRefresh() async {
    Log.d(_TAG, "onRefresh()");

    String? lastGroupId = group != null ?group!.id : null;
    serverResponse.receivingData = true;
    updateMe();

    serverResponse = await server.sGroupListRequest(account, includeAdminInfo: true);
    Group? testGroup = server.getGroupById(lastGroupId);

    group = testGroup != null ? testGroup : server.serverGroups[0];
    if (group != null) {
      serverResponse = await server.eventsByGroup(account, group!, force: true, includeVotingCount: true, includeAdminInfo: true);
      currentGroupEvents = serverResponse.object;
      // currentGroupEvents = (await server.eventsByGroup(account, group, force: true, includeVotingCount: true, includeAdminInfo: true)).object;
    }
    updateMe();
  }

  Future<void> onEventClick(VotingEvent event) async {
    Log.d(_TAG, "onEventClick(${event.id}");
    AdminController adminController = AdminController.to;
    lastSelectedEventId = event.id;

    await Get.to(() => AdminEvent(server: server, account: account, group: group!, event: event))!
        .then((_) => updateMe());

    Log.d(_TAG, "postOnEventClick(${event.id}; forceUpdateEvents: ${adminController.forceUpdateEvents}");
    if (adminController.forceUpdateEvents) {
      await onRefresh();
      adminController.forceUpdateEvents = false;
    }
  }

  /// [onRefresh] will be called after creating new Event
  /// from [AdminEventEditSettingsController.onSubmit]

  Future<void> onCreateNewEvent() async {
    Get.to(() => AdminEventEditText(
      server: server,
      account: account,
      event: null,
      group: group!,
    ))!.then((_) => updateMe());
  }

  void updateMe() {
    update();
  }
}

class AdminEventList extends StatelessWidget {
  final VotingServer server;
  final Account account;

  AdminEventList({
    required this.server,
    required this.account,
  });

  final AdminEventListController controller = Get.put(AdminEventListController());

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return GetBuilder<AdminEventListController>(
        initState: (_) => controller.initController(server, account),
        builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: controller.group != null
                  ? Text("Veranstaltungen (${controller.group!.name})")
                  : Text("Veranstaltungen"),
                actions: [
                  /// refresh
                  IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: () => controller.onRefresh(),
                  ),
                ],
              ),
              body: OrientationBuilder(
                builder: (_, __) => ScrollBody(
                  children: [
                    // SaveWalletSnackBarPage(),
                    ServerErrorMessage(serverResponse: controller.serverResponse),
                    CardWidget(
                        titleListTile: CardWidget.listTileTitle(leading: Icon(Icons.group), title: "Wähle die gewünschte Gruppe"),
                        child: _groupDropDownList()),
                    Widgets.loaderBox(child: _eventCardWidget, loading: controller.serverResponse.receivingData),
                  ],
                ),
              ),
            ));
  }

  Widget _groupDropDownList() {
    return DropdownButton<Group>(
      value: controller.group,
      items: _groups(),
      onChanged: (item) => controller.onGroupsChanges(item as Group),
      elevation: 1,
    );
  }

  List<DropdownMenuItem<Group>> _groups() {
    List<DropdownMenuItem<Group>> list = [];
    for (var i = 0; i < controller.server.serverGroups.length; i++) {
      list.add(_groupItem(controller.server.serverGroups[i]));
    }
    return list;
  }

  DropdownMenuItem<Group> _groupItem(Group group) {
    return DropdownMenuItem(
      child: Container(
        constraints: BoxConstraints(minWidth: 100, maxWidth: fieldBoxMaxWidth * 0.9),
        child: ListTile(
          leading: Icon(
            Icons.group,
          ),
          title: Text(group.name),
          // subtitle: Text("ID: ${event.id}"),
          contentPadding: EdgeInsets.all(0),
          // selected: GetPlatform.isMobile && group == controller.group,
          selected: group == controller.group,
        ),
      ),
      value: group,
    );
  }

  Widget get _eventCardWidget => CardWidget(
        titleListTile: CardWidget.listTileTitle(
          leading: Icon(Icons.event),
          title: "Veranstaltungen",
          subtitle: controller.group != null
            ? Text("${controller.group!.name}")
            : null,
          trailing: AdminController.allow(EditType.eventCreate, controller.group)
              ? ButtonIcon(
                  icon: Icon(Icons.add_circle_outline),
                  onPressed: () => controller.onCreateNewEvent()
                  )
              : null,
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _events,
              controller.currentGroupEvents != null && controller.currentGroupEvents!.length > 3
                ? _bottomNewEventButton
                : Container(),
            ],
          ),
        ),
      );

  Widget get _events {
    if (controller.currentGroupEvents == null) {
      return Widgets.loader;
    }
    return controller.currentGroupEvents!.isNotEmpty ? _eventListView : _noEvents;
  }

  Widget get _noEvents => CardWidget.listTileTitle(
      // leading: Icon(Icons.error),
      title: "In dieser Gruppe finden zur Zeit keine Abstimmungen statt");

  Widget get _eventListView {
    if (controller.currentGroupEvents!.isEmpty) return Container();

    return ListView.separated(
      shrinkWrap: true,
      itemCount: controller.currentGroupEvents!.length,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (_, int index) => _eventItem(index),
      separatorBuilder: (_, index) => listDivider,
    );
  }


  Widget _eventItem(int index) {
    VotingEvent? event = controller.currentGroupEvents![index];

    return ListTile(
      contentPadding: EdgeInsets.all(0),
      tileColor: controller.lastSelectedEventId != null && controller.lastSelectedEventId! == event.id ? Styles.selectedTileColor : null,
      leading: Text(SUtils.formatDate(event.date)),
      title: Text(
        event.name,
        style: AdminController.allow(EditType.eventEdit, event) ? TextStyle(color: Styles.buttonColor) : null,
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Veranstaltungen:"),
          Row(
              children: [
                Text("  - aktiv: "),
                Text(" ${event.votingCount.open}", style: TextStyle(fontWeight: FontWeight.bold),),
              ]
          ),
          Text("  - wartend: ${event.votingCount.idle}"),
          Text("  - beendet: ${event.votingCount.closed + event.votingCount.finished}"),
        ],
      ),
      onTap: () => controller.onEventClick(event),
    );
  }

  Widget get _bottomNewEventButton => Column(

    children: [
      listDivider,
      Align(
          alignment: Alignment.centerRight,
          child: ButtonIcon(
            icon: Icon(Icons.add_circle_outline),
            onPressed: () => controller.onCreateNewEvent()
        ),
      ),
    ],
  );

  // Widget get _listDivider => Divider(
  //   thickness: 1,
  //   color: dividerColor,
  // );
}
