// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/credential_rules.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/utils/field_width.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_main.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';

import 'admin_event_list.dart';
import '../controller/admin_controller.dart';
import '../widgets/read_only.dart';
import '../widgets/table_textfield.dart';

class AdminEventEditSettingsController extends GetxController {
  late VotingServer server;
  late VotingEvent event;
  late Account account;
  late Group group;

  NamingValues _namingDefaults = NamingDefaults();
  LotCodeValues _lotCodeDefaults = LotCodeDefaults();
  CredentialValues _credentialDefaults = CredentialDefaults();

  static const _TAG = "AdminEventEditSettingsController";

  ServerResponse serverResponse = ServerResponse();

  bool get allowEdit => AdminController.allow(EditType.eventLotSettings, event);

  bool get allowSubmit =>
      SUtils.isInt(estimatedTurnoutController.text.trim()) &&
      SUtils.isInt(lotLenController.text.trim()) &&
      SUtils.isInt(lotCrcLenController.text.trim()) &&
      SUtils.isInt(lotGroupLenController.text.trim()) &&
      SUtils.isInt(credentialLenController.text.trim()) &&
      SUtils.isInt(credentialCrcLenController.text.trim()) &&
      SUtils.isInt(credentialGroupLenController.text.trim()) &&
      SUtils.isInt(namingCounterWidthController.text.trim()) &&
      SUtils.isInt(namingCrcLenController.text.trim()) &&
      SUtils.isInt(namingGroupLenController.text.trim()) &&
      (event.paperLots || event.lotCodes) &&
      !serverResponse.receivingData;

  List<bool> expandedPanel = [false, false, false];

  List<CredentialTypeEnum> credentialTypeList = [CredentialTypeEnum.lotCode, CredentialTypeEnum.paperLots];

  TextEditingController estimatedTurnoutController = TextEditingController();

  // lotCode controller
  TextEditingController lotLenController = TextEditingController();
  TextEditingController lotCrcLenController = TextEditingController();
  TextEditingController lotGroupLenController = TextEditingController();
  TextEditingController lotGroupSeparatorController = TextEditingController();

  // paperLots controller
  TextEditingController credentialLenController = TextEditingController();
  TextEditingController credentialCrcLenController = TextEditingController();
  TextEditingController credentialGroupLenController = TextEditingController();
  TextEditingController credentialGroupSeparatorController = TextEditingController();

  // [NamingValues] controller
  // TextEditingController namingPrefixController = TextEditingController();
  TextEditingController namingCounterStartController = TextEditingController();
  TextEditingController namingCounterWidthController = TextEditingController();
  TextEditingController namingCrcLenController = TextEditingController();
  TextEditingController namingGroupLenController = TextEditingController();
  TextEditingController namingGroupSeparatorController = TextEditingController();

  void initController(VotingServer server, Account account, VotingEvent event, Group group) {
    this.server = server;
    this.account = account;
    this.group = group;
    this.event = event;

    if (event.id.isEmpty) {
      /// new
      _namingDefaults = NamingDefaults();
      _lotCodeDefaults = LotCodeDefaults();
      _credentialDefaults = CredentialDefaults();
    } else {
      /// edit
      _namingDefaults = event.namingValues;
      _lotCodeDefaults = event.lotCodeValues;
      _credentialDefaults = event.credentialValues;
    }

    event.lotCodes = true;
    event.paperLots = false;
  }

  Future<void> onPostFrameCallback() async {
    /// new created [VotingEvent] has an empty [event.id]
    if (event.id.isNotEmpty) {
      _setEventEditData();
    } else {
      event.admins = [account.userId!];
      _setDefaults();
    }
    updateMe();
  }

  void onReset() {
    _setDefaults();
    updateMe();
  }

  Future<void> onRefresh() async {
    await event.sEventDetailsRequest(server, account);
    updateMe();
  }

  void _setDefaults() {
    event.lotCodeValues = _lotCodeDefaults;
    event.namingValues = _namingDefaults;
    event.credentialValues = _credentialDefaults;

    lotLenController.text = event.lotCodeValues.finalLength.toString();
    lotCrcLenController.text = event.lotCodeValues.crcLength.toString();
    lotGroupLenController.text = event.lotCodeValues.groupLength.toString();
    lotGroupSeparatorController.text = event.lotCodeValues.groupSeparator;

    credentialLenController.text = event.credentialValues.finalLength.toString();
    credentialCrcLenController.text = event.credentialValues.crcLength.toString();
    credentialGroupLenController.text = event.credentialValues.groupLength.toString();
    credentialGroupSeparatorController.text = event.credentialValues.groupSeparator;

    namingCounterWidthController.text = event.namingValues.counterWidth.toString();
    namingCrcLenController.text = event.namingValues.crcLength.toString();
    namingGroupLenController.text = event.namingValues.groupLength.toString();
    namingGroupSeparatorController.text = event.namingValues.groupSeparator;
  }

  void _setEventEditData() {
    estimatedTurnoutController.text = event.estimatedTurnout.toString();

    lotLenController.text = event.lotCodeValues.finalLength.toString();
    lotCrcLenController.text = event.lotCodeValues.crcLength.toString();
    lotGroupLenController.text = event.lotCodeValues.groupLength.toString();
    lotGroupSeparatorController.text = event.lotCodeValues.groupSeparator;

    credentialLenController.text = event.credentialValues.finalLength.toString();
    credentialCrcLenController.text = event.credentialValues.crcLength.toString();
    credentialGroupLenController.text = event.credentialValues.groupLength.toString();
    credentialGroupSeparatorController.text = event.credentialValues.groupSeparator;

    namingCounterWidthController.text = event.namingValues.counterWidth.toString();
    namingCrcLenController.text = event.namingValues.crcLength.toString();
    namingGroupLenController.text = event.namingValues.groupLength.toString();
    namingGroupSeparatorController.text = event.namingValues.groupSeparator;
  }

  void _setEventData() {
    event.estimatedTurnout = int.tryParse(estimatedTurnoutController.text)!;

    event.lotCodeValues.finalLength = int.tryParse(lotLenController.text)!;
    event.lotCodeValues.crcLength = int.tryParse(lotCrcLenController.text)!;
    event.lotCodeValues.groupLength = int.tryParse(lotGroupLenController.text)!;
    event.lotCodeValues.groupSeparator = lotGroupSeparatorController.text;

    event.credentialValues.finalLength = int.tryParse(credentialLenController.text)!;
    event.credentialValues.crcLength = int.tryParse(credentialCrcLenController.text)!;
    event.credentialValues.groupLength = int.tryParse(credentialGroupLenController.text)!;
    event.credentialValues.groupSeparator = credentialGroupSeparatorController.text;

    event.namingValues.counterWidth = int.tryParse(namingCounterWidthController.text)!;
    event.namingValues.crcLength = int.tryParse(namingCrcLenController.text)!;
    event.namingValues.groupLength = int.tryParse(namingGroupLenController.text)!;
    event.namingValues.groupSeparator = namingGroupSeparatorController.text;
  }

  /// ESTIMATE TURNOUT
  void onEstimateTurnoutChanged() {
    updateMe();
  }

  /// LOT CODES
  // void onLotCodesSwitchChanged(bool value) {
  //   event.lotCodes = value;
  //   updateMe();
  // }

  /// LOT CODES
  // void onPaperLotsSwitchChanged(bool value) {
  //   event.paperLots = value;
  //   updateMe();
  // }

  ///////////////////////////////////////////////////////////////////////////
  /// Lot Values
  ///
  void onLotLen(CredentialTypeEnum type) {
    updateMe();
  }

  void onLotCrcLen(CredentialTypeEnum type) {
    updateMe();
  }

  void onLotGroupLen(CredentialTypeEnum type) {
    updateMe();
  }

  ///////////////////////////////////////////////////////////////////////////
  /// Credential naming Values
  ///
  // void onNamingPrefix() {
  //   namingValues.prefix = namingPrefixController.text;
  // }

  void onNamingCounterWidth() {
    updateMe();
  }

  void onNamingCrcLen() {
    updateMe();
  }

  void onNamingGroupLen() {
    updateMe();
  }


  Future<void> onSubmit() async {
    _setEventData();

    serverResponse.errorMessage = null;
    serverResponse.receivingData = true;
    updateMe();

    final Map<String, dynamic> messageMap = event.id.isEmpty ? eventDefinition(group, event) : eventUpdateRequest(group, event);

    serverResponse = await HttpController().serverRequestSigned(account, messageMap, server);

    if (serverResponse.statusCode == 200) {
      AdminEventListController.to.onRefresh();
      Get.back();
    }
    updateMe();
  }

  void updateMe() {
    update();
  }
}

class AdminEventEditSettings extends StatelessWidget {
  final VotingServer server;
  final Account account;
  final VotingEvent event;
  final Group group;

  AdminEventEditSettings({required this.server, required this.account, required this.event, required this.group});

  final AdminEventEditSettingsController controller = Get.put(AdminEventEditSettingsController());

  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return WillPopScope(
      onWillPop: () async => true,
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<AdminEventEditSettingsController>(
            initState: (_) => controller.initController(server, account, event, group),
            builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: Text("Veranstaltungseinstellungen"),
                    actions: [
                      /// refresh
                      IconButton(
                        icon: Icon(Icons.refresh),
                        onPressed: () => controller.onRefresh(),
                      ),
                    ],
                  ),
                  body: ScrollBody(children: _body),
                )),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(CardWidget(
        titleListTile: CardWidget.listTileTitle(
            leading: Icon(Icons.event),
            title: readOnly(allowModify: controller.allowEdit, title: Text("${controller.event.name}")),
        )
      )
    );

    list.add(_mainSettings);

    list.add(_expansionPanelList);

    if (controller.allowEdit) {
      list.add(ServerErrorMessage(serverResponse: controller.serverResponse, useCardWidget: true));
      list.add(_submitButton);
    }

    return list;
  }

  Widget get _mainSettings {
    List<Widget> items = [
      _estimatedTurnout,
      // _lotCodesSwitch,
      // _paperLotsSwitch,
    ];

    return CardWidget(
      // title: "Angaben zum Event",
      child: Column(
        children: items,
      ),
    );
  }


  /// ESTIMATED TURNOUT
  Widget get _estimatedTurnout => Table(
        columnWidths: tableColumnWidth,
        defaultVerticalAlignment: defaultVerticalAlignment,
        children: [
          /// COUNT
          TableRow(children: [
            Text("Voraussichtliche Teilnehmerzahl", style: TextStyle(fontWeight: FontWeight.bold)),
            tableTextField(
                controller: controller.estimatedTurnoutController,
                maxLen: 7,
                isInt: true,
                onChanged: (_) => controller.onEstimateTurnoutChanged(),
                administrable: controller.allowEdit),
          ]),
        ],
      );

  /// EXPANSION PANEL
  Widget get _expansionPanelList {
    List<ExpansionPanel> list = [];

    list.add(_expansionPanel(0, "Einstellungen für Teilnehmerkennung", _namingSettings));
    list.add(_expansionPanel(list.length, "Einstellungen für Teilnahmecodes", _lotCodeSettings));

    return ExpansionPanelList(
        expansionCallback: (int index, bool isExpanded) {
          controller.expandedPanel[index] = !isExpanded;
          controller.updateMe();
        },
        children: list);
  }

  ExpansionPanel _expansionPanel(int index, String title, Widget settingsWidget) {
    return ExpansionPanel(
      canTapOnHeader: true,
      headerBuilder: (BuildContext context, bool isExpanded) {
        return Container(
          padding: paddingForExpansionPanelTitle,
          child: Center(
            child: CardWidget.listTileTitle(
              leading: Icon(Icons.settings),
              title: title,
            ),
          ),
        );
      },
      body: Padding(
        padding: paddingForExpansionPanel,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: Column(
            children: [
              settingsWidget,
              controller.allowEdit
                  ? Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: ButtonMain(
                        onPressed: () => controller.onReset(),
                        child: Text("Werte zurücksetzen"),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
      isExpanded: controller.expandedPanel[index],
    );
  }

  /// LOT CODE SETTINGS
  Widget get _lotCodeSettings {
    final type = CredentialTypeEnum.lotCode;
    return Table(
      columnWidths: tableColumnWidth,
      defaultVerticalAlignment: defaultVerticalAlignment,
      children: [
        /// CODE LEN
        TableRow(children: [
          Text("Anzahl der Zeichen"),
          tableTextField(
              controller: controller.lotLenController,
              maxLen: 2,
              isInt: true,
              onChanged: (_) => controller.onLotLen(type),
              administrable: controller.allowEdit),
        ]),

        /// CRC LEN
        TableRow(children: [
          Text("davon Prüfziffern"),
          tableTextField(
              controller: controller.lotCrcLenController,
              maxLen: 1,
              isInt: true,
              onChanged: (_) => controller.onLotCrcLen(type),
              administrable: controller.allowEdit),
        ]),

        /// GROUP LEN
        TableRow(children: [
          Text("gruppieren in Blöcken à ... Zeichen"),
          tableTextField(
              controller: controller.lotGroupLenController,
              maxLen: 2,
              isInt: true,
              onChanged: (_) => controller.onLotGroupLen(type),
              administrable: controller.allowEdit),
        ]),
      ],
    );
  }

  /// CREDENTIAL SETTINGS
  Widget get _credentialSettings {
    final type = CredentialTypeEnum.paperLots;
    return Table(
      columnWidths: tableColumnWidth,
      defaultVerticalAlignment: defaultVerticalAlignment,
      children: [
        /// CODE LEN
        TableRow(children: [
          Text("Anzahl der Zeichen"),
          tableTextField(
              controller: controller.credentialLenController,
              maxLen: 2,
              isInt: true,
              onChanged: (_) => controller.onLotLen(type),
              administrable: controller.allowEdit),
        ]),

        /// CRC LEN
        TableRow(children: [
          Text("davon Prüfziffern"),
          tableTextField(
              controller: controller.credentialCrcLenController,
              maxLen: 1,
              isInt: true,
              onChanged: (_) => controller.onLotCrcLen(type),
              administrable: controller.allowEdit),
        ]),

        /// GROUP LEN
        TableRow(children: [
          Text("gruppieren in Blöcken à ... Zeichen"),
          tableTextField(
              controller: controller.credentialGroupLenController,
              maxLen: 1,
              isInt: true,
              onChanged: (_) => controller.onLotGroupLen(type),
              administrable: controller.allowEdit),
        ]),
      ],
    );
  }

  /// NAMING SETTINGS
  Widget get _namingSettings => Table(
        columnWidths: tableColumnWidth,
        defaultVerticalAlignment: defaultVerticalAlignment,
        children: [
          /// COUNTER WIDTH
          TableRow(children: [
            Text("Anzahl der Zeichen"),
            tableTextField(
                controller: controller.namingCounterWidthController,
                maxLen: 1,
                isInt: true,
                onChanged: (_) => controller.onNamingCounterWidth(),
                administrable: controller.allowEdit),
          ]),

          /// CRC LEN
          TableRow(children: [
            Text("davon Prüfziffern"),
            tableTextField(
                controller: controller.namingCrcLenController,
                maxLen: 1,
                isInt: true,
                onChanged: (_) => controller.onNamingCrcLen(),
                administrable: controller.allowEdit),
          ]),

          /// GROUP LEN
          TableRow(children: [
            Text("gruppieren in Blöcken à ... Zeichen"),
            tableTextField(
                controller: controller.namingGroupLenController,
                maxLen: 1,
                isInt: true,
                onChanged: (_) => controller.onNamingGroupLen(),
                administrable: controller.allowEdit),
          ]),

        ],
      );

  /// NO MORE EDIT
  Widget get _noMoreEdit => CardWidget(
        child: Text(
          "Für diesee Veranstaltung würden bereits Teilehmer-Codes erzeugt. Änderungen an der Veranstaltung sind nicht mehr zulässig",
          style: TextStyle(color: Colors.red),
        ),
      );

  /// REQUEST BUTTON
  Widget get _submitButton => controller.allowEdit
      ? CardWidget(
          child: ButtonMain(
            onPressed: controller.allowSubmit ? () => controller.onSubmit() : null,
            child: Text(controller.event.id.isEmpty ? "Veranstaltung erzeugen" : "Veranstaltung ändern"),
          ),
        )
      : Container();

}
