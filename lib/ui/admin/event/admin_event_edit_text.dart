// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/ui/admin/event/admin_event_edit_settings.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_icon.dart';
import 'package:partcp_client/widgets/button_main.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/utils/mark_down_to_html.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';

import 'package:partcp_client/widgets/text_field_widget.dart';
import 'package:partcp_client/widgets/widgets.dart';

import '../admin_markdown_edit.dart';
import '../../../widgets/markdown_view.dart';
import '../controller/admin_controller.dart';
import '../widgets/read_only.dart';

const int TITLE_LENGTH = 100;
const int SHORT_DESCRIPTION_LENGTH = 500;

class AdminEventEditTextController extends GetxController {
  late VotingServer server;
  late VotingEvent event;
  late Account account;
  late Group group;

  static const _TAG = "AdminEventEditTextController";

  ServerResponse serverResponse = ServerResponse();

  bool get editMode => event.lotsCreated == 0;

  bool get allowSubmit => titleController.text.isNotEmpty && !serverResponse.receivingData;

  TextEditingController titleController = TextEditingController();
  TextEditingController shortDescriptionController = TextEditingController();

  TextEditingController dateController = TextEditingController();

  void initController(VotingServer server, Account account, VotingEvent? event, Group group) {
    this.server = server;
    this.account = account;
    this.group = group;

    if (event == null) {
      /// new
      this.event = VotingEvent("", server)..administrable = true;
    } else {
      /// edit
      this.event = event;
    }
  }

  Future<void> onPostFrameCallback() async {
    /// new created [VotingEvent] has an empty [event.id]
    if (event.id.isNotEmpty) {
      _setEventEditData();
    } else {
      event.admins = [account.userId!];
      _setDefaults();
    }
    updateMe();
  }

  void onReset() {
    _setDefaults();
    updateMe();
  }

  Future<void> onRefresh() async {
    serverResponse.receivingData = true;
    updateMe();
    serverResponse = await event.sEventDetailsRequest(server, account);
    updateMe();
  }

  void _setDefaults() {
    dateController.text = SUtils.formatDate(event.date);
  }

  void _setEventEditData() {
    titleController.text = event.name;
    dateController.text = SUtils.formatDate(event.date);
    shortDescriptionController.text = event.clientData!.shortDescription;
  }

  void _setEventData() {
    event.name = titleController.text.trim();
    event.clientData!.shortDescription = shortDescriptionController.text.trim();
  }

  /// TITLE
  void onTitleChanged() {
    // event.name = titleController.text;
    updateMe();
  }

  Future<void> onDescriptionEdit() async {
    String? _result = await Get.to(() => AdminMarkdownEdit(title: event.name, text: event.clientData!.description));

    if (_result != null) {
      Log.d(_TAG, "onDescriptionEdit => result: $_result");
      event.clientData!.description = _result.trim();
    }
    updateMe();
  }

  /// DATE
  Future<void> onDateOpen() async {
    // final DateTime initialDate = event != null && event!.date != null ? event!.date! : DateTime.now();
    final DateTime initialDate = event.date != null ? event.date! : DateTime.now();

    var date = await showDatePicker(
        context: Get.context!,
        initialDate: initialDate,
        firstDate: DateTime(DateTime.now().year),
        lastDate: DateTime((DateTime.now().year) + 10));

    if (date == null) {
      date = initialDate;
    }
    event.date = date;
    dateController.text = SUtils.formatDate(event.date);
    updateMe();
  }

  Future<void> onSubmit() async {
    _setEventData();

    serverResponse.errorMessage = null;
    serverResponse.receivingData = true;
    updateMe();

    /// on new event the next page is event settings
    if (event.id.isEmpty) {
      Route<dynamic> route = Get.rawRoute!;

      Get.to(() => AdminEventEditSettings(server: server, account: account, event: event, group: group));

      /// remove this route
      Get.removeRoute(route);
    } else {
      final Map<String, dynamic> messageMap = event.id.isEmpty ? eventDefinition(group, event) : eventUpdateRequest(group, event);

      // String bla = YamlConverter().toYaml(messageMap);
      // Log.d(_TAG, "onSubmit: $bla");

      serverResponse = await HttpController().serverRequestSigned(account, messageMap, server);

      if (serverResponse.statusCode == 200) {
        Get.back();
      }
      updateMe();
    }
  }

  void updateMe() {
    update();
  }
}

class AdminEventEditText extends StatelessWidget {
  final VotingServer server;
  final Account account;
  final VotingEvent? event;
  final Group group;

  AdminEventEditText({required this.server, required this.account, required this.event, required this.group});

  final _tableColumnWidth = {0: FractionColumnWidth(.7), 1: FractionColumnWidth(.3)};

  final _defaultVerticalAlignment = TableCellVerticalAlignment.middle;

  final AdminEventEditTextController controller = Get.put(AdminEventEditTextController());

  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return WillPopScope(
      onWillPop: () async => true,
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<AdminEventEditTextController>(
            initState: (_) => controller.initController(server, account, event, group),
            builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: Text("Veranstaltung bearbeiten"),
                    actions: [
                      /// refresh
                      IconButton(
                        icon: Icon(Icons.refresh),
                        onPressed: () => controller.onRefresh(),
                      ),
                    ],
                  ),
                  body: Widgets.loaderBox(loading: controller.serverResponse.receivingData, child: ScrollBody(children: _body)),
                )),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(CardWidget(
        titleListTile: CardWidget.listTileTitle(
            leading: Icon(Icons.group),
            title: "Gruppe",
            subtitle: readOnly(
                allowModify: AdminController.allow(EditType.eventEdit, controller.event),
                title: Text("${controller.group.name}")))));

    list.add(_mainSettings);
    list.add(_description);

    if (AdminController.allow(EditType.eventEdit, controller.event)) {
      list.add(ServerErrorMessage(serverResponse: controller.serverResponse, useCardWidget: true));
      list.add(_submitButton);
    }

    return list;
  }

  Widget get _mainSettings {
    List<Widget> items = [
      Padding(
        padding: const EdgeInsets.only(top: 6),
        child: _name,
      ),
      Padding(
        padding: const EdgeInsets.only(top: 6),
        child: _date,
      ),
      Padding(
        padding: const EdgeInsets.only(top: 6),
        child: _shortDescription,
      ),
    ];

    return CardWidget(
      title: "Allgemeine Angaben",
      child: Column(
        children: items,
      ),
    );
  }

  /// Name
  Widget get _name => Padding(
    padding: const EdgeInsets.only(top: 6),
    child: TextFieldWidget(
          textController: controller.titleController,
          labelText: "Name $_required",
          hint: "Name",
          onSubmitted: (_) => controller.onTitleChanged(),
          onChanged: (_) => controller.onTitleChanged(),
          onTap: () => {},
          autoCorrect: true,
          textCapitalization: TextCapitalization.sentences,
          keyboardType: TextInputType.text,
          textAlign: TextAlign.start,
          maxBoxLength: null,
          errorText: null,
          maxLength: AdminController.allow(EditType.eventEdit, controller.event) ? TITLE_LENGTH : null,
          maxLengthEnforcement: MaxLengthEnforcement.enforced,
          readOnly: !AdminController.allow(EditType.eventEdit, controller.event),
        ),
  );

  /// Date
  Widget get _date => Table(
        columnWidths: _tableColumnWidth,
        defaultVerticalAlignment: _defaultVerticalAlignment,
        children: [
          /// COUNT
          TableRow(children: [
            TextFieldWidget(
              textController: controller.dateController,
              labelText: "Datum",
              hint: "Datum",
              onSubmitted: (_) => {},
              onChanged: (_) => {},
              onTap: () => {},
              readOnly: true,
              enabled: false,
            ),
            controller.event.administrable
                ? IconButton(icon: Icon(Icons.edit), onPressed: () => controller.onDateOpen())
                : Container(),
          ]),
        ],
      );

  /// short description
  Widget get _shortDescription => Padding(
    padding: const EdgeInsets.only(top: 12),
    child: TextFieldWidget(
          textController: controller.shortDescriptionController,
          labelText: "Kurzbeschreibung",
          hint: "Kurzbeschreibung",
          minLines: 1,
          maxLines: 10,
          onSubmitted: (_) => {},
          onChanged: (_) => {},
          onTap: () => {},
          autoCorrect: true,
          keyboardType: TextInputType.text,
          maxBoxLength: null,
          errorText: null,
          textCapitalization: TextCapitalization.sentences,
          maxLength: SHORT_DESCRIPTION_LENGTH,
          maxLengthEnforcement: MaxLengthEnforcement.enforced,
          textInputAction: TextInputAction.done,
          enabled: controller.event.administrable,
          readOnly: !controller.event.administrable,
        ),
  );

  /// description
  Widget get _description => CardWidget(
        titleListTile: CardWidget.listTileTitle(
            title: "Beschreibung",
            trailing: AdminController.allow(EditType.eventEdit, controller.event)

                /// edit
                ? ButtonIcon(
                    icon: Icon(Icons.edit),
                    onPressed: () => controller.onDescriptionEdit(),
                  )

                /// view
                : ButtonIcon(
                    icon: Icon(Icons.preview),
                    onPressed: () => Get.to(
                        () => MarkdownView(title: controller.event.name, markdownText: controller.event.clientData!.description)),
                  )),
        child: Container(
          constraints: BoxConstraints(minHeight: 0, maxHeight: 300),
          decoration: BoxDecoration(border: Border.all(width: 1, color: Colors.black26)),
          child: Scrollbar(
            isAlwaysShown: true,
            controller: scrollController,
            child: ListView(
              controller: scrollController,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: HtmlWidget(
                    markdownToHtml(controller.event.clientData!.description),
                    textStyle: TextStyle(color: Colors.black87),
                  ),
                )
              ],
            ),
          ),
        ),
      );

  /// SUBMIT BUTTON
  Widget get _submitButton => AdminController.allow(EditType.eventEdit, controller.event)
      ? CardWidget(
          child: ButtonMain(
          onPressed: controller.allowSubmit ? () => controller.onSubmit() : null,
          child: Text(
            controller.event.id.isEmpty ? "Weiter" : "Änderungen speichern",
          ),
        ))
      : Container();

  String get _required => AdminController.allow(EditType.eventEdit, controller.event) ? " (*)" : "";
}
