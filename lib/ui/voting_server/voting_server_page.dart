// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/storage/user_box.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/wallet.dart';
import 'package:partcp_client/ui/admin/widgets/styles.dart';
import 'package:partcp_client/ui/event/event_list_page.dart';
import 'package:partcp_client/ui/home/index_controller.dart';
import 'package:partcp_client/ui/home/index_page.dart';
import 'package:partcp_client/ui/server_user_manager/server_user_manager_page.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/measure_size.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_main.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/submit_button.dart';
import 'package:partcp_client/widgets/text_field_widget.dart';
import 'package:partcp_client/widgets/widgets.dart';

import '../server_user_manager/key_manager_page.dart';

class ServerUrlController extends GetxController {
  final _TAG = "ServerUrlController";

  // List<VotingServer> serverList = [];
  List<VotingServer> menuServerList = [];
  
  ServerResponse serverResponse = ServerResponse();

  Size dropDownMenuSize = Size(100, 80);

  TextEditingController urlController = new TextEditingController();
  MainController mainController = MainController.to;

  Wallet get wallet => mainController.wallet!;
  int serverItemPos = 0;

  // VotingServer placeHolderServer = VotingServer("")
  //   ..name=""
  //   ..isAssetServer = true;


  // VotingServer get currentServer => menuServerList.isEmpty
  //   ? placeHolderServer
  //   : menuServerList[serverItemPos];

  Future<void> initController() async {
    serverResponse.errorMessage = null;

    await _createServerList();

    /// we got server url from url params, we don't need the server select list
    /// todo
    // if (mainController.urlParams.url != null) {
    //   checkServer();
    // } else {
    //   updateMe();
    // }

    updateMe();
  }

  Future<void> _createServerList() async{
    menuServerList.clear();

    List<VotingServer> serverList = await VotingServer.createServerListFromAssetUrl();

    /// last saved server on the top
    menuServerList.addAll(wallet.serverList);

    for (VotingServer server in serverList) {
      if (mainController.wallet!.getServerByIdHash(server.idHashCode) == null) {
        menuServerList.add(server);
      }
    }

    /// server url can be passed with url params
    if (mainController.urlParams.url != null) {
      for (var i = 0; i < menuServerList.length; i++) {
        if (menuServerList[i].url == mainController.urlParams.url) {
          serverItemPos = i;
          return;
        }
      }

      /// server url from params doesn't match serverList -> create new server
      VotingServer _server = VotingServer(mainController.urlParams.url!);
      _server.name = _server.url.split("//").last;
      menuServerList.insert(0, _server);
      serverItemPos = 0;
    }

    // menuServerList.insert(0, placeHolderServer);
  }


  void onUrlCodeChanged() {
    /// todo
  }

  void onUrlCodeSubmit() {
    /// todo
  }

  bool get urlCodeSubmitAllowed => false;

  void checkServer(VotingServer server) async {
    Log.d(_TAG, "checkServerUrl()");

    // VotingServer _server = menuServerList[serverItemPos];
    VotingServer _server = server;

    serverResponse
      ..errorMessage = null
      ..receivingData = true;
    updateMe();

    VotingServer serverHelper = VotingServer(_server.url);
    serverResponse = await serverHelper.getServerInfo(account: MainController.to.wallet!.walletAccount);

    if (serverResponse.errorMessage == null) {
      /// exists server in wallet?
      VotingServer? walletServer = mainController.wallet!.getServerByIdHash(serverHelper.idHashCode);

      /// sever EXISTS in wallet
      if (walletServer != null) {
        /// todo: check if public key changes

        /// copy account to serverHelper
        serverHelper.accounts = walletServer.accounts;

        /// replace walletServer with serverHelper
        walletServer = serverHelper;

        mainController.onWalletChanged(updateNotification: false);
      }

      /// sever NOT EXISTS in wallet
      else {
        mainController.wallet!.addServer(serverHelper);
        mainController.onWalletChanged();
        walletServer = serverHelper;
      }

      mainController.votingServer = walletServer;
      await mainController.userBox!.setValue(UserBoxEnum.userLastServer, mainController.votingServer!.url);


      /// decrypt accounts userId
      for (Account account in mainController.votingServer!.accounts){
        await account.decryptUserId(mainController.wallet!.pwdBytes!);
      }

      /// if url-params for event exists, go to [IdPage] else go to [IndexPage]
      /// todo:
      if (mainController.urlParams.url != null) {
        /// delete server-url from url-params
        // mainController.urlParams.url = null;
        Get.to(() => EventListPage(walletServer!))!.then((value) => updateMe());
      } else {
        if (mainController.wallet!.walletAccount!.userId == null) {
          IndexController.to.updateMe();
          Route<dynamic> route = Get.rawRoute!;

          // Get.to(() => ServerUserManagerPage());
          Get.to(() => ServerUserManagerPage())!
              .then((_) => Get.offAll(() => IndexPage()));

          /// remove this route
          // Get.removeRoute(route);

        } else {
          // Get.offAll(() => IndexPage());
          Get.back();
        }
      }
      // return;
    }

    updateMe();
  }

  // Future<void> onServerItemChanged(int pos) async {
  //   serverItemPos = pos;
  //   VotingServer server = menuServerList[pos];
  //   if (server.url.isNotEmpty) {
  //     await MainController.to.rememberLastServer(server);
  //     updateMe();
  //   }
  // }
  //
  // void onDropdownMenuCreated(Size size) {
  //   if (dropDownMenuSize != size) {
  //     // Log.d(_TAG, "onDropdownMenuCreated; height: ${size.height}");
  //     dropDownMenuSize = size;
  //     updateMe();
  //   }
  // }

  Future<bool> onWillPop() async {
    return true;
  }

  void updateMe() {
    update();
  }
}

class VotingServerPage extends StatelessWidget {
  final ServerUrlController controller = Get.put(ServerUrlController());
  final _TAG = "VotingServerPage";

  final spacer = Container(
    height: 10,
  );

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.initController();
    });

    return WillPopScope(
      onWillPop: controller.onWillPop,
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<ServerUrlController>(
            // initState: (_) => controller.initController(),
            builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: Text("Mit Server verbinden"),
                  ),
                  body: Widgets.loaderBox(loading: controller.serverResponse.receivingData, child: ScrollBody(children: _body)),
                )),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(CardWidget(
      titleListTile: CardWidget.listTileTitle(
        // title: "Server Übersicht",
        subtitle: Text("Wenn Du einen Zugangscode bekommen hast, trage ihn hier ein und klicke auf „Weiter“:"),
      ),
      child: Column(
        children: [
          /// Zugangscode / Servercode
          TextFieldWidget(
            textController: controller.urlController,
            labelText: "Zugangscode",
            hint: "Zugangscode",
            // onTap: () => (_),
            onSubmitted: (_) => (_),
            onChanged: (_) => controller.onUrlCodeChanged(),
            autoCorrect: false,
            isPasswordField: false,
            keyboardType: TextInputType.visiblePassword,
            textAlign: TextAlign.start,
            maxBoxLength: 550,
          ),

          spacer,
          spacer,

          Center(
            child: ButtonMain(
                onPressed: controller.urlCodeSubmitAllowed
                    ? () => controller.onUrlCodeSubmit()
                    : null,
                child: Text("Weiter")
            ),
          )


        ],
      ),
    ));

    list.add(CardWidget(
      titleListTile: CardWidget.listTileTitle(
        // title: "Server Übersicht",
        subtitle: Text("Oder wähle den Server aus, mit dem Du arbeiten möchtest"),
      ),

      child: Column(
        children: [
          ListView(
            shrinkWrap: true,
            children: _serverList,
          ),
        ],
      ),
    ));

    list.add(ServerErrorMessage(serverResponse: controller.serverResponse));
    return list;
  }


  List<Widget> get _serverList {
    List<Widget> items = [];

    for (var i = 0; i < controller.menuServerList.length; i++) {
      items.add(_serverItem(controller.menuServerList[i], i));
    }
    return items;
  }

  Widget _serverItem(VotingServer server, int pos) {
    return Column(
      children: [
        ListTile(
          isThreeLine: true,
          leading: server.isAssetServer
              ? Icon(Icons.link, color: Colors.black54)
              : Icon(Icons.link, color: Colors.black54),
          title: server.isAssetServer
              ? Text("${server.name}")
              : Text("${server.name}\nAccounts: ${server.accounts.length}"),

          subtitle: Text("${server.description}"),
          // selected: GetPlatform.isMobile && server == controller.currentServer,
          contentPadding: EdgeInsets.all(0),
          onTap: () => controller.checkServer(server),
        ),
        listDivider
      ],
    );
  }

}
