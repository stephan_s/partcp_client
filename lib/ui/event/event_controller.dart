// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/wallet.dart';
import 'package:partcp_client/ui/admin/event/admin_event_list.dart';
import 'package:partcp_client/ui/voting/voting_list_page.dart';
import 'package:partcp_client/utils/crc_pass.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/objects/storage/user_box.dart';

import 'event_lot_page.dart';

class EventController extends GetxController {
  final _TAG = "IdController";

  static const minModeratorTanLength = 6;
  static const minParticipantIdLenth = 6;

  static EventController to = Get.find<EventController>();
  ServerResponse serverResponse = ServerResponse();

  late VotingServer server;
  late Mode mode;

  Account? account;

  Group? group;
  Group blankGroup = Group(id: "blank", name: "Blank Group", administrable: false);
  VotingEvent? lastSelectedEvent;

  VotingEvent? event;

  List<VotingEvent> _currentGroupEvents = [];

  bool eventFilterClosed = false;
  bool eventFilterOpen = true;
  bool eventFilterWithLotCode = false;

  // Size dropDownMenuSize = Size(100, 80);

  MainController mainController = MainController.to;

  late TextEditingController namingTextController;
  late TextEditingController credentialTextController;
  late TextEditingController lotCodeTextController;

  List<VotingEvent> get activeEventsForGroup {
    List<VotingEvent> list = [];

    for (VotingEvent event in _currentGroupEvents) {
      if (event.lotCodes || event.paperLots) {

        bool listCandidateOpen = false;
        bool listCandidateClosed = false;
        bool listCandidateWithLotCode = false;

        bool? hasActiveLotCode;
        if (eventFilterWithLotCode) {
          hasActiveLotCode = _hasActiveAccountForEvent(event);
        }

        bool? hasOpenVotings;
        if (eventFilterOpen || eventFilterClosed) {
          hasOpenVotings = _hasOpenVotings(event);
          Log.d(_TAG, "hasOpenVotings; event: ${event.name} => $hasOpenVotings");
        }

        /// open
        listCandidateOpen = (eventFilterOpen && ((hasOpenVotings != null && hasOpenVotings)));
        // ToDo: Remove if-statements like this? (prettier and more compact)

        /// closed
        if (eventFilterClosed && ( (hasOpenVotings != null && !hasOpenVotings) ) ) {
          listCandidateClosed = true;
        }

        /// with lotCode
        if ( (eventFilterWithLotCode && hasActiveLotCode != null && hasActiveLotCode) ) {
          listCandidateWithLotCode = true;
        }


        if (listCandidateOpen || listCandidateClosed) {
          if (eventFilterWithLotCode && listCandidateWithLotCode) {
            list.add(event);
          }
          else if (!eventFilterWithLotCode) {
            list.add(event);
          }
        }

      }
    }
    return list;
  }

  bool _hasActiveAccountForEvent(VotingEvent event) {
    final Account? _accountForEvent = accountForEvent(event);
    // if (_accountForEvent != null && _accountForEvent.isValidAccount()) {
    if (_accountForEvent != null) {
      return true;
    }
    return false;
  }

  bool _hasOpenVotings(VotingEvent event) {
    for (Voting voting in event.votings) {
      if (voting.status == Voting.STATUS_OPEN) {
        return true;
      }
    }
    return false;
  }

  // bool get moderatorAllowNext {
  //   return (namingTextController.text.length >= minParticipantIdLenth &&
  //           credentialTextController.text.length >= minModeratorTanLength) ||
  //       moderatorAccountForCurrentServer() != null;
  // }

  bool get votingAllowNext {
    if (serverResponse.receivingData || event == null) {
      return false;
    }

    /// userId + tan OR lotCode
    if (event!.paperLots && event!.lotCodes) {
      if (namingTextController.text.isNotEmpty && credentialTextController.text.isNotEmpty) {
        return _checkUserIdAndTanFields;
      }
      if (lotCodeTextController.text.isNotEmpty) {
        return _checkLotCodeField;
      }
      return false;
    }

    /// userId + tan
    if (event!.paperLots) {
      return _checkUserIdAndTanFields;
    }

    /// lotCode
    else if (event!.lotCodes) {
      return _checkLotCodeField || votingAccountForCurrentEvent != null;
    }
    return false;
  }

  bool get _checkUserIdAndTanFields {
    return _checkNamingField && _checkCredentialField;
  }

  bool get _checkNamingField {
    return (!serverResponse.receivingData &&
        crcPass(
            text: namingTextController.text,
            crcLength: event!.namingValues.crcLength,
            finalLength: event!.namingValues.pattern.length));
  }

  bool get _checkCredentialField {
    return (!serverResponse.receivingData &&
        crcPass(
            text: credentialTextController.text,
            crcLength: event!.credentialValues.crcLength,
            //finalLength: event.credentialValues.finalLength
            finalLength: event!.credentialValues.pattern.length));
  }

  bool get _checkLotCodeField {
    Log.d(
        _TAG,
        "_checkLotCodeField; "
        "lotCode: ${lotCodeTextController.text} "
        "lotCode length: ${lotCodeTextController.text.length} "
        //"event.lotCodeValues.finalLength: ${event.lotCodeValues.finalLength}");
        "event.lotCodeValues.finalLength: ${event!.lotCodeValues.pattern.length}");

    return (!serverResponse.receivingData &&
        crcPass(
            text: lotCodeTextController.text,
            crcLength: event!.lotCodeValues.crcLength,
            //finalLength: event.lotCodeValues.finalLength
            finalLength: event!.lotCodeValues.pattern.length));
  }

  /// disabled, when lotCode field has text
  bool get idFieldsEnabled {
    if (event!.paperLots && event!.lotCodes) {
      return lotCodeTextController.text.length == 0;
    }
    return true;
  }

  /// disabled when  naming or credential has text
  bool get lotFieldEnabled {
    if (event!.paperLots && event!.lotCodes) {
      return namingTextController.text.length == 0 && credentialTextController.text.length == 0;
    }
    return true;
  }

  Future<void> initController(VotingServer server, Mode mode) async {
    Log.d(_TAG, "initController");
    this.server = server;
    this.mode = mode;

    serverResponse
      ..errorMessage = null
      ..receivingData = false;

    namingTextController = TextEditingController();
    credentialTextController = TextEditingController();
    lotCodeTextController = TextEditingController();

    /// todo: if URL Params given
    // if (server.getGroupById(MainController.to.userBox!.getLastGroupIdByServer(server)) != null) {
    //   group = server.getGroupById(MainController.to.userBox!.getLastGroupIdByServer(server));
    // } else {
    //   group = blankGroup;
    // }
    group = blankGroup;
    // User box is disabled for Stimm-App GUI development

    /// if URL Params given
    /// EVENT-ID
    // if (mainController.urlParams.eventId != null) {
    //   votingEvent = server.getEventById(mainController.urlParams.eventId);
    //   if (votingEvent == null) {
    //     /// toDo: show warn/error message
    //     votingEvent = server.events[0];
    //   }
    // }
    // /// LOT-CODE
    // if (mainController.urlParams.lotCode != null) {
    //   lotCodeTextController.text = mainController.urlParams.lotCode;
    // }
    // if (votingEvent == null) {
    //   votingEvent = server.events[0];
    // }
  }

  // Future<void> onPostFrameCallback() async {
  //   if ( await _fastJumpToVoting()) {
  //     _goToVotingPage();
  //   }
  //   else {
  //     updateMe();
  //   }
  // }

  Future<void> onPostFrameCallback() async {
    // await onRefresh();
    await onGroupsChanges(group!);
  }

  Future<void> onRefresh() async {
    serverResponse.receivingData = true;
    updateMe();

    String lastGroupId = group!.id;

    String? lastEventId;
    if (event != null) {
      lastEventId = event!.id;
    }

    serverResponse = await server.sGroupListRequest(null);
    Group? testGroup = server.getGroupById(lastGroupId);

    group = testGroup != null ? testGroup : blankGroup;

    serverResponse = await requestEvents(group!, force: true);

    _currentGroupEvents = serverResponse.object;

    if (lastEventId != null) {
      VotingEvent? testEvent = server.getEventById(group!, lastEventId);
      event = testEvent != null ? testEvent : activeEventsForGroup[0];
    }

    updateMe();
  }

  Account? get votingAccountForCurrentEvent {
    Account? account;
    if (event != null) {
      account = accountForEvent(event!);
    }
    return account;
  }

  Account? accountForEvent(VotingEvent event) {
    return MainController.to.wallet!.accountForEvent(server, event.id, Mode.voting);
  }

  // bool wrongLotCodeForEvent(VotingEvent event) {
  //   return MainController.to.wrongLotCodeForEvent(server, event).isNotEmpty;
  // }

  // Account? moderatorAccountForCurrentServer() {
  //   return server.getAccountByMode(Mode.moderator);
  // }

  Future<void> votingOnSubmit() async {
    Log.d(_TAG, "votingOnSubmit()");
    serverResponse.receivingData = true;
    updateMe();

    if (event!.lotCodes) {
      await _requestVotingCredentialsFromLot(lotCodeTextController.text);
    } else {
      VotingCredentialResponse votingCredentialResponse = VotingCredentialResponse();
      votingCredentialResponse = await MainController.to.wallet!.keySubmission(
          votingCredentialResponse: votingCredentialResponse,
          userId: namingTextController.text,
          credential: credentialTextController.text,
          mode: mode,
          event: event!,
          server: MainController.to.votingServer!);
      serverResponse = votingCredentialResponse.keySubmissionServerResponse!;
      updateMe();
    }
  }

  /*
  Future<void> moderatorOnSubmit() async {
    Log.d(_TAG, "moderatorOnSubmit()");
    if (moderatorAccountForCurrentServer() != null) {
      account = moderatorAccountForCurrentServer()!;
      // Route<dynamic> route = Get.rawRoute!;
      _goToModeratorPage();

      /// remove this route
      // Get.removeRoute(route);
    } else {
      serverResponse.receivingData = true;
      updateMe();
      VotingCredentialResponse votingCredentialResponse = VotingCredentialResponse();
      votingCredentialResponse = await MainController.to.wallet!.keySubmission(
          votingCredentialResponse: votingCredentialResponse,
          userId: namingTextController.text,
          credential: credentialTextController.text,
          mode: mode,
          event: event,
          server: MainController.to.votingServer!);
      serverResponse = votingCredentialResponse.keySubmissionServerResponse!;
      updateMe();
      if (votingCredentialResponse.keySubmissionSuccess!) {
        moderatorOnSubmit();
      }
    }
  }
  */


  /// request anonymous userId and tan with lotCode for Voting
  Future<void> _requestVotingCredentialsFromLot(String lotCode) async {
    serverResponse.receivingData = true;
    updateMe();

    VotingCredentialResponse votingCredentialResponse = VotingCredentialResponse();
    votingCredentialResponse = await MainController.to.wallet!
        .requestVotingCredentialsFromLot(lotCode: lotCode, event: event!, mode: mode, server: MainController.to.votingServer!);

    /// error lotRequest
    if (!votingCredentialResponse.lotRequestSuccess!) {
      serverResponse = votingCredentialResponse.lotRequestServerResponse!;
      updateMe();
    }

    /// error keySubmission
    else if (!votingCredentialResponse.keySubmissionSuccess!) {
      serverResponse = votingCredentialResponse.keySubmissionServerResponse!;
      updateMe();
    }

    /// success
    else {
      Route<dynamic> route = Get.rawRoute!;

      /*
      /// GO TO MODERATOR PAGE
      if (mode == Mode.moderator) {
        _goToModeratorPage();
      }

      /// GO TO VOTING PAGE
      else {
        goToVotingPage(event!);
      }
      */

      goToVotingPage(event!);

      /// remove this route
      // Get.removeRoute(route);
    }
  }

  //
  // /// request anonymous userId and tan with lotCode for Voting
  // Future<void> _requestVotingCredentialsFromLot(String lotCode) async {
  //   serverResponse.errorMessage = null;
  //   serverResponse.receivingData = true;
  //   updateMe();
  //
  //   /// we create a temp account with temp keys to decrypt the credentials
  //   Account _account = Account(mode);
  //   await _account.createNewKeys();
  //
  //   /// Generate a v4 (random) id as userId
  //   var _uuid = Uuid();
  //   _account.userId = _uuid.v4();
  //
  //   String lotCodeEnc = await _account.encryptServerMessage(
  //     message: lotCode,
  //     server: server,
  //   );
  //
  //   final Map<String, dynamic> messageMap = lotRequest(
  //       // lotCode: lotCode,
  //       lotCodeEnc: lotCodeEnc,
  //       eventId: event.id
  //   );
  //   serverResponse = await HttpController().serverRequestSigned(_account, messageMap, server);
  //   // Log.d(_TAG, "submitId; resultMap: $resultMap");
  //
  //   if (serverResponse.statusCode != 200) {
  //     updateMe();
  //   }
  //   else {
  //     /// server should response IV  and encrypted yaml credentials
  //     /// Lot-Content~: >
  //     ///   wsoo3iKDqKABvWCL0WP+pw==:Y1BYUTJzS1BscFIrZjdrQW96OHNwdzNYZGZJSGN6aWFpZWk5eTJSVEZ0T0UwMVpwVUYvaUdnRWo3UzI1Y1JzPQ==
  //
  //     String lotContent = (serverResponse.bodyMap["Lot-Content~"]).trim();
  //     Log.d(_TAG, "_requestUserCredentialsFromLot: Lot-Content~: $lotContent");
  //
  //     Credential credential = await _account.decryptCredentialFromLotContent(server, lotContent);
  //     /// we got credential
  //     if (credential.userId != null && credential.credential != null) {
  //       _submitId(credential.userId, credential.credential);
  //     }
  //     else {
  //       serverResponse.errorMessage = credential.errorMsg != null
  //           ? credential.errorMsg
  //           : "Unexpected error";
  //       updateMe();
  //     }
  //   }
  // }
  //
  //
  // Future<void> _submitId(String userId, String tan) async {
  //     serverResponse.errorMessage = null;
  //     serverResponse.receivingData = true;
  //     updateMe();
  //
  //     if (account == null) {
  //       account = Account(mode);
  //       await account.createNewKeys();
  //     }
  //
  //     account.userId = userId;
  //     account.credential = tan;
  //
  //     if (account.mode == Mode.voting) {
  //       account.votingEventId = event.id;
  //     }
  //
  //     serverResponse = await account.submitIdToServer(server);
  //
  //     if (serverResponse.statusCode != 200) {
  //       updateMe();
  //     }
  //     else {
  //       /// add account to Wallet
  //       await mainController.wallet.addAccount(server, account);
  //
  //       /// save wallet in shared prefs
  //       await mainController.onWalletChanged(updateNotification: true);
  //
  //       Route<dynamic> route = Get.rawRoute;
  //
  //       /// GO TO MODERATOR PAGE
  //       if (mode == Mode.moderator) {
  //         _goToModeratorPage();
  //       }
  //       /// GO TO VOTING PAGE
  //       else {
  //         goToVotingPage(event);
  //       }
  //
  //       /// remove this route
  //       Get.removeRoute(route);
  //     }
  // }

  void onEventFilterClosed (bool value) {
    eventFilterClosed = value;
    updateEventFilter();
  }

  void onEventFilterOpen (bool value) {
    Log.d(_TAG, "onEventFilterOpen: $value");
    eventFilterOpen = value;
    updateEventFilter();
  }

  void onEventFilterWithLotCode (bool value) {
    Log.d(_TAG, "onEventFilterWithLotCode: $value");
    eventFilterWithLotCode = value;
    updateEventFilter();
  }


  void updateEventFilter() {
    Log.d(_TAG, "updateEventFilter => eventFilterClosed: $eventFilterClosed; eventFilterOpen: $eventFilterOpen; eventFilterWithLotCode: $eventFilterWithLotCode");
    updateMe();
  }

  Future<void> onGroupsChanges(Group group) async {
    if (group.id != "") {
      this.group = group;
      // await MainController.to.userBox!.setLastGroupIdByServer(server, group);

      serverResponse.receivingData = true;
      updateMe();

      serverResponse = await requestEvents(group);
      _currentGroupEvents = serverResponse.object;

      // event = activeEventsForGroup.isNotEmpty ? activeEventsForGroup[0] : null;
      updateMe();
    }
  }

  Future<ServerResponse> requestEvents(Group group, {bool? force}) async {
    return await server.eventsByGroup(
        account, group,
        includeOpenVotings: true,
        includeAdminInfo: mainController.moderatorTools,
        force: force
    );
  }

  void onInputChanged() {
    Log.d(_TAG, "onInputChanged()");
    updateMe();
  }

  void updateMe() {
    update();
  }

  // Future<bool> _fastJumpToVoting() async {
  //   _currentGroupEvents = await server.eventsByGroup(group);
  //   if (_currentGroupEvents.isEmpty) return false;
  //
  //   /// only 1 voting event AND account already exists
  //   Account _account = votingAccountForCurrentEvent();
  //   if (_account != null && _currentGroupEvents.length == 1 ) {
  //     account = _account;
  //     Log.d(_TAG, "_fastJumpToVoting -> only 1 voting event AND account already exists: true");
  //     return true;
  //   }
  //   /// urlParams contains eventId and account for it exists
  //   if (mainController.urlParams != null && mainController.urlParams.eventId != null) {
  //     VotingEvent _event = server.getEventById(
  //         group,
  //         mainController.urlParams.eventId);
  //     if (_event != null) {
  //       _account = server.getVotingAccountForEvent(_event);
  //       bool _containsEvent = server.getEventById(
  //           group,
  //           mainController.urlParams.eventId) != null;
  //       if (_containsEvent && _account != null) {
  //         event = _event;
  //         account = _account;
  //         Log.d(_TAG,
  //             "_fastJumpToVoting -> urlParams contains eventId and account for it exists: true");
  //         return true;
  //       }
  //     }
  //   }
  //   Log.d(_TAG, "_fastJumpToVoting -> false");
  //   return false;
  // }

  Account? _accountForEvent() {
    VotingServer? _server = mainController.wallet!.getServerByIdHash(server.idHashCode);
    Log.d(_TAG, "_accountForEvent: server exists: ${_server != null}");
    if (_server != null) {
      Account? _account = _server.getVotingAccountForEvent(event!);
      if (_account != null && _account.isValidAccount()) {
        Log.d(_TAG, "_accountForEvent: account exists");
        return _account;
      }
    }
    Log.d(_TAG, "_accountForEvent: account not exists or !isValidAccount");
    return null;
  }

  void goToVotingPage(VotingEvent event) {
    this.event = event;
    this.account = _accountForEvent();
    lastSelectedEvent = event;

    mainController.urlParams.eventId = null;
    Log.d(_TAG, "_goToVoting -> navigator.pushAndRemoveUntil IndexPage");

    Get.to(() => VotingListPage(
              server: server,
              account: account,
              votingEvent: event,
            ))!
        .then((_) {
      if (votingAllowNext) {
        updateMe();
      } else {
        Get.back();
      }
    });
  }

  void goToVotingLotPage(VotingEvent event) {
    this.event = event;
    Log.d(_TAG, "goToVotingLotPage; event: ${this.event!.id}");
    lastSelectedEvent = event;
    Get.to(() => EventLotPage(server, event))!
        .then((value) => updateMe());
  }
}

