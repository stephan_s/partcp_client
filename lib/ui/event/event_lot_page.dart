// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/formatters/masked_input_formatter.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/utils/field_width.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/s_spacer.dart';
import 'package:partcp_client/utils/s_util__date.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/submit_button.dart';
import 'package:partcp_client/widgets/text_field_widget.dart';
import 'package:partcp_client/widgets/widgets.dart';

import 'event_controller.dart';

class EventLotPage extends StatelessWidget {
  final VotingServer server;
  final VotingEvent event;

  final _TAG = "EventLotPage";
  final EventController controller = EventController.to;

  EventLotPage(this.server, this.event);

  @override
  Widget build(BuildContext context) {
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   controller.onPostFrameCallback();
    // });

    return OrientationBuilder(
      builder: (_, __) => GetBuilder<EventController>(
          builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: Text("Anmeldung"),
              ),
              body: Widgets.loaderBox(loading: controller.serverResponse.receivingData, child: ScrollBody(children: _body)))),
    );
  }

  List<Widget> get _body {

    final Account? _accountForEvent = controller.accountForEvent(event);
    bool accountExists = false;
    bool accountIsValid = false;

    if (_accountForEvent != null) {
      accountExists = true;
      if (_accountForEvent.isValidAccount()) {
        accountIsValid = true;
      }
    }

    List<Widget> items = [_head];

    if (accountExists && !accountIsValid) {
      items.add(_wrongLotCodeExists);
    }
    else {
      items.add(_userIdAndTan);
      items.add(_userIdAndTan);
      items.add(_userIdAndTanAndLotCode);
      items.add(_lotCode);
      items.add(_submitBtn);
    }
    items.add(_nextWoRegister);
    items.add(ServerErrorMessage(serverResponse: controller.serverResponse));

    return items;
  }

  /// head
  Widget get _head => CardWidget(
      titleListTile: CardWidget.listTileTitle(
        leading: Icon(Icons.event),
        title: "${SUtilDate.shortDate(dateTime: event.date!, context: Get.context!)}, ${event.name}",
        subtitle: Text(
          "${event.clientData?.shortDescription}",
        ),
        isThreeLine: false,
      ));


  /// userId and Tan
  Widget get _userIdAndTan {
    if (controller.event == null || !controller.event!.paperLots) return Container();

    if (controller.votingAccountForCurrentEvent != null && controller.votingAccountForCurrentEvent!.privateKeyXB64Enc != null) {
      return CardWidget(
        title: "Teilnahmecode für dieses Event bereits vorhanden",
      );
    }

    Widget _child = Column(
      children: [_userId, Sspacer.s10, _tan],
    );

    return CardWidget(
      title: "Bitte gebe Deine User-ID und TAN ein",
      child: _child,
    );
  }


  Widget get _submitBtn => CardWidget(
      child: Column(
        children: [
          SubmitButton(
            submitting: controller.serverResponse.receivingData,
            onPressed: controller.votingAllowNext ? () => controller.votingOnSubmit() : null,
            text: "Anmelden",
          ),
        ],
      ));

  Widget get _nextWoRegister => CardWidget(
    // title: "Teilnahmecode für dieses Event bereits vorhanden",
    child: TextButton(onPressed: () => controller.goToVotingPage(event), child: Text("Weiter ohne Anmeldung (Abstimmen nicht möglich)")),
  );


  /// NAMING (USER-ID)
  Widget get _userId {
    return Center(
      child: TextFieldWidget(
        textController: controller.namingTextController,
        labelText: "User-ID",
        hint: controller.event!.namingValues.pattern,
        inputFormatters: [
          // MaskedInputFormatter(controller.event.credentialNamingValues.pattern, anyCharMatcher: RegExp(r'[a-zA-Z0-9\.]+'))
          MaskedInputFormatter(controller.event!.namingValues.pattern)
        ],
        onTap: () => ({}),
        onSubmitted: (_) => ({}),
        onChanged: (_) => controller.onInputChanged(),
        autoCorrect: false,
        isPasswordField: true,
        keyboardType: TextInputType.text,
        textAlign: TextAlign.start,
        maxBoxLength: fieldBoxMaxWidth,
        obscuringCharacter: "#",
        enabled: controller.idFieldsEnabled,
      ),
    );
  }

  /// CREDENTIAL (TAN, PWD)
  Widget get _tan {
    return Center(
      child: TextFieldWidget(
        textController: controller.credentialTextController,
        labelText: "TAN",
        hint: controller.event!.credentialValues.pattern,
        inputFormatters: [MaskedInputFormatter(controller.event!.credentialValues.pattern)],
        onTap: () => {},
        onSubmitted: (_) => {},
        onChanged: (_) => controller.onInputChanged(),
        autoCorrect: false,
        isPasswordField: true,
        keyboardType: int.tryParse(controller.event!.credentialValues.charList) != null ? TextInputType.number : TextInputType.text,
        textAlign: TextAlign.start,
        maxBoxLength: fieldBoxMaxWidth,
        obscuringCharacter: "#",
        enabled: controller.idFieldsEnabled,
      ),
    );
  }

  Widget get _userIdAndTanAndLotCode {
    if (controller.event != null && controller.event!.lotCodes && controller.event!.paperLots) {
      return Column(
        children: [
          Sspacer.s10,
          Text(
            "ODER",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ],
      );
    }
    return Container();
  }


  Widget get _wrongLotCodeExists {
    return CardWidget(
        titleListTile: CardWidget.listTileTitle(
          leading: Icon(Icons.new_releases, color: Colors.red),
          title: "Teilnahmecode für diese Abstimmung konnte nicht entschlüsselt werden",
          subtitle: Text("Der Veranstaltungsleiter hat Dir bereits einen verschlüsselten Teilnahmecode hinterlegt, welcher von dieser ParTCP App abgerufen wurde, "
              "die App konnte den Teilnahmecode jedoch nicht entschlüsseln.\n\n"
              "Die wahrscheinichste Ursache dafür ist, dass Du Dich erneut, mit einem anderem Gerät, registriert hast, nachdem der Teilnahmecode verschlüsselt wurde.\n\n"
              "Lösungsvorschläge:\n\n"
              "- Wenn Du den ursprünglichen Schlüsselbund exportiert und gespeichert hast, importiere diesen in diese App.\n\n"
              "- Wenn der ursprünglichen Schlüsselbund sich auf einem anderen Gerät befindet, welchen Du jetzt nutzen kannst, wechsle zu dem anderen Gerät und rufe 'Neue Teilnahmecodes' erneut auf\n\n"
              "- Wende Dich an den Veranstaltungsleiter, damit er für Dich Deinen Teilnahmecode erneut verschlüsseln und auf den Server hinterlassen kann")),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SubmitButton(
            submitting: controller.serverResponse.receivingData,
            onPressed: () => Get.back(),
            text: "Zurück",
          ),
        ),);
  }


  Widget get _lotCode {
    if (controller.event == null ||
        (controller.votingAccountForCurrentEvent != null && controller.votingAccountForCurrentEvent!.privateKeyXB64Enc != null) ||
        !controller.event!.lotCodes) {
      Log.d(_TAG, "_lotCode; event: ${controller.event}");
      Log.d(_TAG, "_lotCode; votingAccountForCurrentEvent: ${controller.votingAccountForCurrentEvent}");
      Log.d(_TAG, "_lotCode; votingAccountForCurrentEvent!.privateKeyXB64Enc: ${controller.votingAccountForCurrentEvent!.privateKeyXB64Enc}");
      Log.d(_TAG, "_lotCode; event!.lotCodes: ${controller.event!.lotCodes}");
      return Container();
    }

    Widget _child = TextFieldWidget(
      textController: controller.lotCodeTextController,
      labelText: "Teilnahmecode",
      hint: controller.event!.lotCodeValues.pattern,
      inputFormatters: [MaskedInputFormatter(controller.event!.lotCodeValues.pattern)],
      onTap: () => ({}),
      onSubmitted: (_) => controller.votingOnSubmit(),
      onChanged: (_) => controller.onInputChanged(),
      autoCorrect: false,
      isPasswordField: false,
      keyboardType: int.tryParse(controller.event!.lotCodeValues.charList) != null ? TextInputType.number : TextInputType.text,
      textAlign: TextAlign.start,
      maxBoxLength: fieldBoxMaxWidth,
      obscuringCharacter: "#",
      enabled: controller.lotFieldEnabled,
    );

    return CardWidget(
        titleListTile: CardWidget.listTileTitle(
            title: "Bitte gebe Deinen Teilnahmecode ein",
            subtitle: Text("Um mit abzustimmen, ist ein Teilnahmecode erforderlich. Du erhälst diesen in der Regel vom Veranstaltungsleiter.")),
        child: _child);
  }
}
