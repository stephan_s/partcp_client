// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/styles/styles.dart';
import 'package:partcp_client/ui/admin/widgets/styles.dart';
import 'package:partcp_client/utils/field_width.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/readmore.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/text_label.dart';
import 'package:partcp_client/widgets/widgets.dart';

import 'event_controller.dart';

class EventListPage extends StatelessWidget {
  final VotingServer server;
  EventListPage(this.server);

  final _TAG = "IdPage";
  final EventController controller = Get.put(EventController());

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return OrientationBuilder(
      builder: (_, __) => GetBuilder<EventController>(
          initState: (_) => controller.initController(server, Mode.voting),
          builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: Text("Veranstaltungen & Abstimmungen"),
                actions: [
                  /// refresh
                  IconButton(icon: Icon(Icons.refresh), onPressed: () => controller.onRefresh())
                ],
              ),
              // body: ScrollBody(children: _body),
              body: Widgets.loaderBox(
                loading: controller.serverResponse.receivingData,
                child: ScrollBody(
                  children: _body,
                ),
              ))),
    );
  }

  List<Widget> get _body => [
        ServerErrorMessage(serverResponse: controller.serverResponse),
        CardWidget(title: "Bitte wähle eine Gruppe aus", child: _groupDropDownList),
        _eventCardWidget,
      ];

  Widget get _groupDropDownList {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        DropdownButton<Group>(
          value: controller.group,
          items: _groups,
          onChanged: (item) => controller.onGroupsChanges(item!),
          elevation: 1,
        ),
        _eventListFilter,
      ],
    );
  }

  Widget get _eventListFilter {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 20),
      child: Column(
        // mainAxisSize: MainAxisSize.min,
        children: [
          TextLabel(
            label: "Veranstaltungsfilter:",
            child: Container(
              // width: 400,
              // height: 200,
              child: ButtonBar(
                alignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  /// open
                  Container(
                    width: 160,
                    child: CheckboxListTile(
                        dense: true,
                        value: controller.eventFilterOpen,
                        onChanged: (val) => controller.onEventFilterOpen(val!),
                        title: Text("laufende"),
                        controlAffinity: ListTileControlAffinity.leading),
                  ),

                  /// closed
                  Container(
                    width: 160,
                    child: CheckboxListTile(
                        dense: true,
                        value: controller.eventFilterClosed,
                        onChanged: (val) => controller.onEventFilterClosed(val!),
                        title: Text("beendete"),
                        controlAffinity: ListTileControlAffinity.leading),
                  ),

                  /// with LotCode
                  Container(
                    width: 200,
                    child: CheckboxListTile(
                        dense: true,
                        value: controller.eventFilterWithLotCode,
                        onChanged: (val) => controller.onEventFilterWithLotCode(val!),
                        title: Text("mit Zugangsdaten"),
                        controlAffinity: ListTileControlAffinity.leading),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget get _noEventsInGroup {
    return CardWidget(
      titleListTile: CardWidget.listTileTitle(
          leading: Icon(
            Icons.cancel_outlined,
            color: Colors.red,
          ),
          title: "In dieser Gruppe finden zur Zeit keine Veranstaltung statt."),
    );
  }

  Widget get _eventCardWidget {
    if (controller.activeEventsForGroup.isEmpty) return _noEventsInGroup;

    return CardWidget(
      title: "Bitte wähle die Veranstaltung, an der Du teilnehmen möchten: ",
      // expandChild: true,
      child: Padding(
        padding: const EdgeInsets.only(top: 6, bottom: 6),
        child: _eventList,
      ),
    );
  }

  List<DropdownMenuItem<Group>> get _groups {
    List<DropdownMenuItem<Group>> list = [];
    if (controller.group == null || (controller.group != null && controller.group!.id == "")) {
      list.add(_groupItem(controller.blankGroup));
    }

    for (var i = 0; i < controller.server.serverGroups.length; i++) {
      list.add(_groupItem(controller.server.serverGroups[i]));
    }
    return list;
  }

  Widget get _eventList {
    // ToDo: Change to map statement? (prettier and more compact)
    List<Widget> _items = [];
    for (var i = 0; i < controller.activeEventsForGroup.length; i++) {
      _items.add(_eventItem(controller.activeEventsForGroup[i]));
    }
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      children: _items,
    );
  }

  Widget _eventItem(VotingEvent event) {
    final Account? _accountForEvent = controller.accountForEvent(event);
    bool accountExists = false;
    bool accountIsValid = false;

    if (_accountForEvent != null) {
      accountExists = true;
      if (_accountForEvent.isValidAccount()) {
        accountIsValid = true;
      }
    }

    Widget _leading;
    String? bla =  _accountForEvent?.userIdEnc;
    if (accountExists && accountIsValid) {
      _leading = Icon(Icons.check, color: Colors.green);
    } else if (accountExists && !accountIsValid) {
      _leading = Icon(Icons.new_releases, color: Colors.red);
    } else {
      _leading = Icon(Icons.new_releases, color: Colors.black12);
    }

    Widget? subtitle() {
      if (event.clientData!.shortDescription.isEmpty) return null;

      Widget _shortDescription = ReadMoreText(event.clientData!.shortDescription,
          trimLines: 3,
          colorClickableText: Colors.pink,
          trimMode: TrimMode.Line,
          trimCollapsedText: 'Show more',
          trimExpandedText: ' Show less',
          style: TextStyle(fontSize: 16),
          moreStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold));

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [_shortDescription],
      );
    }

    Widget? _subtitle = subtitle();

    int openVotedCount = 0;
    if (_accountForEvent != null) {
      for (Voting voting in event.votings) {
        Log.d(_TAG, "openVotedCount_ : ${voting.id}; voting.status: ${voting.status}");
        if (voting.status == enumToString(VotingStatusEnum.open) && _accountForEvent.votedList.contains(voting.id)) {
          openVotedCount++;
          Log.d(_TAG, "openVotedCount_ : $openVotedCount");
        }
      }
    }

    final int newOpenVotingsCount = event.votingCount.open - openVotedCount;

    late Text openVotings;

    if (accountIsValid) {
      openVotings = Text("neu: ${newOpenVotingsCount} (von ${event.votingCount.open})",
        style: TextStyle(
            fontWeight: newOpenVotingsCount > 0 && accountExists ? FontWeight.bold : FontWeight.normal,
            color: Colors.green),
      );
    }
    else {
      openVotings = Text("offen: ${event.votingCount.open}");
    }

    Widget _trailing = Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        openVotings,
        Text("beendet: ${event.votingCount.finished}")
      ],
    );

    return Column(
      children: [
        Container(
          // constraints: BoxConstraints(minWidth: 100, maxWidth: fieldBoxMaxWidth * 0.9),
          child: ListTile(
            tileColor: controller.lastSelectedEvent != null && controller.lastSelectedEvent! == event ? Styles.selectedTileColor : null,
            leading: _leading,
            title: Text(
              event.name,
              style: TextStyle(fontWeight: accountExists ? FontWeight.bold : FontWeight.normal),
            ),
            subtitle: _subtitle != null ? Padding(padding: const EdgeInsets.only(top: 6), child: _subtitle) : null,
            trailing: _trailing,

            isThreeLine: false, //_subtitle != null,
            contentPadding: EdgeInsets.symmetric(horizontal: 20),
            onTap: _accountForEvent != null && _accountForEvent.isValidAccount()
                ? () => controller.goToVotingPage(event)
                : () => controller.goToVotingLotPage(event),

            // selected: event == controller.event,
          ),
        ),
        Divider(color: dividerColor),
      ],
    );
  }

  DropdownMenuItem<Group> _groupItem(Group group) {
    bool hasAccounts = false;
    bool hasWrongAccounts = false;

    for (Account account in controller.server.accounts) {
      if (account.votingEventId != null && account.votingEventId!.split("/")[0] == group.id) {
        if (account.privateKeyXB64Enc != null) {
          hasAccounts = true;
        } else {
          hasWrongAccounts = true;
        }
      }
    }

    MaterialColor? iconColor;
    if (hasAccounts && !hasWrongAccounts) {
      iconColor = Colors.green;
    } else if (!hasAccounts && hasWrongAccounts) {
      iconColor = Colors.red;
    } else if (hasWrongAccounts && hasWrongAccounts) {
      iconColor = Colors.orange;
    }

    return DropdownMenuItem(
      child: Container(
        constraints: BoxConstraints(minWidth: 100, maxWidth: fieldBoxMaxWidth * 0.9),
        child: ListTile(
          leading: Icon(
            Icons.group,
            color: iconColor,
          ),
          title: Text(group.name),
          contentPadding: EdgeInsets.all(0),
          selected: group == controller.group,
        ),
      ),
      value: group,
    );
  }
}
