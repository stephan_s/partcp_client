// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/utils/field_width.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_main.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/s_spacer.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/text_field_widget.dart';

import 'event_controller.dart';

/*
class IdModeratorPage extends StatelessWidget {
  final VotingServer server;
  IdModeratorPage(this.server);

  final _TAG = "IdModeratorPage";

  final EventController controller = Get.put(EventController());

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return GetBuilder<EventController>(
        initState: (_) => controller.initController(server, Mode.moderator),
        builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: Text("Voting Event"),
              ),
              body: OrientationBuilder(
                builder: (_, __) => SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: _moderatorBody,
                  ),
                ),
              ),
            ));
  }

  List<Widget> get _moderatorBody => [
        _moderatorModeCard,
        _moderatorIdAndTan(),
        ServerErrorMessage(serverResponse: controller.serverResponse),
        Container(
          child: CardWidget(
            child: ButtonMain(
              onPressed: controller.moderatorAllowNext
                  ? () => controller.moderatorOnSubmit()
                  : null,
              child: controller.moderatorAccountForCurrentServer() != null
                  ? Text("Zum Moderator Account")
                  : Text("Moderator Account erstellen"),
            ),
          ),
        ),
      ];

  Widget get _moderatorModeCard => CardWidget(
        titleListTile: CardWidget.listTileTitle(
            leading: Icon(Icons.warning_amber_sharp, color: Colors.red),
            title: "Moderator Mode"),
      );

  Widget _moderatorIdAndTan() {
    if (controller.moderatorAccountForCurrentServer() != null) {
      return CardWidget(
          titleListTile: CardWidget.listTileTitle(
              leading: Icon(Icons.check, color: Colors.green),
              title: "Schlüssel bereits vorhanden",
              subtitle: Text(
                  "ID: ${controller.moderatorAccountForCurrentServer()!.userId}")));
    }

    Widget _child = Column(
      children: [_moderatorId(), Sspacer.s10, _tan()],
    );

    return CardWidget(
      title: "Bitte geben Sie Ihre Moderator-ID und TAN ein",
      child: _child,
    );
  }

  /// NAMING (USER-ID)
  Widget _moderatorId() {
    return Center(
      child: TextFieldWidget(
        textController: controller.namingTextController,
        labelText: "Moderator-ID",
        hint: "",
        onTap: () => ({}),
        onSubmitted: (_) => ({}),
        onChanged: (_) => controller.onInputChanged(),
        autoCorrect: false,
        isPasswordField: false,
        keyboardType: TextInputType.text,
        textAlign: TextAlign.start,
        maxBoxLength: fieldBoxMaxWidth,
        // obscuringCharacter: "",
        enabled: true,
      ),
    );
  }

  /// CREDENTIAL (TAN, PWD)
  Widget _tan() {
    return Center(
      child: TextFieldWidget(
        textController: controller.credentialTextController,
        labelText: "TAN",
        hint: null,
        // hint: controller.server.participantCredentialFormat,
        // inputFormatters: [
        //   MaskedInputFormatter(controller.server.participantCredentialFormat)
        // ],
        onTap: () => {},
        onSubmitted: (_) => {},
        onChanged: (_) => controller.onInputChanged(),
        autoCorrect: false,
        isPasswordField: true,
        keyboardType: TextInputType.number,
        textAlign: TextAlign.start,
        maxBoxLength: fieldBoxMaxWidth,
        obscuringCharacter: "#",
        // enabled: controller.idFieldEnabled,
      ),
    );
  }
}

 */
