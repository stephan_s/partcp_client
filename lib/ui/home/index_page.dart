// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:getwidget/getwidget.dart';
import 'package:partcp_client/constants.dart';
import 'package:partcp_client/objects/wallet.dart';
import 'package:partcp_client/ui/app_update/app_update_page.dart';
import 'package:partcp_client/ui/info/info_page.dart';
import 'package:partcp_client/utils/launch_url.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_util__date.dart';
import "package:universal_html/html.dart" as html;

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/widgets.dart';
import 'package:move_to_background/move_to_background.dart';

import '../wallet/export_wallet.dart';
import 'index_controller.dart';

enum _MenuItems {
  exportWallet,
  deleteWallet,
  forgetKeys,
  fullscreen,
  showAdminTools,
  info,
  appUpdate,
  registerOnKeyManager,

  nfcTest,
  mqttTest
}

const buttonPadding = 10.0;

class IndexPage extends StatelessWidget with WidgetsBindingObserver {
  final _TAG = "IndexPage";
  final urlParams;

  IndexPage({this.urlParams});

  static IndexController to = Get.find<IndexController>();

  final MainController mainController = MainController.to;
  final IndexController controller = Get.put(IndexController());

  void _deleteWalletDialog() async {
    /// ask to delete
    final result = await showOkCancelAlertDialog(
      context: Get.context!,
      title: "Schlüsselbund '${mainController.wallet!.walletAccount!.title}' löschen?",
      isDestructiveAction: true,
      message: "Bist Du sicher, dass Du den Schlüsselbung '${mainController.wallet!.walletAccount!.title}' und alle dazugegörigen Daten löschen möchtest?"
          "\nFalls Du keine Sichernungkopie von dem Schlüsselbund exportiert hast, kannst Du diesen nie wieder verwenden.",
      okLabel: 'Löschen',
      cancelLabel: 'Abbrechen',
    );

    if (result == OkCancelResult.ok) {
      await mainController.deleteWallet();
      Get.back();
      controller.initController(null);
      controller.updateMe();
    }
  }

  Future<void> _handleMenuClick(_MenuItems value) async {
    switch (value) {
      case _MenuItems.exportWallet:
        ExportWallet()..download().then((value) => controller.updateMe());
        break;

      case _MenuItems.deleteWallet:
        _deleteWalletDialog();
        break;

      case _MenuItems.forgetKeys:
        controller.forgetKey();
        break;

      case _MenuItems.fullscreen:
        MainController.to.isFullScreen ? html.document.exitFullscreen() : html.document.documentElement!.requestFullscreen();
        MainController.to.isFullScreen = !MainController.to.isFullScreen;
        break;

      case _MenuItems.info:
        Get.to(() => InfoPage());
        break;

      case _MenuItems.appUpdate:
        Get.to(() => AppUpdatePage());
        break;

      case _MenuItems.registerOnKeyManager:
        controller.goToServerUserManagerPage();
        break;

      case _MenuItems.nfcTest:
      // Get.to(() => NfcTest());
        break;

      case _MenuItems.mqttTest:
        // runMqtt();
        break;

      case _MenuItems.showAdminTools:
        // TODO: Handle this case.
        break;
    }
  }

  List<PopupMenuItem<_MenuItems>> _popupMenuItems() {
    List<PopupMenuItem<_MenuItems>> list = [];

    // list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.importWallet, child: Text("Schlüsselbund importieren")));

    list.add(PopupMenuItem<_MenuItems>(
        enabled: (mainController.encryptedWalletExists() || mainController.walletIsDecrypted()),
        value: _MenuItems.exportWallet,
        child: Text("Schlüsselbund exportieren")));

    // list.add(PopupMenuItem<_MenuItems>(enabled: true, value: _MenuItems.createWallet, child: Text("Schlüsselbund erstellen")));

    list.add(PopupMenuItem<_MenuItems>(
        enabled: (mainController.encryptedWalletExists() || mainController.walletIsDecrypted()),
        value: _MenuItems.forgetKeys,
        child: Text("Schlüsselbund sperren")));

    if (GetPlatform.isMobile && GetPlatform.isWeb) {
      list.add(
          PopupMenuItem<_MenuItems>(value: _MenuItems.fullscreen, child: Text(MainController.to.isFullScreen ? "Vollbild verlassen" : "Vollbild")));
    }

    list.add(PopupMenuItem<_MenuItems>(
        enabled: (mainController.encryptedWalletExists() || mainController.walletIsDecrypted()),
        value: _MenuItems.deleteWallet,
        child: Text("Schlüsselbund löschen")));

    list.add(PopupMenuItem<_MenuItems>(
        enabled: (mainController.encryptedWalletExists() || mainController.walletIsDecrypted()),
        value: _MenuItems.mqttTest,
        child: Text("Messaging Test")));

    // list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.imagePickerTest, child: Text("ImagePicker Test")));

    list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.info, child: Text("App Info")));

    if ((GetPlatform.isMobile && GetPlatform.isAndroid) || GetPlatform.isDesktop) {
      Widget child = controller.appHasUpdate
          ? Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5),
                  child: Text("App update"),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: GFBadge(
                    size: 15,
                    shape: GFBadgeShape.circle,
                  ),
                )
              ],
            )
          : Text("App update");
      list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.appUpdate, child: child));
    }

    if (mainController.encryptedWalletExists() && mainController.walletIsDecrypted()) {
      list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.registerOnKeyManager, child: Text("KeyManager")));
    }

    list.add(PopupMenuItem<_MenuItems>(
        value: _MenuItems.showAdminTools,
        child: CheckboxListTile(
            title: Text("Moderator"),
            value: controller.mainController.moderatorTools,
            onChanged: (val) {
              Log.d(_TAG, "IndexMenu onClick Moderator => new Val: $val");
              controller.mainController.moderatorTools = val;
              controller.updateMe();
              Get.back();
            },
            // controlAffinity: ListTileControlAffinity.leading
        )));

    return list;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.initController(urlParams);
      //_run();
    });


    return WillPopScope(
      onWillPop: () async {
        if (GetPlatform.isWeb) {
          return false;
        }
        else if (GetPlatform.isAndroid) {
          MoveToBackground.moveTaskToBack();
          return false;
        }
        else {
          return true;
        }
      },
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<IndexController>(
          initState: (_) {
            WidgetsBinding.instance.addObserver(this);
          },
          builder: (_) => Scaffold(
            appBar: MyAppBar(
              title: Text("ParTCP Client ${controller.mainController.currentWalletAppBarTitle}"),
              isStartPage: true,
              actions: [
                PopupMenuButton<_MenuItems>(
                    //icon: Icon(Icons.menu),
                    icon: controller.appHasUpdate
                        ? GFIconBadge(
                            position: GFBadgePosition.topStart(top: 0, start: 0),
                            child: Icon(Icons.menu),
                            counterChild: GFBadge(
                              size: 15,
                              shape: GFBadgeShape.circle,
                            ),
                          )
                        : Icon(Icons.menu),
                    onSelected: _handleMenuClick,
                    itemBuilder: (BuildContext context) => _popupMenuItems()),
              ],
            ),
            body: controller.controllerReady
                // ? ScrollBody(children: _body())
                ? Widgets.loaderBox(child: ListView(children: _body(), shrinkWrap: true,), loading: controller.serverResponse.receivingData)
                // ? loaderBox(child: _body(), loading: controller.serverResponse.receivingData),
                : _splash(),
          ),
        ),
      ),
    );
  }

  Widget _splash() {
    return Center(
      child: Container(child: Text("loading...")),
    );
  }

  List<Widget> _body() {

    List<Widget> list = [];
    bool allowCheckNewKeys = false;
    ////////////////////////////////////////////////////
    /// NO WALLET
    if (!mainController.walletIsDecrypted()) {
      final List<Wallet> wallets = mainController.walletsInBox();

      /// CREATE
      if (wallets.isEmpty) {
        list.add(_welcome());
        // list.add(_createWallet());
        // list.add(_importWallet());
      }

      /// DECRYPT
      // else if (wallets.length == 1) {
      //   list.add(_decryptWallet(wallets[0]));
      // }

      /// CHOOSE
      else {
        list.add(_decryptedWalletList(wallets));
      }

      list.add(_createWallet());
      list.add(_importWallet());

      // list.add(_appRev());
    }

    ////////////////////////////////////////////////////
    /// WALLET LOADED
    else {
      /// SERVER
      if (mainController.votingServer != null) {
        list.add(_usedServer());
        if (mainController.wallet!.walletAccount!.userId == null) {
          list.add(_goToRegisterKey());
        } else if (mainController.pubKeysOnServerValid == null || !mainController.pubKeysOnServerValid!) {
          list.add(_goToUpdateKey());
        } else {
          // list.add(_goToEvents());
          allowCheckNewKeys = true;
        }

        list.add(_goToEvents());
        list.add(_goToModerator());

        if (allowCheckNewKeys) list.add(_checkForNewKeys());
      } else {
        list.add(_goToServer());
      }

      /// EXPORT WALLET
      list.add(_exportWallet());
    }

    if (controller.appHasUpdate) {
      list.add(_appUpdate);
    }
    return list;
  }

  Widget _welcome() {
    return CardWidget(
      title: "Willkommen bei ParTCP",
      childNeedsExtraTopPadding: true,
      child: Text("Um die ParTCP-App nutzen zu können, brauchst Du einen Schlüsselbund, in dem Deine persönlichen Daten gespeichert werden. "
          "Wenn Du bereits einen Schlüsselbund besitzt (z. B. weil Du die ParTCP-App auf einem anderen Gerät benutzt hast), "
          "klicke auf „Schlüsselbund importieren“, ansonsten auf „Neuen Schlüsselbund erstellen“."),
    );
  }

  /// CREATE WALLET
  Widget _createWallet() {
    return _cardWidgetWithButton(
        title: "Neuen Schlüsselbund erstellen",
        text: "Wenn Du noch keinen Schlüsselbund erstellt hast, erstelle ein neues.",
        onPressed: () => controller.goToCreateWallet(),
        onPressedText: "erstellen");
  }

  /// IMPORT WALLET
  Widget _importWallet() {
    return _cardWidgetWithButton(
        title: "Schlüsselbund importieren",
        text: "Wenn Du bereits auf einem anderen Gerät oder zu einem früheren Zeitpunkt ein Schlüsselbund erstellt und exportiert hast, kannst dieses hier importieren.",
        onPressed: () => controller.goToImportWallet(),
        onPressedText: "importieren");
  }

  /// EXPORT WALLET
  Widget _exportWallet() {
    return mainController.walletWasChanged && GetPlatform.isWeb
        ? _cardWidgetWithButton(
            title: "Schlüsselbund exportieren",
            text: "Dein Schlüsselbund wurde verändert und sollte zur Sicherheit exportiert werden.",
            onPressed: () => ExportWallet()..download().then((value) => controller.updateMe()),
            onPressedText: "exportieren")
        : Container();
  }

  /// DECRYPT WALLET
  Widget _decryptWallet(Wallet wallet) {
    return _cardWidgetWithButton(
        title: "Schlüsselbund entsperren",
        text: "Schlüsselbund ist bereits vorhanden.",
        onPressed: () => controller.onWalletChoose(wallet),
        onPressedText: "entsperren");
  }

  /// USED SERVER
  Widget _usedServer() {
    if (!mainController.votingServer!.lotCodesReceived) {
      controller.triggerCheckForNewKeys();
    }

    return _cardWidgetWithButton(
        title: "Verbunden",
        text: "Server: ${mainController.votingServer!.name}",
        onPressed: () {
          mainController.forgetServer();
          controller.updateMe();
        },
        onPressedText: "trennen");
  }

  /// SERVER MODERATOR
  Widget _goToModerator() {
    if (!controller.isServerModerator) return Container();
    return _cardWidgetWithButton(
        title: "Veranstaltungsleitung",
        text: "Abstimmungen erstellen und durchführen",
        onPressed: () {
          controller.goToModeratorPage();
        },
        onPressedText: "organisieren");
  }

  /// GO TO SERVER
  Widget _goToServer() {
    return _cardWidgetWithButton(
        title: "Verbinden",
        text: "Dein Schlüsselbund ist bereit, mit Server verbinden",
        onPressed: () => controller.goToServerPage(),
        onPressedText: "verbinden");
  }

  /// GO TO KEY MANAGER PAGE
  Widget _goToRegisterKey() {
    return _cardWidgetWithButton(
        title: "Registrierung erforderlich",
        text: "Eine Registrierung am Server ist erforderlich",
        onPressed: () => controller.goToServerUserManagerPage(),
        onPressedText: "registrieren");
  }

  /// GO TO KEY MANAGER PAGE
  Widget _goToUpdateKey() {
    return _cardWidgetWithButton(
        title: "Wiederholung der Registrierung erforderlich",
        text:
            "Eine erneute Registrierung am Server ist erforderlich, da der am Server hinterlegte öffentliche Schlüssel nicht mit dem Schlüssel dieses Schlüsselbundes übereinstimmt",
        onPressed: () => controller.goToServerUserManagerPage(),
        onPressedText: "registrieren");
  }

  /// GO TO EVENTS
  Widget _goToEvents() {
    return _cardWidgetWithButton(
        title: "Veranstaltungen", text: "Zu den Veranstaltungen und Abstimmungen", onPressed: () => controller.goToIdPage(), onPressedText: "abstimmen");
  }

  /// CHECK FOR NEW KEYS
  Widget _checkForNewKeys() {
    return _cardWidgetWithButton(
        title: "Neue Teilnahmecodes",
        text: "Prüfen, ob auf dem Server neue Teilnahmecodes für Veranstaltungen für Dich vorliegen",
        onPressed: controller.receivingLotCodes ? null : () => controller.checkForNewKeys(),
        onPressedText: controller.receivingLotCodes ? "prüfe ..." : "prüfen");
  }

  /// APP REV
  // Widget _appRev() {
  //   final String _appRev = controller.appRev == null ? "" : controller.appRev!;
  //   return CardWidget(
  //     // child: Text("AppRev: $_appRev (${controller.appHasUpdate})"),
  //     child: Text("AppRev: $_appRev"),
  //   );
  // }

  /// APP UPDATE
  Widget get _appUpdate {
    return _cardWidgetWithButton(
        title: "Neue Version",
        text: "Eine neue Version ist vorhanden",
        onPressed: () {
          if (GetPlatform.isWeb) {
            launchURL(Constants.APP_HOME);
          } else {
            Get.to(() => AppUpdatePage());
          }
        },
        onPressedText: "update");
  }

  /// CARD WIDGET WITH BUTTON
  Widget _cardWidgetWithButton({required String title, required String text, required Function()? onPressed, required String onPressedText}) {
    return InkWell(
      onTap: onPressed,
      child: CardWidget(
        titleListTile:
            CardWidget.listTileTitle(title: title, subtitle: Text(text), trailing: TextButton(onPressed: onPressed, child: Text(onPressedText))),
      ),
    );
  }

  /// CARD WIDGET
  Widget _cardWidget({required String title, required Widget body}) {
    return CardWidget(
      titleListTile: CardWidget.listTileTitle(title: title, subtitle: body),
    );
  }

  /// DECRYPTED WALLET LIST
  Widget _decryptedWalletList(List<Wallet> wallets) {
    List<Widget> items = [];
    for (Wallet wallet in wallets) {
      items.add(_walletTile(wallet));
    }
    Widget list = ListView(
      children: items,
      shrinkWrap: true,
    );

    return _cardWidget(title: "Vorhandenen Schlüsselbund verwenden:", body: list);
  }

  Widget _walletTile(Wallet wallet) {
    final String dateC = SUtilDate.shortDate(dateTime: wallet.walletAccount!.dateC!, context: Get.context!);
    final String dateU = SUtilDate.shortDate(dateTime: wallet.walletAccount!.dateU!, context: Get.context!);
    return ListTile(
      onTap: () => controller.onWalletChoose(wallet),
      title: Text(wallet.walletAccount!.title),
      subtitle: Text("Erstellt am $dateC - zulätzt verwendet $dateU"),
      leading: InkWell(
          // onTap: () => controller.walletTileDialog(wallet),
          // child: Icon(Icons.settings)),
          child: Icon(Icons.wallet)),
    );
  }
}
