// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/controller/result_status_enum.dart';
import 'package:partcp_client/message_type/lot_code_request.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/lot_code_from_deposit.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/url_params.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/wallet.dart';
import 'package:partcp_client/ui/event/event_list_page.dart';
import 'package:partcp_client/ui/server_user_manager/server_user_manager_page.dart';
import 'package:partcp_client/ui/wallet/create_wallet_page.dart';
import 'package:partcp_client/utils/check_for_update.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:vibration/vibration.dart';

import '../admin/event/admin_event_list.dart';
import '../voting_server/voting_server_page.dart';
import '../wallet/decrypt_wallet_page.dart';
import '../wallet/import_wallet_page.dart';


enum WalletTileDialogKey {
  export,
  rename,
  delete
}


class IndexController extends GetxController {
  final String _TAG = "IndexController";

  static IndexController to = Get.find<IndexController>();

  MainController mainController = MainController.to;
  ServerResponse serverResponse = ServerResponse();

  bool isInBackground = false;
  String? appRev;
  String? urlParams;
  bool controllerReady = false;
  bool appHasUpdate = false;

  bool receivingLotCodes = false;
  bool controllerInitialized = false;

  bool get allowCheckForNewKeys {
    if (receivingLotCodes) {
      return false;
    }
    if (mainController.votingServer == null) {
      Log.w(_TAG, "checkForNewKeys: Not checked; server == null");
      return false;
    }
    if (mainController.votingServer!.url.isEmpty) {
      Log.w(_TAG, "checkForNewKeys: Not checked; server url: ${mainController.votingServer!.url}");
      return false;
    }
    if (mainController.pubKeysOnServerValid == null) {
      Log.w(_TAG, "checkForNewKeys: Not checked; pubKeysOnServerValid == null");
      return false;
    }
    if (mainController.pubKeysOnServerValid!) {
      Log.w(_TAG, "checkForNewKeys: Not checked; pubKeysOnServerValid != true");
      return false;
    }
    return true;
  }



  Future<void> initController(var urlParams) async {
    if (controllerInitialized) return;

    controllerInitialized = true;
    CheckForUpdate().check(onResponse: (isNew) {
      appHasUpdate = isNew;
      updateMe();
      Log.d(_TAG, "CheckForUpdate: $isNew");
    });

    // urlParams = "&url=${Uri.encodeComponent('https://reg.virthos.net/')}";
    // urlParams += "&eventId=20210526-test-event-2605";
    // urlParams += "&lotCode=Z2W1Y6L6G2PPZVUAZ5QEFTVQ7Q7C5SHU";
    // urlParams += "&pubKey=${Uri.encodeComponent('8BbYgAcG4yPfKKhh64+uo/AxV0NkcxhIAmnnHq1aAosBJQqBJk1Fy6Hj3g6vsZxOv4W/gN4NwzLTIGerDMNxHg==')}";

    // urlParams = "lotCode=N6URD4GXVVXHNYUF68XDGSAY6VCA7JFN&eventId=20210602-test-event-0206a&url=https%3A%2F%2Fdemo01.partcp.org";
    // Log.d(_TAG, "urlParams: $urlParams");

    if (urlParams != null) {
      this.urlParams = urlParams;
      var uri = Uri.dataFromString("?${urlParams}");
      Map<String, String> params = uri.queryParameters;
      mainController.urlParams = UrlParams.fromMap(params);
      Log.d(_TAG, "urlParams C: $params");
    }

    if (mainController.wallet == null) {
      await mainController.initBoxen();
    }

    if (appRev == null) {
      appRev = await _setAppRev();
    }
    controllerReady = true;
    updateMe();
  }

  bool get isServerModerator =>
      mainController.wallet != null &&
          mainController.votingServer != null &&
          mainController.moderatorTools;


  Future<String> _setAppRev() async {
    if (mainController.appRev == null) {
      return await mainController.getAppRev();
    } else {
      return mainController.appRev!;
    }
  }


  Future<void> forgetKey() async {
    await mainController.forgetKey();
    updateMe();
  }


  /// CREATE WALLET
  void goToCreateWallet() async {
    ResultStatus _result = await Get.to(() => CreateWalletPage());
    Log.d(_TAG, "goToCreateWallet => result: $_result");
    updateMe();
  }


  /// IMPORT WALLET
  Future<void> goToImportWallet() async {
    /// we will get back wallet or null
    Wallet wallet = await Get.to(() => ImportWalletPage());
    _onWalletDecrypted(wallet);
  }


  Future<void> onWalletChoose(Wallet wallet) async {
    Log.d(_TAG, "onWalletChoose()");
    Wallet? curWallet = await Get.to(() => DecryptWalletPage(wallet: wallet));

    if (curWallet != null) {
      _onWalletDecrypted(curWallet);
    }
  }


  Future<void> _onWalletDecrypted(Wallet? wallet) async {
    if (wallet != null) {
      Log.d(_TAG, "_onWalletDecrypted -> wallet != null -> OK");

      serverResponse = ServerResponse();
      serverResponse.receivingData = true;
      updateMe();

      await mainController.onWalletDecrypted(wallet);
      if (mainController.wallet!.serverList.isNotEmpty) {
        VotingServer? _server = await mainController.getLastServer();
        Log.d(_TAG, "_onWalletDecrypted -> getLastServer:  ${_server}");
        if (_server != null && _server.url.isNotEmpty) {
          await _connectToServer(_server);
        }
      }

      serverResponse.receivingData = false;
      updateMe();
    }

    /// FATAL ERROR: WALLET == null !!!
    else {
      Log.e(_TAG, "_onWalletDecrypted; FATAL ERROR: wallet == null!");
    }
    updateMe();
  }


  Future<void> _connectToServer(VotingServer server) async {
    Log.d(_TAG, "_connectToServer; url: ${server.url}");

    ServerResponse serverInfo = await server.getServerInfo(account: MainController.to.wallet!.walletAccount);
    if (serverInfo.errorMessage == null) {
      mainController.votingServer = server;
      await mainController.votingServer!.decryptAccountsUserId(mainController.wallet!.pwdBytes!);
      await _checkUsersPubKeyOnServer();
      // checkForNewKeys();
    }
  }


  Future<void> _checkUsersPubKeyOnServer() async {
    if (mainController.votingServer != null) {
      final result = await mainController.wallet!.walletAccount!.checkUsersPubKeyOnServer(mainController.votingServer!);
      Log.d(_TAG, "_checkUsersPubKeyOnServer; result:$result");
      if (result != null && result is bool) {
        mainController.wallet!.walletAccount!.pubKeysOnServerValid = result;
      }
      else {
        mainController.wallet!.walletAccount!.pubKeysOnServerValid = false;
      }
    }
  }


  Future<void> triggerCheckForNewKeys() async {
    if (!allowCheckForNewKeys) {
      Future.delayed(const Duration(seconds: 1)).then((value) => checkForNewKeys());
    }
  }

  /// todo: rename to connectToWsServer
  Future<void> checkForNewKeys() async {
    mainController.wsController?.initWs();
  }


  /// todo: temp until messaging server is working
  Future<void> checkForNewKeys___old() async {

    if (!allowCheckForNewKeys) {
      return;
    }

    receivingLotCodes = true;
    updateMe();

    VotingServer server = mainController.votingServer!;
    Account walletAccount = mainController.wallet!.walletAccount!;

    server.lotCodesReceived = true;

    await server.sGroupListRequest(walletAccount);
    Log.d(_TAG, "_checkForNewKeys; groups: ${server.serverGroups.length}");
    for (Group group in server.serverGroups) {
      await server.eventsByGroup(walletAccount, group, force: true);
    }

    for (List<VotingEvent> events in server.groupEventsMap.values.toList()) {
      for (VotingEvent event in events) {
        if (event.lotCodeDeposited && mainController.wallet!.accountForEvent(server, event.id, Mode.voting, keyPairMustExists: true) == null) {
          Map<String, dynamic> messageMap = lotCodeRequest(eventId: event.id);

          ServerResponse serverResponse = await HttpController().serverRequestSigned(walletAccount, messageMap, server);
          if (serverResponse.errorMessage == null) {
            Log.d(_TAG, "_checkForNewKeys -----------------------------------------------------");
            Log.d(_TAG, "_checkForNewKeys: ${serverResponse.body}");

            LotCodeFromDeposit? lotCodeFromDeposit;
            try {
              lotCodeFromDeposit = await LotCodeFromDeposit.fromMap(serverResponse.bodyMap!, event, mainController.votingServer!);
            } catch (e) {
              // var bla = "ignore";
            }

            /// fatal error
            if (lotCodeFromDeposit == null) {
              showOkAlertDialog(
                  context: Get.context!,
                  title: "Fehler",
                  message: 'Du hast für die Veranstaltung "${event.name}" Schlüssel erhalten, '
                      'diese konnten jedoch nicht entschüsselt werden.');
            } else {
              if (await lotCodeFromDeposit.wrongCodeAlreadySaved == false) {
                /// could not be decrypted
                if (lotCodeFromDeposit.lotCode == null) {
                  await lotCodeFromDeposit.setAsWrong();
                  showOkAlertDialog(
                      context: Get.context!,
                      title: "Fehler",
                      message: 'Du hast für die Veranstaltung "${event.name}" Schlüssel erhalten, '
                          'diese konnten jedoch nicht entschüsselt werden.');
                }

                /// ok
                else {
                  Log.d(_TAG, "_checkForNewKeys: lotCodeFromDeposit: ${lotCodeFromDeposit.lotCode}");

                  VotingCredentialResponse vcResponse = VotingCredentialResponse();
                  vcResponse = await MainController.to.wallet!.requestVotingCredentialsFromLot(
                      lotCode: lotCodeFromDeposit.lotCode!,
                      event: event,
                      mode: Mode.voting,
                      server: MainController.to.votingServer!);

                  /// success
                  if (vcResponse.keySubmissionSuccess != null && vcResponse.keySubmissionSuccess!) {
                    showOkAlertDialog(
                        context: Get.context!,
                        title: "Neue Zugangsschlüssel",
                        message: 'Du hast für die Veranstaltung "${event.name}" neue Schlüssel erhalten '
                            'und kannst an dieser teilnehmen, sobald die Veranstaltung gestarted wird');
                  }
                }
              }

              /// lotCode already marked as wrong
              else {
                Log.d(_TAG, "_checkForNewKeys: lotCodeFromDeposit: old wrong lotCode:  ${lotCodeFromDeposit.lotCode}");
              }
            }

            Log.d(_TAG, "_checkForNewKeys -----------------------------------------------------");
          }
        }
      }
    }

    receivingLotCodes = false;
    updateMe();
  }


  /// GO TO SERVER PAGE
  Future<void> goToServerPage() async {
    Log.d(_TAG, "goToServerPage()");
    Get.to(() => VotingServerPage())!.then((_) async {
      Log.d(_TAG, "goToServerPage().then");
      updateMe();
      await _checkUsersPubKeyOnServer();
      Log.d(_TAG, "goToServerPage().then await _checkUsersPubKeyOnServer: pubKeysOnServerValid==${mainController.pubKeysOnServerValid}, Server: ${mainController.votingServer!.url}" );
      updateMe();
      checkForNewKeys();
      updateMe();
    });
  }


  /// GO TO ID PAGE
  void goToIdPage() {
    Get.to(() => EventListPage(mainController.votingServer!))!.then((_) => updateMe());
  }


  /// GO TO MODERATOR ID-PAGE
  // void goToModeratorIdPage() {
  //   Get.to(() => IdModeratorPage(mainController.votingServer!))!.then((value) => updateMe());
  // }


  /// GO TO MODERATOR PAGE
  void goToModeratorPage() {
    Get.to(() => AdminEventList(server: mainController.votingServer!, account: mainController.wallet!.walletAccount!))
    ?.then((_) => updateMe());
  }


  /// GO TO [ServerUserManagerPage]
  void goToServerUserManagerPage() {
    Get.to(() => ServerUserManagerPage())!.then((_) => updateMe());
  }

  /*
  Future<void> walletTileDialog(Wallet wallet) async {
    WalletTileDialogKey? result = await showModalActionSheet(
      context: Get.context!,
      title: "Schlüsselbund: ${wallet.walletAccount!.title}",
      // message: 'bla',
      actions: [
        SheetAction(label: "Sicherungskopie exportieren", key: WalletTileDialogKey.export),
        SheetAction(label: "Umbenennen", key: WalletTileDialogKey.rename),
        SheetAction(label: 'Löschen', key: WalletTileDialogKey.delete, isDestructiveAction: true)
      ],
    );

    if (result != null) {
      switch (result) {
        case WalletTileDialogKey.export:
          break;
        case WalletTileDialogKey.rename:
          // TODO: Handle this case.
          break;
        case WalletTileDialogKey.delete:
          OkCancelResult delResult = await _walletTileDialogDelete(wallet);
          if (delResult ==  OkCancelResult.ok) {
            Log.d(_TAG, "_walletTileDialogDelete result: $delResult");
            await mainController.deleteWallet(walletId: await wallet.walletAccount!.walledId);
            Get.back();
            updateMe();
          }
          break;
      }
    }

    Log.d(_TAG, "walletTileDialog result: $result");
  }

  Future<OkCancelResult> _walletTileDialogDelete(Wallet wallet) async {
    return await showOkCancelAlertDialog(
      context: Get.context!,
      title: "Schlüsselbund '${wallet.walletAccount!.title}' löschen?",
      isDestructiveAction: true,
      message: "Bist Du sicher, dass Du den Schlüsselbung '${wallet.walletAccount!.title}' und alle dazugegörigen Daten löschen möchtest?"
          "\nFalls Du keine Sichernungkopie von dem Schlüsselbund exportiert hast, kannst Du diesen nie wieder verwenden.",
      okLabel: 'Löschen',
      cancelLabel: 'Abbrechen',
    );
  }
  */



  SheetAction _walletTileActionExport (Wallet wallet) {
    return SheetAction(
      label: "Exportieren"
    );
  }

  void updateMe() {
    Log.d(_TAG, "updateMe()");
    update();
  }

  Future<void> onRefresh() async {
    Log.d(_TAG, "onRefresh()");
    update();
  }
}
