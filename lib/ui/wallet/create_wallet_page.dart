// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/wallet.dart';
import 'package:partcp_client/ui/home/index_controller.dart';
import 'package:partcp_client/ui/voting_server/voting_server_page.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_main.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/text_field_widget.dart';


class CreateWalletController extends GetxController {
  final _TAG = "CreateWalletController";

  MainController mainController = MainController.to;

  TextEditingController nameController = TextEditingController();
  TextEditingController pwd1Controller = TextEditingController();
  TextEditingController pwd2Controller = TextEditingController();

  bool _keysSaved = false;
  bool isEncrypting = false;
  bool walletCreated = false;

  Wallet get wallet => mainController.wallet!;
  set wallet (Wallet _wallet) => mainController.wallet = _wallet;

  /// on some browsers we can not create kesy
  bool fatalErrorCreateKeys = false;
  String fatalErrorMessage = "";

  bool get allowEncrypt => pwd1Controller.text == pwd2Controller.text &&
      pwd1Controller.text.length >= MainController.MIN_KEY_PWD_LEN &&
      !_keysSaved && !isEncrypting && nameController.text.length >= 2;

  // bool get allowFinish => _keysSaved;
  bool get pwdFieldEnabled => !_keysSaved;

  void initController() {
    nameController.text = "";
    pwd1Controller.text = "";
    pwd2Controller.text = "";
    _keysSaved = false;
    walletCreated = false;
  }

  Future<void> onClickEncryptKeys() async {
    if (allowEncrypt) {
      isEncrypting = true;
      updateMe();
      await _createAndEncryptWallet(pwd1Controller);
      isEncrypting = false;
      updateMe();
    }
  }

  // Future<void> onClickGoBack(ResultStatus result) async {
  //   Get.back(result: result);
  // }


  void onInputChanged()  {
    updateMe();
  }


  /// ENCRYPT KEYS + CREATE FILE CONTENT
  Future<void> _createAndEncryptWallet(TextEditingController textEditingController) async {
    String _pass = "";
    try {
      _pass += "Create Wallet..";
      wallet = Wallet();
      _pass += "\nCreate Wallet: OK";
      wallet.pwdBytes = textEditingController.text.codeUnits;

      /// create wallet keys
      _pass += "\nCreate Account..";
      wallet.walletAccount = Account(Mode.wallet);
      wallet.walletAccount!.name = nameController.text;
      _pass += "\nCreate Account: OK";
      _pass += "\nCreate Keys...";

      if (await wallet.walletAccount!.createNewKeys()) {
        _pass += "\nCreate Keys: OK";
        _pass += "\nEncrypt Keys...";
        await wallet.walletAccount!.encryptKeys(wallet.pwdBytes!);
        _pass += "\nEncrypt Keys: OK";

        _pass += "\nEncrypt onWalletChanged...";
        await mainController.onWalletDecrypted(wallet);
        await mainController.onWalletChanged(updateNotification: false);
        _pass += "\nEncrypt onWalletChanged: OK";

        _pass += "\nGo to Server Page";
        // _goToServerPage();
        walletCreated = true;
      }
      else {
        _pass += "\nCreate Keys: FAILS";
        fatalErrorCreateKeys = true;
      }
    }
    catch(e, s) {
      _pass += "\n\nerror: $e; \nstack: $s";
      Log.e(_TAG, "_createAndEncryptWallet: $_pass");
      fatalErrorCreateKeys = true;
      fatalErrorMessage = "$_pass";
    }

    updateMe();
  }

  void onFinish() {
    _goToServerPage();
  }

  void _goToServerPage() {
    IndexController.to.updateMe();
    Route<dynamic> route = Get.rawRoute!;

    Get.to(() => VotingServerPage());

    /// remove this route
    Get.removeRoute(route);
  }


  void updateMe() {
    update();
  }
}


class CreateWalletPage extends StatelessWidget {
  final _TAG = "CreateWalletPage";
  final CreateWalletController controller = Get.put(CreateWalletController());

  final spacer = Container(height: 20,);

  final String createWalletTitle = "Schlüsselbund erstellen";
  final String walletCreatedTitle = "Schlüsselbund erstellt";

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (_, __) => GetBuilder<CreateWalletController>(
          initState: (_) => controller.initController(),
          builder: (_) => Scaffold(
            appBar: MyAppBar(
              title: Text(controller.walletCreated
                ? walletCreatedTitle
                : createWalletTitle),
            ),
            body: SingleChildScrollView(
              child: controller.walletCreated
                  ? _bodyWalletCreated
                  : _bodyCreateWallet
            ),
          )),
    );
  }


  /// Body create Wallet
  Widget get _bodyCreateWallet => Column(
    mainAxisSize: MainAxisSize.max,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [

      CardWidget(
        title: "Ein Schlüsselbund für Deine persönlichen Zugangsdaten",
        childNeedsExtraTopPadding: true,
        child: Text("Ein Schlüsselbund dient dazu, Deine persönlichen Daten zu speichern. "
            "Der Name wird nirgends veröffentlicht. Er verbleibt auf diesem Gerät und dient nur dazu, "
            "Dir den Überblick zu erleichtern. Verwende am besten Deinen Vornamen bzw. den Namen der Person, für die Du den Schlüsselbund erstellst."
            "\n\nUm zu verhindern, dass Unbefugte von außen Daten abhören oder manipulieren können, werden kryptografische Schlüssel eingesetzt. "
            "Damit diese Schlüssel auf Deinem Gerät sicher gespeichert werden können, brauchst Du ein passwortgeschütztes Schlüsselbund."
            "\n\nBitte wähle ein Passwort, das sich nicht einfach erraten oder durch Ausprobieren ermitteln lässt. "
            "Notiere Dir das Passwort an einem sicheren Ort. Wenn Du es verlierst, gibt es keine Möglichkeit mehr, auf das Schlüsselbund zuzugreifen. "
            "Du müsstest sich in dem Fall neu registrieren, neue Teilnahmecodes für laufende Abstimmungen anfordern u. a."),
      ),

      CardWidget(
        // title: "Passwort",

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Text("Bitte geben Sie ein Passwort zur Verschlüsselung Ihres Schlüsselbundes ein. "),
            Center(
              child: Column(
                children: [

                  /// NAME
                  TextFieldWidget(
                    textController: controller.nameController,
                    labelText: "Name",
                    hint: "Name",
                    // onTap: () => (_),
                    onSubmitted: (_) => (_),
                    onChanged: (_) => controller.onInputChanged(),
                    autoCorrect: false,
                    isPasswordField: false,
                    keyboardType: TextInputType.visiblePassword,
                    textAlign: TextAlign.start,
                    maxBoxLength: 350,
                    enabled: controller.pwdFieldEnabled,
                  ),

                  spacer,
                  spacer,

                  /// PWD
                  TextFieldWidget(
                    textController: controller.pwd1Controller,
                    labelText: "Passwort",
                    hint: "Passwort",
                    // onTap: () => (_),
                    onSubmitted: (_) => (_),
                    onChanged: (_) => controller.onInputChanged(),
                    autoCorrect: false,
                    isPasswordField: true,
                    keyboardType: TextInputType.visiblePassword,
                    textAlign: TextAlign.start,
                    maxBoxLength: 350,
                    enabled: controller.pwdFieldEnabled,
                  ),

                  spacer,

                  /// PWD REPEAT
                  Center(
                    child: TextFieldWidget(
                      textController: controller.pwd2Controller,
                      labelText: "Passwort wiederholen",
                      hint: "Passwort",
                      // onTap: () => (_),
                      onSubmitted: controller.allowEncrypt
                          ? (_) => controller.onClickEncryptKeys()
                          : null,
                      onChanged: (_) => controller.onInputChanged(),
                      autoCorrect: false,
                      isPasswordField: true,
                      keyboardType: TextInputType.visiblePassword,
                      textAlign: TextAlign.start,
                      maxBoxLength: 350,
                      enabled: controller.pwdFieldEnabled,
                    ),
                  ),

                  Center(
                    child: _fatalErrorCreateKeys(),
                  ),

                  spacer,
                  spacer,

                  Center(
                    child: ButtonMain(
                        onPressed: controller.allowEncrypt
                            ? () => controller.onClickEncryptKeys()
                            : null,
                        child: Text("Schlüsselbund erstellen")
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    ],
  );


  /// Body create Wallet
  Widget get _bodyWalletCreated => Column(
    mainAxisSize: MainAxisSize.max,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [

      CardWidget(
        title: "Schlüsselbund erstellt",
        childNeedsExtraTopPadding: true,
        child: Column(
          children: [
            Text("Der Schlüsselbund wurde erfolgreich erstellt.\n\n"
                "Bitte notiere das Passwort an einem sicheren Ort, damit Du es gegebenenfalls nachschlagen kannst. "
                "Wenn Du das Passwort verlierst, gibt es keine Möglichkeit, den Schlüsselbund weiter zu verwenden. "
                "Du musst in dem Fall die darin gespeicherten Schlüssel neu erstellen und erneut verifizieren lassen. "),

            spacer,
            spacer,

            Center(
              child: ButtonMain(
                  onPressed: () => controller.onFinish(),
                  child: Text("Verstanden")
              ),
            )

          ],
        ),
      ),

    ],
  );


  Widget _fatalErrorCreateKeys() {
    if (controller.fatalErrorCreateKeys) {
      return Text("Fatal Error: Can not create keys"
          "\n\n"
          "${controller.fatalErrorMessage}",
        textScaleFactor: 1.1,
        style: TextStyle(
          color: Colors.red
      ),
      );
    }
    else {
      return Container();
    }
  }
}