// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/wallet.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/submit_button.dart';
import 'package:partcp_client/widgets/text_field_widget.dart';

const _TAG = "DecryptKeysController";

class DecryptWalletController extends GetxController {

  MainController mainController = MainController.to;

  TextEditingController pwdController = new TextEditingController();

  late Wallet wallet;
  String? decryptionError;
  bool submitting = false;

  bool _keysDecrypted = false;

  bool get allowDecrypt => pwdController.text.length >= MainController.MIN_KEY_PWD_LEN &&
      !_keysDecrypted;

  bool get pwdFieldEnabled => !_keysDecrypted;

  void initController(Wallet wallet) async {
    this.wallet = wallet;

    pwdController.text = "";
    decryptionError = null;
    _keysDecrypted = false;
    updateMe();
  }

  void onInputChanged()  {
    updateMe();
  }


  void onClickDecryptWallet() {
    Log.d(_TAG, "onClickDecryptWallet");
    if (allowDecrypt) {
      submitting = true;
      updateMe();
      _decryptWallet(pwdController);
    }
  }

  /// ENCRYPT KEYS + CREATE FILE CONTENT
  Future<void> _decryptWallet(TextEditingController textEditingController) async {
    decryptionError = null;
    bool isSuccess = await wallet.decryptAccounts(textEditingController.text.codeUnits);

    /// Pop decrypted wallet
    if (isSuccess) {
      Get.back(result: wallet);
    }
    else {
      Log.e(_TAG, "_decryptKeys: FAILS");
      decryptionError = "Passwort falsch";
      submitting = false;
      updateMe();
    }
  }

  void updateMe() {
    update();
  }
}

typedef CallbackWallet = void Function(Wallet callbackWallet);

class DecryptWalletPage extends StatelessWidget {
  final Wallet wallet;
  // final CallbackWallet callbackWallet;

  const DecryptWalletPage({
    required this.wallet,
    // @required this.callbackWallet
  });

  Future<bool> _onWillPop() async {
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final DecryptWalletController controller = Get.put(DecryptWalletController());

    final spacer = Container(height: 20,);


    return OrientationBuilder(
      builder: (_, __) => GetBuilder<DecryptWalletController>(
          initState: (_) => controller.initController(wallet),
          builder: (_) => WillPopScope(
            onWillPop: _onWillPop,
            child: Scaffold(
              appBar: MyAppBar(
                title: Text("Schlüsselbund entsperren (${wallet.walletAccount!.title})"),
              ),
              body: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    CardWidget(
                      title: "Schlüsselbund entsperren",
                      childNeedsExtraTopPadding: true,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Bitte gebe Dein Passwort ein, um den Schlüsselbund zu entsperren."),
                              // "\n\n"
                              // "(Mindestens ${MainController.MIN_KEY_PWD_LEN} Zeichen)"),
                          spacer,

                          /// PWD
                          Center(
                            child: TextFieldWidget(
                              textController: controller.pwdController,
                              labelText: "Passwort",
                              hint: "Passwort",
                              onTap: () => (_),
                              onSubmitted: controller.allowDecrypt
                                  ? (_) => controller.onClickDecryptWallet()
                                  : null,
                              onChanged: (_) => controller.onInputChanged(),
                              autoCorrect: false,
                              isPasswordField: true,
                              keyboardType: TextInputType.visiblePassword,
                              textAlign: TextAlign.start,
                              maxBoxLength: 250,
                              enabled: controller.pwdFieldEnabled,
                              errorText: controller.decryptionError,
                            ),
                          ),

                          spacer,

                          SubmitButton(
                              onPressed: () =>  controller.onClickDecryptWallet(),
                              text: "Schlüsselbund entsperren",
                              submitting: controller.submitting,
                              enabled: controller.allowDecrypt),
                        ],
                      ),
                    ),

                  ],
                ),
              ),
            ),
          )),
    );
  }

}
