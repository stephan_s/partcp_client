// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:partcp_client/objects/voting.dart';

String votingHeroTag(Voting voting, String optionId, int value) {
  return "${voting.id}-$optionId-${value.toString()}";
}