// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/styles/styles.dart';
import 'package:partcp_client/ui/voting/voting_page.dart';
import 'package:partcp_client/ui/voting/voting_result_page.dart';
import 'package:partcp_client/ui/wallet/export_wallet.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/save_wallet_menu_icon.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/widgets.dart';

import 'widgets/info_widget.dart';

const _TAG = "VotingListController";

class VotingListController extends GetxController {
  ServerResponse serverResponse = ServerResponse();
  MainController mainController = MainController.to;

  late VotingServer server;
  Account? account;
  late VotingEvent event;
  late bool previewOnly;
  String previewString = "";

  Voting? lastSelectedVoting;

  late Key scrollBodyKey;

  /// list with status open, idle
  List<dynamic> openList = [];

  /// temp list with status closed
  List<dynamic> closedList = [];

  /// [gotData] is using to prevent to display "no items" on init page
  bool gotData = false;

  VotingStatusEnum votingStatusEnum = VotingStatusEnum.all;


  void initController(VotingServer server, Account? account, VotingEvent votingEvent, bool previewOnly) async {
    this.server = server;
    this.account = account;
    this.event = votingEvent;
    this.previewOnly = previewOnly;

    if (previewOnly) {
      previewString = "[Vorschau] ";
    }

    gotData = false;
    scrollBodyKey = ValueKey("voting_list;" + DateTime.now().millisecondsSinceEpoch.toString());
  }

  bool isVoted(Voting voting) {
    return account != null
      ? account!.isVoted(voting)
      : false;
  }

  Future<void>onGoToVoting(Voting voting) async {
    lastSelectedVoting = voting;
    if (voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED) {
      Get.to(() =>
          VotingResultPage(
            server: server,
            account: account,
            voting: voting,
            votingEvent: event
          ))!.then((_) => updateMe());
    }
    else {
      Get.to(() =>
          VotingPage(
            server: server,
            account: account,
            voting: voting,
            votingEvent: event,
            previewOnly: previewOnly,
          ))!.then((_) => updateMe());
    }
  }

  void onPostFrameCallback() async {
    Log.d(_TAG, "onPostFrameCallback");
    refreshList();
  }

  Future<void> refreshList() async {
    serverResponse
      ..errorMessage = null
      ..receivingData = true;
    updateMe();
    await _requestVotings();
    gotData = true;
    updateMe();
  }


  Future<void> _requestVotings() async {
    serverResponse = await event.sVotingListRequest(server, account, VotingStatusEnum.all);

    openList.clear();
    closedList.clear();

    for (Voting voting in event.votings) {
      if (previewOnly) {
        voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED
            ? closedList.add(voting)
            : openList.add(voting);
      }
      else {
        if (voting.status == Voting.STATUS_OPEN) {
          openList.add(voting);
        }
        else if(voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED) {
          closedList.add(voting);
        }
      }
    }

    Log.d(_TAG, "openList: ${openList.length}");
    Log.d(_TAG, "closedList: ${closedList.length}");
  }

  void updateMe() {
    update();
  }
}

class VotingListPage extends StatelessWidget {
  final VotingServer server;
  final Account? account;
  final VotingEvent votingEvent;
  final bool previewOnly;

  VotingListPage({
    required this.server,
    required this.account,
    required this.votingEvent,
    this.previewOnly = false,
  });

  final VotingListController controller = Get.put(VotingListController());

  PreferredSizeWidget get _myAppBar => MyAppBar(
        title: Text("${controller.previewString}${controller.event.name}"),
        actions: [
          /// save wallet
          saveWalletMenuIcon(onPressed: () {
            ExportWallet().download();
            controller.updateMe();
          }),

          /// refresh
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () => controller.refreshList(),
          ),
        ],
      );

  Future<bool> onWillPop() async {
    return true;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return WillPopScope(
      onWillPop: onWillPop,
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<VotingListController>(
          initState: (_) => controller.initController(server, account, votingEvent, previewOnly),
          builder: (_) => Scaffold(
            appBar: _myAppBar,
            body: Widgets.loaderBox(
              loading: controller.serverResponse.receivingData,
              child: ScrollBody(
                key: controller.scrollBodyKey,
                children: _body,
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(ServerErrorMessage(serverResponse: controller.serverResponse));
    // list.add(SaveWalletSnackBarPage());

    if (controller.openList.isEmpty && controller.closedList.isEmpty) {
      if (controller.gotData) {
        list.add(_noItems());
      }
    } else {
      list.add(
          infoWidget(
            title: controller.event.name,
            shortDescription: controller.event.clientData!.shortDescription,
            description: controller.event.clientData!.description
          )
      );
      list.addAll(_votingList);
    }
    return list;
  }


  List<Widget> get _votingList {
    List<Widget> list = [];

    list.add(_headlineItem("Aktive Abstimmungen"));
    if (controller.openList.isNotEmpty) {
      controller.openList.forEach((voting) {
        list.add(_votingItem(voting));
      });
    }
    else {
      list.add(_noItems());
    }

    if (controller.closedList.isNotEmpty) {
      if (controller.openList.isNotEmpty) {
        list.add(SizedBox(height: 30,));
      }
      list.add(_headlineItem("Beendete Abstimmungen"));
      controller.closedList.forEach((voting) {
        list.add(_votingItem(voting));
      });
    }

    return list.isNotEmpty
        ? list
        : [Container()];
  }

  Widget _noItems() {
    return CardWidget(
      child: ListTile(
        leading: Icon(Icons.event_busy),
        title: Text(
          "Zur Zeit sind keine Abstimmungen vorhanden",
        ),
      ),
    );
  }


  Widget _votingItem(Voting voting) {
    return Card(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
      child: ListTile(
        // selected: controller.lastSelectedVoting != null && controller.lastSelectedVoting! == voting,
        tileColor: controller.lastSelectedVoting != null && controller.lastSelectedVoting! == voting
          ? Styles.selectedTileColor
          : Colors.black.withOpacity(0.03),
        title: Text(
          voting.title,
          style: TextStyle(fontWeight: voting.status == Voting.STATUS_OPEN ? FontWeight.bold : FontWeight.normal),
        ),
        subtitle: Text(voting.clientData.shortDescription),
        contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
        trailing: _votingItemIcon(voting),
        onTap: () => controller.onGoToVoting(voting)
      ),
    );
  }

  Widget? _votingItemIcon(Voting voting) {
    if (voting.status == Voting.STATUS_FINISHED) {
      return voting.statusIcon;
    }

    if (controller.isVoted(voting)) {
      return Icon(
        Icons.check_circle_outline_rounded,
        color: Colors.green,
      );
    }
    return null;
  }


  Widget _headlineItem(String title) {
    return Card(
      margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
      elevation: 1,
      child: ListTile(
        title: Text(
          title,
          textScaleFactor: 1.1,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        tileColor: Colors.black12,
      ),
    );
  }
}
