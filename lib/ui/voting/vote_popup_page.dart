// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_type.dart';
import 'package:partcp_client/widgets/app_bar.dart';

import 'widgets/voting_icon_widget.dart';

typedef VotingCallback = void Function(String id, int vote);

class VotePopupPage extends StatelessWidget {
  final Voting voting;
  final String name;
  final String optionsId;
  final VotingCallback callback;

  const VotePopupPage({
    required this.voting,
    required this.name,
    required this.optionsId,
    required this.callback,
  });

  _callback(String id, int value) {
    callback(id, value);
    Get.back();
  }

  VotingIconWidget get iw => VotingIconWidget.large(voting.type!);

  @override
  Widget build(BuildContext context) {
    Widget votingWidget() {
      switch (voting.type) {
        case VotingType.YES_NO_RATING:
          return _yesNo();

        case VotingType.CONSENSUS_3:
          return _consensus10;

        case VotingType.CONSENSUS_5:
          return _consensus5;

        case VotingType.CONSENSUS_10:
          return _consensus10;

        case VotingType.SINGLE_CHOICE:
          return _consensus10;

        default:
          return Container();
      }
    }

    return OrientationBuilder(
      builder: (_, __) => Scaffold(
        appBar: MyAppBar(
          title: Text(name),
        ),
        body: InkWell(
          onTap: () => Get.back(),
          // splashColor: Colors.transparent,
          child: Center(
            child: FittedBox(
                fit: BoxFit.contain,
                child: Container(
                    height: context.height - MyAppBar().preferredSize.height,
                    width: (Get.width > Get.height && GetPlatform.isMobile) ? null : Get.width,
                    child: Center(child: votingWidget()))),
          ),
        ),
        backgroundColor: Colors.black54.withOpacity(0.7),
      ),
    );
  }

  Widget get _cancelButton => GetPlatform.isWeb
      ? Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 0),
            child: IconButton(
              icon: Icon(
                Icons.cancel,
                color: Colors.white,
                size: 30,
              ),
              onPressed: () => Get.back(),
            ),
          ),
        )
      : Container();

  Widget _yesNo() {
    final Widget _spacer = Container(
      constraints: BoxConstraints(minHeight: 0, maxHeight: 50),
    );

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _cancelButton,

        _spacer,

        /// -- (NO_VOTE_VALUE)
        iw.squareButtonWidget(Vote.NO_VOTE_VALUE, () => _callback(optionsId, Vote.NO_VOTE_VALUE), namedValue: "ENTHALTUNG"),

        /// YES (1)
        iw.squareButtonWidget(1, () => _callback(optionsId, 1), namedValue: "JA"),

        /// NO (0)
        iw.squareButtonWidget(0, () => _callback(optionsId, 0), namedValue: "NEIN"),

        _spacer,
        _spacer,
      ],
    );
  }

  Widget get _consensus5 {
    final Widget _spacer = Container(
      constraints: BoxConstraints(minHeight: 0, maxHeight: 50),
    );

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _cancelButton,

        /// -- (NO_VOTE_VALUE)
        iw.squareButtonWidget(Vote.NO_VOTE_VALUE, () => _callback(optionsId, Vote.NO_VOTE_VALUE), namedValue: "ENTHALTUNG"),

        ButtonBar(
          alignment: MainAxisAlignment.center,
          children: [
            _circleButton(0, 0),
            _circleButton(1, 3),
            _circleButton(2, 5),
            _circleButton(3, 8),
            _circleButton(4, 10),
          ],
        ),
        _spacer,
      ],
    );
  }

  Widget get _consensus10 => GetPlatform.isMobile && Get.width > Get.height ? _consensus10Landscape() : _consensus10Portrait();

  /// Consensus 10 Portrait
  Widget _consensus10Portrait() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _cancelButton,
        Text(
          "Widerstandspunkte",
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        Spacer(),

        /// -- (Vote.NO_VOTE_VALUE)
        Container(
          constraints: BoxConstraints(maxWidth: 300),
          child:
              iw.squareButtonWidget(Vote.NO_VOTE_VALUE, () => _callback(optionsId, Vote.NO_VOTE_VALUE), namedValue: "ENTHALTUNG"),
        ),

        Spacer(),

        ///0
        _circleButton(0, 0),

        /// 1, 2, 3
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            _circleButton(1, 1),
            _circleButton(2, 2),
            _circleButton(3, 3),
          ],
        ),

        /// 4, 5, 6
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _circleButton(4, 4),
            _circleButton(5, 5),
            _circleButton(6, 6),
          ],
        ),

        /// 7, 8, 9
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _circleButton(7, 7),
            _circleButton(8, 8),
            _circleButton(9, 9),
          ],
        ),

        /// 10
        _circleButton(10, 10),

        /*
            return Hero(
      tag: votingHeroTag(controller.votingEvent, controller.voting.id, value),
      child: iw.circleButtonWidget(value, () => _goToVotePopupPage())
    );
         */

        Spacer(),
      ],
    );
  }

  /// Consensus 10 Portrait
  Widget _consensus10Landscape() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Align(alignment: Alignment.topLeft, child: _cancelButton),

        Text(
          "Widerstandspunkte",
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
        ),

        Spacer(),

        /// -- (Vote.NO_VOTE_VALUE)
        Container(
          constraints: BoxConstraints(maxWidth: 300),
          child:
              iw.squareButtonWidget(Vote.NO_VOTE_VALUE, () => _callback(optionsId, Vote.NO_VOTE_VALUE), namedValue: "ENTHALTUNG"),
        ),

        Spacer(),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _circleButton(0, 0),
            _circleButton(1, 1),
            _circleButton(2, 2),
            _circleButton(3, 3),
            _circleButton(4, 4),
            _circleButton(5, 5),
            _circleButton(6, 6),
            _circleButton(7, 7),
            _circleButton(8, 8),
            _circleButton(9, 9),
            _circleButton(10, 10),
          ],
        ),

        Spacer(),
      ],
    );
  }

  Widget _circleButton(int value, int colorValue) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(6.0),
        child: iw.circleButtonWidget(
          value,
          colorValue,
          () => _callback(optionsId, value),
          // selected: value == controller.getVoteValue(optionsId.toString())
        ),
      ),
    );
  }
}
