// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/scroll_body.dart';

import 'widgets/info_widget.dart';
import 'widgets/option_result_item.dart';

class VotingResultController extends GetxController {

  late VotingServer server;
  Account? account;
  late Voting voting;
  late VotingEvent event;

  void initController(VotingServer server, Account? account, VotingEvent event, Voting voting, ) {
    this.server = server;
    this.account = account;
    this.voting = voting;
    this.event = event;
  }

  void onPostFrameCallback() {
    // getVotingResults();
  }

  void updateMe() {
    update();
  }
}

class VotingResultPage extends StatelessWidget {
  final VotingServer server;
  final Account? account;
  final Voting voting;
  final VotingEvent votingEvent;

  VotingResultPage({
    required this.server,
    this.account,
    required this.voting,
    required this.votingEvent,
  });

  final VotingResultController controller = Get.put(VotingResultController());

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return OrientationBuilder(
      builder: (_, __) => GetBuilder<VotingResultController>(
          initState: (_) => controller.initController(server, account, votingEvent, voting),
          builder: (_) => Scaffold(
            appBar: MyAppBar(
              title: Text("${voting.title}"),
            ),
            body: ScrollBody(children: _body)),
          ),
    );
  }


  List<Widget> get _body {

    List<Widget> _items = [
      infoWidget(
          title: controller.voting.title,
          shortDescription: controller.voting.clientData.shortDescription,
          description: controller.voting.clientData.description
      ),
      _resultSummary,
    ];
    for (VotingOption option in controller.voting.votingOptions) {
      _items
          .add(OptionResultItem(controller: controller, option: option).optionItem());
    }
    return _items;
  }

  Widget get _resultSummary => Card(
    child: Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
      child: ListTile(
        title: Text("Zusammenfassung der Auswertung"),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Abgegebene Stimmzettel: ${controller.voting.votingResult.participants}"),
            Text("Ungültige Stimmzettel: ${controller.voting.votingResult.invalid}")
          ],
        ),
      ),
    ),
  );

}
