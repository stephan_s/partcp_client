// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_type.dart';
import 'package:partcp_client/ui/voting/widgets/embedded_voting.dart';
import 'package:partcp_client/utils/launch_url.dart';

import 'icons_colors.dart';
import 'voting_icon_widget.dart';
import '../voting_controller.dart';

class OptionItem {
  final VotingController controller;
  final VotingOption option;


  const OptionItem({required this.controller, required this.option});


  ///
  bool get _hasEnoughSpace => Get.width > Get.height || MainController.to.isDesktopOrWeb;


  Widget optionItem() {
    Color tileColor = const Color.fromARGB(255, 242, 242, 242);
    return Card(
      color: tileColor,
      margin: EdgeInsets.fromLTRB(0, 12, 0, 5),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            /// Option Title + clientData
            ListTile(
              // tileColor: tileColor,

              title: Text(
                option.name,
                style: TextStyle(
                    // fontWeight: controller.isVoted(option.id)
                    //     ? FontWeight.normal
                    //     : FontWeight.bold,
                  fontWeight: FontWeight.bold
                ),
              ),
              subtitle: _mainSubtitle,
              contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 30.0),
              // onTap: () => controller.goToVotePopupPage(option),
            ),

            Divider(),

            /// voting icons
            ListTile(
              tileColor: const Color.fromARGB(0, 255, 255, 255),

              title: Text(
                _votingTileTitle,
                style: TextStyle(
                  fontStyle: FontStyle.italic,
                ),
              ),
              subtitle: _embeddedVoting,
              contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 30.0),

              trailing: _votingResultWidget(
                  controller.voting.type!, controller.getVoteValue(option.id)),

              // onTap: _onVotingTileAllowTap
              //         ? () => controller.goToVotePopupPage(option)
              //         : null,
            ),

          ],
        ),
    );
  }


  ///////////////////////////////////////////////////////////////////////////
  /// MAIN TILE
  ///
  Widget? get _mainSubtitle {
    List<Widget> _subtitleItems = [];
    Widget w;

    /// subtitle short description
    if (option.clientData.shortDescription.isNotEmpty) {
      w = Padding(
        padding: const EdgeInsets.only(top: 6),
        child: Text(option.clientData.shortDescription.trim()),
      );
      _subtitleItems.add(w);
    }

    /// subtitle link
    if (option.clientData.linkUrl.isNotEmpty) {
      w = Linkify(
        onOpen: (element) {
          print("Clicked $element!");
          launchURL(element.url);
        },
        text: "\nMehr Infos: ${option.clientData.linkUrl}",
      );
      _subtitleItems.add(w);
    }

    return _subtitleItems.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.only(left: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _subtitleItems,
            ),
          )
        : null;
  }


  ///////////////////////////////////////////////////////////////////////////
  /// VOTING TILE
  ///

  String get _votingTileTitle {
    if (VotingType.isConsensus(controller.voting.type!)) {
      return "Deine Widerstand-Punkte";
    }
    if (controller.voting.type == VotingType.YES_NO_RATING) {
      return "Deine Zustimmung";
    }

    String maxCount = controller.requestedVotesCount < controller.voting.votingOptions.length
      ? " (max: ${controller.requestedVotesCount})"
      : "";
    return "Deine Auswahl$maxCount";
  }


  Widget? get _embeddedVoting {
    /// EMBEDDED VOTING
    EmbeddedVoting embeddedVoting = EmbeddedVoting(
        controller: controller,
        voting: controller.voting,
        option: option,
        callback: (id, value) => controller.setVotingValued(id, value));

    double? height = 60;
    if (controller.voting.type == VotingType.YES_NO_RATING) {
      height = 50;
    }

    /// consensus-10 and mobile in portrait
    else if (controller.voting.type == VotingType.CONSENSUS_10 && !_hasEnoughSpace ||
        controller.voting.type == VotingType.SINGLE_CHOICE ||
        VotingType.isMultipleChoice(controller.voting.type!)) {

      height = null;
    }

    return height != null
       ? Container(
          height: height,
          child: embeddedVoting.createItems()
          )
      : null;
  }


  Widget? _votingResultWidget(String type, int? value) {
    VotingIconWidget iw = VotingIconWidget.middle(controller.voting.type!);

    Widget w;

    /// CONSENSUS
    if (VotingType.isConsensus(controller.voting.type!)) {
      w = iw.circleButtonWidget(
          value,
          consensuColor(controller.voting.type!, value),
          controller.voting.type == VotingType.CONSENSUS_10 || controller.voting.type == VotingType.CONSENSUS_5
              ? () => controller.goToVotePopupPage(option)
              : () => {});
    }
    /// YES-NO
    else if (controller.voting.type == VotingType.YES_NO_RATING) {
      w = iw.squareButtonWidget(
          value,
          null, //() => controller.goToVotePopupPage(option),
          imageOnly: true
      );
    }
    /// SINGLE CHOICE
    else if (controller.voting.type == VotingType.SINGLE_CHOICE) {
      w = Radio(
        value: option.id,
        groupValue: controller.votingSingleChoiceGroup,
        onChanged: (value) => controller.setVotingSingleChoice(value.toString()),
      );
    }
    /// MULTIPLE CHOICE
    else if (VotingType.isMultipleChoice(controller.voting.type!)) {
      w = Checkbox(
        value: controller.isVoted(option.id),
        onChanged: (value) {
          if (!controller.setVotingMultiple(option.id, value!)) {
            showOkAlertDialog(
              context: Get.context!,
              message: 'Anzahl der Optionen überschritten. \n\n'
                  'Maximale Anzahl: ${controller.requestedVotesCount}',
              okLabel: 'ok',
              barrierDismissible: true,
            );
          }
        },
      );
    }

    else {
      w = Container(width: 40, height: 40, color: Colors.cyanAccent,);
    }
    return w;
  }
}
