// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_type.dart';

import 'icons_colors.dart';
import 'voting_icon_widget.dart';


class EmbeddedResult {
  final Voting voting;
  final VotingOption option;
  final int highestCount;

  VotingIconWidget get iw => VotingIconWidget.small(voting.type!);

  const EmbeddedResult({
    required this.voting,
    required this.option,
    required this.highestCount
  });

  Widget? createItems() {
    return _voting;
  }


  Widget? get _voting {
    /// CONSENSUS 3, 5, 10 long
    if (voting.type == VotingType.CONSENSUS_3 ||
        voting.type == VotingType.CONSENSUS_5 ||
        voting.type == VotingType.CONSENSUS_10) {
      return _consensus;
    }

    /// YES-NO
    else if(voting.type == VotingType.CONSENSUS_10) {
      return _consensus10Short;
    }
    else if(voting.type == VotingType.YES_NO_RATING) {
      return _yesNo;
    }
    else {
      return null;
    }
  }


  /// CONSENSUS
  Widget get _consensus {
    switch (voting.type) {
      case VotingType.CONSENSUS_3:
        return _consensus3;

      case VotingType.CONSENSUS_5:
        return _consensus5;

      default:
        return _consensus10Long;
    }
  }


  /// CONSENSUS 10 SHORT
  Widget? get _consensus10Short {
    return null;
  }

  Widget get _yesNo {
    return FittedBox(
      fit: BoxFit.contain,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          _squareButtonWithSelected(1, yesNoColor(1), "Ja"),
          _squareButtonWithSelected(0, yesNoColor(0), "NEIN"),
          _squareButtonWithSelected(Vote.NO_VOTE_VALUE, iconsColors(Vote.NO_VOTE_VALUE), "ENTHALTUNG"),
        ],
      ),
    );
  }


  Widget get _consensus3 {
    return FittedBox(
      fit: BoxFit.contain,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          // iw.circleSpacer,
          _consensusChart(0, consensu3Color(0)),
          _consensusChart(1, consensu3Color(1)),
          _consensusChart(2, consensu3Color(2)),
          _consensusChart(Vote.NO_VOTE_VALUE, Vote.NO_VOTE_VALUE),
        ],
      ),
    );
  }


  /// CONSENSUS 5
  Widget get _consensus5 {
    return FittedBox(
      fit: BoxFit.contain,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          // iw.circleSpacer,
          _consensusChart(0, consensu5Color(0)),
          _consensusChart(1, consensu5Color(1)),
          _consensusChart(2, consensu5Color(2)),
          _consensusChart(3, consensu5Color(3)),
          _consensusChart(4, consensu5Color(4)),
          _consensusChart(Vote.NO_VOTE_VALUE, Vote.NO_VOTE_VALUE),
        ],
      ),
    );
  }

  /// CONSENSUS 10 LONG
  Widget get _consensus10Long {
    return FittedBox(
      fit: BoxFit.contain,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          // iw.circleSpacer,
          _consensusChart(0, 0),
          _consensusChart(1, 1),
          _consensusChart(2, 2),
          _consensusChart(3, 3),
          _consensusChart(4, 4),
          _consensusChart(5, 5),
          _consensusChart(6, 6),
          _consensusChart(7, 7),
          _consensusChart(8, 8),
          _consensusChart(9, 9),
          _consensusChart(10, 10),
          _consensusChart(Vote.NO_VOTE_VALUE, Vote.NO_VOTE_VALUE),
        ],
      ),
    );
  }



  Widget _squareButtonWithSelected(int value, Color color, String namedValue) {
    int votes = voting.votingResult.valueByVote(option.id, value);

    return Padding(
      padding: const EdgeInsets.only(right: 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            "$votes",
            style: TextStyle(
                fontSize: VotingIconWidget.FONT_SIZE_SMALL,
                fontWeight: FontWeight.bold
            ),
          ),
          Container(
            height: 100*votes/(highestCount+0.0001)*0.7,
            color: color,
            width: VotingIconWidget.ICON_SIZE_SMALL * 0.8,
            // child: Text("$value")
          ),
          // Text("$value"),
          iw.squareButtonWidget(
              value,
              null,
              selected: true,
              namedValue: namedValue
          )
        ],
      ),
    );
  }

  Widget _consensusChart(int value, int colorValue) {
    int votes = voting.votingResult.valueByVote(option.id, value);
    return Padding(
      padding: const EdgeInsets.only(right: 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
              "$votes",
              style: TextStyle(
                fontSize: VotingIconWidget.FONT_SIZE_SMALL,
                fontWeight: FontWeight.bold
              ),
          ),
          Container(
              height: 100*votes/(highestCount+0.0001)*0.7,
              color: iconsColors(colorValue),
              width: VotingIconWidget.ICON_SIZE_SMALL * 0.8,
              // child: Text("$value")
          ),
          // Text("$value"),
          iw.circleButtonWidget(
            value,
            colorValue,
            null,
            selected: true,
            // vsync: useSingleTickerProvider()
          )
        ],
      ),
    );

  }

}