// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/objects/voting_type.dart';

Color iconsColors(int? value) {
  if (value == null) {
    return Colors.white;
  }
  
  switch (value) {
    case 0: return Color.fromRGBO(0, 255, 0, 1);
    case 1: return Color.fromRGBO(51, 255, 0, 1);
    case 2: return Color.fromRGBO(102, 255, 0, 1);
    case 3: return Color.fromRGBO(153, 255, 0, 1);
    case 4: return Color.fromRGBO(204, 255, 0, 1);
    case 5: return Color.fromRGBO(255, 255, 0, 1);
    case 6: return Color.fromRGBO(255, 204, 0, 1);
    case 7: return Color.fromRGBO(255, 153, 0, 1);
    case 8: return Color.fromRGBO(255, 102, 0, 1);
    case 9: return Color.fromRGBO(255, 51, 0, 1);
    case 10: return Color.fromRGBO(255, 0, 0, 1);
    default: return Colors.grey;
  }

}

Color consensusResultColor(double value) {
  return iconsColors(value.round());
}

int? consensuColor(String type, int? value) {
  if (type == VotingType.CONSENSUS_3) {
    return consensu3Color(value);
  }
  else if (type == VotingType.CONSENSUS_5) {
    return consensu5Color(value);
  }
  else {
    return value;
  }
}

Color yesNoColor(int? value) {
  switch (value) {
    case 1: return Colors.green;
    case 0: return Colors.red;
    default: return iconsColors(Vote.NO_VOTE_VALUE);
  }
}

int consensu3Color(int? value) {
  switch (value) {
    case 0: return 0;
    case 1: return 5;
    case 2: return 10;
    default: return Vote.NO_VOTE_VALUE;
  }
}

int consensu5Color(int? value) {
  switch (value) {
    case 0: return 0;
    case 1: return 2;
    case 2: return 4;
    case 3: return 7;
    case 4: return 10;
    default: return Vote.NO_VOTE_VALUE;
  }
}




