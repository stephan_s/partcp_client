// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/widgets/markdown_view.dart';

Widget infoWidget({required String title, required String shortDescription, required String description}) {
  List<Widget> _subtitleList = [];
  if (shortDescription.trim().isNotEmpty) {
    _subtitleList.add(Padding(
      padding: const EdgeInsets.only(top: 6),
      child: Text(shortDescription.trim()),
    ));
  }
  if (description.trim().isNotEmpty) {
    Widget _w = Align(
      alignment: Alignment.centerRight,
      child: TextButton(
        child: Text("Details"),
        onPressed: () => Get.to(() => MarkdownView(
              title: title,
              markdownText: description,
            )),
      ),
    );
    _subtitleList.add(_w);
  }

  Widget? _subtitle = _subtitleList.isNotEmpty
      ? Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _subtitleList,
        )
      : null;

  return Card(
    child: Padding(
      padding: const EdgeInsets.only(top: 16, bottom: 16),
      child: ListTile(
        title: Text(title),
        subtitle: _subtitle,
      ),
    ),
  );
}


