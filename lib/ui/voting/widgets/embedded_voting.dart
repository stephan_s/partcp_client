// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_type.dart';
import 'package:partcp_client/utils/log.dart';

import '../vote_popup_page.dart';
import '../voting_controller.dart';
import 'icons_colors.dart';
import 'voting_icon_widget.dart';

const _TAG = "EmbeddedVoting";

class EmbeddedVoting {
  final Voting voting;
  final VotingOption option;
  final VotingCallback callback;
  final VotingController controller;

  VotingIconWidget get iw => VotingIconWidget.small(voting.type!);

  const EmbeddedVoting({
    required this.voting,
    required this.option,
    required this.callback,
    required this.controller,
  });

  Widget? createItems() {
    return _voting;
  }
  

  Widget? get _voting {
    /// CONSENSUS 3, 5, 10 (has space for long button list)
    Log.d(_TAG, "_voting; controller.hasSpaceForLongButtonsList: ${controller.hasSpaceForLongButtonsList}");

    if (controller.voting.type == VotingType.CONSENSUS_3 ||
        controller.voting.type == VotingType.CONSENSUS_5 ||
        (controller.voting.type == VotingType.CONSENSUS_10 &&
            controller.hasSpaceForLongButtonsList)) {

      return _consensus;
    }

    /// 10 long (less space)
    else if(controller.voting.type == VotingType.CONSENSUS_10) {
      return _consensus10Short;
    }

    /// YES-NO
    else if(controller.voting.type == VotingType.YES_NO_RATING) {
      return _yesNo;
    }
    else {
      return null;
    }
  }


  /// CONSENSUS
  Widget get _consensus {
    switch (controller.voting.type) {
      case VotingType.CONSENSUS_3:
        return _consensus3;

      case VotingType.CONSENSUS_5:
        return _consensus5;

      default:
        return _consensus10Long;
    }
  }


  /// CONSENSUS 10 SHORT
  Widget? get _consensus10Short {
    return null;
  }

  Widget get _yesNo {
    return FittedBox(
      fit: BoxFit.contain,
      child: ButtonBar(
        alignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          _squareButtonWithSelected(1, "Ja"),
          _squareButtonWithSelected(0, "NEIN"),
          _squareButtonWithSelected(Vote.NO_VOTE_VALUE, "ENTHALTUNG"),
        ],
      ),
    );
  }


  Widget get _consensus3 {
    return FittedBox(
      fit: BoxFit.contain,
      child: ButtonBar(
        alignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        // buttonPadding: EdgeInsets.all(3),
        // overflowButtonSpacing: 13,
        children: [
          // iw.circleSpacer,
          _circleButtonWithSelected(0, consensu3Color(0)),
          _circleButtonWithSelected(1, consensu3Color(1)),
          _circleButtonWithSelected(2, consensu3Color(2)),
          _circleButtonWithSelected(Vote.NO_VOTE_VALUE, Vote.NO_VOTE_VALUE),
        ],
      ),
    );
  }


  /// CONSENSUS 5
  Widget get _consensus5 {
    return FittedBox(
      fit: BoxFit.contain,
      child: ButtonBar(
        alignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          // iw.circleSpacer,
          _circleButtonWithSelected(0, consensu5Color(0)),
          _circleButtonWithSelected(1, consensu5Color(1)),
          _circleButtonWithSelected(2, consensu5Color(2)),
          _circleButtonWithSelected(3, consensu5Color(3)),
          _circleButtonWithSelected(4, consensu5Color(4)),
          _circleButtonWithSelected(Vote.NO_VOTE_VALUE, Vote.NO_VOTE_VALUE),
        ],
      ),
    );
  }

  /// CONSENSUS 10 LONG
  Widget get _consensus10Long {
    return FittedBox(
      fit: BoxFit.contain,
      child: ButtonBar(
        alignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          // iw.circleSpacer,
          _circleButtonWithSelected(0, 0),
          _circleButtonWithSelected(1, 1),
          _circleButtonWithSelected(2, 2),
          _circleButtonWithSelected(3, 3),
          _circleButtonWithSelected(4, 4),
          _circleButtonWithSelected(5, 5),
          _circleButtonWithSelected(6, 6),
          _circleButtonWithSelected(7, 7),
          _circleButtonWithSelected(8, 8),
          _circleButtonWithSelected(9, 9),
          _circleButtonWithSelected(10, 10),
          _circleButtonWithSelected(Vote.NO_VOTE_VALUE, Vote.NO_VOTE_VALUE),
        ],
      ),
    );
  }



  // Widget _circleButton(int value, int colorValue, {dynamic sCallback()}) {
  //   return iw.circleButtonWidget(
  //       value,
  //       colorValue,
  //       sCallback != null
  //           ? () => sCallback()
  //           : () => callback(option.id, value),
  //       // selected: value == controller.getVoteValue(optionsId.toString()),
  //       // vsync: useSingleTickerProvider()
  //   );
  // }

  Widget _squareButtonWithSelected(int value, String namedValue) {
    return iw.squareButtonWidget(
      value,
      () => callback(option.id, value),
      selected: value == controller.getVoteValue(option.id.toString()),
      namedValue: namedValue
      // vsync: useSingleTickerProvider()
    );
  }

  Widget _circleButtonWithSelected(int value, int colorValue) {
    return iw.circleButtonWidget(
        value,
        colorValue,
        () => callback(option.id, value),
        selected: value == controller.getVoteValue(option.id.toString()),
        // vsync: useSingleTickerProvider()
    );
  }

}