// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/utils/log.dart';

import 'icons_colors.dart';

const ICON_COLOR = "ICON_COLOR";
const BOX_COLOR = "BOX_COLOR";
const BOX_ICON = "BOX_ICON";

class VotingIconWidget{
  final double iconSize;
  final double fontSize;
  final double buttonElevation;
  final String votingType;

  static const _TAG = "VotingIconWidget";
  static const double increaseSizeFactor = 0.0; //0.4;

  static const double ICON_SIZE_SMALL = 30;
  static const double ICON_SIZE_MIDDLE = 50;
  static const double ICON_SIZE_LARGE = 60;

  static const double FONT_SIZE_SMALL = 16;
  static const double FONT_SIZE_MIDDLE = 18;
  static const double FONT_SIZE_LARGE = 24;

  int? _value;
  VoidCallback? _callback;

  int? _colorValue;
  late Widget _boxIcon;
  late Color _boxColor;
  late Color _iconTextColor;

  late String valueString;

  /// spacer
  // Widget get circleSpacer => _spacerCircleButton();

  /// CONSENSUS
  Widget circleButtonWidget(int? value, int? colorValue, VoidCallback? callback,
      {String? namedValue, bool? selected}) {
    _value = value;
    _colorValue = colorValue;
    this._callback = callback;

    _boxColor = iconsColors(_colorValue);
    _iconTextColor = Colors.black;
    valueString = _valueString(namedValue: namedValue);

    return selected != null
      ? _circleButton(increaseSize: selected, decreaseTransparency: !selected)
      : _circleButton();
  }


  Widget squareButtonWidget(int? value, VoidCallback? callback,
      {String? namedValue, bool? selected, bool imageOnly = false}) {
    _value = value;
    this._callback = callback;
    valueString = _valueString(namedValue: namedValue);
    _squareButtonValues(value);

    return selected != null
        ? _squareButton(increaseSize: selected,
              decreaseTransparency: !selected,
              imageOnly: imageOnly)
        : _squareButton(imageOnly: imageOnly);
  }

  String _valueString({String? namedValue}) {
    if (namedValue != null) return namedValue;

    if (_value == null) {
      return "?";
    } else if (_value == Vote.NO_VOTE_VALUE) {
      return "/";
    } else {
      return _value.toString();
    }
  }

  VotingIconWidget({
      required this.iconSize,
      required this.fontSize,
      required this.buttonElevation,
      required this.votingType
  });

  static VotingIconWidget small(String votingType) {
    return VotingIconWidget(
        iconSize: ICON_SIZE_SMALL, fontSize: FONT_SIZE_SMALL, buttonElevation: 4, votingType: votingType);
  }

  static VotingIconWidget middle(String votingType) {
    return VotingIconWidget(
        iconSize: ICON_SIZE_MIDDLE, fontSize: FONT_SIZE_MIDDLE, buttonElevation: 6, votingType: votingType);
  }

  static VotingIconWidget middleResult(String votingType) {
    return VotingIconWidget(
        //iconSize: ICON_SIZE_MIDDLE, fontSize: FONT_SIZE_MIDDLE*0.8, buttonElevation: 4, votingType: votingType);
        iconSize: ICON_SIZE_LARGE, fontSize: FONT_SIZE_MIDDLE*0.8, buttonElevation: 4, votingType: votingType);
  }

  static VotingIconWidget large(String votingType) {
    return VotingIconWidget(
        iconSize: ICON_SIZE_LARGE, fontSize: FONT_SIZE_LARGE, buttonElevation: 6, votingType: votingType);
  }

  /// CONSENSUS BUTTON (0-10)
  Widget _circleButton({bool? increaseSize, bool? decreaseTransparency}) {

    double _iconSize = increaseSize != null && increaseSize
      ? iconSize + iconSize * increaseSizeFactor
      : iconSize;

    double _fontSize = increaseSize != null && increaseSize
        ? fontSize + fontSize * increaseSizeFactor
        : fontSize;

    _boxColor = decreaseTransparency != null && decreaseTransparency
      ? _boxColor.withOpacity(0.5)
      : _boxColor;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        constraints: BoxConstraints(
            minHeight: 30,
            maxHeight: _iconSize,
            minWidth: 30,
            maxWidth: _iconSize),
        child: ElevatedButton(
          child: Text(
            valueString,
            style: TextStyle(
                color: _iconTextColor,
                fontSize: _fontSize,
                fontWeight: FontWeight.bold),
          ),
          onPressed: _callback != null
            ? _callback
            : () => {},
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            primary: _boxColor,
            elevation: buttonElevation,
            minimumSize: Size(_iconSize, _iconSize),
            padding: const EdgeInsets.all(2.0),
          ),
        ),
      ),
    );
  }

  /// YES-NO BUTTON
  Widget _squareButton ({bool? increaseSize, bool? decreaseTransparency, required bool imageOnly}) {

    // double _iconSize = increaseSize != null && increaseSize
    //     ? iconSize + iconSize * increaseSizeFactor
    //     : iconSize;

    // double _fontSize = increaseSize != null && increaseSize
    //     ? fontSize + fontSize * increaseSizeFactor
    //     : fontSize;
    double _fontSize = fontSize;

    _boxColor = decreaseTransparency != null && decreaseTransparency
        ? _boxColor.withOpacity(0.5)
        : _boxColor;

    Log.d(_TAG, "_squareButton; valueString: $valueString");

    Widget _button;
    if (imageOnly) {
      _button = ElevatedButton(
        child: _boxIcon,
        onPressed: _callback != null
          ? _callback
          : () => {},
        style: ElevatedButton.styleFrom(
            minimumSize: Size(50, 50),
            primary: _boxColor,
            elevation: buttonElevation),
      );
    }
    else {
      _button = ElevatedButton.icon(
        icon: _boxIcon,
        label: Text(
          valueString,
          style: TextStyle(
              color: _iconTextColor,
              fontSize: _fontSize,
              fontWeight: FontWeight.bold),
        ),
        onPressed: _callback,
        style: ElevatedButton.styleFrom(
            minimumSize: Size(50, 50),
            primary: _boxColor,
            elevation: buttonElevation),
      );
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        // constraints: BoxConstraints(minHeight: 10, maxHeight: 60, minWidth: 10, maxWidth: 60),
        child: _button,
      ),
    );
  }

  void _squareButtonValues(int? value) {
    switch (value) {

      /// yes
      case 1:
        _boxIcon = Icon(Icons.thumb_up_alt_outlined, color: Colors.white);
        _boxColor = yesNoColor(1);
        _iconTextColor = Colors.white;
        break;

      /// no
      case 0:
        _boxIcon = Icon(Icons.thumb_down_alt_outlined, color: Colors.white);
        _boxColor = yesNoColor(0);
        _iconTextColor = Colors.white;
        break;

      /// abstention
      case Vote.NO_VOTE_VALUE:
        _boxIcon = Text(
          "/",
          textScaleFactor: 2,
          style: TextStyle(color: Colors.white),
        );
        _boxColor = Colors.grey;
        _iconTextColor = Colors.white;
        break;

      /// null
      default:
        _boxIcon = Text(
          "?",
          textScaleFactor: 1.7,
          style: TextStyle(color: Colors.black),
        );
        _boxColor = Colors.white;
        _iconTextColor = Colors.black;
        break;
    }
  }

  Widget _spacerCircleButton () {
    double _iconSize = iconSize + iconSize * increaseSizeFactor;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Container(
        constraints: BoxConstraints(
            minHeight: 30,
            maxHeight: _iconSize,
            minWidth: 1,
            maxWidth: 1),
      ),
    );

  }

}
