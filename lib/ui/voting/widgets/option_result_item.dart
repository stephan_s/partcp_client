// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_type.dart';
import 'package:partcp_client/ui/voting/voting_result_page.dart';
import 'package:partcp_client/utils/launch_url.dart';
import 'package:partcp_client/utils/s_utils.dart';

import 'embedded_result.dart';
import 'icons_colors.dart';
import 'voting_icon_widget.dart';

class OptionResultItem {
  final VotingResultController controller;
  final VotingOption option;

  const OptionResultItem({required this.controller, required this.option});

  // void _launchURL(String url) async => await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';

  ///
  bool get _hasEnoughSpace => Get.width > Get.height || GetPlatform.isWeb;

  Widget optionItem() {
    Color tileColor = const Color.fromARGB(255, 242, 242, 242);
    return Card(
      color: tileColor,
      margin: EdgeInsets.fromLTRB(0, 12, 0, 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          /// Option Title + clientData
          ListTile(
            // tileColor: tileColor,
            title: Text(
              option.name,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: _mainSubtitle,
            contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 30.0),
          ),

          Divider(),

          /// voting icons
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: Container(
              constraints: BoxConstraints(minWidth: 100, maxWidth: double.infinity),
              // color: Colors.orange,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 15, bottom: 15, right: 20),
                      child: Text(_votingTileTitle,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black87
                          )),
                    ),
                  ),
                  // Container(width: 80, height: 80, child: _votingResultWidget),
                  Container(child: _votingResultWidget),
                ],
              ),
            ),
          ),

          ListTile(
            tileColor: const Color.fromARGB(0, 255, 255, 255),
            contentPadding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 30.0),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Stimmen: ${controller.voting.votingResult.votingsCount(option.id)}"),
                Text("Enthaltungen: ${controller.voting.votingResult.valueByVote(option.id, Vote.NO_VOTE_VALUE)}"),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 30.0),
            child: Center(child: _embeddedResults),
          )
        ],
      ),
    );
  }

  ///////////////////////////////////////////////////////////////////////////
  /// MAIN TILE
  ///
  Widget? get _mainSubtitle {
    List<Widget> _subtitleItems = [];
    Widget w;

    /// subtitle short description
    if (option.clientData.shortDescription.isNotEmpty) {
      w = Padding(
        padding: const EdgeInsets.only(top: 6),
        child: Text(option.clientData.shortDescription.trim()),
      );
      _subtitleItems.add(w);
    }

    /// subtitle link
    if (option.clientData.linkUrl.isNotEmpty) {
      w = Linkify(
        onOpen: (element) {
          print("Clicked $element!");
          launchURL(element.url);
        },
        text: "\nMehr Infos: ${option.clientData.linkUrl}",
      );
      _subtitleItems.add(w);
    }

    return _subtitleItems.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.only(left: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _subtitleItems,
            ),
          )
        : null;
  }

  ///////////////////////////////////////////////////////////////////////////
  /// VOTING TILE
  ///
  String get _votingTileTitle {
    if (VotingType.isConsensus(controller.voting.type!)) {
      return "Widerstand-Punkte";
    }
    if (controller.voting.type == VotingType.YES_NO_RATING) {
      return "Zustimmung";
    }
    if (controller.voting.type == VotingType.SINGLE_CHOICE) {
      return "Stimmen";
    }
    return "Auswahl";
  }

  Widget? get _embeddedResults {
    return EmbeddedResult(
            voting: controller.voting,
            option: option,
            highestCount: controller.voting.votingResult.highestConsensusCount(option.id))
        .createItems();
  }

  ///////////////////////////////////////////////////////////////////////////
  /// VOTING RESULT WIDGET
  ///
  Widget? get _votingResultWidget {
    VotingIconWidget iw = VotingIconWidget.middleResult(controller.voting.type!);

    /// CONSENSUS
    if (VotingType.isConsensus(controller.voting.type!)) {
      double value = controller.voting.votingResult.resistance(option.id);

      return iw.circleButtonWidget(value.round(), consensuColor(controller.voting.type!, value.round()), null,
          namedValue: "${SUtils.formatDouble(value)}", selected: true);

      // return Text(
      //   "${SUtils.formatDouble(value)}",
      //   style: TextStyle(
      //     fontWeight: FontWeight.bold,
      //     fontSize: 26,
      //     color: consensusResultColor(value)
      //   ),
      // );
    }

    /// YES-NO
    else if (controller.voting.type == VotingType.YES_NO_RATING) {
      int yes = controller.voting.votingResult.valueByVote(option.id, 1);
      int no = controller.voting.votingResult.valueByVote(option.id, 0);
      return iw.squareButtonWidget(yes > no ? 1 : 0, null, imageOnly: true);
    }

    /// SINGLE CHOICE & MULTIPLE CHOICE
    else if (controller.voting.type == VotingType.SINGLE_CHOICE || VotingType.isMultipleChoice(controller.voting.type!)) {
      int highestCount = controller.voting.votingResult.highestOptionCount();
      int thisCount = controller.voting.votingResult.votingsCount(option.id);
      return Text(
        "${thisCount}",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26, color: thisCount == highestCount ? yesNoColor(1) : null),
      );
    } else {
      return null;
    }
  }
}

class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
