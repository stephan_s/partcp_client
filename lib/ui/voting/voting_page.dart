// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/button_main.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/submit_button.dart';
import 'package:partcp_client/widgets/widgets.dart';

import 'voting_controller.dart';
import 'widgets/info_widget.dart';
import 'widgets/option_item.dart';

class VotingPage extends StatelessWidget {
  final VotingServer server;
  Account? account;
  final Voting voting;
  final VotingEvent votingEvent;
  final bool previewOnly;

  VotingPage({
    required this.server,
    required this.account,
    required this.voting,
    required this.votingEvent,
    this.previewOnly = false,
  });

  final VotingController controller = Get.put(VotingController());

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => controller.onWillPop(),
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<VotingController>(
            initState: (_) => controller.initController(server, account, voting, votingEvent, previewOnly),
            builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: Text("${controller.previewString}${voting.title}"),
                  ),
                  body: Widgets.loaderBox(
                    loading: controller.serverResponse.receivingData,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          /// loadingItems / Voting List
                          Expanded(child: ScrollBody(children: _optionsList)),

                          /// SERVER ERROR && VOTE BUTTON
                          Card(
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                                  /// ERROR MESSAGE
                                  ServerErrorMessage(serverResponse: controller.serverResponse),

                                  /// VOTE BUTTON
                                  controller.account != null
                                      ? SubmitButton(
                                          submitting: controller.serverResponse.receivingData,
                                          text: "Abstimmen (${controller.votes.length} von ${controller.requestedVotesCount})",
                                          enabled: true,
                                          onPressed: controller.allowSubmit
                                              ? () => controller.sendVotes()
                                              : null,
                                          buttonStyle: ButtonMain.styleVoting,
                                        )
                                      : Container(),
                                ]),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )),
      ),
    );
  }

  /// OPTIONS LIST
  List<Widget> get _optionsList {
    List<Widget> _items = [
      infoWidget(
          title: controller.voting.title,
          shortDescription: controller.voting.clientData.shortDescription,
          description: controller.voting.clientData.description)
    ];
    for (VotingOption option in controller.voting.votingOptions) {
      _items.add(OptionItem(controller: controller, option: option).optionItem());
    }
    return _items;
  }
}
