// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/message_type/message_type.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/objects/vote.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/voting_type.dart';
import 'package:partcp_client/utils/log.dart';

import 'vote_popup_page.dart';

const _TAG = "VotingController";
class
VotingController extends GetxController {

  static VotingController to = Get.find<VotingController>();

  MainController mainController = MainController.to;

  ServerResponse serverResponse = ServerResponse();

  late VotingServer server;
  Account? account;
  late Voting voting;
  late VotingEvent votingEvent;
  late bool previewOnly;
  bool dataChanged = false;

  /// for AppBar, as Preview info
  String previewString = "";

  /// For RadioButton on SINGLE-CHOICE
  String _radioGroupValue = "";
  
  List<Vote> get votes => voting.votes;

  bool get allowSubmit => votes.length >= 1;

  int get requestedVotesCount {
    int count = 0;

    /// CONSENSUS
    if (VotingType.isConsensus(voting.type!)) {
      count = voting.votingOptions.length;
    }
    /// YES-NO
    if (voting.type == VotingType.YES_NO_RATING) {
      count = voting.votingOptions.length;
    }
    /// SINGLE CHOICE
    if (voting.type == VotingType.SINGLE_CHOICE) {
      count = 1;
    }
    /// MULTIPLE CHOICE
    else if (VotingType.isMultipleChoice(voting.type!)) {
      int _count = VotingType.getMultipleChoiceCountFromType(voting.type!);
      count = _count > 900
        ? voting.votingOptions.length
        : _count;
    }
    return count;
  }

  VotingOption listItem(int index) => voting.votingOptions[index];

  bool get hasSpaceForLongButtonsList => Get.width > Get.height || MainController.to.isDesktopOrWeb;

  void initController(VotingServer server, Account? account,
      Voting voting, VotingEvent votingEvent, bool previewOnly) {
    this.server = server;
    this.account = account;
    this.voting = voting;
    this.votingEvent = votingEvent;
    this.previewOnly = previewOnly;
    dataChanged = false;

    if (previewOnly) {
      previewString = "[Vorschau] ";
    }
    _initDetailsRequest();
  }


  /// consensus, yes-no
  void setVotingValued(String id, int vote) {
    voting.setVoteValued(id, vote);
    dataChanged = true;
    Log.d(_TAG, "setVotingValued; id:$id; vote:$vote");
    updateMe();
  }

  /// multiple-choice
  bool setVotingMultiple(String id, bool vote) {
    Log.d(_TAG, "setVotingMultiple; id:$id; vote:$vote");
    updateMe();
    dataChanged = true;
    return voting.setVoteMulti(id, vote);
  }
  
  /// single-choice
  void setVotingSingleChoice(String id) {
    voting.setVoteSingle(id);
    dataChanged = true;
    _radioGroupValue = id;
    updateMe();
  }
  String get votingSingleChoiceGroup => _radioGroupValue;

  Vote? getVote(String id) {
    return voting.vote(id);
  }

  int? getVoteValue(String id) {
    if (getVote(id) != null) {
      return getVote(id)!.value;
    }
    else {
      return null;
    }
    // return getVote(id) != null
    //   ? getVote(id)!.value
    //   : null;
  }

  bool isVoted(String id) {
    Log.d(_TAG, "isVoted; id:$id; return -> ${voting.vote(id) != null}");
    return voting.vote(id) != null;
  }

  Future<dynamic> goToVotePopupPage(VotingOption option) {
    return Get.to(
        VotePopupPage(
          voting: voting,
          optionsId: option.id,
          name: option.name,
          callback: (String id, int value) {
            setVotingValued(id, value);
          },
        ),
        duration: Duration(milliseconds: 200),
        transition: Transition.fadeIn,
        opaque: false)!;
  }

  //////////////////////////////////////////////
  /// SEND VOTES
  Future<void> previewOnlyMessage(BuildContext context) async {
    await showOkAlertDialog(
      context: context,
      message: 'In der Abstimmungs-Vorschau werden keine Daten an der Server gesendet',
      okLabel: 'ok',
      barrierDismissible: true,
    );
  }

  Future<bool> askSendVotes(BuildContext context) async {
    final result = await showOkCancelAlertDialog(
      context: context,
      message: 'Du hast nicht alle Abstimmungen bewertet.\n\n'
          'Möchtest Du die Anstimmung dennoch abschicken?',
      okLabel: 'Ja',
      cancelLabel: "Nein",
      barrierDismissible: false,
    );
    return result == OkCancelResult.ok;
  }

  Future<bool> onWillPop() async {
    // if (votes.length >= 1 && (account != null && ! account!.votedList.contains(voting.id))) {
    if (dataChanged && !previewOnly && account != null ) {

      final OkCancelResult result = await showOkCancelAlertDialog(
        context: Get.context!,
        title: "Stimmzettel noch nicht gesendet",
        message: 'Möchtest Du den Stimmzettel jetzt absenden?\n '
            'Wenn Du dies nicht tust, wird Deine Stimme nicht mitgezählt.\n '
            'Du kannst den Stimmzettel auch nach dem Absenden noch ändern und erneut absenden, falls Du Deine Meinung ändertst.',
        okLabel: 'Absenden',
        cancelLabel: "Nicht absenden",
        barrierDismissible: true,
      );

      Log.d(_TAG, "onWillPop; result:$result");
      if (result == OkCancelResult.ok) {
        await sendVotes();
      }
      else {
        return true;
      }
    }
    return true;
  }

  Future<void> sendVotes() async {
    bool doNext = true;

    /// ask for send votes
    if (votes.length != requestedVotesCount && !VotingType.isMultipleChoice(voting.type!)) {
      doNext = await askSendVotes(Get.context!);
    }
    if (!doNext) return;

    /// if previewOnly
    if (previewOnly) {
      await previewOnlyMessage(Get.context!);
      return;
    }

    serverResponse.receivingData = true;
    updateMe();

    final Map<String, dynamic> messageMap = ballot(
      event: votingEvent,
      voting: voting
    );

    serverResponse = await HttpController().serverRequestSigned(account!, messageMap, server);

    if (serverResponse.statusCode != 200) {
      updateMe();
    }
    else {
      Log.d(_TAG, "------- sendVotes: YES -------");
      account!.saveVoted(voting);

      /// save wallet in shared prefs
      await mainController.onWalletChanged(updateNotification: false);
      Get.back();
    }
  }

  void _initDetailsRequest() async {
    if (requestedVotesCount == 0) {
      await _getDetailsFromServer();
      updateMe();
    }
  }


  Future<void> _getDetailsFromServer() async {
    updateMe();

    final Map<String, dynamic> messageMap = votingDetailsRequest(event: votingEvent, voting: voting);
    serverResponse = await HttpController().serverRequestSigned(account!, messageMap, server);

    if (serverResponse.statusCode == 200) {
      List<VotingOption> tempList = await _fillDetailsTempList(serverResponse.bodyMap!);
      voting.votingOptions.clear();
      voting.votingOptions.addAll(tempList);
    }

    updateMe();
  }


  /// loop over votings
  Future<List<VotingOption>> _fillDetailsTempList(Map<String, dynamic> map) async {
    Map<String, dynamic> bodyMap = map[HTTP_BODY_MAP];

    /// Voting options
    Map<String, dynamic> votingDataMap = bodyMap[Voting.VOTING_DATA];

    /// voting options
    // voting.fillFromDetailsMap(votingDataMap);

    /// option list
    List<dynamic> optionsList = votingDataMap[VotingOption.OPTIONS];

    List<VotingOption> _tempList = [];

    Log.d(_TAG, "options: ${optionsList.length}");

    for (var i = 0; i< optionsList.length; i++) {
      VotingOption options = VotingOption.fromMap(optionsList[i]);
      _tempList.add(options);
    }

    return _tempList;
  }

  void updateMe() {
    update();
  }

  @override
  void onClose() {
    Log.d(_TAG, "onClose()");
  }
}