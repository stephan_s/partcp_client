import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/ui/home/index_controller.dart';
import 'package:partcp_client/ui/home/index_page.dart';
import 'package:partcp_client/ui/server_user_manager/key_manager_page.dart';
import 'package:partcp_client/utils/field_width.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/text_field_widget.dart';
import 'package:partcp_client/widgets/widgets.dart';

import 'wirgets.dart';

/// Routing class to decide which

const _TAG = "ServerUserManagerController";

class ServerUserManagerController extends GetxController {
  MainController mainController = MainController.to;
  ServerResponse serverResponse = ServerResponse();

  final TextEditingController namingTextController = TextEditingController();
  final TextEditingController credentialTextController = TextEditingController();

  bool get allowManualRegister => namingTextController.text.length > 3 && credentialTextController.text.length > 3;
  bool identityCreated = false;

  void initController() {
    Log.d(_TAG, "initController");
    namingTextController.text = "";
    credentialTextController.text = "";
  }

  ////////////////////////////////////////////////////////
  /// SEND KEY SECTION
  ///

  Future<void> onManualRegistration() async {
    Log.d(_TAG, "onManualSubmit()");
    serverResponse.receivingData = true;
    updateMe();

    MainController mainController = MainController.to;
    Account account = mainController.wallet!.walletAccount!;
    account
      ..credential = credentialTextController.text
      ..userId = namingTextController.text;

    serverResponse = await account.submitIdToServer(mainController.votingServer!);

    if (serverResponse.statusCode == 200) {
      await mainController.onWalletChanged(updateNotification: true);
      mainController.wallet!.walletAccount!.pubKeysOnServerValid = true;
      identityCreated = true;
      IndexController.to.updateMe();
    }
    updateMe();
  }

  void onGoToServerUserManagerPage() {
    Get.to(() => KeyManagerPage());
  }

  void onInputChanged() {
    updateMe();
  }

  Future<bool> onWillPop() async {
    return true;
  }

  void goToIndexPage() {
    IndexController.to.updateMe();
    Get.offAll(() => IndexPage());
  }

  void updateMe() {
    update();
  }
}

class ServerUserManagerPage extends StatelessWidget {
  final ServerUserManagerController controller = Get.put(ServerUserManagerController());

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.initController();
    });

    return WillPopScope(
      onWillPop: () => controller.onWillPop(),
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<ServerUserManagerController>(
            builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: Text("Registrierung"),
                  ),
                  body: Widgets.loaderBox(loading: controller.serverResponse.receivingData, child: ScrollBody(children: _items)),
                )),
      ),
    );
  }

  List<Widget> get _items {
    List<Widget> items = [];
    if (controller.identityCreated) {
      /// identity  created Widget
      items.add(identityCreatedWidget(server: controller.mainController.votingServer!, onFinishGoto: controller.goToIndexPage));
    } else {
      items.add(ServerErrorMessage(serverResponse: controller.serverResponse));
      if (controller.mainController.votingServer!.keyManagerUrl != null) {
        items.add(_keyManager);
      }
      items.add(_manualRegister);
    }
    return items;
  }

  Widget get _keyManager {
    return CardWidget(
      // title: "Verifizierung Deiner Zugangsdaten",
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// server name
          Text(controller.mainController.votingServer!.name, style: TextStyle(fontSize: 20)),

          /// server url
          Text(controller.mainController.votingServer!.url,
              style: TextStyle(
                  // fontSize: 14
                  )),
          Padding(padding: const EdgeInsets.all(10.0)),
          Text(
              'In Deinem aktuellen Schlüsselbund ist keine Teilnehmerkennung für den Server ${controller.mainController.votingServer!.name} vorhanden. '
              'Um eine solche zu erstellen, mußt Du zunächst die Identitätsprüfung durchlaufen, die der Server bereitstellt. Solltest Du bereits auf einem anderen Gerät eine Teilnehmerkennung für '
              'diesen Server erstellt haben, empfiehlt es sich, diesen Vorgang abzubrechen und das Schlüsselbund von dem anderen Gerät auf dieses zu übertragen.'
              '\n\nDer Server "${controller.mainController.votingServer!.name}" stellt einen automatisierten Verifizierungsprozess bereit.'),
          Center(
              child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextButton(
              onPressed: () => controller.onGoToServerUserManagerPage(),
              child: Text("Identitätsprüfung starten"),
            ),
          ))
        ],
      ),
    );
  }

  Widget get _manualRegister {
    return CardWidget(
      // title: "Verifizierung Deiner Zugangsdaten",
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Wenn Du bereits verifiziert wurdest, kannst Du hier die Daten eintragen:"),
          Padding(padding: const EdgeInsets.all(20.0)),

          /// Teilnehmerkennung
          Center(
            child: TextFieldWidget(
              textController: controller.namingTextController,
              labelText: "Teilnehmerkennung",
              hint: "",
              onTap: () => ({}),
              onSubmitted: (_) => ({}),
              onChanged: (_) => controller.onInputChanged(),
              autoCorrect: false,
              isPasswordField: false,
              keyboardType: TextInputType.text,
              textAlign: TextAlign.start,
              maxBoxLength: fieldBoxMaxWidth,
              // obscuringCharacter: "",
              enabled: true,
            ),
          ),

          Padding(padding: const EdgeInsets.all(20.0)),

          /// Verifizierungscode
          Center(
            child: TextFieldWidget(
              textController: controller.credentialTextController,
              labelText: "Verifizierungscode",
              hint: "",
              onTap: () => ({}),
              onSubmitted: (_) => ({}),
              onChanged: (_) => controller.onInputChanged(),
              autoCorrect: false,
              isPasswordField: false,
              keyboardType: TextInputType.text,
              textAlign: TextAlign.start,
              maxBoxLength: fieldBoxMaxWidth,
              // obscuringCharacter: "",
              enabled: true,
            ),
          ),

          Padding(padding: const EdgeInsets.all(20.0)),

          Center(
              child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextButton(
              onPressed: controller.allowManualRegister ? () => controller.onManualRegistration() : null,
              child: Text("weiter"),
            ),
          ))
        ],
      ),
    );
  }
}
