

import 'package:flutter/material.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/widgets/card_widget.dart';

Widget identityCreatedWidget({required VotingServer server, required dynamic onFinishGoto}) {
  return CardWidget(
    title: "Identität erstellt",
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Deine Identität wurde erfolgreich überprüft, und es wurden neue Schlüssel für den Zugriff auf ${server.name} erstellt. "
            "Du solltest Dein Schlüsselbund jetzt exportieren, um die erzeugten Schlüssel zu sichern. Falls Du weitere Geräte im Einsatz hast, "
            "empfiehlt es sich, das exportierte Schlüsselbund auf diese zu übertragen"
        ),
        Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextButton(
                onPressed: () => onFinishGoto(),
                child: Text("OK, verstanden"),
              ),
            ))
      ],
    ),
  );

}