// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved



import 'dart:collection';
import 'dart:convert';

import 'package:desktop_webview_window/desktop_webview_window.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import 'package:get/get.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/key_data.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/ui/home/index_controller.dart';
import 'package:partcp_client/ui/home/index_page.dart';
import 'package:partcp_client/utils/js_for_web.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/s_utils.dart';
import 'package:partcp_client/widgets/app_bar.dart';
import 'package:partcp_client/widgets/card_widget.dart';
import 'package:partcp_client/widgets/scroll_body.dart';
import 'package:partcp_client/widgets/server_error_message.dart';
import 'package:partcp_client/widgets/widgets.dart';

import 'wirgets.dart';


enum RegStatus {
  none,
  startLogin,
  error,
  createKeys,
  registerKeys,
  finish,
  aborted,
  canceled
}

const _TAG = "KeyManagerPageController";


class KeyManagerPageController extends GetxController {
  static const BROWSER_WIN_NAME = "myWindow";
  static const int CHECK_STATUS_ON_WEB_INTERVAL = 1000; // 1000 milliseconds

  MainController mainController = MainController.to;

  ServerResponse serverResponse = ServerResponse();
  KeyData? keyData;

  RegStatus regStatus = RegStatus.none;
  bool keyServerRequest = false;
  bool webTabIsOpen = false;
  bool showWebView = false;
  bool _isKeyCloakRedirect = true;
  String? errorMessage;
  String? redirect;

  String? urlStatus;

  String _externalUrl = "";
  bool _waitingForOpenWindowClick = false;

  bool get waitingForOpenWindowClick {
    Log.d(_TAG, "get waitingForOpenWindowClick: $_waitingForOpenWindowClick");
    return _waitingForOpenWindowClick;
  }

  set waitingForOpenWindowClick(bool status) {
    Log.d(_TAG, "set waitingForOpenWindowClick($status)");
    _waitingForOpenWindowClick = status;
  }

  InAppWebViewController? webViewController;
  Webview? desktopWebView;

  // bool get useExternalWebView => GetPlatform.isWeb || (GetPlatform.isDesktop && (GetPlatform.isLinux || GetPlatform.isWindows));
  // bool get useExternalWebView => (GetPlatform.isDesktop && (GetPlatform.isLinux || GetPlatform.isWindows));
  bool get useExternalWebView => !(GetPlatform.isAndroid || GetPlatform.isIOS);

  void _restoreDefaultsAfterCancel() {
    serverResponse = ServerResponse();
    keyData = null;
    keyServerRequest = false;
    showWebView = false;
    waitingForOpenWindowClick = false;
    _isKeyCloakRedirect = true;
    errorMessage = null;
    redirect = null;
    urlStatus = null;
    _externalUrl = "";
  }


  void startRegistration() {
    Log.d(_TAG, "startRegistration()");
    _restoreDefaultsAfterCancel();
    waitingForOpenWindowClick = true;
    regStatus = RegStatus.startLogin;
    updateMe();
    requestApiStatus();
  }


  Future<void> onPostFrameCallback() async {
  }


  Future<void> initWebViewController(InAppWebViewController controller) async {
    webViewController = controller;
  }

  void onLoadStop(Uri url) {
    Log.d(_TAG, "onLoadStop()");
    if (url.host == Uri.parse(mainController.votingServer!.keyManagerUrl!).host) {
      if (_isKeyCloakRedirect) {
        requestApiStatus();
      }

      /// on privacy confirmation url contains "success=1" when confirmed
      else {
        final isSuccess = SUtils.mapValueUrlDecoded(url.queryParameters, "success", 0);
        if (isSuccess.toString() == "1") {
          requestApiStatus();
        }
      }
    }
  }

  Future<void> requestApiStatus({String? url}) async {
    Log.d(_TAG, "");
    Log.d(_TAG, "");
    Log.d(_TAG, "requestApiStatus(url: $url): regStatus: $regStatus");
    if (regStatus == RegStatus.createKeys || regStatus == RegStatus.finish || keyServerRequest || regStatus == RegStatus.canceled || regStatus == RegStatus.aborted) {
      Log.d(_TAG, "regStatus == RegStatus.createKeys || regStatus == RegStatus.finish || keyServerRequest || regStatus == RegStatus.canceled || regStatus == RegStatus.aborted ==> return");
      return;
    }

    String _url;
    if (url != null) {
      _url = url;
    } else {
      _url = urlStatus != null ? urlStatus! : mainController.votingServer!.keyManagerUrl!;
    }

    Log.d(_TAG, "requestApiStatus: _url: $_url");
    updateMe();

    String keyServerResponse = await _loadDataFromKeyServer(_url);
    if (regStatus == RegStatus.canceled) {
      return;
    }

    _parseJson(keyServerResponse);
    if (regStatus != RegStatus.error) {

      /*
      if (webTabIsOpen && (useExternalWebView && keyData == null || (keyData != null && keyData!.credential == null))) {
        Log.d(_TAG, "requestApiStatus; webTabIsOpen => DELAYED requestApiStatus");
        Future.delayed(const Duration(milliseconds: CHECK_STATUS_ON_WEB_INTERVAL)).then((_) {
          if (regStatus != RegStatus.canceled && regStatus != RegStatus.aborted) {
            if (_isKeyCloakRedirect || keyData == null) {
              _url = urlStatus != null ? urlStatus! : mainController.votingServer!.keyManagerUrl!;
              Log.d(_TAG, "delayed run: urlStatus: $urlStatus, _url: $_url");

              requestApiStatus(url: _url);
            } else {
              onRegister();
              // await onRegister();
            }
          }
        });
      }
      */

      if (redirect != null && (keyData == null || (keyData != null && keyData!.credential == null))) {
        Log.d(_TAG, "requestApiStatus; redirect != null");

        if (webTabIsOpen) return;

        showWebView = true;
        _isKeyCloakRedirect = (Uri
            .parse(redirect!)
            .host == Uri
            .parse(mainController.votingServer!.keyManagerUrl!)
            .host) ? false : true;
        keyServerRequest = false;
        updateMe();
        await externalWebGoTo(redirect!);
        redirect = null;
      } else {
        showWebView = false;
        updateMe();

        if (keyData != null && keyData!.participantId != null) {
          Log.d(_TAG, "requestApiStatus; participantId: ${keyData!.participantId}");
          Log.d(_TAG, "requestApiStatus; participantStatus: ${keyData!.participantStatus}");

          // if (webTabIsOpen && _isKeyCloakRedirect) {
          //   Log.d(_TAG, "requestApiStatus; webTabIsOpen && _isKeyCloakRedirect => closeBrowserWindow()");
          //   closeBrowserWindow();
          // }

          if (keyData!.credential == null) {
            if (keyData!.participantStatus == KeyData.STATUS_UNREGISTERED) {
              onRegister();
            }
            else {
              onRenewal();
            }
          }

          if (webTabIsOpen && _isKeyCloakRedirect) {
            Log.d(_TAG, "requestApiStatus; webTabIsOpen && _isKeyCloakRedirect => closeBrowserWindow()");
            closeBrowserWindow();
          }


          /// we got credential or status Complete
          else {
            if (webTabIsOpen) {
              Log.d(_TAG, "requestApiStatus; keyData!.credential != null &&  webTabIsOpen => closeBrowserWindow()");
              closeBrowserWindow();
            }
            keyServerRequest = false;

            if (keyData!.credential != null) {
              _registerParticipant(keyData!);
            }
          }
        }
        else if (keyData == null) {
          Log.d(_TAG, "requestApiStatus; keyData == null => set regStatus = RegStatus.startLogin");
          regStatus = RegStatus.startLogin;
          updateMe();
        }
      }

      if (webTabIsOpen && (useExternalWebView && keyData == null || (keyData != null && keyData!.credential == null))) {
        Log.d(_TAG, "requestApiStatus; webTabIsOpen => DELAYED requestApiStatus");
        Future.delayed(const Duration(milliseconds: CHECK_STATUS_ON_WEB_INTERVAL)).then((_) {
          if (regStatus != RegStatus.canceled && regStatus != RegStatus.aborted) {
            if (_isKeyCloakRedirect || keyData == null) {
              _url = urlStatus != null ? urlStatus! : mainController.votingServer!.keyManagerUrl!;
              Log.d(_TAG, "delayed run: urlStatus: $urlStatus, _url: $_url");

              requestApiStatus(url: _url);
            } else {
              onRegister();
              // await onRegister();
            }
          }
        });
      }


    }
  }

  Future<String> _loadDataFromKeyServer(String url) async {
    errorMessage = null;
    keyServerRequest = true;
    Log.d(_TAG, "_loadDataFromKeyServer; url: $url");

    // await Future.delayed(const Duration(seconds: 5));

    GetXProvider getXProvider = GetXProvider();
    getXProvider.timeout = const Duration(seconds: 10);
    dynamic response = await getXProvider.get(url);
    Log.d(_TAG, "_loadDataFromKeyServer; response: ${response.bodyString}");
    keyServerRequest = false;
    if (response.hasError) {
      Log.w(_TAG, "_loadDataFromKeyServer; error: ${response.statusText}");
      return "error";
    } else {
      return response.bodyString;
    }
  }

  void _parseJson(String jsonData) {
    try {
      Map<String, dynamic> map = json.decode(jsonData);
      if (map.containsKey("redirect")) {
        redirect = map["redirect"];
      }
      else {
        redirect = null;
      }
      if (map.containsKey("url_status")) {
        urlStatus = map["url_status"];
      }
      if (map.containsKey("profile")) {
        keyData = KeyData.fromMap(map);
      }
      if (map.containsKey("credential")) {
        keyData!.credential = map["credential"];
      }
      if (map.containsKey("email_verified")) {
        keyData!.credential = map["email_verified"];
      }
      Log.d(_TAG, "_parseJson; map:$map");
      Log.d(_TAG, "_parseJson; redirect:$redirect");
      Log.d(_TAG, "_parseJson; urlStatus:$urlStatus");
    } catch (e) {
      Log.w(_TAG, "_parseJson; parse error");
      Log.w(_TAG, "_parseJson; jsonData: $jsonData");
      regStatus = RegStatus.error;
      updateMe();
    }
  }

  Future<void> externalWebGoTo(String url) async {
    if (useExternalWebView) {
      _externalUrl = url;
      waitingForOpenWindowClick = true;
      updateMe();
    } else {
      await _setWebViewUrl(url);
    }
  }

  void onOpenBrowserWindow() {
    waitingForOpenWindowClick = false;
    webTabIsOpen = true;
    updateMe();
    _onOpenBrowserWindow(_externalUrl);
    _externalUrl = "";
    updateMe();
  }

  void _onOpenBrowserWindow(String url) {
    Log.d(_TAG, "_onOpenBrowserWindow(url: $url)");
    if (GetPlatform.isWeb) {
      JsForWeb.openBrowserWindow(url, BROWSER_WIN_NAME);
    }
    else {
      if (GetPlatform.isDesktop) {
        _onOpenDesktopWindow(url);
      }
    }
    Future.delayed(Duration(milliseconds: CHECK_STATUS_ON_WEB_INTERVAL)).then((_) => requestApiStatus());
  }


  Future<void> _onOpenDesktopWindow(String url) async {
    Log.d(_TAG, "_onOpenDesktopWindow()");
    if (desktopWebView == null) {
      desktopWebView = await WebviewWindow.create(
        configuration: CreateConfiguration(
          windowHeight: 1024,
          windowWidth: 1024,
          title: "Registrierung",
          // titleBarTopPadding: GetPlatform.isMacOS ? 20 : 0,
          titleBarHeight: 0
        ),
      );
    }
    desktopWebView!.onClose.whenComplete(() {
      Log.d(_TAG, "desktopWebView!.onClose.whenComplete");
      onWindowCosed();
    }) ;
    desktopWebView!.launch(url);
  }


  void closeBrowserWindow() {
    Log.d(_TAG, "closeBrowserWindow");
    webTabIsOpen = false;
    if (GetPlatform.isWeb) {
      try {
        Log.d(_TAG, "closeBrowserWindow -> try close");
        JsForWeb.closeBrowserWindow();
        Log.d(_TAG, "closeBrowserWindow -> close OK");
      }
      catch (e) {
        Log.d(_TAG, "closeBrowserWindow -> close fail");
        /// ignored. Happens on mobiles & Firefox. We can not close the window.
      }
    }
    else if (GetPlatform.isAndroid) {
      showWebView = false;
      webViewController?.clearCache();
      webViewController = null;
    }
    else {
      if (desktopWebView != null) {
        desktopWebView!.close();
        desktopWebView = null;
        WebviewWindow.clearAll();
      }
    }
  }

  void onLogout() {
    requestApiStatus(url: keyData!.urlLogout);
    keyData = null;
  }

  Future<void> onRegister() async {
    Log.d(_TAG, "onRegister");
    requestApiStatus(url: keyData!.urlRegister);
  }

  void onRenewal() {
    Log.d(_TAG, "onRenewal");
    requestApiStatus(url: keyData!.urlRenewal);
  }

  /*
  Future<void> onComplete() async {
    // await mainController.onWalletChanged(updateNotification: true);
    // regStatus = RegStatus.finish;
    // IndexController.to.pubKeysOnServerValid = true;
    Log.d(_TAG, "onComplete");
    requestApiStatus(url: keyData!.urlRenewal);
  }
  */

  Future<bool> onWillPop() async {
    if (regStatus == RegStatus.finish) {
      goToIndexPage();
      return false;
    }
    else {
      regStatus = RegStatus.aborted;
      closeBrowserWindow();
      return true;
    }
  }

  void onWindowCosed() {
    Log.d(_TAG, "---------onWindowCosed(); regStatus:$regStatus");
    if (webTabIsOpen) {
      onCancel();
    }
  }

  void onUserAbort() {
    Log.d(_TAG, "---------onUserAbort(); regStatus:$regStatus");
    regStatus = RegStatus.canceled;
    onCancel();
  }

  void onCancel() {
    if (keyData != null && keyData!.participantStatus == KeyData.STATUS_COMPLETE) {
      Log.d(_TAG, "---------onCancel() STATUS_COMPLETE, regStatus:$regStatus");
    }
    else {
      Log.d(_TAG, "---------onCancel(); regStatus:$regStatus");
      regStatus = RegStatus.canceled;
    }

    closeBrowserWindow();
    waitingForOpenWindowClick = false;
    updateMe();
  }


  Future<void> _setWebViewUrl(String url) async {
    Log.d(_TAG, "_setUrl for WEB-VIEW: $url");
    if (webViewController != null) {
      await webViewController!.loadUrl(urlRequest: URLRequest(url: Uri.parse(url)));
    }
  }

  ////////////////////////////////////////////////////////
  /// SEND KEY SECTION
  ///

  Future<void> _registerParticipant(KeyData keyData) async {
    Log.d(_TAG, "_registerParticipant");

    if (regStatus == RegStatus.registerKeys || regStatus == RegStatus.createKeys || regStatus == RegStatus.finish) {
      Log.d(_TAG, "_registerParticipant -> regStatus: $regStatus -> ignore & return");
      updateMe();
      return;
    }

    regStatus = RegStatus.registerKeys;
    serverResponse.receivingData = true;
    updateMe();

    MainController mainController = MainController.to;
    Account account = mainController.wallet!.walletAccount!;
    account
      ..credential = keyData.credential
      ..userId = keyData.participantId;

    serverResponse = await account.submitIdToServer(mainController.votingServer!);
    if (serverResponse.statusCode != null && serverResponse.statusCode == 200) {
      /// save wallet in shared prefs
      await mainController.onWalletChanged(updateNotification: true);
      regStatus = RegStatus.finish;
      account.pubKeysOnServerValid = true;
    } else {
      regStatus = RegStatus.error;
      account
        ..credential = null
        ..userId = null;
    }
    updateMe();
  }


  void goToIndexPage() {
    IndexController.to.updateMe();
    Get.offAll(() => IndexPage());
  }

  void updateMe() {
    update();
  }
}



class KeyManagerPage extends StatelessWidget {
  final KeyManagerPageController controller = Get.put(KeyManagerPageController());
  final GlobalKey webViewKey = GlobalKey();

  final InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
      controller.startRegistration();
    });

    return WillPopScope(
      onWillPop: () => controller.onWillPop(),
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<KeyManagerPageController>(
            builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: Text("Verifizierung der Identität"),
              ),
              body: Widgets.loaderBox(loading: controller.serverResponse.receivingData, child: ScrollBody(children: _body)),
            )),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> items = [];
    items.add(ServerErrorMessage(serverResponse: controller.serverResponse));
    items.add(_infoBox);

    if (!controller.useExternalWebView) {
      items.add(_webView);
    }
    return items;
  }

  Widget get _buttonsOnError {
    List<Widget> items = [];

    items.add(TextButton(onPressed: () => controller.goToIndexPage(), child: Text("Zurück")));
    items.add(TextButton(onPressed: () => controller.startRegistration(), child: Text("Verifizierung erneut starten")));

    items.add(TextButton(onPressed: () => controller.requestApiStatus(), child: Text("Check Status")));

    if (controller.redirect != null) {
      items.add(TextButton(onPressed: () => controller.externalWebGoTo(controller.redirect!), child: Text("Go")));
    }
    if (controller.keyData != null) {
      items.add(TextButton(onPressed: () => controller.onLogout(), child: Text("Logout")));
      items.add(TextButton(onPressed: () => controller.onRegister(), child: Text("Register")));
      items.add(TextButton(onPressed: () => controller.onRenewal(), child: Text("Renewal")));
    }

    return ButtonBar(
      children: items,
    );
  }

  Widget get _buttonsOnCancel {
    List<Widget> items = [];

    items.add(TextButton(onPressed: () => controller.goToIndexPage(), child: Text("Zurück")));
    items.add(TextButton(onPressed: () => controller.startRegistration(), child: Text("Verifizierung erneut starten")));

    return ButtonBar(
      children: items,
    );
  }


  Widget get _webView {
    Log.d(_TAG, "_webView; showWebView: ${controller.showWebView}");
    return Container(
      width: Get.width,
      height: controller.showWebView ? Get.height - MyAppBar().preferredSize.height - 55 : 0,
      child: InAppWebView(
        key: webViewKey,
        gestureRecognizers: Set()..add(Factory<VerticalDragGestureRecognizer>(() => VerticalDragGestureRecognizer())),
        initialUrlRequest: URLRequest(url: Uri.parse("")),
        initialUserScripts: UnmodifiableListView<UserScript>([]),
        initialOptions: options,
        onWebViewCreated: (webViewController) {
          controller.initWebViewController(webViewController);
        },
        onLoadStart: (controller, url) {
          Log.d(_TAG, "onLoadStart: ${url.toString()}");
        },
        androidOnPermissionRequest: (controller, origin, resources) async {
          return PermissionRequestResponse(resources: resources, action: PermissionRequestResponseAction.GRANT);
        },
        onLoadStop: (webController, url) async {
          Log.d(_TAG, "onLoadStop: ${url.toString()}");
          controller.onLoadStop(url!);
        },
        onLoadError: (webController, url, code, message) {
          Log.w(_TAG, "onLoadError: ${url.toString()}");
          controller.serverResponse
            ..statusCode = code
            ..errorMessage = message;
          controller.updateMe();
        },
        onProgressChanged: (controller, progress) {
          if (progress == 100) {
            // pullToRefreshController.endRefreshing();
          }
        },
        onConsoleMessage: (controller, consoleMessage) {
          print(consoleMessage);
        },
      ),
    );
  }

  Widget get _infoBox {
    if (controller.showWebView && !controller.waitingForOpenWindowClick) {
      return Container();
    }

    Log.d(_TAG, "regStatus: ${controller.regStatus}");

    if (controller.regStatus == RegStatus.finish) {
      return identityCreatedWidget(
         server: controller.mainController.votingServer!,
         //onFinishGoto: Get.offAll(() => IndexPage())
          onFinishGoto: controller.goToIndexPage
      );
    }

    if (controller.regStatus == RegStatus.registerKeys) {
      return CardWidget(
        title: "Identitätsprüfung wird abgeschlossen",
        child:  Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextButton(
                onPressed: () => controller.onUserAbort(),
                child: Text("Abbrechen"),
              ),
            )),
      );
    }

    if (controller.waitingForOpenWindowClick && controller.useExternalWebView) {
      return CardWidget(
        title: "Für die Identitätsprüfung muss ein neues Fenster geöffnet werden. Sollte dies nicht automatisch geschehen, klicke bitte auf „Fenster öffnen“.",
        child:  Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextButton(
                onPressed: () => controller.onOpenBrowserWindow(),
                child: Text("Fenster öffnen"),
              ),
            )),
      );
    }

    if (controller.regStatus == RegStatus.error) {
      return CardWidget(
        title: "Identitätsprüfung Deiner Zugangsdaten",
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Der Vorgang konnte nicht automatisch abgeschlossen werden. Bitte versuche es manuell"),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: _buttonsOnError,
            ),
          ],
        ),
      );
    }



    if (controller.regStatus == RegStatus.canceled) {
      return CardWidget(
        title: "Verifizierung abgebrochen",
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Die Verifizierung wurde abgebrochen"),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: _buttonsOnCancel,
            ),
          ],
        ),
      );
    }


    return CardWidget(
      title: "Verifizierung Deiner Zugangsdaten",
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Text("Verifizierungsvorgang abbrechen"),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextButton(
                child: Text("Verifizierungsvorgang abbrechen"),
                onPressed: () => controller.onUserAbort(),
              ),
            ),
          )
        ],
      ),
    );
  }

}





