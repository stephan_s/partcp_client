
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:partcp_client/constants.dart';
import 'package:partcp_client/objects/app_update.dart';
import 'package:partcp_client/transferable/file_download.dart';
import 'package:partcp_client/transferable/file_read.dart';
import 'package:partcp_client/utils/log.dart';
import 'package:partcp_client/utils/yaml_converter.dart';
import 'package:version/version.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;

class AppUpdateController extends GetxController {
  static const _TAG = "AppUpdateController";

  String? errorMessage;
  String _apkDir = "";

  AppUpdate? appUpdate;
  FileDownload? _fileDownload;

  bool downloading = false;
  int progress = 0;
  bool fileReady = false;

  PackageInfo packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
    buildSignature: 'Unknown',
  );

  String get appRev => appUpdate == null ? "Unknown" : appUpdate!.version;
  String? get appPath => appUpdate == null ? "" : "$_apkDir/${appUpdate!.appFilename}";
  String get appUrl => appUpdate == null ? "Unknown" : "${Constants.APP_HOST}${appUpdate!.appDir}${appUpdate!.appFilename}";
  bool get allowUpdate => appPath != null && _isNewRev(packageInfo, appUpdate);

  /// test
  String? getApplicationDocumentsDirectory = "";
  String? getApplicationSupportDirectory = "";
  String? getDownloadsDirectory = "";
  String appDirectory = "";


  void initController() async {
    // _apkDir = (await Constants.appApkDir)!.path;
  }

  Future<void> onPostFrameCallback() async {
    packageInfo = await PackageInfo.fromPlatform();
    await pathTest();
    updateMe();
    await getAppRev();
  }

  Future<void> pathTest () async {
    try {
      getApplicationDocumentsDirectory = (await pathProvider.getApplicationDocumentsDirectory()).path;
      getApplicationSupportDirectory = (await pathProvider.getApplicationSupportDirectory()).path;
      getDownloadsDirectory = (await pathProvider.getDownloadsDirectory())?.path;

      appDirectory = "${getApplicationSupportDirectory!.split(".").first}.${Constants.APP_DIR_NAME}";
    }
    catch(e){
      Log.e(_TAG, "pathTest error: $e");
    }

  }

  Future<void> _requestUpdateData({required Function() onResponse} ) async {

    if (GetPlatform.isAndroid || (GetPlatform.isLinux && GetPlatform.isDesktop)) {
      packageInfo = await PackageInfo.fromPlatform();

      try {
        FileRead(
          fileUrl: Constants.APP_REV_URL,
          onError: (int status, String message) {
            Log.e(_TAG, "check; onError: $message");
            onResponse();
          },
          onResponse: (dynamic response) {
            Log.d(_TAG, "check; response: $response");
            try {
              Map<String, dynamic> responseMap = YamlConverter().toMap(response);

              appUpdate = AppUpdate.fromMap(responseMap);

              Log.d(_TAG, "check; App-Revision: ${appUpdate!.version}");
              Log.d(_TAG, "check; App-Filename: ${appUpdate!.appFilename}");
              Log.d(_TAG, "check; App-Dir: ${appUpdate!.appDir}");
              onResponse();
            } catch (e) {
              Log.e(_TAG, "check: $e");
              onResponse();
            }
          },
        );
      }
      catch(e){
        onResponse();
      }
    }
    else {
      onResponse();
    }
  }

  static _isNewRev(PackageInfo? packageInfo, AppUpdate? appUpdate) {
    if (appUpdate == null || packageInfo == null) return false;

    /// Android version have at the end String a
    /// "d" -> debug
    /// "r" -> release

    Version currentVersion = Version.parse(packageInfo.version.replaceAll(RegExp(r'[^0-9.]'), ''));
    Version latestVersion = Version.parse(appUpdate.version.replaceAll(RegExp(r'[^0-9.]'), ''));

    var currentSplitted = packageInfo.version.split("");
    var latestSplitted = appUpdate.version.split("");

    Log.d(_TAG, "currentVersion: $currentVersion");
    Log.d(_TAG, "latestVersion: $latestVersion");

    if (latestVersion == currentVersion && currentSplitted.last == "d" && latestSplitted.last == "r") {
      return true;
    }
    return latestVersion > currentVersion;
  }


  Future<void> check({required Function(bool avialable) onResponse}) async {
    _requestUpdateData(
        onResponse: () {
          if (appUpdate != null) {
            onResponse(_isNewRev(packageInfo, appUpdate));
          }
          else {
            onResponse(false);
          }
        }
    );
  }


  Future<void> getAppRev() async {
    errorMessage = null;
    _requestUpdateData(
        onResponse: () {
          updateMe();
        }
    );
  }

  Future<void> cancelDownload() async {
    if (_fileDownload != null) {
      _fileDownload!.cancel();
      _onFinish();
      updateMe();
    }
  }

  void _onFinish() {
    _fileDownload = null;
    progress = 0;
    downloading = false;
  }

  Future<void> downloadApp() async {
    errorMessage = null;
    downloading = true;
    fileReady = false;
    updateMe();

    String appSavePath = (await Constants.appApkDir)!.path + "/" + appUpdate!.appFilename;
    _fileDownload = FileDownload(
        fileUrl: appUrl,
        savePath: appSavePath,
        onError: (int code, String message) {
          errorMessage = message;
          _onFinish();
          updateMe();
        },
        onProgress: (int progress) {
          this.progress = progress;
          updateMe();
        },
        onFinish: () {
          fileReady = true;
          _onFinish();
          updateMe();
        });
  }

  void updateMe() {
    update();
  }
}

