// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';

class Styles {

  static const Color buttonColor = Color(0xff0b2ebf);

  static const Color selectedTileColor = Color.fromARGB(255, 242, 242, 242);

}