import 'package:flutter/material.dart';
import 'package:partcp_client/main.dart';

import 'dart:io';

import 'package:path_provider/path_provider.dart';

import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/ui/wallet/decrypt_wallet_page.dart';
import 'package:partcp_client/objects/wallet.dart';

import '../ui/home.dart';
import '../ui/common_widgets.dart';
import '../ui/michel_controller.dart';

/// Page where the user can log in to a voting server.
class LoginPage extends StatelessWidget {

  final mainController = MainController.to;
  final michelController = MichelController.to;

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(title: const Text('Login')),
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CommonWidgets.fullWidthTextInput(usernameController, 'Benutzername'),
          CommonWidgets.verticalSpacer(),
          CommonWidgets.fullWidthTextInput(passwordController, 'Passwort'),
          CommonWidgets.verticalSpacer(),
          CommonWidgets.fullWidthButton('Mock-up login', () => _mockUpLogin(context))
        ],
      ),
    ),
  );

  void _mockUpLogin(BuildContext context) async {
      // Mock-up login
      final directory = await getApplicationDocumentsDirectory();
      final path = '${directory.path}/stimm_app/ParTCP-stephan-dev-login.txt';
      final file = File(path);
      final fileContent = await file.readAsString();

      final wallet = Wallet.fromYaml(fileContent);

      final decryptController = DecryptWalletController();
      decryptController.initController(wallet!);
      decryptController.pwdController.text = 'abc123';
      decryptController.onClickDecryptWallet();

      // await mainController.onWalletDecrypted(wallet);

      final server = wallet.serverList.first;
      mainController.votingServer = server;
      michelController.wallet = wallet;
      await server.getServerInfo();
      michelController.group = server.serverGroups.firstWhere(
        (element) => element.id == 'rudulin'
      );

      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => HomePage())
      );
  }

}