import 'package:flutter/material.dart';

class CommonWidgets {

  static loadingSplash() => Center(
    child: Container(child: Text("loading...")),
  );

  static final fullWidth = 240.0;

  static final defaultHeight = 50.0;

  static fullWidthTextInput(TextEditingController controller, String labelText) => TextInputField(
      controller: controller,
      labelText: labelText,
      width: fullWidth
  );

  static fullWidthButton(String label, void Function() onPressed) =>
    SizedBox(
      width: fullWidth,
      height: defaultHeight,
      child: ElevatedButton(
        child: Text(label),
        onPressed: onPressed,
      )
    );


  static verticalSpacer() => SizedBox(height: 24);

}

class TextInputField extends StatelessWidget {

  final TextEditingController controller;
  final width;
  final String labelText;

  const TextInputField({
    super.key,
    required this.controller,
    required this.width,
    required this.labelText
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: CommonWidgets.defaultHeight,
      child:Card(
        elevation: 0,
        child: TextField(
          controller: controller,
          decoration: InputDecoration(
            labelText: labelText,
          ),
        ),
      )
    );
  }

}