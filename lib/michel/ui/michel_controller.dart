import 'package:get/get.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/wallet.dart';

/// Builds an interface between Stimm-App GUI elements and ParTCP functional
/// elements during runtime to be accessed by Stimm-App GUI elements.
class MichelController extends GetxController {

  Wallet? wallet;
  Group? group;

  static MichelController to = Get.find<MichelController>();

}