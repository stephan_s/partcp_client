import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/ui/voting/voting_controller.dart';

import '../ui/displayable_item.dart';
import '../ui/displayable_event.dart';
import '../ui/item_page.dart';
import '../ui/michel_controller.dart';

import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/ui/event/event_controller.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/widgets/server_error_message.dart';

/// Home page section which lists currently relevant votings by some metric yet
/// to be defined.
class ListTab extends StatelessWidget {

  final eventController = Get.put(EventController());
  final mainController = MainController.to;
  final votingController = Get.put(VotingController());

  void _awaitServerInfo() async {
    await eventController.initController(mainController.votingServer!, Mode.voting);
    await eventController.server.getServerInfo();
    await eventController.onGroupsChanges(MichelController.to.group!);
    eventController.update();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      eventController.onPostFrameCallback();
    });

    return OrientationBuilder(
      builder: (context, __) => GetBuilder<EventController>(
        initState: (_) => _awaitServerInfo(),
        builder: (_) => Center(
          child: Container(
            width: 350,
            alignment: Alignment.center,
            child: ListView(
              padding: const EdgeInsets.all(10),
              children: _children(context),
            ),
          ),
        )
      )
    );

  }
  List<Widget> _children(BuildContext context) {
    final events = eventController.activeEventsForGroup;
    return events.map((event) {
      DisplayableItem item = DisplayableEvent(event: event);
      return Container(
        width: 300,
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.amberAccent,
            width: 3,
            style: BorderStyle.solid,
          ),
        ),
        child: ElevatedButton(
          child: itemSummary(item),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ItemPage())
            );
          },
        ),
      );
    }).toList();
  }

}