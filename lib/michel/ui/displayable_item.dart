abstract class DisplayableItem {

  abstract final String title;

  abstract final String headerImageResource;

  abstract final String description;

  abstract final String hashtags;

}