import 'package:partcp_client/objects/voting_event.dart';

import '../ui/displayable_item.dart';

class DisplayableEvent extends DisplayableItem {

  final VotingEvent event;

  DisplayableEvent({required this.event});

  // ToDo: Add meaningful content

  @override
  String get description => event.name;

  @override
  String get hashtags => '';

  @override
  String get headerImageResource => event.clientData!.linkUrl;

  @override
  String get title => event.name;

}