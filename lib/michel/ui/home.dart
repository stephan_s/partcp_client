import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';

import 'package:partcp_client/objects/url_params.dart';
import 'package:partcp_client/ui/home/index_controller.dart';
import 'package:partcp_client/ui/event/event_controller.dart';
import 'package:partcp_client/widgets/widgets.dart';

import '../ui/tab_navigation.dart';
import '../ui/submit.dart';

/// Home Page consisting of an app bar, a tab navigation system and a center
/// content area.
class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {

  @override
  void initState() {
    super.initState();
    TabNavigationController.to.initController(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      bottomNavigationBar: navigationBar(),
      body: Stack(
        children: [
          navigationTabs(),
          _submissionButton(),
        ],
      ),
    );
  }

  Widget _submissionButton() => Positioned(
    right: 40,
    bottom: 40,
    child: FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SubmitPage())
        );
      },
    ),
  );

}
