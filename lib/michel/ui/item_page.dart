import 'package:flutter/material.dart';
import 'package:partcp_client/ui/voting/voting_controller.dart';

import '../ui/displayable_item.dart';
import '../ui/displayable_voting.dart';

Widget itemDetail(DisplayableItem item) => Column(
  mainAxisSize: MainAxisSize.min,
  mainAxisAlignment: MainAxisAlignment.center,
  children: <Widget>[
    Text(
        item.title,
        style: const TextStyle(fontSize: 30),
        textAlign: TextAlign.center
    ),
    Image.asset(
      item.headerImageResource,
      height: 300,
      width: 300,
    ),
    Flexible(
        child: Container(
          padding: const EdgeInsets.only(
              left: 50,
              right: 50,
              top: 20,
              bottom: 20
          ),
          child: Text(
              item.description,
              style: const TextStyle(fontSize: 16)
          ),
        )
    ),
    Text(item.hashtags),
  ],
);

Widget itemSummary(DisplayableItem item) => Container(
  padding: const EdgeInsets.all(20),
  child: Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.network(
          item.headerImageResource,
          height: 300,
          width: 300,
        ),
        Text(
            item.title,
            style: const TextStyle(fontSize: 30),
            textAlign: TextAlign.center
        ),

        Text(item.hashtags),
      ]
  ),
);


class ItemPage extends StatelessWidget {

  const ItemPage({super.key});

  @override
  Widget build(BuildContext context) {
    DisplayableItem item = DisplayableVoting(voting: VotingController.to.voting);
    return Scaffold(
      appBar: AppBar(
          title: const Text('Inspect')
      ),
      body: Center(
          child: itemDetail(item)
      ),
    );
  }

}