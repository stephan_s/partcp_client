import 'package:flutter/material.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/account.dart';
import 'package:partcp_client/objects/client_data.dart';
import 'package:partcp_client/objects/credential_rules.dart';
import 'package:partcp_client/objects/group.dart';
import 'package:partcp_client/objects/voting.dart';
import 'package:partcp_client/objects/voting_event.dart';
import 'package:partcp_client/objects/server_response.dart';
import 'package:partcp_client/message_type/event_definition.dart';
import 'package:partcp_client/message_type/event_update_request.dart';
import 'package:partcp_client/controller/http_controller.dart';
import 'package:partcp_client/objects/voting_option.dart';
import 'package:partcp_client/objects/voting_server.dart';
import 'package:partcp_client/objects/voting_type.dart';

import '../ui/common_widgets.dart';
import '../ui/michel_controller.dart';

/// Page where users can submit/propose a voting.
class SubmitPage extends StatelessWidget {

  final titleController = TextEditingController();
  final descriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(title: const Text('Abstimmung erstellen')),
    body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CommonWidgets.fullWidthTextInput(titleController, 'Titel'),
          CommonWidgets.verticalSpacer(),
          CommonWidgets.fullWidthTextInput(descriptionController, 'Beschreibung'),
          CommonWidgets.verticalSpacer(),
          IconButton(
            icon: Icon(Icons.camera),
            onPressed: () {
              // ToDo: Implement
            },
          ),
          CommonWidgets.verticalSpacer(),
          CommonWidgets.fullWidthButton('Abschicken', () {
            _submit();
            Navigator.pop(context);
          })
        ],
      ),
    ),
  );

  void _submit() async {
    final mainController = MainController.to;
    final stimmAppController = MichelController.to;
    final server = mainController.votingServer!;
    final group = stimmAppController.group!;
    final account = stimmAppController.wallet!.walletAccount!;

    final title = titleController.text.trim();
    final description = descriptionController.text.trim();

    VotingEvent event = _createEvent(server, title, description);

    ServerResponse serverResponse = ServerResponse();
    serverResponse.errorMessage = null;
    serverResponse.receivingData = true;

    final Map<String, dynamic> messageMap = event.id.isEmpty ? eventDefinition(group, event) : eventUpdateRequest(group, event);

    serverResponse = await HttpController().serverRequestSigned(account, messageMap, server);

    final Map<String, dynamic> eventMap = serverResponse.bodyMap!['Event-Data'];
    final eventId = eventMap['id'];

    event = await _fetchEvent(server, account, group, eventId);

    Voting voting = _createVoting(title);

    serverResponse = await voting.sendDefinitionOrUpdateRequest(server, account, event);

    final Map<String, dynamic> votingMap = serverResponse.bodyMap!['Voting-Data'];
    final votingId = votingMap['id'];

    // event = await _fetchEvent(server, account, group, eventId);
    // voting = event.votings.firstWhere((element) => element.id == votingId);

    voting.id = votingId;
    voting.status = Voting.STATUS_IDLE;

    serverResponse = await voting.sendStatusChangeRequest(server, event, account);
  }

  Future<VotingEvent> _fetchEvent(VotingServer server, Account account, Group group, String eventId) async {
    // await server.getServerInfo();
    // event = server.getEventById(group, eventId)!;
    ServerResponse serverResponse = await server.eventsByGroup(account, group, force: true);
    List<VotingEvent> eventList = serverResponse.object;
    return eventList.firstWhere((element) => element.id == eventId);
  }

  VotingEvent _createEvent(VotingServer server, String title, String description) {
    final VotingEvent event = VotingEvent("", server)..administrable = true;

    event
      ..name = title
      ..clientData!.shortDescription = description
      ..namingValues = NamingDefaults()
      ..lotCodeValues = LotCodeDefaults()
      ..credentialValues = CredentialDefaults()
      ..estimatedTurnout = 10
      ..clientData!.linkUrl = 'https://www.sherwoodfoods.co.uk/wp-content/uploads/2020/10/Bratwurst-Sausage-04.jpg';

    return event;
  }

  Voting _createVoting(String title) {
    final optionYes = VotingOption(
        id: 'JA',
        name: 'Ja',
        uuid: VotingOption.createUuid(),
        clientData: ClientData()
    );
    final optionNo = VotingOption(
        id: 'NEIN',
        name: 'Nein',
        uuid: VotingOption.createUuid(),
        clientData: ClientData()
    );

    final voting = Voting();
    voting
      ..name = title
      ..title = title
      ..type = VotingType.SINGLE_CHOICE
      ..votingOptions.add(optionYes)
      ..votingOptions.add(optionNo);

    return voting;
  }

}