import 'package:partcp_client/objects/voting.dart';

import '../ui/displayable_item.dart';

class DisplayableVoting extends DisplayableItem {

  final Voting voting;

  DisplayableVoting({required this.voting});

  // ToDo: Sort out description for voting
  @override
  String get description => voting.clientData.description;

  // ToDo: Add hashtags to Voting
  @override
  String get hashtags => '';

  // ToDo: Add image resource to Voting
  @override
  String get headerImageResource => throw UnimplementedError();

  @override
  String get title => voting.title;

}