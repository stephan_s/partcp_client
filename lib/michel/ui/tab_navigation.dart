import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../ui/list_tab.dart';

/// Getx controller for accessing and managing the main page tab navigation
/// system.
class TabNavigationController extends GetxController {

  static const initialTabIndex = 0;
  static const numberOfTabs = 3;

  late TabController tabController;

  /// Tab titles to be displayed in the top app bar.
  static final tabTitles = <String>[
    'Home', 'List', 'Queue'
  ];

  /// Tab icons to be displayed in the bottom navigation bar.
  static final tabIcons = <IconData>[
    Icons.home_filled, Icons.dashboard, Icons.star
  ];

  /// Title of the tab currently viewed.
  var currentTabTitle = 'N/A'.obs;

  void initController(TickerProvider tickerProvider) {
    tabController = TabController(
      initialIndex: initialTabIndex,
      length: numberOfTabs,
      vsync: tickerProvider,
    );
    tabController.addListener(() {
      currentTabTitle.value = tabTitles[tabController.index];
    });
    currentTabTitle.value = tabTitles[initialTabIndex];
  }

  static TabNavigationController to = Get.find<TabNavigationController>();

}

/// Creates the tab bar view as well as contained navigation tabs.
TabBarView navigationTabs() => TabBarView(
  controller: TabNavigationController.to.tabController,
  children: <Widget>[
    Center(child: Text("Placeholder")),
    ListTab(),
    Center(child: const Text('Placeholder')),
  ],
);

/// Creates the bottom tab bar using static data from [TabNavigationController].
TabBar navigationBar() => TabBar(
  controller: TabNavigationController.to.tabController,
  indicatorColor: Colors.amber,
  unselectedLabelColor: Colors.amber,
  tabs: TabNavigationController.tabIcons.map((e) => Tab(
    icon: Icon(e, size: 40)
  )).toList()
);

enum MenuItem {profile, settings}

/// Creates the app bar where the title displayed changes with the current tab.
AppBar appBar() => AppBar(
  title: Obx(() => Text(TabNavigationController.to.currentTabTitle.value)),
  actions: <Widget>[
    PopupMenuButton<MenuItem>(
      icon: const Icon(Icons.menu),
      initialValue: MenuItem.profile,
      onSelected: (item) {
        // if (item == MenuItem.item0) Navigator.push(context, route)
      },
      itemBuilder: (context) => <PopupMenuEntry<MenuItem>> [
        const PopupMenuItem<MenuItem>(
          value: MenuItem.profile,
          child: Text('Profile'),
        ),
        const PopupMenuItem<MenuItem>(
          value: MenuItem.settings,
          child: Text('Settings'),
        ),
      ],
    ),
  ],
);
