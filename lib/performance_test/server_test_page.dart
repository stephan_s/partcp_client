// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:partcp_client/performance_test/voting_list_test.dart';
// import 'package:partcp_client/widgets/app_bar.dart';
//
// import 'server_details_test.dart';
//
// class ServerTestPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: MyAppBar(
//         title: Text("Server Test"),
//       ),
//       body: Center(
//         child: Column(
//           children: [
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: ElevatedButton(
//                   onPressed: () => Get.to(ServerDetailsTest()),
//                   child: Text("ServerDetails Request Test")),
//             ),
//
//             Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: ElevatedButton(
//                   onPressed: () => Get.to(VotingListTest()),
//                   child: Text("VotingList Request Test")),
//             ),
//
//           ],
//         ),
//       ),
//     );
//   }
//
// }