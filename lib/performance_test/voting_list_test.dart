// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:partcp_client/controller/http_controller.dart';
// import 'package:partcp_client/objects/account.dart';
// import 'package:partcp_client/objects/voting_event.dart';
// import 'package:partcp_client/utils/log.dart';
// import 'package:partcp_client/utils/yaml_converter.dart';
// import 'package:partcp_client/widgets/app_bar.dart';
//
// class ResultObj {
//   final int count;
//   final int time;
//   final String message;
//
//   const ResultObj({
//     this.count,
//     this.time,
//     this.message
//   });
// }
//
// const _TAG = "GetServerDataTest";
//
// class TestController extends GetxController {
//
//   int requests = 10000;
//   int requestsDone = 0;
//
//   Account account;
//   VotingEvent votingEvent;
//
//   List<ResultObj> result = [];
//   DateTime timeStart;
//   DateTime timeLastUpdate;
//   int timeElapsed = 0;
//   bool isRunning = false;
//
//   final YamlConverter _yamlConverter = YamlConverter();
//   final HttpController _httpController = HttpController();
//   final _serverUrl = "https://demo01.partcp.org/";
//
//   int _counter = 0;
//   int get counter {
//     _counter++;
//     return _counter;
//   }
//
//   String message = "Signature: '4m+LUQcwuqotyO5rUPihA/CI0Ohc+UJoHgdoCprXY5CrrWnyfPrLSNNyyXq/8suznVJVsam9GxraesgVd+efCw=='\n"
//       "Message-Type: 'voting-list-request'\n"
//       "Event-Id: '20210602-test-event-0206a'\n"
//       "Voting-Status: all\n"
//       "From: 'p00.109'\n"
//       "Public-Key: 'SDZ03ywzNIGPMTN33kok/nqaSVfc5UmRJ0IIj7pKIz5LJQs3Es/TnA5/tVOSJujYgUH0jFkML9jF86if6Xx2EQ=='\n";
//
//   // Future<void> _getData() async {
//   void _getData() async {
//     String responseErrorMessage = "";
//     DateTime _time1 = DateTime.now();
//
//     // VotingStatusEnum votingStatusEnum = VotingStatusEnum.all;
//     //
//     // final Map<String, dynamic> messageMap = votingListRequest(
//     //     votingStatus: votingStatusEnum,
//     //     votingEventId: votingEvent.id
//     // );
//     // String message = await _httpController.createServerRequest(
//     //     account, messageMap);
//
//
//    final response = await _httpController.serverRequest(
//        YamlConverter().toMap(message),
//         null,
//         _serverUrl
//    );
//
//
//     DateTime _time2 = DateTime.now();
//     var _timeDiff = _time2.difference(_time1).inMilliseconds;
//
//
//     var httpStatus = response.statusCode;
//     if (httpStatus == null || httpStatus != 200) {
//       responseErrorMessage = "error";
//     }
//
//     int count = counter;
//     timeElapsed =  _time2.difference(timeStart).inSeconds;
//     Log.d(_TAG, "c: $count; dt: $_timeDiff ms; time elapsed: $timeElapsed sec");
//
//     ResultObj obj = ResultObj(
//         count: count,
//         time: _time2.difference(_time1).inMilliseconds,
//         message: responseErrorMessage
//     );
//     result.add(obj);
//
//     if (count > requests-10 || _time2.difference(timeLastUpdate).inMilliseconds > 1000) {
//       requestsDone = count;
//       updateMe();
//     }
//
//     if (count > requests-1) {
//       requestsDone = count;
//       isRunning = false;
//       updateMe();
//     }
//
//   }
//
//   void startTest() async {
//     Log.d(_TAG, "Test started");
//     isRunning = true;
//     timeStart = DateTime.now();
//     timeLastUpdate = timeStart;
//     _counter = 0;
//     updateMe();
//
//     for (var i=0; i<requests; i++) {
//       _getData();
//     }
//
//     Log.d(_TAG, "Test finished");
//   }
//
//   void updateMe() {
//     update();
//   }
//
// }
//
// class VotingListTest extends StatelessWidget{
//   final TestController controller = Get.put(TestController());
//
//   @override
//   Widget build(BuildContext context) {
//     return GetBuilder<TestController>(
//       builder: (_) => Scaffold(
//         appBar: MyAppBar(
//           title: Text("Test: Get Server Data"),
//         ),
//         body: Center(
//           child: Column(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: ElevatedButton(
//                     onPressed: controller.isRunning
//                         ? null
//                         : () => controller.startTest(),
//                     child: Text("Start Test")),
//               ),
//               Text("Requests to do: ${controller.requests}"),
//               Text("Requests done: ${controller.requestsDone}"),
//               Text("Time elapsed: ${controller.timeElapsed} sec"),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
// }